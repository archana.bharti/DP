﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GridMvc;
using DP.Admin.Models;
using DP.Services.Business;
using DP.Common;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;

namespace DP.Admin.Controllers.Api
{
    public class DatabaseController : ApiController
    {
        private DatabaseManager databaseManager;
        private CommonManager commonManager;
        public DatabaseController(DatabaseManager databaseManager, CommonManager commonManager)
        {
            this.databaseManager = databaseManager;
            this.commonManager = commonManager;
        }

        [HttpGet]
        public IHttpActionResult Rows(string entity = "")
        {
            return Ok(Format<City>(this.commonManager.GetCities(),entity));
        }

        private object Format<T>(List<T> data, string table)
        {
            var props = typeof(T).GetType().GetProperties();
            List<GridColumn> Columns = new List<GridColumn>();
            foreach (var prop in props)
            {
                Columns.Add(new GridColumn() { Field = prop.Name, Title = prop.Name });
            }

            return new
            {
                Columns = Columns,
                Data = data,
                Table = table
            };
        }
    }

    public class GridColumn
    {
        public string Field { get; set; }
        public string Title { get; set; }
    }
}
