﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using GridMvc;
using DP.Admin.Models;
using DP.Services.Business;
using DP.Common;
using DP.Services.Business.Administration;

namespace DP.Admin.Controllers
{
    public class DatabaseController : Controller
    {
        private DatabaseManager databaseManager;
        private CommonManager commonManager;
        public DatabaseController(DatabaseManager databaseManager, CommonManager commonManager)
        {
            this.databaseManager = databaseManager;
            this.commonManager = commonManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Tables()
        {
            return PartialView("_Tables", this.databaseManager.GetTableNames());
        }

       
    }

}
