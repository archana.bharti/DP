﻿using System.Web;
using System.Web.Optimization;

namespace DP.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/bootstrap-modal.min.js",
                      "~/Scripts/bootstrap-table.min.js",
                      "~/Scripts/bootstrap-select.min.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/bower_components/metisMenu/dist/metisMenu.min.css",
                    "~/Content/bootstrap.min.css",
                    "~/Content/bootstrap-dialog.min.css",
                    "~/Content/bootstrap-select.min.css",
                    "~/Content/bootstrap-table.min.css",
                    "~/Content/bootstrap.min.css",
                    "~/Content/timeline.css",
                    "~/Content/sb-admin-2.css"));
        }
    }
}
