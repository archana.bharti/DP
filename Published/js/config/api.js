define([], function() {

    var config = {},
        baseUrl = "",
        api = "",
        fixtures = "",
        organisationId = 1;

    //beta
    api = "http://snt.doctorpursuit.com/api/";

    //int
   //api = "http://dp-dev.gaween.com/api/";

    //local
    //api = "http://localhost:20921/api/";
	api = "/api/";

    /***************************************************
                SECURITY
    ****************************************************/

    config.security = {
        login: api + "auth/Login",
        isLoggedIn: api + "auth/isLoggedIn",
        logout: api + "auth/logout",
        forgotPassword: api + "auth/forgotPassword?email=",
        changePassword: api + "auth/changePassword"
    }


    /***************************************************
                ENUMS/COMMON
    ****************************************************/

    config.enums = {
        roles: api + "common/getRoles",
        states: api + "common/getStates",
        cities: api + "common/getCities",
        locations: api + "common/getLocations",
        specializations: api + "common/getSpecializations",
        collegeCities: api + "common/getCollegeCities",
    }

    /***************************************************
     SEARCH MODULES
     ****************************************************/

    config.search = {
        getResults: api + "search",
        zone: api + "search/zone",
        insurance : api + "insurance/search"
    }

    /***************************************************
     GOOGLE MAPS
     ****************************************************/

    config.gmaps = {
        key: 'AIzaSyA26KarT8wTUqmIsc_6qi2H5Q2gZbCeS9g'
    }

    /***************************************************
                DASHBOARD
    ****************************************************/

    config.dashboard = {

    }

    /***************************************************
               USER
   ****************************************************/

    config.user = {
        create: api + "user",
        suggest: api + "user/userFinder",
    }

    /***************************************************
               PROFILE
   ****************************************************/

    config.profile = {
        userProfile: api + "userProfile?uId=",
        update: api + "userProfile?uid=",
        updateSummary: api + "userProfile/updateSummary?uid=",
        userScore: api + "Score",
        userSpecializations: api + "Specialization",
        userEndorsements: api + "userprofile/endorsements",
        endorseSpecialization: api + 'endorsement',
        bookmarkProfile: api + "bookmark",
        topRated: api + "review/getTopRated?entityId={0}&entityType={1}",
        uploadImage: api + "userProfile/uploadImage",
        qualifications: {
            allLongShort: api + "userProfile/getUnifiedQualifications",
            allUserLongShort: api + "userProfile/GetUnifiedUserQualifications?userId=",
            addLong: api + "userProfile/addlongQualification",
            addShort: api + "userProfile/addShortQualification",
            deleteLong: api + "userProfile/deleteLongQualification",
            deleteShort: api + "userProfile/deleteShortQualification",
            addQualif: api + "userProfile/addQualification",
            removeQualif: api + "userProfile/removeQualification"
        }
    }

    /***************************************************
     PDA
     ****************************************************/

    config.pda = {
        advice: api + "pda",
        specializations: api + "pda/Specializations"
    }

    /***************************************************
                RECOMMEND
  ****************************************************/
    config.recommend = {
        addOrUpdate: api + "recommend"
    }

  /***************************************************
                REPORTS
  ****************************************************/
    config.report = {
        getFeaturedReport: api + "report/getFeaturedReport"
    }

    /***************************************************
            REVIEW
    ****************************************************/
    config.review = {
        getParameters: api + "review/GetRatingParameters?type=",
        add: api + "review",
        update: api + "review",
        userReviews: api + "review/getUserReviews",
        myReviews: api + "userProfile/myReviews?uid="
    }

    /***************************************************
           OFFERS
   ****************************************************/

    config.offer = {
        get: api + "offer",
        getById: api + "offer",
        claim: api + "offer/claim",
        myOffers: api + "offer/myOffers",
    }

    /***************************************************
          APPOINTMENT
  ****************************************************/

    config.appointment = {
        save: api + "appointment",
        checkAvailability: api + "appointment/checkAvailability"
    }

    /***************************************************
         APPOINTMENT
 ****************************************************/
    config.insurance = {
        get: api + 'insurance/',
        update: api + 'insurance',
        updateSummary: api + 'insurance/updateSummary',
    }

    return config;
});
