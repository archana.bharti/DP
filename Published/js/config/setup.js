﻿
require.config({
    //urlArgs: "nocache=" +  (new Date()).getTime(),
    baseUrl: "./js",
    waitSeconds : 20,
    paths: {

        //jquery
        "jquery": "libs/jquery/jquery.min",
        "jqueryUi": "libs/jquery/jquery-ui.min",

        //underscore & backbone
        "underscore": "libs/backbone/lodash",
        "backbone": "libs/backbone/backbone",
        "backbone-fu": "libs/backbone/backbone-fileupload",
        "backbone.Validation": "libs/backbone/validation",
        /*"backbone.Filter": "libs/backbone/backbone-route-filter",*/
        "backbone-gmaps": "libs/backbone/backbone.googlemaps",
        "text": "libs/require/text",
        "bar-rating": "libs/jquery/jquery.barrating.min",
        "summernote": "libs/summernote.min",
        "circliful": "libs/jquery/jquery.circliful.min",
        "waterbubble": "libs/plugins/waterbubble",

        "gmaps": "libs/google/gmaps",
        "async": "libs/plugins/async",

        // css libraries
        "z-tabs": "libs/plugins/tabs",
        "owl-carousel": "libs/plugins/owl.carousel",

        //lazy
        "lazy": "libs/lazy/lazy",
        "lazy-builder": "libs/lazy/lazy-builder",
        "promise-adaptor": "libs/lazy/promise-adaptor-jquery",

        //bootstrap
        "bootstrap": "libs/bootstrap/bootstrap.min",
        "bootstrapValidator": "libs/bootstrap/bootstrap-validator",
        "bootstrapSelect": "libs/bootstrap/select2.min",
        "bootstrap-dialog": "libs/bootstrap/bootstrap-modal-more",
        "bootstrapTable": "libs/bootstrap/bootstrap-table",
        "bootstrapDatetimePicker": "libs/bootstrap/bootstrap-datepicker",
        "bootstrap-rating": "libs/bootstrap/bootstrap-star-rating",
        "metis-menu": "libs/bootstrap/metis-menu.min",

        //highcharts
        "highcharts": "libs/highcharts/highcharts",
        "highcharts-more": "libs/highcharts/highcharts-more",
        "highcharts-guage": "libs/highcharts/highcharts-guage",

        //Plugins
        "tooltip" : "libs/plugins/tooltip",
        "chain": "libs/plugins/chain",
        "convert": "libs/plugins/convert",
        "jqExt": "libs/plugins/jqExtensions",
        "cookie": "libs/plugins/cookie.min",
        "slimScroll": "libs/plugins/slim-scroll",
        "hcm": "libs/plugins/hcm",
        "moment" : "libs/plugins/moment",

        //Configs
        "apiConfig": "config/api",
        "constantsConfig": "config/constants",

        // Applications bases
        "modelBase": "core/model",
        "collectionBase": "core/collection",
        "viewBase": "core/view",
        "controllerBase": "core/controller",

    },
    shim: {
        "jqueryUi": ["jquery"],
        "bootstrap": ["jquery"],
        "bootstrapValidator": ["jquery"],
        "bootstrapTable": ["jquery"],
        "bootstrap-rating": ["jquery"],
        "bootstrapDatetimePicker": ["jquery", "moment"],
        "metis-menu": ["jquery","bootstrap"],
        "bootstrapSelect": ["jquery"],
        "highcharts": ["jquery"],
        "highcharts-more": ["jquery", "highcharts"],
        "highcharts-guage": ["jquery", "highcharts", "highcharts-more"],
        "tooltip": ["jquery"],
        "cookie": ["jquery"],
        "convert": ["jquery"],
        "gmaps": ["jquery"],
        "async": ["jquery"],
        "jqExt": ["jquery"],
        "chain": ["backbone"],
        "slimScroll" : ["jquery"],
        "bar-rating":["jquery"],
        "summernote": ["jquery"],
        "circliful": ["jquery"],
        "waterbubble": ["jquery"],
        "z-tabs": ["jquery"],
        "owl-carousel": ['jquery'],
        "backbone-gmaps": ["jquery"],
        'bootstrap-dialog': {
            deps: ["jquery", "underscore", "backbone", "bootstrap"]
        },
        "backbone": {
            "deps": ["underscore", "jquery"],
            "exports": "Backbone"
        },
        "underscore": {
            "exports": '_'
        },
        'backbone.Validation': ['backbone'],
        /*'backbone.Filter' : ['backbone'],*/
        'backbone-fu': ['backbone']
    },
    map: {
        "*": {
            "lodash": "underscore"
        }
    }
});

require(
    ["jquery",
    "backbone",
    /*"backbone.Filter",*/
    "bootstrap",
    "bootstrapValidator",
    "bootstrapTable",
    "tooltip",
    "lazy!config/route",
    "jqueryUi"],
    function (
        $,
        backbone,
        /*backboneFilter,*/
        bootstrap,
        bootstrapValidator,
        bootstrapTable,
        tooltip,
        router) {

        window.onerror = function (errorMsg, url, lineNumber) {
            $("#loading-control").hide();
            console.log(((errorMsg == undefined) ? "" : errorMsg) + ":" + ((lineNumber == undefined) ? "" : lineNumber) + ":" + ((url == undefined) ? "" : url));
        }

        window.refresh = function (withHash) {
            if (withHash == undefined || withHash == false)
                window.location.href = window.location.href;
            else if (withHash != undefined && withHash == true) {
                window.location.href = window.location.href.substring(0, window.location.href.indexOf('#'))
            }
        }

        Backbone.history.restart = function () {
            Backbone.history.stop();
            Backbone.history.start({ root: '/', silent: true, pushState: true });
        }

        $.get("http://ipinfo.io/json", function (res) {
            window.IP = res.ip;
        });

        router.get().then(function (Router) {
            new Router();
        });
});
