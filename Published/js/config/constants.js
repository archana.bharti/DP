define([], function () {

    "use strict";

    var constants = {

        MENU_NODE: "#side-menu",
        SIDEBAR: ".sidebar",
        MAIN: "#main",
        CONTENT: "#page-wrap",
        DEFAULT_HOMEPAGE: "home",
        DEFAULT_LOCATIONID: 2,

        SEARCH_FEATURED_DEFAULT_INITIAL_RADIUS: "5",
        SEARCH_NONFEATURED_DEFAULT_INITIAL_RADIUS: "1",

        AjaxSettings: {
            timeout: 100000,
            retry: 0
        },

        USER_ROLES: {
            "1": "CLINIC",
            "2": "DOCTOR",
            "3": "HOSPITAL",
            "4": "COLLEGE",
            "5": "USER",
            "6" :"INSURANCE"
        },

        USER_ROLES_ID: {
            "CLINIC": "1",
            "DOCTOR": "2",
            "HOSPITAL": "3",
            "COLLEGE": "4",
            "USER": "5",
            "INSURANCE" : "6",
            "clinic": "1",
            "doctor": "2",
            "hospital": "3",
            "college": "4",
            "user": "5",
            "insurance": "6"
        },
        YEAR_QUARTER: {
            Q1: [1, 2, 3],
            Q2: [4, 5, 6],
            Q3: [7, 8, 9],
            Q4: [10, 11, 12]
        },
        REVIEW_PARAMETER_MAP: {
            "yourExperience": { title: "Latest Technology", field: "yourExperience" },
            "subscriberName": { title: "", field: "subscriberName" },
            "cleanliness": { title: "Cleanliness", field: "cleanliness" },
            "feesCharged": { title: "Fees Charged", field: "feesCharged" },
            "waitingHours": { title: "Waiting Hours", field: "waitingHours" },
            "listen": { title: "Listening", field: "listen" },
            "comfort": { title: "Comfort & Care", field: "comfort" },
            "friendlyStaff": { title: "Courteous Staff  ", field: "friendlyStaff" },
            "qualityOfService": { title: "Quality of Care", field: "qualityOfService" },
            "onCall": { title: "On Call", field: "onCall" },
            "customerSatisfaction": { title: "Overall Satisfaction", field: "customerSatisfaction" },
            "infrastructure": { title: "Infrastructure", field: "Infrastructure" },
            "safetyCleanliness": { title: "Cleanliness", field: "safetyCleanliness" },
            "courteousStaff": { title: "Courteous Staff", field: "courteousStaff" },
            "cafeFood": { title: "Cafe Food", field: "cafeFood" },
            "qualityOfCare": { title: "Quality of Care", field: "qualityOfCare" },
            "overnightStay": { title: "Overnight stay", field: "overnightStay" },
            "emergencyCare": { title: "Emergency Care", field: "emergencyCare" },
            "intenetConnectivity": { title: "Intenet Connectivity", field: "intenetConnectivity" },
            "overallSatisfaction": { title: "Overall Satisfaction", field: "overallSatisfaction" },
            "qualityOfEducation": { title: "Quality Of Education", field: "qualityOfEducation" },
            "food": { title: "Campus cafe", field: "food" },
            "vistingHours": { title: "Visiting Hours", field: "vistingHours" },

            getMap: function (key) {
                try{
                    return this[key].title
                }
                catch(e)
                {
                    return key.capitalizeFirstLetter();
                }
            }
        },
       
        
    };
    return constants;
});