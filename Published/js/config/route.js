 define([
    "apiConfig",
    "constantsConfig",
    "app",
    "lazy!modules/login/controller",
    "lazy!modules/register/controller",
    "lazy!modules/dashboard/controller",
    "lazy!modules/search/controller",
    "lazy!modules/user/controller",
    "lazy!modules/pda/controller",
    "lazy!modules/studentPursuit/controller",
    "lazy!modules/healthyWay/controller",
    "lazy!modules/featured/controller",
    "lazy!modules/insurance/controller",
    "lazy!modules/offers/controller",
], function (
    ApiConfig,
    Constants,
    Application,
    LoginController,
    RegisterController,
    DashboardController,
    SearchController,
    UserController,
    PDAController,
    StudentPursuitController,
    HealthyWayController,
    FeaturedController,
    InsuranceController,
    OffersController
) {

    "use strict";

    var router = Backbone.Router.extend({

        initialize: function () {

            this.setupAjax();
            window.app = new Application(this);
            Backbone.history.start({ root: '/', silent: true, pushState: true });
            app.trigger("start");
        },

        setupAjax: function () {
            var self = this;
            $.ajaxSetup({
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                timeout: Constants.AjaxSettings.timeout,
                retry: Constants.AjaxSettings.retry,
                statusCode: {
                    401: function () {
                        app.router.redirect(window.location.pathname.substring(1));
                    }
                    ,
                    400: function (a, b, c) {
                        app.trigger("response:error", a.responseJSON);
                    },

                    403: function (a, b, c) {
                        app.trigger("response:error", a.responseJSON);
                    },

                    500: function (a, b, c) {
                        app.trigger("response:error", (a.responseJSON || a.responseText));
                    },
                    //0: function (a, b, c) {
                    //    app.trigger("response:error", "Unable to reach server. Check your internet connection or try after sometime");
                    //}
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if (jqXHR.status == 404) {
                    //    self.login();
                    //} else {
                    //    app.trigger("response:error", "Error: " + textStatus + ": " + errorThrown);
                    //}
                }
            });

            $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                options.beforeSend = function (xhr, obj) {
                    $("#loading-control").show();
                };
                options.complete = function () {
                    $("#loading-control").hide();
                };
            });
        },


        // ------------------------------------------------------------------------------------------
        // Backbone route handling on page refresh
        // ------------------------------------------------------------------------------------------
        redirect: function (route) {

            var url = window.location.pathname;
            if (url != '/' && url.substring(1) === route)
                Backbone.history.loadUrl(route)
            else
                app.router.navigate(route, { trigger: true });
        },

        // ------------------------------------------------------------------------------------------
        // Routes
        // ------------------------------------------------------------------------------------------

        routes: {
            "(/)": "default",
            "login": "login",
            "register": "register",

            "account":"account",
            "account/bookmarks": "bookmarks",
            "profile/:type/:uid": "profile",
            "profile/edit/:type/:id": "editProfile",

            "home": "home",
            "search/results": "searchResults",

            "pda": "pda",

            "studentpursuit": "studentPursuit",
            "healthyway": "healthyWay",
            "featured": "featured",
            "insurance": "insurance",
            "offers": "offers",
            "offers/user": "userOffers",
            "offer/claim/:id": "offerClaim",

            "settings": "settings",
            "themes": "themes"
        },

        // ------------------------------------------------------------------------------------------
        // Route Handlers
        // ------------------------------------------------------------------------------------------
        
        login: function() {
            app.executeController(LoginController, "login", null);
        },

        register: function() {
            app.executeController(RegisterController, "register", null);
        },

        //TODO: this needs to be changed
        account : function(){
            app.executeController(UserController, "profile", { account: true });
        },
        
        default: function (params) {
            //app.shell();
        },

        shell: function (callback) {
            app.shell();
        },

        home: function () {
            $("#loading-control").hide();
            app.executeController(DashboardController, "dashboard", null);
        },

        profile: function (type, id) {
            if (app.sessionManager.get("USER") != undefined) {
                if (app.sessionManager.get("USER").subscriberId == id) {
                    app.router.redirect("account");
                    return;
                }
            }
            app.executeController(UserController, "profile", { type: type, id: id, account: false });
        },

        bookmarks: function (type, id) {
            app.executeController(UserController, "bookmarks", { type: type, id: id });
        },

        editProfile: function(type, id){
            app.executeController(UserController, "editProfile", {type: type, id: id});
        },

        pda: function(){
            app.executeController(PDAController, "pda", null);
        },

        studentPursuit: function(){
            app.executeController(StudentPursuitController, "studentPursuit", null);
        },

        healthyWay: function(){
            app.executeController(HealthyWayController, "healthyWay", null);
        },

        featured: function(){
            app.executeController(FeaturedController, "featured", null);
        },

        insurance: function(){
            app.executeController(InsuranceController, "insurance", null);
        },

        offers: function(){
            app.executeController(OffersController, "offers", null);
        },

        offerClaim  : function(id){
            app.executeController(OffersController, "claim", { id: id });
        },

        userOffers : function(){
            app.executeController(OffersController, "myOffers");
        },

        /***************************************************
              SEARCH RESULTS
        ****************************************************/

        searchResults: function () {
            app.executeController(SearchController, "searchResults", null);
        }
    });

    return router;

});
