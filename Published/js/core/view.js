define(["jquery", "backbone", "constantsConfig"], function ($, Backbone, Constants) {

    "use strict";


    var View = Backbone.View.extend({

        clear: function () {
            var self = this;
            self.undelegateEvents();
            self.$el.unbind();
            self.delegateEvents();
            return this;
        },

        serializeForm: function (form) {

            var serializedArray = form.serializeArray();

            var object = {};
            $.each(serializedArray, function (idx, item) {
                object[item.name] = item.value;
            })

            return object;
        },

        ajaxFormPost: function (form, url, progressbar, callback) {
            var self = this;
            var formData = new FormData(form[0]);
            $.ajax({
                url: url,
                async: true,
                type: 'POST',
                xhr: function () {
                    var jqXhr = $.ajaxSettings.xhr();
                    progressbar.attr('aria-valuenow', 0);
                    if (jqXhr.upload) { // Check if upload property exists
                        jqXhr.upload.addEventListener('progress', function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                progressbar.attr('aria-valuenow', percentComplete);
                            }
                        }, false); 
                    }
                    return jqXhr;
                },
                success: function (a) {
                    progressbar.hide();
                    callback(true, a)
                },
                error: function (a, b, c) {
                    progressbar.hide();
                    callback(false, a)
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                crossDomain: true
            })
        },

        imageUpload: function (data, image, url, progressbar, callback) {
            var form = $("<form/>");
            for(var key in data){
                $("<input/>").attr({ name: key, value: data[key] }).appendTo(form);
            }
            form.append(image);
            progressbar.show();
            this.ajaxFormPost(form, url, progressbar, callback);
        },

        showImage: function (input, image) {

            input = input[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    image.attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

                image.show();
            }
        },

        freezeForm: function (form) {
            form.find("a,input,select").attr("disabled", "disabled");
        },

        unfreezeForm: function (form) {
            form.find("a,input,select").removeAttr("disabled");
        }
    });

    return View;

});

