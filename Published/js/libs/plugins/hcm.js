﻿/*HighchartManager*/
define(["jquery",
    "highcharts",
    "highcharts-more",
    "highcharts-guage"], function ($) {

        var hcm = {
            guage: function (selector, data, options) {

                selector.highcharts(Highcharts.merge(hcm_defaults.gaugeOptions, {
                    yAxis: {
                        min: 0,
                        max: 100,
                        //title: {
                        //    text: data.title
                        //}
                    },

                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'test',
                        data: [data.value],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y} %</span>'
                        }
                        //,
                        //tooltip: {
                        //    valueSuffix: ' km/h'
                        //}
                    }]

                }));
            },

            pie: function (selector, data, options) {
                hcm_defaults.pie_colors();
                selector.highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: '',
                        style: {
                            display: 'none'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        enabled :false
                        //pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                dataLabels: {
                                    connectorWidth: 0
                                },
                                enabled: true,
                                format: '{point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        name: data.seriesName,
                        colorByPoint: true,
                        data: data.value
                    }]
                });
            }
        }


        var hcm_defaults = {

            gaugeOptions: {

                chart: {
                    type: 'solidgauge'
                },

                title: null,

                pane: {
                    center: ['50%', '85%'],
                    size: '140%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },

                tooltip: {
                    enabled: false
                },

                // the value axis
                yAxis: {
                    stops: [
                        [0.1, '#DF5353'], // green
                        [0.5, '#DDDF0D'], // yellow
                        [0.9, '#55BF3B'] // red
                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickPixelInterval: 400,
                    tickWidth: 0,
                    title: {
                        y: -70
                    },
                    labels: {
                        y: 16
                    }
                },

                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                }
            },

            pie_colors: function () {
                // Make monochrome colors and set them as default for all pies
                Highcharts.getOptions().plotOptions.pie.colors = (function () {
                    var colors = [],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }())
            }

        }

    return hcm;
})