define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/register/views/register"
], function (
    Config,
    Constants,
    Controller,
    RegisterView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {
            return this;
        },

        register: function () {
            var self = this,
                view = new RegisterView({ controller: self, el: Constants.CONTENT });
            view.clear().render();
        }
    });

    return Controller;

});
