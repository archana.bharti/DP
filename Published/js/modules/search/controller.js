define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/search/views/searchView"
], function (
    Config,
    Constants,
    Controller,
    SearchView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        searchResults: function () {
            var self = this,
                view = new SearchView({ controller : self, el: Constants.CONTENT });
            view.clear().render();
        }
    });

    return Controller;

});
