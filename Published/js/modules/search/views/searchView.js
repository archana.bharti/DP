define([
        "apiConfig",
        "viewBase",
        "constantsConfig",
        'core/model',
        "modules/shared/models/map",
        "modules/shared/views/mapView",
        "text!modules/search/templates/searchResults.html",
        "text!modules/search/templates/featured-item.html",
        "text!modules/search/templates/non-featured-item.html",
        "text!modules/search/templates/mapListItems.html",
        "text!modules/search/templates/featured-carousel-item.html",
        "text!modules/shared/templates/left-sidebar.html",
        "text!modules/shared/templates/right-sidebar.html",
        "text!modules/search/templates/appointment.html",
        "bootstrap-dialog",
        "owl-carousel",
        "slimScroll",
        "bootstrapDatetimePicker"
    ],

    function (Config,
              Base,
              Constants,
              Model,
              Map,
              MapView,
              SearchResultsTpl,
              FeaturedItemTpl,
              NonFeaturedItemTpl,
              MapListItemTpl,
              FeaturedCarouselItemTpl,
              LeftSideBarTpl,
              RightSideBarTpl,
              AppointmentFormTpl,
              BootstrapDialog) {

        "use strict";

        var View = Base.extend({

            initialize: function (options) {
                var self = this;
                $.extend(self, {
                    mapLoaded: false,
                    markers: [],
                    featured: [],
                    carouselRendered : false,
                    searchParams: JSON.parse(localStorage.getItem('searchParams'))
                });

            },

            events:{
                'click li[data-tab-view]': 'switchTab',
                'click .search-btn' : 'search',
                'click .map-item-name' : 'onMapItemClick',
                'click .btn-save-bookmark': 'bookmark',
                'click .btn-recommend': 'recommend',
                'click .btn-book-appoinment': 'initAppointment'
            },

            render: function () {
                var self = this;
                self.$el.html(_.template(SearchResultsTpl, {
                    leftSideBar: _.template(LeftSideBarTpl,{}),
                    rightSideBar: _.template(RightSideBarTpl, {}),
                    showDistance : (self.searchParams != 3)
                }));

                if(self.searchParams.locationId == null){
                    $('.search-by-distance-col').remove();
                }

                self.FGrid = self.$el.find("#grid-featured")
                self.NFGrid = self.$el.find("#grid-non-featured");
                setTimeout(function (v) {
                    v.getSearchResults();
                    v.initZonal();
                }, 1000, self);
            },

            search: function(e){
                var self = this;
                self.FGrid.bootstrapTable('refresh', { query: self.getFilters(true, true) });
                self.NFGrid.bootstrapTable('refresh', { query: self.getFilters(false, true) });
            },

            switchTab: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    ctx = ele.data('tab-view');
                
                switch (ctx) {
                    
                    case 'map':
                        self.$el.find('.left-side-column, .right-side-column, .search-sidebar-main, .middle-column, #listView').hide();
                        self.$el.find('.middle-column').removeClass('col-md-6').addClass('col-md-12').show();
                        self.$el.find("#mapView").show();
                        self.renderGmap();
                       
                        break;
                    case 'list':
                        self.$el.find('.left-side-column, .right-side-column, .search-sidebar-main, #listView').show();
                        self.$el.find('.middle-column').removeClass('col-md-12').addClass('col-md-6').show();
                        self.$el.find("#mapView").hide();
                        break;
                }
            },

            bookmark: function(e){
                var self = this,
                    ele = $(e.currentTarget);
                var request = {userId : app.sessionManager.get('USER').id,
                                bookmarkId: $(e.currentTarget).data('user-id'),
                                type: Constants.USER_ROLES_ID[$(e.currentTarget).data('profile').toUpperCase()]};
                var saveContactModel = new Model();
                saveContactModel.url = Config.profile.bookmarkProfile;
                saveContactModel.save(request, {
                    success: function(model, resp){
                        $(e.currentTarget).text('Saved');
                    }
                });
            },

            renderGmap: function(){
                var self = this;
                if (self.mapView) {
                    //TODO: dont know i have commented this line
                    //self.mapView.destroy();
                    self.$el.find("#gmaps-container").html('');
                }
                self.mapView = new MapView();
                self.$el.find("#gmaps-container").append(self.mapView.render(self.markers).el);
            },

            onMapItemClick: function(e){
                var self = this;
                var address = $(e.currentTarget).html() + ", " +$(e.currentTarget).closest('.map-text-item').find('.map-location-name').html();
                var isFeatured = ($(e.currentTarget).closest('.map-text-item').data('featured') == 'featured') ? true : false;
                self.mapView.getLocationDetails(address, isFeatured);
            },

            getSearchResults: function(){
                var self = this;
                self.$('.featured-ul, .non-featured-ul, .carousel-featured-items,.map-list-ul').html('');
                self.getNonFeaturedData(false, false);
                self.getFeaturedData(true, false);
            },

            getFeaturedData: function(){
                var self = this;
                self.FGrid.bootstrapTable({
                    url: Config.search.getResults,
                    sidePagination: 'server',
                    method : "post",
                    cache: false,
                    pagination: false,
                    showLoading: true,
                    clickToSelect: false,
                    sortable: false,
                    showHeader: false,
                    showPaginationSwitch: false,
                    queryParams: function (params) {
                        $.extend(params, self.getFilters(true, false));
                        return params;
                    },
                    columns: [
                        {
                            field: 'name',
                            title: 'UID',
                            valign: 'middle',
                            align: 'left',
                            formatter: _.bind(self.itemFormatter,self, true)
                        }]
                });

                self.FGrid.on("load-success.bs.table", function () {
                    var rows = $(this).bootstrapTable('getData');
                    if (rows.length == 0) {
                        $(this).closest('.featured-list-panel').hide();
                    }
                }).on("page-change.bs.table", function () { })
            },

            getNonFeaturedData: function () {

                var self = this;
                self.NFGrid.bootstrapTable({
                    url: Config.search.getResults,
                    sidePagination: 'server',
                    method: "post",
                    cache: false,
                    pagination: true,
                    pageSize: 5,
                    showLoading: true,
                    clickToSelect: false,
                    sortable: false,
                    showHeader: false,
                    showPaginationSwitch :false,
                    queryParams: function (params) {
                        $.extend(params, self.getFilters(false, false));
                        return params;
                    },
                    columns: [
                        {
                            field: 'name',
                            title: 'UID',
                            valign: 'middle',
                            align: 'left',
                            formatter: _.bind(self.itemFormatter, self, false)
                        }]
                });

                self.NFGrid.on("load-success.bs.table", function () { })
                           .on("page-change.bs.table", function () { })
            },

            getFilters : function(featured, resetPagination){
                var self = this,
                reviewVal = self.$el.find(".reviews-select").val(),
                recommendVal = self.$el.find(".recommended-select").val(),
                distanceVal = (featured == true) ? Constants.SEARCH_FEATURED_DEFAULT_INITIAL_RADIUS : self.$el.find(".distance-select").val(),
                sparams = $.extend(true, {}, self.searchParams);
                var p = $.extend(sparams, { byFeatured: featured, cityId: 2, distance: distanceVal, byRecommended: recommendVal, byReview: reviewVal });
                return p;
            },

            itemFormatter: function (isFeatured, value, row, index) {
                var self = this;
                var tpl = isFeatured == true ? FeaturedItemTpl : NonFeaturedItemTpl;
                var model = row;
                switch (row.subType.toLowerCase()) {
                    case 'clinic':
                        model.iconName = 'Hospital.png';
                        model.sDisplay = 'block';
                        break;
                    case 'doctor':
                        model.iconName = 'Doctor.png'
                        model.sDisplay = 'block';
                        break;
                    case 'hospital':
                        model.iconName = 'Hospital.png'
                        model.sDisplay = 'block';
                        break;
                    case 'college':
                        model.iconName = 'College.png'
                        model.sDisplay = 'none';
                        break;
                    default:
                        model.iconName = 'college-icon-new.jpg'
                        model.sDisplay = 'none';
                        break;
                }

                self.$('.map-list-ul').append(_.template(MapListItemTpl, {
                    item: row,
                    iconName: model.iconName,
                    featured: isFeatured == true ?  'featured' : '',
                    iconPath: isFeatured == true ? 'css/img/gmap-marker-blue.png' : 'css/img/gmap-marker-red.png'
                }));

                if (_.where(self.markers, { name: row.name }).length == 0) {
                    self.markers.push({ latitude: row.latitude, longitude: row.longitude, name: row.name, isFatured: isFeatured });
                }

                //if (isFeatured == true && _.where(self.featured, { name: row.name }).length == 0) {
                   // self.featured.push(row);
                //}


                return _.template(tpl, {
                    item: model
                })
            },

            initZonal: function () {

                var self = this,
                    locationId = (self.searchParams == undefined || self.searchParams.locationId == undefined)
                                 ? null : self.searchParams.locationId;
                
                var model = new Model();
                model.url = Config.search.zone;
                model.save({ locationId: locationId }, {
                    success: function (m, res) {
                        _.each(res.response.model, function (i) {
                            self.featured.push(i);
                        });
                        self.renderZonal();
                    }
                })
            },

            renderZonal: function () {
                var self = this;
                //  if (!self.carouselRendered) {
                var flist = self.featured.slice(0);
                var mod = flist.length % 5, i = 0;
                //if (mod != 0) {
                //    i = 0;
                //    while (flist.length % 5 != 0) {
                //        flist.push(flist[i]);
                //        i++;
                //    }
                //}

                var splits = [], size = 5;

                while (flist.length > 0)
                    splits.push(flist.splice(0, size));

                var parent = self.$('.carousel-featured-items'), owlWrapper, firstChild, owlItemStyle;

                $.each(splits, function (i, d) {
                    self.$('.carousel-featured-items').append(_.template(FeaturedCarouselItemTpl, {
                        item: d
                    }))
                });


                $(".carousel-featured-items").owlCarousel({
                    navigation: true, // Show next and prev buttons
                    slideSpeed: 40,
                    paginationSpeed: 55,
                    singleItem: true,
                    autoPlay: true,
                    stopOnHover: true,
                    pagination: false,
                    autoHeight: true,
                    navigationText: ["<i class='fa fa-backward' />", "<i class='fa fa-forward' />"],
                    transitionStyle: "backSlide"
                });
                // }
            },

            recommend: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    spnRm = ele.closest(".search-result-item").find(".spnRecommendCount"),
                    data = ele.data("entity").split(':'),
                    request = { entityId: data[0], entityType: Constants.USER_ROLES_ID[data[1].toUpperCase()], remove: false, ipAddress: window.IP },
                    cnt = parseInt(spnRm.text().trim()),
                    model = new Model();

                model.url = Config.recommend.addOrUpdate;
                request.remove = false;


                model.save(request, {
                    success: function () {
                        spnRm.text(cnt + 1);
                        ele.replaceWith('<i class="fa fa-thumbs-up fa-lg clr-green"></i>');
                    }
                })

            },

            initAppointment: function (e) {

                var self = this,
                   ele = $(e.currentTarget),
                   data = ele.data("entity").split(':'),

                   fn_save = function (btn, form, entityId, entityType, entityName, callback) {
                       ele.addClass("disabled");
                       var data = self.serializeForm(form);

                       if ($.trim(data.email).length == 0 ||
                           $.trim(data.startTime).length == 0 ||
                           $.trim(data.mobile).length == 0 ||
                           $.trim(data.name).length == 0) { return false; }

                       var rx = /^[0-9]{10}$/;
                       if (rx.test($.trim(data.mobile)) == false) { return false; }
                       btn.text('Saving...').addClass("disabled");
                       data.entityId = entityId;
                       data.entityType = Constants.USER_ROLES_ID[entityType.toUpperCase()];
                       data.entityName = entityName;

                       var model = new Model();
                       model.url = Config.appointment.save;

                       model.save(data, {
                           success: function () {
                               callback();
                           },
                           error: function (a) {
                               app.trigger("response:error", arguments[1].responseJSON);
                               btn.text('Save').removeClass("disabled");
                           }
                       })
                   },

                   fn_chkAvailability = function () {
                       $.get(Config.appointment.checkAvailability, function (res) {
                           if (res && res.response.model === true) {
                               BootstrapDialog.show({
                                   message: $('<div></div>').html(AppointmentFormTpl),
                                   title: "Appointment",
                                   closable: true,
                                   cssClass: "appoinment-dialog",
                                   data: {
                                       'entityId': data[0],
                                       'entityType': data[1],
                                       'entityName': data[2]
                                   },
                                   onshown: function (dialogRef) {
                                       $('#datetimepicker-appointment').datetimepicker({
                                           format: 'YYYY-MM-DD HH:mm'
                                       });
                                       $(".btn-save-appointment").on('click', function () {
                                           fn_save($(this),
                                                $('.form-appointment'),
                                                dialogRef.getData('entityId'),
                                                dialogRef.getData('entityType'),
                                                dialogRef.getData('entityName'),
                                                function () {
                                                    dialogRef.close();
                                                })
                                       })
                                   }
                               });
                           }
                       })
                   };

                fn_chkAvailability();
            }
        });

        return View;

    });