﻿define(["apiConfig", 'core/model'], function (Config, Base) {
    "use strict";

    var tvModel = Base.extend({
        defaults: {
            value: null,
            text: null
        },
        idAttribute: "value",
    });

    return tvModel
});