﻿define([
        "apiConfig",
        'core/collection',
        'modules/shared/models/textValue'],
    function (
        Config,
        Base,
        TextValueModel) {
        "use strict";
        var Collection = Base.extend({
            initialize: function (models, options) {
                this.id = (options) ? options.id : ''
            },
            model: TextValueModel,
            url: function () {
                var stateId = (this.id) ? '?stateId=' + this.id : '';
                return Config.enums.collegeCities + stateId;
            }
        });

        return Collection;
    });
