﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "text!modules/offers/views/templates/offers.html",
        "jqExt"
    ],

    function (Config, Constants, Base, Model, OffersTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function () {
                var self = this;
            },

            events : {
                'click button.btn-claim' : "claim"
            },

            render: function () {
                var self = this;
                var m = new Model();
                m.url = Config.offer.get;
                m.fetch(
                    {
                        success:
                        function (r) {
                            self.$el.html(_.template(OffersTpl, {
                                model: r.toJSON()

                            }));
                        }
                    })
            },
            claim: function (e) {
                var ele = $(e.currentTarget),
                    self = this,
                    data = {},
                    user = app.sessionManager.get("USER");
                data.ipAddress = window.IP;
                data.userEmail = user.email;
                data.userPhone = user.phone_1;
                data.OfferMasterId = ele.data("offer-id");
                ele.text("Saving...");
                $.post(Config.offer.claim, data, function (res) {
                    ele.text("Claim");
                    app.trigger("response:success", "You have successfully claimed this offer. Please wait for the further communication.");
                }).error(function () {
                    ele.text("Claim");
                    app.trigger("response:error", "Unable to process your request now. Please try after sometime");
                })
            }
        });

        return View;

    });