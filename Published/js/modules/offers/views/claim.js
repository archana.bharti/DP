﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "text!modules/offers/views/templates/claim.html",
       "bootstrapValidator"
],
 function (Config, Constants, Base, Model,  ClaimTpl) {

     "use strict";
     var View = Base.extend({

         initialize: function (params) {
             var self = this;
             self.model = new Model();
             self.model.url = Config.offer.get + '/'+ params.id;
             self.model.fetch({ success: function () { self.render(); } });
             return this;
         },

         events: {
             'click .btn-claim': "claim"
         },

         render: function () {
             var self = this,
                 _setupForm = function () {
                     $('#form-claim-offer').bootstrapValidator({
                         live: 'disabled',
                         submitButtons: 'button[type="submit"]',
                         fields: {
                             userEmail: {
                                 validators: {
                                     notEmpty: {
                                         message: 'Email should not be empty.'
                                     },
                                     emailAddress: {
                                         message: 'Not a valid email address'
                                     }
                                 }
                             },
                             userPhone: {
                                 validators: {
                                     notEmpty: {
                                         message: 'Contact number should not be empty.'
                                     },
                                     callback: {
                                         message: 'Not a valid phone number',
                                         callback: function (value, validator, $field) {

                                             if ($.trim(value).length == 0) return true;

                                             return /^\d{10}$/.test(value) == true;
                                         }
                                     }
                                 }
                             }
                         }
                     });
                 }

             self.$el.html(_.template(ClaimTpl, self.model.toJSON()));
             _setupForm();
         },

         claim: function (e) {
             var self = this,
                 ele = $(e.currentTarget),
                 form = $('#form-claim-offer'),
                _validateForm = function () {
                    form.data("bootstrapValidator").validate();
                    return $('#form-claim-offer').data("bootstrapValidator").isValid();
                },
                _resetForm = function () {
                    form.data("bootstrapValidator").resetForm();
                },
                _eleDisable = function () {
                    ele.addClass("disabled").text("Saving...");
                },
                _eleEnable = function () {
                    ele.removeClass("disabled").text("Submit");
                }
                        
             if (_validateForm()) {
                 var data = self.serializeForm(form);
                 data.ipAddress = window.IP;
                 self.freezeForm(form);
                 _eleDisable();

                 $.post(Config.offer.claim, data, function (res) {
                     app.router.redirect("offers");
                     app.trigger("response:success", "You have successfully claimed this offer. Please wait for the further communication.");
                 }).error(function () {
                     app.router.redirect("offers");
                     app.trigger("response:error", "Unable to process your request now. Please try after sometime");
                 })
             }

             return false;
         }
     });

     return View;

 });