﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "text!modules/offers/views/templates/myoffers.html",
       "jqExt"
],

   function (Config, Constants, Base, Model, OffersTpl) {

       "use strict";

       var View = Base.extend({

           initialize: function () {
               var self = this;
           },

           events: {

           },

           render: function () {
               var self = this;
               var m = new Model();
               m.url = Config.offer.myOffers;
               m.fetch(
                   {
                       success:
                       function (r) {
                           self.$el.html(_.template(OffersTpl, {
                               model: r.toJSON()
                           }));
                       }
                   })
           }
       });

       return View;

   });