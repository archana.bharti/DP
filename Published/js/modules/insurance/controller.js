define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/insurance/views/view"
], function (
    Config,
    Constants,
    Controller,
    InsuranceView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        insurance: function (args) {
            var self = this,
                view = new InsuranceView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().render();
        }
    });

    return Controller;

});
