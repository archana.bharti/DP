﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "text!modules/insurance/views/templates/profile.html",
       "text!modules/user/templates/change-password.html",
       "text!modules/user/templates/upload-image.html",
       "bootstrap-dialog",
       "bootstrapValidator"
], function (Config, Constants, Base, Model, ProfileTpl, ChangePasswordTpl, UploadImageTpl, BootstrapDialog) {

    var planAttr = [
                        { key: "cashlessFacility", text: "Cashless Facility" },
                        { key: "maternityBenefits", text: "Maternity Benefits" },
                        { key: "ambulanceCharges", text: "Ambulance Charges" },
                        { key: "dayCareProcedures", text: "DayCare Procedures" },
                        { key: "healthCheckup", text: "Health Checkup" },
                        { key: "taxBenefits", text: "Tax Benefits" },
                        { key: "hospitalizationExpenses", text: "Hospitalization Expenses" },
                        { key: "freelookPeriod", text: "Freelook Period" },
                        { key: "alternativeTreatment", text: "Alternative Treatment" },
                        { key: "cumulativeBonus", text: "Cumulative Bonus" },
                        { key: "organDonorExpenses", text: "OrganDonor Expenses" },
                        { key: "prePostHospitalizationCharges", text: "PrePost HospitalizationCharges" },
                        { key: "portability", text: "Portability" }]

    var setupForm = {
        validation: function () {
            var form = $('.form-insurance-edit');
            form.bootstrapValidator({
                live: 'enabled',
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Name should not be empty.'
                            }
                        }
                    },
                    displayName: {
                        validators: {
                            notEmpty: {
                                message: 'Display Name should not be empty.'
                            }
                        }
                    },
                    phone_1: {
                        validators: {
                            notEmpty: {
                                message: ' '
                            },
                            callback: {
                                message: 'Not a valid phone number',
                                callback: function (value, validator, $field) {

                                    if ($.trim(value).length == 0) return true;

                                    return /^\d{10}$/.test(value) == true;
                                }
                            }
                        }
                    },
                }
            });

        },
        validate: function () {
            var form = $('.form-insurance-edit');
            form.data("bootstrapValidator").validate();
            return form.data("bootstrapValidator").isValid();
        },
        reset: function () {
            $('.form-insurance-edit').data("bootstrapValidator").resetForm();
        }
    }

    var ChangePasswordView = Base.extend({

        template: _.template(ChangePasswordTpl),

        tagName: "div",

        initialize: function (options) {
            this.dialogRef = options.dialogRef
        },

        events: {
            'click .btn-change-password': "changePassword"
        },
        render: function () {
            var self = this;
            var _initFormValidator = function () {
                $('#form-change-password').bootstrapValidator({
                    live: 'disabled',
                    submitButtons: 'button[type="submit"]',
                    fields: {
                        oldPassword: {
                            validators: {
                                notEmpty: {
                                    message: 'Password should not be empty.'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Password must be of 6 or more characters'
                                },
                                callback: {
                                    message: 'Old password and new password should not be same',
                                    callback: function (value, validator, $field) {
                                        var _oldp = $field.val();
                                        var _newP = $("[name='newPassword']").val();
                                        if (btoa(_newP) === btoa(_oldp)) {
                                            return {
                                                valid: false,       // or true
                                                message: 'Old password and new password should not be same'
                                            }
                                        }

                                        return {
                                            valid: true
                                        }
                                    }
                                }
                            }
                        },
                        newPassword: {
                            validators: {
                                notEmpty: {
                                    message: 'New password should not be empty.'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Password must be of 6 or more characters'
                                },
                                identical: {
                                    field: 'newPassword2',
                                    message: 'The new password and password entered again are not identical.'
                                }
                            }
                        },
                        newPassword2: {
                            validators: {
                                notEmpty: {
                                    message: 'Confirm password again for security'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Password must be of 6 or more characters'
                                },
                                identical: {
                                    field: 'newPassword',
                                    message: 'The new password and password entered again are not identical.'
                                }
                            }
                        }
                    }
                })
            }
            self.$el.html(self.template(self.model));
            _initFormValidator();
            return this;
        },

        changePassword: function (e) {
            var self = this, ele = $(e.currentTarget), form = $('#form-change-password')
            var _validateForm = function () {
                form.data("bootstrapValidator").validate();
                return $('#form-change-password').data("bootstrapValidator").isValid();
            },
            _resetForm = function () {
                form.data("bootstrapValidator").resetForm();
            },
            _eleDisable = function () {
                ele.addClass("disabled").text("Saving...");
            },
            _eleEnable = function () {
                ele.removeClass("disabled").text("Submit");
            }

            if (_validateForm()) {

                var data = self.serializeForm(form);
                self.freezeForm(form);
                _eleDisable();

                data.oldPassword = btoa(data.oldPassword);
                data.newPassword = btoa(data.newPassword);
                delete data.newPassword2;

                $.post(Config.security.changePassword, data, function (res) {
                    app.trigger("response:success", "Password changed successfully.");
                    self.dialogRef.close();
                    self.remove();
                }).error(function () {
                    _eleEnable();
                    self.unfreezeForm(form);
                    _resetForm();
                })
            }

            return false;
        }
    });

    var UploadImageView = Base.extend({

        template: _.template(UploadImageTpl),

        tagName: "div",

        initialize: function (options) {
            this.dialogRef = options.dialogRef;
            this.parentView = options.parentView;
        },

        events: {
            'click .btn-upload-image': "uploadImage"
        },
        render: function () {
            var self = this;
            self.$el.html(self.template());
            return this;
        },
        uploadImage: function (e) {
            var self = this,
                form = self.$('#form-user-image-upload'),
                ele = $(e.currentTarget),
                pbar = form.find('.progress');

            ele.addClass("disabled").text("Saving...");
            self.imageUpload(
                           { id: 0 },
                           form.find('input[type="file"]'),
                           Config.profile.uploadImage,
                           pbar,
                           function (res, data) {
                               if (res == true) {
                                   self.parentView.$('.user-img')
                                       .find('img')
                                       .attr("src", "")
                                       .attr("src", data.response.model)
                               }
                               self.dialogRef.close();
                           });
            pbar.show();
        }
    });

    var View = Base.extend({

        initialize: function (options) {
            var self = this;
            self.template = _.template(ProfileTpl);
            self.subscriber = options.subscriber;
            self.subscriber.readOnly = !(self.subscriber.account)
            self.subscriber.showWriteReview = false;
            self.subscriber.showMyReviews = false;
            self.render();
        },

        events: {
            "click .btn-edit-summary": "editSummary",
            "click .btn-show-edit": "initEdit",
            'click .btn-hide-edit': "hideEdit",
            'click .btn-update': "update",
            'click .btn-change-password-init': "showChangePassword",
            'click .img-edit-icon': "showUploadImage"
        },

        render: function () {
            var self = this;
            $.get(Config.insurance.get + self.subscriber.subscriberId, function (res) {
                self.subscriber = $.extend({}, res.response.model, self.subscriber);
                var trueList = [];
                _.each(planAttr, function (attr, i) {
                    if (res.response.model[attr.key] == true) trueList.push('<label class="label label-primary" style="display:inline-block">' + attr.text.toUpperCase() + '</label>');
                })
                self.subscriber.trueList = trueList.join(' ');

                if (app.isAuthenticated == true && app.sessionManager.get("USER").subscriberType == 5) {
                    if (self.subscriber.account == false) self.subscriber.showWriteReview = true;
                    if (self.subscriber.account == true) self.subscriber.showMyReviews = true;
                }

                self.$el.html(self.template(self.subscriber));

                $(".rating").rating({ showClear: false, showCaption: false });
                setupForm.validation();
                self.$el.find(".wyswg").summernote(
                    {
                        height: 200,
                        focus: true,
                        tooltip: ""
                    })
            })
        },

        editSummary: function (e) {
            var self = this,
                editPanel = $("#edit-summary"),
                viewPanel = $("#view-summary"),
                ele = $(e.currentTarget);
            if (viewPanel.data("toggle") == "show") {
                editPanel.hide();
                viewPanel.show();
                viewPanel.data("toggle", "hide");
                ele.find(".fa").removeClass("fa-save").addClass("fa-edit");
                self.subscriber.summary = self.$el.find(".wyswg").summernote('code');
                $.post(Config.insurance.updateSummary,
                    { id: self.subscriber.subscriberId, summary: self.subscriber.summary }, function (resp) {
                        self.subscriber.summary = self.$el.find(".wyswg").summernote('code');
                        self.render();
                });
            }
            else {
                editPanel.show();
                viewPanel.hide();
                viewPanel.data("toggle", "show");
                ele.find(".fa").removeClass("fa-edit").addClass("fa-save");
            }
        },

        initEdit: function (e) {
            $(".edit-profile").slideDown();
        },

        hideEdit: function (e) {
            $(".edit-profile").slideUp();
        },

        update: function (e) {
            var self = this,
                form = $('.form-insurance-edit');
            ele = $(e.currentTarget);

            if (setupForm.validate()) {
                var params = self.serializeForm(form);
                $.post(Config.insurance.update, params, function (res) {
                    $(".edit-profile").slideUp();
                    $.extend(self.subscriber, res.response.model);
                    self.render();
                })
            }
            return false;
        },

        showChangePassword: function () {
            var self = this;
            if (self.changePasswordView != null) self.changePasswordView.remove();
            BootstrapDialog.show({
                title: 'Change Password',
                message: '<div id="change-password-view"></div>',
                closeByBackdrop: false,
                onshown: function (dialogRef) {
                    self.changePasswordView = new ChangePasswordView($.extend({ el: '#change-password-view', parentView: self, dialogRef: dialogRef }));
                    self.changePasswordView.render();
                },
            });
        },

        showUploadImage: function () {
            var self = this;
            if (self.uploadImageView != null) self.uploadImageView.remove();
            BootstrapDialog.show({
                title: 'Profile Image',
                message: '<div id="upload-image-view"></div>',
                closeByBackdrop: false,
                onshown: function (dialogRef) {
                    self.uploadImageView = new UploadImageView($.extend({ el: '#upload-image-view', parentView: self, dialogRef: dialogRef }));
                    self.uploadImageView.render();
                },
            });
        },

    });

    return View;
});
