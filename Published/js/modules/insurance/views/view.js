﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "modules/insurance/views/view",
       "text!modules/insurance/views/templates/insurance.html",
       "text!modules/insurance/views/templates/search-result-item.html",
       "bootstrapSelect"
],

   function (Config, Constants, Base, Model, InsuranceView, InsuranceTpl, SearchResultItem) {

       "use strict";

       var insuranceType = {
           "any": { value: 0, text: "Any" },
           "individual": { value: 1, text: "Individual Plans" },
           "familyFloater": { value: 2, text: "Family Floater Plans" },
           "topup": { value: 3, text: "Top up" },
           "individualAndFloater": { value: 4, text: "Individual and Floater Plans" },
           "criticalIllness": { value: 5, text: "Critical Illness" },
           "seniorCitizen": { value: 6, text: "Senior Citizen" }
       }

       var View = Base.extend({

           initialize: function () {
               var self = this;
           },

           events: {
               'change #ddl-insurnace-types': "onInsuranceDdlChange",
               'click .btn-search': "search",
               'click .btn-recommend': 'recommend',
           },

           render: function () {
               var self = this;
               self.$el.html(_.template(InsuranceTpl, {}));
               self.FGrid = $("#grid-insurnace-featured")
               self.FGridLoaded = false;
               self.NFGrid = $("#grid-insurnace-non-featured");
               self.NFGridLoaded = false;
               self.$(".btn-search").trigger('click');
           },

           onInsuranceDdlChange: function (e) {
               var self = this,
                   ele = $(e.currentTarget),
                   val = ele.val();
               if (val == insuranceType.topup.value || val == insuranceType.criticalIllness.value) {
                   self.$('.insurnace-search-options').find("input:checkbox").prop("checked", false).attr("disabled", "disabled");
                   return;
               }

               self.$('.insurnace-search-options').find("input:checkbox").removeAttr("disabled");
           },

           search: function (e) {
               var self = this,
                   planAttr = [
                        { key: "cashlessFacility", text: "Cashless Facility" },
                        { key: "maternityBenefits", text: "Maternity Benefits" },
                        { key: "ambulanceCharges", text: "Ambulance Charges" },
                        { key: "dayCareProcedures", text: "DayCare Procedures" },
                        { key: "healthCheckup", text: "Health Checkup" },
                        { key: "taxBenefits", text: "Tax Benefits" },
                        { key: "hospitalizationExpenses", text: "Hospitalization Expenses" },
                        { key: "freelookPeriod", text: "Freelook Period" },
                        { key: "alternativeTreatment", text: "Alternative Treatment" },
                        { key: "cumulativeBonus", text: "Cumulative Bonus" },
                        { key: "organDonorExpenses", text: "OrganDonor Expenses" },
                        { key: "prePostHospitalizationCharges", text: "PrePost HospitalizationCharges" },
                        { key: "portability", text: "Portability" }]

               var fnItemFormatter = function (value, row, index) {
                   var view = this,
                       trueList = [];
                   _.each(planAttr, function (attr,i) {
                       if (row[attr.key] == true) trueList.push('<label class="label label-primary" style="display:inline-block">' + attr.text.toUpperCase() + '</label>');
                   })
                   row.trueList = trueList.join(' ');
                   return _.template(SearchResultItem)(row)
               }

               if (self.NFGridLoaded == true) {
                   self.NFGrid.bootstrapTable('refresh', { query: self.getFilters(false) });
               }
               else {
                   self.NFGrid.bootstrapTable({
                       url: Config.search.insurance + '?ip=' + window.IP,
                       sidePagination: 'server',
                       method: "post",
                       cache: false,
                       pagination: true,
                       pageSize: 5,
                       showLoading: true,
                       clickToSelect: false,
                       sortable: false,
                       showHeader: false,
                       showPaginationSwitch: false,
                       queryParams: function (params) {
                           $.extend(params, self.getFilters(false));
                           return params;
                       },
                       columns: [
                           {
                               field: 'providerName',
                               title: 'Provider',
                               valign: 'middle',
                               align: 'left',
                               formatter: fnItemFormatter
                           }]
                   });

                   self.NFGrid.on("load-success.bs.table", function () {
                       self.NFGridLoaded = true;
                   })
               }

               if (self.FGridLoaded == true) {
                   self.FGrid.bootstrapTable('refresh', { query: self.getFilters(true) });
               }
               else {
                   self.FGrid.bootstrapTable({
                       url: Config.search.insurance+ '?ip=' + window.IP,
                       sidePagination: 'server',
                       method: "post",
                       cache: false,
                       pagination: false,
                       showLoading: true,
                       clickToSelect: false,
                       sortable: false,
                       showHeader: false,
                       showPaginationSwitch: false,
                       queryParams: function (params) {
                           $.extend(params, self.getFilters(true));
                           return params;
                       },
                       columns: [
                           {
                               field: 'providerName',
                               title: 'Provider',
                               valign: 'middle',
                               align: 'left',
                               formatter: fnItemFormatter
                           }]
                   });
                   self.FGrid.on("load-success.bs.table", function () {
                       self.FGridLoaded = true;
                   })
               }

               $(".panel-footer").show();

           },

           getFilters: function (isFeatured) {
               var self = this,
                   data = self.serializeForm($('#form-insurance')),
                   lenghtOfSelections = $('#form-insurance').find(":checkbox:checked").length;

               for (var key in data) {
                   if (data[key] == "on") {
                       data[key] = 1;
                   }
                   else if(data[key] == "off"){
                       data[key] = 0;
                   }
               }
               data.planType = _.filter(insuranceType, { value: parseInt($("#ddl-insurnace-types").val()) })[0].text
               data.isFeatured = isFeatured;
               return { insurance: data };
           },
           
           recommend: function (e) {
               var self = this,
                    ele = $(e.currentTarget),
                    spnRm = ele.closest(".insurance-result-item").find(".spnRecommendCount"),
                    eid = ele.data("entityId"),
                    request = { entityId: eid, entityType: 6 , remove: false, ipAddress: window.IP },
                    cnt = parseInt(spnRm.text().trim()),
                    model = new Model();

               model.url = Config.recommend.addOrUpdate;
               request.remove = false;


               model.save(request, {
                   success: function () {
                       spnRm.text(cnt + 1);
                       ele.replaceWith('<i class="fa fa-thumbs-up fa-lg clr-green"></i>');
                   }
               })
           }
       });

       return View;

   });