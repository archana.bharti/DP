define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/featured/views/view"
], function (
    Config,
    Constants,
    Controller,
    FeaturedView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        featured: function (args) {
            var self = this,
                view = new FeaturedView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear();
        }
    });

    return Controller;

});
