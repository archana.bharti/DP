define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/studentPursuit/views/view"
], function (
    Config,
    Constants,
    Controller,
    StudentPursuitView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        studentPursuit: function (args) {
            var self = this,
                view = new StudentPursuitView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().render();
        }
    });

    return Controller;

});
