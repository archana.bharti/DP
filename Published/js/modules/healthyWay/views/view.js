﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "modules/healthyWay/views/view",
        "text!modules/healthyWay/views/templates/healthy-way.html",
        "bootstrapSelect"
    ],

    function (Config, Constants, Base, Model, HealthyWayView, HealthyWayTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function () {
                var self = this;
            },

            events : {

            },

            render: function () {
                var self = this;
                self.$el.html(_.template(HealthyWayTpl, {

                }));
            }
        });

        return View;

    });