﻿define([
    "apiConfig",
    "modules/dashboard/views/base",
    'core/model',
    'modules/shared/collections/states',
    'modules/shared/collections/specializations',
    'modules/shared/collections/cities',
    'modules/shared/collections/collegeCities',
    'modules/shared/collections/locations',
    "text!modules/dashboard/templates/dashboard.html",
    "text!modules/dashboard/templates/features-carousel-item.html",
    "bootstrap-dialog",
    "bootstrapSelect",
    "owl-carousel"
    ],

    function (Config, Base, Model, StatesColl, SpecializationsColl, CitiesColl, CollegeCitiesColl, LocationsColl, WelcomeTpl, FeaturesCarouselItemTpl, BootstrapDialog) {

        "use strict";

        var View = Base.extend({

            initialize: function (options) {
                var self = this;
                self.specializationsModel = new Model();
                self.specializationsModel.url = Config.enums.specializations;

                self.citiesModel = new Model();
                self.citiesModel.url = Config.enums.cities;

                self.statesModel = new Model();
                self.statesModel.url = Config.enums.states;
                
                $.when(
                    self.specializationsModel.fetch(),
                    self.citiesModel.fetch(),
                    self.statesModel.fetch()
                    ).done(function () {
                        self.render();
                })
            },

            events: {
                "click .btn-search": "search",
                //"click .btn-reset": "reset",
                "click .search-nav li": "onTabClick",
                "click #doctor-clinic-search-form input:radio": "initSpecializations",
                "change [data-city-location]": "loadLocations",
                "change [data-state-city]": "loadCitiesByState",
                "change #state-college": "loadCollegeCitiesByState"
            },

            render: function () {
                var self = this;
                self.$el.html(_.template(WelcomeTpl, {
                    cities: self.citiesModel.toJSON(),
                    hospitalSpzs: self.specializationsModel.toJSON().hospital,
                    states: self.statesModel.toJSON()
                }));

                $('#spz-doctor-clinic, #spz-hospital').select2({
                    style: "width:100%"
                });

                self.$('#doctor-clinic-search-form input:radio').first().trigger('click');
                self.currentNavId = $('.search-nav li.active').data('panel');
                self.initializeNameSelect()
                    .initializeFormValidations(self.currentNavId)
                    .loadFeatures();
            },

            initSpecializations: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.val(), spzs = self.specializationsModel.toJSON(),
                    spzDdl = self.$('#spz-doctor-clinic');
                spzDdl.find("option").remove();
                spzDdl.append($("<option/>").val("null").text('Any Specialization'));
                switch (val.toString()) {
                    case "1":
                        $.each(spzs.doctor, function (i, o) {
                            spzDdl.append($("<option/>").val(o.id).text(o.name));
                        })
                        break;
                    case "2":
                        $.each(spzs.dental, function (i, o) {
                            spzDdl.append($("<option/>").val(o.id).text(o.name));
                        })
                        break;
                    case "3":
                        $.each(spzs.alternativeMedicine, function (i, o) {
                            spzDdl.append($("<option/>").val(o.id).text(o.name));
                        })
                }

                spzDdl.select2().val("null").trigger("change");
            },

            loadLocations : function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    cityId = ele.val(),
                    target = $(ele.data("cityLocation"));
                if (cityId == "null") {
                    target.find("option").remove();
                    target.attr("disabled", "disabled").append($("<option/>").val("null").text('Any Location'));
                    return;
                }

                new LocationsColl([], { id: cityId }).fetch({
                    success: function (m) {
                        var data = [];
                        data.push({ id: "null", text: 'Any Location' });
                        $.each(m.models, function (index, item) { // Iterates through a collection
                            data.push({ id: item.attributes.value, text: item.attributes.text });
                        });

                        if (m.model.length == 0) target.children().remove();
                        target.select2({
                            disabled: m.models.length == 0 ? true : false,
                            data: m.models.length == 0 ? null : data,
                            placeholder: "No locations found"
                        })
                        // ele.select2("open");
                    }
                });

                target.removeAttr("disabled")
            },

            /* Events handler */

            search: function (e) {
                var self = this;
                var values = self.serializeForm($('#'+self.currentNavId+' form'));
                values.SearchType = $('#' + self.currentNavId).data('searchtype');
                if(values.SearchType == 3){
                    values.DepartmentId = $('input[name=optradio]:checked', '#'+self.currentNavId).val();
                }else{
                    values.DoctorOrClinicCategoryType = $('input[name=optradio]:checked', '#'+self.currentNavId).val();
                }

                if(values.SpecializationId == 'Select Specialization'){
                    values.SpecializationId = null;
                }
                if(values.locationId == 'Select Location'){
                    values.locationId = null;
                }
                values.ip = window.IP;
                localStorage.clear();
                console.log("search params",values);
                localStorage.setItem("searchParams", JSON.stringify(values));
                app.router.navigate('search/results', true);
                return false;
            },          

            onTabClick: function(e){
                var self = this;
                self.currentNavId = $(e.currentTarget).data('panel');
                self.initializeFormValidations(self.currentNavId);
                return self;
            },

            /* Helper methods */

            initializeNameSelect: function(){
                var self = this;
                var activePanel = $("#" + self.currentNavId);
                var _formatUser = function (user) {
                    if (user.loading) return user.text;
                    var $user = $(
                        '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>'
                    );
                    return $user;
                }

                var _selection = function (user) {

                    if (user.name == undefined) {
                        return '<option value="null" selected="selected">Name</option>';
                    }else{
                        user.name = user.name.replace('Dr.', '');
                    }

                    return '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>'
                }
                self.$(".suggest-ddl").select2({
                    ajax: {
                        url: Config.user.suggest,
                        dataType: 'json',
                        type : "POST",
                        delay: 250,
                        data: function (params) {
                            var userType = parseInt($('#'+self.currentNavId).data('usertype'));
                            //for doctor and clinic the suggestion should get both merged types as DoctorsAndClinics(6)
                            if(userType == 1 || userType == 2){ userType = -1; }
                            return {
                                searchTerm: params.term,
                                userType: userType
                            };
                        },
                        processResults: function (data, page) {
                            // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data
                            return {

                                results: data.response.model.results
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    minimumInputLength: 3,
                    templateResult: _formatUser, // omitted for brevity, see the source of this page
                    templateSelection: _selection // omitted for brevity, see the source of this page
                });

                return self;
            },

            initializeFormValidations: function(currentNavId){
                var self = this;
                self.formSetup($('#' + currentNavId + ' form'));
                return self;
            },

            loadFeatures: function () {

                var self = this,
                    locationId = (localStorage.getItem("searchParams") == undefined || JSON.parse(localStorage.getItem("searchParams")).locationId == undefined)
                                 ? null : JSON.parse(localStorage.getItem("searchParams")).locationId;
                var model = new Model();
                model.url = Config.search.zone;
                model.save({ locationId: locationId }, {
                    success: function (m, res) {
                        self.$('#features-carousel').append(_.template(FeaturesCarouselItemTpl, {
                            //this model is dependent on locationId it is been processed as a flat model rather as a collection
                            model: res.response.model
                        }));
                        self.$('#features-carousel').owlCarousel({
                            navigation: true, // Show next and prev buttons
                            slideSpeed: 50,
                            paginationSpeed: 75,
                            singleItem: true,
                            autoPlay: true,
                            stopOnHover: true,
                            pagination: false,
                            autoHeight: true,
                            navigationText: ["<i class='fa fa-backward' />", "<i class='fa fa-forward' />"],
                            transitionStyle: "backSlide"
                        });
                    }
                })
            },           

            loadCitiesByState: function (e) {
                var self = this,
                  ele = $(e.currentTarget),
                  stateId = ele.val(),
                  target = $(ele.data("stateCity"));

                target.children().remove();

                if (stateId == "null") {
                    target.find("option").remove();
                    target.attr("disabled", "disabled").append($("<option/>").val("null").text('Any City'));
                    return;
                }

                new CitiesColl([], { id: stateId }).fetch({
                    success: function (m) {
                        var data = [];
                        data.push({ id: "null", text: 'Any City' });
                        $.each(m.models, function (index, item) { // Iterates through a collection
                            data.push({ id: item.attributes.value, text: item.attributes.text });
                        });
                        if (m.model.length == 0) target.children().remove();
                        target.select2({
                            disabled: m.models.length == 0 ? true : false,
                            data: m.models.length == 0 ? null : data,
                            placeholder: "No cities found"
                        })
                    }
                });
                target.removeAttr("disabled")
            },

            loadCollegeCitiesByState: function (e) {
                var self = this,
                   ele = $(e.currentTarget),
                   stateId = ele.val(),
                   target = $('#city-college');

                target.children().remove();

                if (stateId == "null") {
                    target.find("option").remove();
                    target.attr("disabled", "disabled").append($("<option/>").val("null").text('Any City'));
                    return;
                }

                new CollegeCitiesColl([], { id: stateId }).fetch({
                    success: function (m) {
                        var data = [];
                        data.push({ id: "null", text: 'Any City' });
                        $.each(m.models, function (index, item) { // Iterates through a collection
                            data.push({ id: item.attributes.value, text: item.attributes.text });
                        });
                        if (m.model.length == 0) target.children().remove();
                        target.select2({
                            disabled: m.models.length == 0 ? true : false,
                            data: m.models.length == 0 ? null : data,
                            placeholder: "No cities found"
                        })
                    }
                });
                target.removeAttr("disabled")
            }
        });

        return View;

    });