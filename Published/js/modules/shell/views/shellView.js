﻿﻿define([
        "apiConfig",
        "viewBase",
        'core/model',
        "modules/shell/utilities/menuUtil",
        "bootstrap-dialog",
        "text!modules/shell/templates/shell.html",
        "text!modules/shell/templates/user.html"
    ],

    function (Config, Base, Model, MenuUtil, BootstrapDialog, ShellTpl, UserTpl) {

        //"use strict";

        var View = Base.extend({

            initialize: function (options) {
                var self = this;
                self.$el.html(_.template(ShellTpl)({}));
                $(".dp-footer").show();
                MenuUtil.init();
            },

            events : {
                'click #btn-logout': 'logout',
                'click .dp-sign-in' : 'showLoginModal',
                'click .btn-login' : 'onLogin',
                'click .calculate-bmi-btn': 'calculateBMI',
                'click li[data-panel]': 'activatePanel',
                'click a[data-route]': 'navigate',
                'click a[data-need-auth]' : 'needAuth',
                'click .btn-login': 'login',
                'click [data-login-panel-toggle]': 'togglePanel',
                'click .btn-forgot-password': 'forgotPassword'
            },

            render: function () {
                var self = this;
                self.formSetup();
                if (app.sessionManager.get("USER") != null) {
                    self.renderUser();
                }
                app.trigger("shell:complete");
            },

            activatePanel: function(e){
                var self = this;
                var ele = $(e.currentTarget);
                var target = $('#' + ele.attr('data-panel'));
                self.$el.find("li[data-panel]").removeClass('active');
                self.$el.find("#doctor-clinic-search-panel, #college-search-panel, #hospital-search-panel").hide();
                ele.addClass("active");
                target.show();
            },

            login: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    form = self.$el.find(".form-login");
                if (self.validateForm()) {
                    var values = self.serializeForm($('.form-login'));

                    self.freezeForm(form);
                    ele.text("Processing...");

                    var model = new Model(); //cannot pass url in the constructor
                    model.url = Config.security.login;
                    model.save(values, {
                        success: function (model, res) {
                            app.isAuthenticated = true;
                            app.sessionManager.set("USER", res.response.model);
                            self.renderUser();
                            app.router.redirect("account");
                        },
                        error: function () {
                            self.unfreezeForm(form);
                            self.resetForm();
                            ele.text("Login");
                            app.trigger("response:error", arguments[1].responseJSON.response.error1);
                        }
                    });
                }
            },

            logout: function (e) {
                var self = this;
                $.get(Config.security.logout, function () {
                    for (var key in window.sessionStorage) {
                        window.sessionStorage.removeItem(key);
                    }
                    window.location.href = "/";
                });
            },

            calculateBMI: function(e){
                e.stopImmediatePropagation();
                e.preventDefault();
                if($('#edit-body-height').val().length == 0 || $('#edit-body-weight').val().length == 0){
                    return;
                }
               
                var weight = $('#edit-body-weight').val(),
                    height = $('#edit-body-height').val();
                var bmi = (weight / ( (height/100) * (height/100)) ).toFixed(2);
                $('#edit-bmi-result').val(bmi);
                return false;
            },

            navigate: function (e) {
                var self = this,
                    route = $(e.currentTarget).data('route');
                app.router.redirect(route);
            },

            needAuth : function(e){
                var self = this,
                    ele = $(e.currentTarget);
                app.trigger("warning", "Please Sign In or Register.", "Authentication")
            },

            formSetup: function () {
                var self = this;
                var form = self.$el.find(".form-login");
                form.bootstrapValidator({
                    live: 'disabled',
                    submitButtons: 'button[type="submit"]',
                    fields: {
                        userName: {
                            validators: {
                                notEmpty: {
                                    message: 'Please enter your user name'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'Pleae enter your password.'
                                }
                            }
                        }
                    }
                })
            },

            validateForm: function () {
                $('.form-login').data("bootstrapValidator").validate();
                return $('.form-login').data("bootstrapValidator").isValid();
            },

            resetForm: function () {
                $('.form-login').data("bootstrapValidator").resetForm();
            },

            renderUser: function () {
                var self = this;
                var user = app.sessionManager.get("USER");
                self.$(".my-account-li").html(_.template(UserTpl, { model: user })).show();
                $('.dp-sign-in').remove();
            },

            togglePanel: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    data = ele.data("login-panel-toggle").split(';');
                $(data[0]).show();
                $(data[1]).hide();
            },

            forgotPassword: function (e) {
                var self = this,
                   ele = $(e.currentTarget),
                   form = self.$el.find(".form-forgot-password");
                var values = self.serializeForm(form);
                if ($.trim(values.email).length > 0 && values.email.indexOf('@') != -1) {
                    $.get(Config.security.forgotPassword + values.email, function () {
                        app.trigger("response:success", "Your password has been reset. Please check your email");
                    })
                }
            }
        });

        return View;

    });