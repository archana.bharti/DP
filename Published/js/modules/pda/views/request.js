﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       'modules/pda/collections/specializations',
       'modules/shared/collections/cities',
       "text!modules/pda/templates/form.html",
       "text!modules/pda/templates/conversation-item.html",
       "bootstrapSelect"
], function (Config, Constants, Base, Model, SpzColl, CitiesColl, RequestTpl, ConversationItemTpl) {

    "use strict";

    var View = Base.extend({
        tagName: 'div',

        events :{
            "click .btn-submit-pda": "onPDASubmit"
        },

        initialize: function (options) {
            this.parentView = options.parent
            return this;
        },

        render: function () {
            var self = this;

            self.$el.html(_.template(RequestTpl, {}));

            self.$('#cities').select2({
                placeholder: "Select city",
                style: "width:100%"

            }).on("select2:opening", function (e) {
                var tat = $(this);
                tat.select2({ placeholder: "Loading..." });
                self.loadCities(tat);
            })

            self.$('#spz').select2({
                placeholder: "Select specialization",
                style: "width:100%"

            }).on("select2:opening", function (e) {
                var tat = $(this);
                tat.select2({ placeholder: "Loading..." });
                self.loadSpecializations(tat);
            })

            self.setupForm();

            return self;
        },

        loadSpecializations : function(ele){
            new SpzColl().fetch({
                success: function (m) {
                    var data = [];
                     data.push({id: "-1", text: 'Select specialization' })
                    $.each(m.models, function (index, item) {
                        data.push({ id: item.attributes.value, text: item.attributes.text });
                    });
                    ele.select2({
                        data: data
                    })
                    ele.off("select2:opening");
                    ele.select2("open");
                }
            });
        },
        
        loadCities: function (ele) {
            new CitiesColl().fetch({
                success: function (m) {
                    var data = [];
                    data.push({id  : "-1", text :'Select city'})
                    $.each(m.models, function (index, item) {
                        data.push({ id: item.attributes.value, text: item.attributes.text });
                    });

                    ele.select2({
                        data: data
                    })
                    ele.off("select2:opening");
                    ele.select2("open");
                }
            });
        },

        onPDASubmit: function (e) {
            var self = this, form = $('.pda-form'), ele = $(e.currentTarget);
            if (!self.$('#pdaTandQ').prop('checked')) { app.trigger("warning", "Please accept terms and conditions", "Terms and Conditions"); return false; }
            var values = self.serializeForm(form);
            values.userId = app.sessionManager.get('USER').id;
            var model = new Model(); //cannot pass url in the constructor
            model.url = Config.pda.advice;
            ele.text("Saving...").addClass("disabled"); 
            model.save(values, {
                success: function (m, res) {
                    var role = app.sessionManager.get('USER').type;
                    self.parentView.$('#pda-conversations-list').prepend(_.template(ConversationItemTpl,{ model: m.toJSON(), role :role }));
                    self.unfreezeForm(form);
                    self.resetForm();
                    ele.text("Submit").removeClass("disabled");
                },
                error: function (model, data) {
                    app.trigger("response:error",data.responseJSON.response.error1 );
                    self.unfreezeForm(form);
                    self.resetForm();
                    ele.text("Submit").removeClass("disabled");
                }
            });
            return false;
        },

        setupForm: function () {
            var self = this,
                form = self.$(".pda-form");
        },

        resetForm: function () {
            var self = this;
            self.$("#subject, #question").val('');
            self.$('#pdaTandQ').prop('checked', false);
            self.$('#cities, #spz').select2("val", "-1");
        }

    });

    return View;
});