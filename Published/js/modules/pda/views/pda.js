﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "text!modules/pda/templates/pda.html",
        "text!modules/pda/templates/conversations.html",
        "modules/pda/views/request",
        "bootstrapSelect"
    ],

    function (Config, Constants, Base, Model, PdaTpl, ConversationsTpl, RequestForm) {

        "use strict";

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.role =  app.sessionManager.get('USER').type;
                self.questionsModel =  new Model();
                self.questionsModel.url = Config.pda.advice;
                self.$el.html(_.template(PdaTpl, {}));
                self.renderConversations();

                if (self.role.toLowerCase() == 'user')
                    self.renderRequestForm();
            },

            events : {
                "click .btn-reply": "reply",
                "click .btn-send": "answer"
            },

            renderConversations: function () {
                var self = this;
                self.questionsModel.fetch({
                    success: function (model, resp) {
                        self.$el.find('#pda-conversations-list').html(_.template(ConversationsTpl, {
                            model: resp.response.model,
                            role: self.role
                        }));
                    }
                });
            },

            renderRequestForm : function(){
                var self = this,
                    accordion = self.$("#accordion");
                accordion.append(new RequestForm({parent : self}).render().el);
            },

            reply: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                   replyBox = ele.next();
                ele.hide();
                replyBox.show();
            },
            
            answer: function (e) {
                var self = this,
                   ele = $(e.currentTarget),
                   replyBox = ele.prev(),
                   section =  ele.closest('.reply-section'),
                   replyBtn = section.prev();
                if (replyBox.val().trim().length > 0) {
                    var request = {
                        answer: replyBox.val().trim(),
                        id: ele.data('conversation-id'),
                        userId: ele.data('user-id')
                    };
                    var answerModel = new Model({ id: request.id });
                    answerModel.url = Config.pda.advice + '/' + app.sessionManager.get('USER').userId;
                    answerModel.save(request, {
                        success: function (model, resp) {
                            section.remove();
                            replyBtn.replaceWith('<h5><span class="label label-success">Reply sent</span><h5>');

                            //ele.closest('.reply-section').html('<span class="answer-text"><b>Answer : </b>' + resp.response.model.answer + '</span>');
                            //ele.closest('.replied-date').html('<span><b>Replied </b>' + resp.response.model.repliedOn + '</span>');
                        }
                    })
                }
            }
        });

        return View;

    });