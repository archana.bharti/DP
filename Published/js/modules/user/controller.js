define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/user/views/view",
    "modules/user/views/savedProfiles"
], function (
    Config,
    Constants,
    Controller,
    UserView,
    SavedProfiles
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        profile: function (args) {
            var self = this,
                view = new UserView({ controller : self, el: Constants.CONTENT, args: args });
        },

        bookmarks: function (args) {
            var self = this,
                view = new SavedProfiles({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().render();
        },

        editProfile: function(args){
            var self = this,
                view = new UserView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().renderEditProfile();
        }
    });

    return Controller;

});
