define([
    "apiConfig",
    "core/model",
    "core/collection",
    "core/base"
], function(
    Config,
    Model,
    Collection) {

    var ViewModel = Model.Base.extend({
        create: function(args){
            this.profileInfo = new Model({url: Config.profile.userProfile});
            this.summaryInfo = new Model({url: Config.profile.userSummary});
            return this;
        },

        getInfo: function(uid, callback){
            $.when(
                this.profileInfo.fetch(),
                this.summaryInfo.fetch()
            ).done(function(profileInfo, summaryInfo){
                var data = {
                    profileInfo: profileInfo.response.model,
                    summaryInfo: summaryInfo.response.model
                    };

                    callback(data);
            })
        }
    })
})