﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/profile.html",
        "text!modules/user/templates/change-password.html",
        "text!modules/user/templates/upload-image.html",
        "bootstrap-dialog",
        "bootstrapValidator"
    ],

    function (Config, Constants, Base, Model, ProfileTpl, ChangePasswordTpl, UploadImageTpl, BootstrapDialog) {

        "use strict";


        var ChangePasswordView = Base.extend({

            template: _.template(ChangePasswordTpl),

            tagName : "div",

            initialize: function (options) {
                this.dialogRef = options.dialogRef
            },

            events: {
                'click .btn-change-password' : "changePassword"
            },
            render: function () {
                var self = this;
                var _initFormValidator = function () {
                    $('#form-change-password').bootstrapValidator({
                        live: 'disabled',
                        submitButtons: 'button[type="submit"]',
                        fields: {
                            oldPassword: {
                                validators: {
                                    notEmpty: {
                                        message: 'Password should not be empty.'
                                    },
                                    stringLength: {
                                        min: 6,
                                        message: 'Password must be of 6 or more characters'
                                    },
                                    callback: {
                                        message: 'Old password and new password should not be same',
                                        callback: function (value, validator, $field) {
                                            var _oldp = $field.val();
                                            var _newP = $("[name='newPassword']").val();
                                            if (btoa(_newP) === btoa(_oldp)) {
                                                return {
                                                    valid: false,       // or true
                                                    message: 'Old password and new password should not be same'
                                                }
                                            }

                                            return {
                                                valid:  true
                                            }
                                        }
                                    }
                                }
                            },
                            newPassword: {
                                validators: {
                                    notEmpty: {
                                        message: 'New password should not be empty.'
                                    },
                                    stringLength: {
                                        min: 6,
                                        message: 'Password must be of 6 or more characters'
                                    },
                                    identical: {
                                        field: 'newPassword2',
                                        message: 'The new password and password entered again are not identical.'
                                    }
                                }
                            },
                            newPassword2: {
                                validators: {
                                    notEmpty: {
                                        message: 'Confirm password again for security'
                                    },
                                    stringLength: {
                                        min: 6,
                                        message: 'Password must be of 6 or more characters'
                                    },
                                    identical: {
                                        field: 'newPassword',
                                        message: 'The new password and password entered again are not identical.'
                                    }
                                }
                            }
                        }
                    })
                }
                self.$el.html(self.template(self.model));
                _initFormValidator();
                return this;
            },

            changePassword: function (e) {
                var self = this, ele = $(e.currentTarget), form = $('#form-change-password')
                var _validateForm = function () {
                    form.data("bootstrapValidator").validate();
                    return $('#form-change-password').data("bootstrapValidator").isValid();
                },
                _resetForm = function () {
                    form.data("bootstrapValidator").resetForm();
                },
                _eleDisable = function () {
                    ele.addClass("disabled").text("Saving...");
                },
                _eleEnable = function () {
                    ele.removeClass("disabled").text("Submit");
                }

                if (_validateForm()) {
                   
                    var data = self.serializeForm(form);
                    self.freezeForm(form);
                    _eleDisable();

                    data.oldPassword = btoa(data.oldPassword);
                    data.newPassword = btoa(data.newPassword);
                    delete data.newPassword2;

                    $.post(Config.security.changePassword, data, function (res) {
                        app.trigger("response:success", "Password changed successfully.");
                        self.dialogRef.close();
                        self.remove();
                    }).error(function () {
                        _eleEnable();
                        self.unfreezeForm(form);
                        _resetForm();
                    })
                }

                return false;
            }
        });

        var UploadImageView = Base.extend({

            template: _.template(UploadImageTpl),

            tagName: "div",

            initialize: function (options) {
                this.dialogRef = options.dialogRef;
                this.parentView = options.parentView;
            },

            events: {
                'click .btn-upload-image': "uploadImage"
            },
            render: function () {
                var self = this;
                self.$el.html(self.template());
                return this;
            },
            uploadImage: function (e) {
                var self = this,
                    form = self.$('#form-user-image-upload'),
                    ele = $(e.currentTarget),
                    pbar = form.find('.progress');

                ele.addClass("disabled").text("Saving...");
                self.imageUpload(
                               { id: 0 },
                               form.find('input[type="file"]'),
                               Config.profile.uploadImage,
                               pbar,
                               function (res, data) {
                                   self.parentView.$('.user-img')
                                       .find('img')
                                       .attr("src", "")
                                       .attr("src", data.response.model)
                                   self.dialogRef.close();
                               });
                pbar.show();
            }
        });

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.parent = obj.view;
                self.subscriber = obj.subscriber;
                self.$el = obj.container;
                self.roleId = self.subscriber.subscriberType;
                self.profileInfo = new Model();
                self.changePasswordView = null;
                self.uploadImageView = null;
            },

            events : {
                "click .edit-profile": "showForm",
                "click .btn-cancel-user-profile": "hideForm",
                'click .btn-save-user-profile': "save",
                'click .btn-change-password-init': "showChangePassword",
                'click .img-edit-icon' : "showUploadImage"
            },

            render: function () {
                var self = this;
                self.renderProfile(self.subscriber);
            },

            showChangePassword : function(){
                var self = this;
                if (self.changePasswordView != null) self.changePasswordView.remove();
                BootstrapDialog.show({
                    title: 'Change Password',
                    message: '<div id="change-password-view"></div>',
                    closeByBackdrop: false,
                    onshown: function (dialogRef) {
                        self.changePasswordView = new ChangePasswordView($.extend({ el: '#change-password-view', parentView: self, dialogRef: dialogRef }));
                        self.changePasswordView.render();
                    },
                });
            },

            showUploadImage : function(){
                var self = this;
                if (self.uploadImageView != null) self.uploadImageView.remove();
                BootstrapDialog.show({
                    title: 'Profile Image',
                    message: '<div id="upload-image-view"></div>',
                    closeByBackdrop: false,
                    onshown: function (dialogRef) {
                        self.uploadImageView = new UploadImageView($.extend({ el: '#upload-image-view', parentView: self, dialogRef: dialogRef }));
                        self.uploadImageView.render();
                    },
                });
            },

            renderProfile : function(model){
                var self = this;                
                var fnGetQualifications = function (arr, key) {
                    var temp = [];
                    $.each(arr, function (idx, q) {
                        temp.push(q[key]);
                    });
                    return temp.join(', ');
                }
                self.$el.find('.user-profile').html(_.template(ProfileTpl, $.extend(model,{
                    readOnly: !(self.subscriber.account),
                    roleId: self.subscriber.subscriberType,
                    longQualifications: model.longQualifications == undefined ? [] : fnGetQualifications(model.longQualifications, 'longEndDegree'),
                    shortQualifications: model.shortQualifications == undefined ? [] : fnGetQualifications(model.shortQualifications, 'shortEndDegree'),
                    email: (self.subscriber.account == true) ? self.subscriber.email : null
                })));
            },

            showForm: function(e){
                var self = this;
                self.$('.user-edit-row').show();
            },

            hideForm: function(e){
                var self = this;
                self.$('.user-edit-row').hide();
            },

            save: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    form = self.$(".form-user-profile");
                var request = self.serializeForm(form);
                request.subscriberType = self.subscriber.subscriberType;

                var m = new Model({id : self.subscriber.userId})
                m.url = Config.profile.update + self.subscriber.userId;
                m.save(request, {
                    success: function (model, resp) {
                        self.$('.user-edit-row').hide();
                        self.renderProfile(resp.response.model);
                    }
                });
            }
        });

        return View;

    });