﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/saved-profiles.html"
    ],

    function (Config, Constants, Base, Model, SavedProfilesTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.savedProfilesModel = new Model();
            },

            events : {

            },

            render: function () {
                var self = this;
                self.savedProfilesModel.url = Config.profile.bookmarkProfile+ '?id=' + app.sessionManager.get('USER').id;
                self.savedProfilesModel.fetch({
                    success: function(model, resp){
                        self.$el.html(_.template(SavedProfilesTpl, {
                            data: resp.response.model
                        }));
                    }
                });
            }
        });

        return View;

    });