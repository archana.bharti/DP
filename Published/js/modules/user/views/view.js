define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "modules/user/views/about",
        "modules/user/views/reviews",
        "modules/user/views/summary",
        "modules/user/views/specializations",
        "modules/user/views/qualifications",
        "modules/user/views/scoreReport",
        "modules/insurance/views/profile",
        "text!modules/user/templates/main.html",
        "text!modules/user/templates/category-score-report.html",
        "bar-rating",
        "summernote",
        "circliful"
    ],

    function(Config,
            Constants,
            Base, 
            Model,
            About,
            Reviews,
            Summary,
            Specializations,
            Qualifications,
            ScoreReport,
            InsuranceView,
            MainTpl,
            CategoryScoreReportTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function(options) {
                var self = this;
                self.subscriber = {};
                self.subscriber.account = options.args.account;
                if (options.args.account == true) {
                   _.extend(self.subscriber, app.sessionManager.get("USER"));
                    self.render();
                }
                else {
                    var m = new Model();
                    var uid = btoa(options.args.id + ':'+ Constants.USER_ROLES_ID[options.args.type]);
                    m.url = Config.profile.userProfile + uid
                    m.fetch({
                        success: function (res) {
                            _.extend(self.subscriber, res.toJSON());
                            self.render();
                        }
                    })
                }
            },

            events: {

            },

            render: function() {
                var self = this;

                var sectionsVisibility = self.getSectionsVisibility(self.subscriber.subscriberTypeText, self.subscriber.account);
                self.$el.html(_.template(MainTpl, {
                    sectionsVisibility: sectionsVisibility,
                    readOnly: !(self.subscriber.account)
                }));


                switch (parseInt(self.subscriber.subscriberType)) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5: 
                        var fnViews = {
                            about: function () {
                                self.aboutView = new About({ subscriber: self.subscriber, container: self.$el, view: self });
                                self.aboutView.render();
                            },
                            summary: function () {
                                self.summaryView = new Summary({ subscriber: self.subscriber, container: self.$el, view: self });
                                self.summaryView.render()
                            },
                            specializations: function () {
                                self.specializationsView = new Specializations({ subscriber: self.subscriber, container: self.$el, view: self })
                                self.specializationsView.render();
                            },
                            qualifications: function () {
                                self.qualificationsView = new Qualifications({ subscriber: self.subscriber, container: self.$el, view: self });
                                self.qualificationsView.render()

                            },
                            reviews: function () {
                                self.reviewsView = new Reviews({ subscriber: self.subscriber, container: self.$el, view: self });
                                self.reviewsView.render();
                            },
                            scoreReport: function () {
                                self.scoreReportView = new ScoreReport({ subscriber: self.subscriber, container: self.$el, view: self });
                                self.scoreReportView.render();
                            }
                        }

                        try { fnViews.about(); } catch (e) { console.log("failed to render about", e) };
                        try { if (sectionsVisibility.showSummary) fnViews.summary() } catch (e) { console.log("failed to render summary", e) };
                        try { if (sectionsVisibility.showSpecializations) fnViews.specializations(); } catch (e) { console.log("failed to render specializations", e) };
                        try { if (sectionsVisibility.showQualifications) fnViews.qualifications(); } catch (e) { console.log("failed to render qualifications", e) };
                        try { if (sectionsVisibility.showReports) fnViews.scoreReport(); } catch (e) { console.log("failed to render scoreReport", e) };
                        try { fnViews.reviews(); } catch (e) { console.log("failed to render reviews", e) };
                    
                        break;

                    case 6:
                        self.$el.html();
                        self.insurnaceView = new InsuranceView({ subscriber: self.subscriber, el: Constants.CONTENT });
                        self.insurnaceView.clear().render();
                        break;
                }

               
            }
           
        });

        return View;

    });
