﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/specializations.html",
        "text!modules/user/templates/endorsements.html",
         "text!modules/user/templates/endorsers.html",
        'modules/shared/collections/specializations'
    ],

    function (Config, Constants, Base, Model, SpecializationsTpl, EndorsementsTpl, EndorsersTpl, SpecializationsColl) {

        "use strict";

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.parent = obj.view;
                self.subscriber = obj.subscriber;
                self.$el = obj.container;
                self.specializationsInfo = new Model();
                self.endorsementsInfo = new Model();
            },

            events: {

                "keyup .search-specialization": 'onSearchList',
                "click .btn-endorse": "onEndorse",
                "click a.has-endorsers": "showEndorsers",
                "click input.chkUpdateSpecialization" : "updateSpecialization"
            },

            render: function () {
                var self = this;
                self.getUserSpecializations(function (userSpzs) {
                    self.getEndorsements(function (endorsedSpzs) {
                        self.renderSpecializations(endorsedSpzs, userSpzs);
                    });
                });
            },
            

            getUserSpecializations : function(callback){
                var self= this;
                self.specializationsInfo.url = Config.profile.userSpecializations + '/' + self.subscriber.subscriberId + '?type=' + self.subscriber.subscriberType;
                self.specializationsInfo.fetch({
                    success: function (model, resp) {
                        var spz = model.toJSON();
                        _.bind(callback(spz), self);
                    }
                });
            },

            renderSpecializations: function (endorsedSpzs, userSpzs) {
                var self = this;

                var _isCurrentUserEndorsed = function (spzId, endorsement) {
                    var yes = false;
                    if (!app.isAuthenticated) { return false; }
                    else {

                        if (endorsement == undefined) { return false;}
                        var endorserType = Constants.USER_ROLES_ID[app.sessionManager.get('USER').subscriberType],
                          endorserId = app.sessionManager.get('USER').userId;
                        _.each(endorsement.endorsers, function (en) {
                            if (en.endorserId == endorserId && en.endorserType == endorserType) {
                                yes = true;
                                return false;
                            }
                        })
                    }
                    return yes;
                }

                var _sort = function () {
                    var hasSpz = self.$(".spz-list").find("input:checked").closest(".list-group-item");
                   
                    self.$(".spz-list").find("input:checked").closest(".list-group-item").remove();
                    hasSpz.insertAfter(self.$("li.list-group-item.header"))
                }
                $.getJSON(Config.enums.specializations, function (res) {
                    var data = [];
                    var _model = (self.subscriber.subscriberType == '1' || self.subscriber.subscriberType == '2') ? res.response.model.doctor : res.response.model.hospital
                    $.each(_model, function (index, item) {
                        data.push({ id: item.id, text: item.name });
                    });

                    self.$el.find('.user-specializations').html(_.template(SpecializationsTpl, {
                        model: {
                            specializations: data.splice(1),
                            endorsements: endorsedSpzs,
                            showEndorsement: true,
                            allowToEndorse: self.canEndorse(),
                            allowToUpdateSpecializations: self.subscriber.account,
                            hasSpecialization: function (spzId) {
                                var spz = _.findWhere(this.endorsements, { specializationId: parseInt(spzId) });
                                return (spz == undefined) ? false : true;
                            },
                            getEndorsements: function (spzId) {
                                var _endorsement = _.findWhere(this.endorsements, { specializationId: parseInt(spzId) });
                                return _.template(EndorsementsTpl,
                                    {
                                        model:
                                          {
                                              endorsement: _endorsement,
                                              canEndorse: this.allowToEndorse,
                                              specializationId: spzId,
                                              isCurrentUserEndorsed: _isCurrentUserEndorsed(spzId, _endorsement)
                                          }
                                    });
                            }
                        }
                    }));

                    _sort();
                })
               
            },

            getEndorsements: function(callback){
                var self = this;
                self.endorsementsInfo.url = Config.profile.userEndorsements + '?id=' + self.subscriber.subscriberId + '&type=' + self.subscriber.subscriberType;
                self.endorsementsInfo.fetch({
                    success : function(model, resp){
                       _.bind(callback(model.toJSON()), self);
                    }
                })
            },

            showEndorsers : function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    data = JSON.parse(ele.next().val());
                ele.popover({
                    content: _.template(EndorsersTpl, { model: data }),
                    title: "Endorsers",
                    animation: true,
                    container: 'body',
                    placement: 'top',
                    html:true
                }).popover('show');
            },

            onEndorse: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    parent = ele.closest("li"),
                    spzId = ele.data("specialization-id"),
                    endorserType = app.sessionManager.get('USER').subscriberType,
                    endorserId = app.sessionManager.get('USER').subscriberId,
                    endorseeId = self.subscriber.subscriberId,
                    endorseeType = self.subscriber.subscriberType,

                    specializations = [];
                    specializations.push(spzId);

                var request = {EndorserId: endorserId, EndorserType: endorserType, EndorseeId: endorseeId, EndorseeType: endorseeType, Specializations: specializations};

                var endorseModel = new Model();
                endorseModel.url = Config.profile.endorseSpecialization;
                endorseModel.save(request, {
                    success: function () {
                        $.when(parent.effect("highlight", {}, 1000)).done(function () {
                            setTimeout(function (view) {
                                view.render();
                            }, 100, self);
                        });
                    }
                })
            },

            canEndorse: function(){
                var self = this,
                    searchRole, accountRole;

                var searchRole = self.subscriber.subscriberTypeText.toLowerCase();

                if (self.subscriber.account == true) {
                    return false;
                }

                if (app.isAuthenticated)
                    accountRole = app.sessionManager.get("USER").subscriberTypeText.toLowerCase();
                else {
                    return false;
                }

                switch (searchRole) {
                    case "doctor":
                    case "clinic":
                        if (accountRole == 'doctor' || accountRole == 'clinic')
                            return true;
                        else return false;
                    case "hospital":
                        if (accountRole == 'hospital' || accountRole == 'doctor' || accountRole == 'clinic')
                            return true;
                        else return false;
                    case "college":
                        if (accountRole == 'college' || accountRole == 'doctor' || accountRole == 'clinic')
                            return true;
                        else return false;
                }

                return false;
            },

            onSearchList: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.val().trim(),
                    header = self.$(".spz-list").find(".header"),
                    items = self.$(".spz-list").find(".list-group-item");

                items.show();
                if (val.length > 0) {
                    var itemsToShow = items.find("div.checkbox:contains('" + val + "')").closest(".list-group-item");
                    items.not(itemsToShow).not(header).hide();
                }
            },

            updateSpecialization: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    isChecked = ele.prop("checked"),
                    parent = ele.closest("li"),
                    header = self.$(".spz-list").find(".list-group-item.header"),
                    spzId = ele.val(),
                    userId = self.subscriber.id;
                
                var m = new Model();
                if (isChecked) {
                    //add
                    var request = { specializationId: spzId, subscriberId: userId };
                    m.url = Config.profile.userSpecializations + '?type=' + self.subscriber.subscriberType;
                    m.save(request, {
                        success: function () {
                            parent.insertAfter(header)
                        }
                    });
                }
                else {
                    //destroy
                    m.url = Config.profile.userSpecializations + '/' + spzId + '?type=' + self.subscriber.subscriberType;
                    m.set("id", spzId);
                    m.destroy({
                        success: function () {
                        }
                    });
                }
            }
        });

        return View;

    });