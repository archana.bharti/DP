﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/reviews.html",
        "text!modules/user/templates/hospital-reviews.html",
        "text!modules/user/templates/college-reviews.html",
        "text!modules/user/templates/user-reviews.html",
        "text!modules/user/templates/write-review.html",
        "text!modules/user/templates/my-reviews.html",
        "bootstrap-rating",
        "jqExt"
    ],

    function (Config, Constants, Base, Model, ReviewsTpl, HospitalReviewsTpl, CollegeReviewsTpl, UserReviewsTpl, WriteReviewTpl, MyReviewsTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.parent = obj.view;
                self.subscriber = obj.subscriber;
                self.role = self.subscriber.subscriberType;
                self.accountRole = app.sessionManager.get("USER")  != undefined ? app.sessionManager.get("USER").subscriberType : -1;
                self.$el = obj.container;
                self.reviewsInfo = new Model();
            },

            events : {
                "click .btn-save-reviews": "save"
            },

            render: function () {
                var self = this, tpl, apiUrl;
                var visibility = {
                    showWriteReview:  false,
                    showMyReviews: false,
                    showUserReviews: false
                }

                if (self.subscriber.account == true) {
                    if (parseInt(self.accountRole) == 5) {
                        visibility.showWriteReview = false;
                        visibility.showMyReviews = true;
                    }
                    else {
                        visibility.showUserReviews = true;
                    }
                } else {
                    visibility.showUserReviews = true;
                    if (app.isAuthenticated && self.accountRole == 5) {
                        visibility.showWriteReview =true;
                    }
                }

              
                    self.$el.find('.user-reviews').append(_.template(ReviewsTpl, {
                        model: visibility
                    }));

                    //activate first table
                    var tab = $(".reviews-section > ul > li:first-child");
                    tab.addClass("active");
                    $(tab.find("a").attr('href')).addClass('active').addClass('in')

                    if (visibility.showWriteReview) {
                        self.renderWriteReview();
                    }
                    
                    if (visibility.showUserReviews) {
                        self.renderUserReviews();
                    }

                    if (visibility.showMyReviews) {
                        self.renderMyReviews();
                    }
            },

            renderWriteReview : function(){
                var self = this, m = new Model();
                m.url = Config.review.getParameters + self.role;
                m.fetch({
                    success: function (m, res) {
                        //TODO:m.toJSON() - injecting function objects rather model attributes alone
                        self.writeReviewParameters = res.response.model;
                        var model = {
                            role: self.role,
                            getTitle: function (key) {
                                return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                            },
                            parameters: self.writeReviewParameters
                        }
                        self.$el.find("#write-review").html(
                            _.template(WriteReviewTpl, {
                                model: model
                            }));
                        $(".rating").rating({ showClear: false, showCaption: false });
                    }
                })
            },

            renderUserReviews : function(){
                var self = this;
                var qs = "?id=" + self.subscriber.subscriberId + "&type=" + self.role;
                var m = new Model();
                m.url = Config.review.userReviews + qs;
                m.fetch({
                    success: function (m) {
                        self.$el.find("#user-reviews").html(
                            _.template(UserReviewsTpl, {
                                model: m.toJSON(),
                                getTitle: function (key) {
                                    return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                                },
                                omit: function (r) {
                                    return _.omit(r, "id", "userId", "userName", "subscriberName", "reviewedDate", "clinicOrDoctorId", "hospitalId", "collegeId", "yourExperience");
                                }
                            }));
                        $(".rating").rating({
                            size: 'sm',
                            symbol: "\uf0c8 ",
                            glyphicon: false,
                            ratingClass: "rating-fa", showClear: false, showCaption: false
                        });
                    }
                });
            },

            renderMyReviews : function(){
                var self = this,
                    m = new Model();
                m.url = Config.review.myReviews + app.sessionManager.get("USER").userId;
                m.fetch({
                    success: function (model, resp) {
                        var data = resp.response.model;
                      
                        self.$el.find("#my-reviews").html(
                           _.template(MyReviewsTpl, {
                               model: data,
                               getTitle : function(key){
                                   return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                               },
                               omit: function (r) {
                                   return _.omit(r, "id", "userId", "userName", "subscriberName", "reviewedDate", "clinicOrDoctorId", "hospitalId", "collegeId");//, "yourExperience");
                               }
                           }));
                        $(".rating").rating({ showClear: false, showCaption: false });
                    }
                })
            },

            save: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    request = {},
                    parameters = {},
                    val;

                _.each(self.$el.find("[data-rv-param]"), function (p) {
                    val = parseInt($(p).rating().val());
                    val = (val == 0) ? 1 : val; //default val set to 1
                    parameters[$(p).data("rv-param")] = val;
                });

                switch (self.subscriber.subscriberTypeText.toLowerCase()) {
                    case "clinic":
                    case "doctor":
                        request["clinic"] = parameters;
                        request.clinic.clinicOrDoctorId = self.subscriber.subscriberId;
                        request.clinic.userId = app.sessionManager.get('USER').userId;
                        break;
                    case "hospital":
                        request["hospital"] = parameters;
                        request.hospital.hospitalId = self.subscriber.subscriberId;
                        request.hospital.userId = app.sessionManager.get('USER').userId;
                        break;
                    case "college":
                        request["college"] = parameters;
                        request.college.collegeId = self.subscriber.subscriberId;
                        request.college.userId = app.sessionManager.get('USER').userId;
                }

                request.userType = self.role;
                var reviewModel = new Model();
                reviewModel.url = Config.review.add;
                reviewModel.save(request, {
                    success: function (model, resp) {
                        $(e.currentTarget).addClass('disabled');
                        app.trigger("response:success", "Your review is submitted.");
                    }
                });

                e.stopPropagation();
                return false;
            },

            renderReviews: function(review, index, tpl){
                var self = this;
                self.$el.find('.user-reviews').append(_.template(tpl, {
                    data: review,
                    index: index
                }));
                /*if(index == 0){
                    self.$el.find('.user-reviews').append(_.template(tpl, {
                        reviewedUser: review.userName + "'s Reviews",
                        reviewedUserId: review.userId,
                        reviewItemClass: 'reviewItem'+index
                    }));
                    $('.reviewItem'+index).closest('.ratings-block').find('.strip-bg a').hide();
                    $('.reviewItem'+index).barrating({
                        theme: 'bars-movie',
                        showSelectedRating: false,
                        readonly: true
                    });
                    self.$el.find('.btn-edit-reviews').css('display', 'none');
                    if(self.roleId == "1"){
                        $('.reviewItem'+index+'.cleanliness').barrating('set', review.cleanliness);
                        $('.reviewItem'+index+'.feesCharged').barrating('set', review.feesCharged);
                        $('.reviewItem'+index+'.waitingHours').barrating('set', review.waitingHours);
                        $('.reviewItem'+index+'.listen').barrating('set', review.listen);
                        $('.reviewItem'+index+'.comfort').barrating('set', review.comfort);
                        $('.reviewItem'+index+'.friendlyStaff').barrating('set', review.friendlyStaff);
                        $('.reviewItem'+index+'.customerSatisfaction').barrating('set', review.customerSatisfaction);
                        $('.reviewItem'+index+'.qualityOfService').barrating('set', review.qualityOfService);
                        $('.reviewItem'+index+'.onCall').barrating('set', review.onCall);
                    }
                }*/
            }
        });

        return View;

    });