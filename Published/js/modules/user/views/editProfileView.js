define([
        "apiConfig",
        "modules/doctor/views/base",
        "core/model",
        "text!modules/user/templates/edit-profile.html",
        "bootstrap-dialog"
    ],

    function(Config, 
            Base, 
            Model, 
            EditProfileTpl,
            BootstrapDialog) {

        "use strict";

        var View = Base.extend({

            initialize: function(options) {
                var self = this;
                self.options = options;
                self.renderEditProfile();
            },

            events: {
                "click .btn-cancel-edit-form" : "goToMainProfile"
            },

            renderEditProfile: function() {
                var self = this;
                var user = app.sessionManager.get('USER');
                self.options.el.html(_.template(EditProfileTpl, {
                    username: user.name,
                    email : user.email,
                    contact : user.contact
                }))
            },

            /* Events handler */

            goToMainProfile: function(e){
                $('.my-account-li').trigger('click');
                return false;
            }

            /* Helper methods */

        });

        return View;

    });
