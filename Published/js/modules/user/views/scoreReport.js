﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/category-score-report.html",
        "hcm"
    ],

    function (Config, Constants, Base, Model, ScoreReportTpl, HCM) {

        "use strict";

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.parent = obj.view;
                self.options = obj.options;
                self.$el = obj.container;
                self.scoreModel = new Model();
            },

            events : {

            },

            render: function () {
                var self = this;
                self.scoreModel.url = Config.profile.userScore+ '/' +self.options.id + '?type=' + Constants.USER_ROLES_ID[self.options.type.toUpperCase()];
                self.scoreModel.fetch({
                    success: function (model, resp) {
                        var data = resp.response.model;
                        data = _.omit(data, ['id', 'userId']);
                        self.$el.find('.user-score-report').html(_.template(ScoreReportTpl, {
                            model: data
                        }));
                        var _hcModel;
                        for (var r in data) {
                            _hcModel = {
                                selector: $("#hc_" + r),
                                seriesName: r.capitalizeFirstLetter(),
                                title: r.capitalizeFirstLetter(),
                                value: [
                                    { name: data[r], y: data[r] },
                                    { name: (100 - data[r]), y: (100 - data[r]), dataLabels: { enabled: false, connectorWidth: 0 } }]
                            }
                            HCM.pie(_hcModel.selector, _hcModel);
                        }
                    }
                });
            }
        });

        return View;

    });

//[{
//    name: 'Microsoft Internet Explorer',
//    y: 56.33
//}, {
//    name: 'Chrome',
//    y: 24.03,
//    sliced: true,
//    selected: true
//}, {
//    name: 'Firefox',
//    y: 10.38
//}, {
//    name: 'Safari',
//    y: 4.77
//}, {
//    name: 'Opera',
//    y: 0.91
//}, {
//    name: 'Proprietary or Undetectable',
//    y: 0.2
//}]