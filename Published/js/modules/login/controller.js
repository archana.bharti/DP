﻿define([
	"apiConfig",
    "constantsConfig",
	"core/controller",
	"modules/login/views/login"
], function (
	Config,
    Constants,
	Controller,
	LoginView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {
            
            return this;
        },

       
        events: function (args) {
            return this;
        },

        login: function () {
            var self = this,
                view = new LoginView({ controller: self, el: Constants.MAIN });
                view.clear().render();
        },
    });

    return Controller;

});
