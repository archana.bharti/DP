﻿define([
    "apiConfig",
    "viewBase",
    'core/model',
    "text!modules/login/templates/login.html"],
    function (
        Config,
        ViewBase,
        Model,
        LoginTpl) {

        var view = ViewBase.extend({

            initialize: function () {
                var self = this;
                self.template = _.template(LoginTpl);
                self.$el.html(self.template);
                this.formSetup();
            },

            events: {
                "click .btn-login" : "login"
            },

            render: function () {
                var self = this;
                var user = app.sessionManager.get("USER");
                if (user != null) {
                    self.hideNodes();
                }
            },
        
        });

        return view;

})