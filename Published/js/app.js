﻿define([
     "apiConfig",
     "constantsConfig",
     "bootstrap-dialog",
     "modules/shell/controller"
], function (Config, Constants, BootstrapDialog, ShellController) {


    "use strict"
    var Application = function (Router) {

        this.views = {};
        this.models = {};
        this.controllers = {};
        this.cacheManager = {};
        this.isAuthenticated = false;
        this.router = Router;

        var self = this;
        var cache = {};


        _.extend(this, Backbone.Events);


        // --------------------------------------------------------------------------------------------
        // listeners
        // --------------------------------------------------------------------------------------------
        this.on("start", function (args) { this.start(args); }, this);
        this.on("shell", function (args) { this.shell(args); }, this);
        this.on("shell:complete", function (args) { this.shellComplete(args); }, this);
        this.on("response:error", function (args) { _ajaxResponeError(args); });
        this.on("response:success", function (args, autoClose) { if (autoClose == undefined) { autoClose = false; } _ajaxResponeSuccess(args, autoClose); });
        this.on("warning", function (message, title) { _warning(message, title) });

        this.start = function () {
            app.shell();
            self.checkLogin(function (userModel) {
                if (userModel) {
                    app.isAuthenticated = true;
                    app.sessionManager.set("USER", userModel);
                }
            });
        },

        this.checkLogin = function (callback) {
            $.get(Config.security.isLoggedIn, function (res) {
                if (res == null) {
                    app.isAuthenticated = false;
                    app.router.redirect("home");
                } else {
                    app.sessionManager.set("USER", res.response.model);
                    callback(app.sessionManager.get("USER"));
                    app.router.redirect(Backbone.history.fragment);
                }
            })
        };

        this.shell = function () {

            var self = this,
               name = "shell",
               params = null;

            try {
                if (!self.controllers.hasOwnProperty(name)) {

                    self.controllers[name] = new ShellController();
                    self.controllers[name].setup(name)
                                            .events()
                                            .executeView("shell", params);


                } else {
                    self.controllers[name].executeView(view, params);

                }
            }
            catch (ex) {
                console.log("Failed to load shell");
            }
        };

        this.shellComplete = function () {
            console.log("shell complete");
            var fragment = Backbone.history.fragment;
            fragment = fragment.length == 0 ? "home" : fragment;
            if (!Backbone.History.started) Backbone.history.restart();
            app.router.redirect(fragment);
        };


        // ------------------------------------------------------------------------------------------
        // State Management
        // ------------------------------------------------------------------------------------------

        this.cacheManager = {
            set: function (key, value) {
                cache[key] = value;
            },
            get: function (key) {
                return cache[key];
            }
        };

        this.sessionManager = {
            set: function (key, value) {
                if(typeof value === 'object')
                    sessionStorage.setItem(key, JSON.stringify(value));
                else
                    sessionStorage.setItem(key, value.toString());
            },
            get: function (key) {
                var val;
                try{
                    val = JSON.parse(sessionStorage.getItem(key));
                }
                catch(e){
                    val = sessionStorage.getItem(key);
                }
                return val;
            },
            remove: function (key) {
                sessionStorage.removeItem(key);
            }
        };

        //controller and view mapper
        this.executeController = function (controller, view, options, callback) {

            var self = this,
                name = controller.name.substring(controller.name.indexOf('/') + 1).replace('/', '').toUpperCase();

            try{
                if (!self.controllers.hasOwnProperty(name)) {
                    controller.get().then(function (Ctrl) {
                        self.controllers[name] = new Ctrl();
                        self.controllers[name].setup(name)
                                                .events()
                                                .executeView(view, options);
                        if (callback) callback();
                    });
                } else {
                    self.controllers[name].executeView(view, options);
                    if (callback) callback();
                }
            }
            catch (ex) {
                console.log("Controller Error:" + ex);
            }
        }


        // ------------------------------------------------------------------------------------------
        // Private handler
        // ------------------------------------------------------------------------------------------
        var _ajaxResponeError = function (obj) {

            if (obj != undefined) {

                var type = typeof obj;
                var alert = $("#errorAlert");

                switch (type.toUpperCase()) {

                    case "OBJECT":
                        var res = obj.response;
                        if (res.error != undefined) {
                            alert.find('span.message').text(res.error)
                            alert.fadeIn();
                        }
                        else if (res.error1 != undefined) {
                            alert.find('span.message').text(res.error1 + '.' + (res.error2 || ''));
                            alert.fadeIn();
                        }
                        break;
                    case "STRING":
                        //BootstrapDialog.show({
                        //    type: BootstrapDialog.TYPE_DANGER,
                        //    title: 'Error',
                        //    message: obj
                        //});
                        alert.find('span.message').text(obj)
                        alert.fadeIn();
                        break;
                }
                setTimeout(function () { alert.hide(); }, 3500);
            }
        },

        _ajaxResponeSuccess = function (obj, autoClose) {

            if (obj != undefined) {

                var type = typeof obj;

                switch (type.toUpperCase()) {

                    case "STRING":
                        var instance = BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_SUCCESS,
                            title: 'Success',
                            message: obj
                        });
                        if (autoClose === true) {
                            setTimeout(function () { instance.close() }, Config.UI.SUCCESS_DIALOG_AUTOCLOSE_TIME);

                        }
                        break;
                }

            }
        },
         _warning = function (message, title) {

             BootstrapDialog.show({
                 type: BootstrapDialog.TYPE_WARNING,
                 title: title || 'Warning!',
                 message: message
             });
         }
    }

    return Application;

});