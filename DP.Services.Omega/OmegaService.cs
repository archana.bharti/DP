﻿using DP.Services.Omega.Jobs;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DP.Services.Omega
{
   public class OmegaService
    {
        private Timer _timer = null;
        readonly ILog _log = LogManager.GetLogger(
                                     typeof(OmegaService));

        public OmegaService()
        {
            double interval = double.Parse(System.Configuration.ConfigurationManager.AppSettings["POLLING_INTERVAL"]);
            _timer = new Timer(interval);
            _timer.Elapsed += new ElapsedEventHandler(OnTick);
        }

        protected virtual void OnTick(object sender, ElapsedEventArgs e)
        {
            Run();
        }

        public void Start()
        {
            Run();
            _timer.AutoReset = true;
            _timer.Enabled = true;
            _timer.Start();
            _log.Info("Omega Service Started.");
        }

        public void Stop()
        {
            _timer.AutoReset = false;
            _timer.Enabled = false;
        }

        public void Run()
        {
            Task.Factory.StartNew(() => new AppointmentJob());
            Task.Factory.StartNew(() => new PdaJob());
            Task.Factory.StartNew(() => new OfferJob());
            Task.Factory.StartNew(() => new SubscriptionJob());
        }
    }
}
