﻿using DP.Services.Data;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Omega.Jobs
{
    public class AppointmentJob : BaseJob
    {
        readonly ILog _log = LogManager.GetLogger(typeof(AppointmentJob));

        public AppointmentJob()
        {
            _log.Info("Appointment Job Started at:"+DateTime.Now.ToLongDateString());
            Run();
        }

        private void Run()
        {
            using (base.dbContext = new DPContext())
            {
                
            }
        }
    }
}
