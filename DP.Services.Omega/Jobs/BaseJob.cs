﻿using DP.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Omega.Jobs
{
    public abstract class BaseJob: IDisposable
    {
        protected DPContext dbContext { get; set; }
        protected readonly string ADMIN_EMAIL = System.Configuration.ConfigurationManager.AppSettings["ADMIN_EMAIL"].ToString();
        public void Dispose()
        {
            if (this.dbContext != null)
            {
                this.dbContext.Dispose();
            }
        }
    }
}
