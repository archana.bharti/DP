﻿using DP.Common.Enums;
using DP.Services.Data;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DP.Services.Data.Models;
using DP.Common.Utilities;

namespace DP.Services.Omega.Jobs
{
    public class SubscriptionJob : BaseJob
    {
        readonly ILog _log = LogManager.GetLogger(typeof(SubscriptionJob));

        public SubscriptionJob()
        {
            _log.Info("Subscription Job Started at:" + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));
            Run();
        }

        private void Run()
        {
            var expireList = new List<Feature>();
            using (base.dbContext = new DPContext())
            {
                DateTime dtNow = DateTime.Now;
                var q = dbContext.Features.Where(x => dtNow > x.EndDate && x.StatusValue == (int)OfferStatus.Active);
                foreach(var item in q)
                {
                    item.StatusValue = (int)UserStatus.Deactive;
                    item.EnableBasic = false;
                    item.EnableFeatured = false; //zonal
                    item.EnableTextual = false;
                    expireList.Add(item);
                }
                dbContext.SaveChanges();
                _log.Info("Subscription Job Ended at:" + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));
                if (expireList.Count > 0)
                {
                    _log.Debug("Subscription Job Expired Id List:" + string.Join(",", expireList.Select(x => x.ToString()).ToList<string>()));
                    SendEmail(expireList);
                }
            }
        }

        private void SendEmail(List<Feature> expireList)
        {
            if (expireList.Count > 0)
            {
                var table = new List<string>();
                table.Add(@"<table>
                                <thead> 
                                    <tr> 
                                        <th>FeatureID</th> 
                                        <th>EntityID</th> 
                                        <th>EntityType</th> 
                                    </tr>
                                </thead>
                                <tbody>");

                foreach (var item in expireList)
                {
                    table.Add(@"<tr><td>" + item.Id.ToString() + "</td><td>" + item.EntityId.ToString() + "</td><td>" + EnumHelper<UserType>.GetDescription((UserType)item.EntityType) + "</td></tr>");
                }

                table.Add("<table></tbody>");
                _log.Debug("Sending Feature Expiry Email to " + ADMIN_EMAIL);
                Task.Run(() =>
                {
                    try
                    {
                        var status = EmailManager.SendExpiryOfPDAToAdmin(ADMIN_EMAIL, string.Join("", table)); _log.Debug(status.ToString());
                    }
                    catch (Exception ex) { _log.Error("Feature Expiry email sending failed:" + ex.Message); }
                });

            }
        }
    }
}
