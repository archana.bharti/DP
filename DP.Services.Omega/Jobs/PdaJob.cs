﻿using DP.Common.Enums;
using DP.Services.Data;
using DP.Services.Data.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DP.Common.Utilities;

namespace DP.Services.Omega.Jobs
{
    public class PdaJob : BaseJob
    {
        readonly ILog _log = LogManager.GetLogger(
                                     typeof(PdaJob));
        
        public PdaJob()
        {
            _log.Info("PDA Job Started at:" + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));
            Run();
        }

        private void Run()
        {
            var expireList = new List<PdaSubscription>();
            using (base.dbContext = new DPContext())
            {
                DateTime dtNow = DateTime.Now;
                var q = dbContext.PdaSubscriptions.Where(x => dtNow > x.EndDate && x.StatusValue == (int)OfferStatus.Active).Include(x=>x.DoctorOrClinic);
                foreach (var item in q)
                {
                    item.StatusValue = (int)PdaSubscriptionStatus.Deactive;
                    expireList.Add(item);
                }
                dbContext.SaveChanges();
            }
            _log.Info("PDA Job Ended at:" + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));
            if (expireList.Count > 0)
            {
                _log.Debug("PDA Job Expired Id List:" + string.Join(",", expireList.Select(x => x.ToString()).ToList<string>()));
                SendEmail(expireList);
            }
        }

        private void SendEmail(List<PdaSubscription> expireList)
        {
            if (expireList.Count > 0)
            {
                var table = new List<string>();
                table.Add(@"<table>
                                <thead> 
                                    <tr> 
                                        <th>PdaSubscriptionID</th> 
                                        <th>DoctorOrClinicID</th> 
                                        <th>Name</th> 
                                    </tr>
                                </thead>
                                <tbody>");

                foreach (var item in expireList)
                {
                    table.Add(@"<tr><td>" + item.Id.ToString() + "</td><td>" + item.DoctorOrClinicId.ToString() + "</td><td>" + item.DoctorOrClinic.Name + "</td></tr>");
                }

                table.Add("<table></tbody>");
                _log.Debug("Sending PDA Expiry to " + ADMIN_EMAIL);
                Task.Run(() =>
                {
                    try
                    {
                        var status = EmailManager.SendExpiryOfPDAToAdmin(ADMIN_EMAIL, string.Join("", table)); _log.Debug(status.ToString());
                    }
                    catch (Exception ex) { _log.Error("PDA Expiry email sending failed:" + ex.Message); }
                });

            }
        }
    }
}
