﻿using DP.Common.Enums;
using DP.Common.Utilities;
using DP.Services.Data;
using DP.Services.Data.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Omega.Jobs
{
    public class OfferJob : BaseJob
    {
        readonly ILog _log = LogManager.GetLogger(
                                    typeof(OfferJob));


        public OfferJob()
        {
            _log.Info("Offer Job Started at:" + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));
            Run();
        }

        private void Run()
        {
            var expireList = new List<OfferMaster>();
            using (base.dbContext = new DPContext())
            {
                DateTime dtNow = DateTime.Now;
                var q = dbContext.OfferMasters.Where(x => dtNow > x.EndDateTime && x.OfferStatusValue == (int)OfferStatus.Active);
                foreach (var item in q)
                {
                    item.OfferStatusValue = (int)OfferStatus.Deactive;
                    expireList.Add(item);
                }
                dbContext.SaveChanges();
            }

            _log.Info("Offer Job Ended at:" + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));
            if (expireList.Count > 0)
            {
                _log.Debug("Offer Job Expired Id List:" + string.Join(",", expireList.Select(x => x.ToString()).ToList<string>()));
                SendEmail(expireList);
            }
        }

        private void SendEmail(List<OfferMaster> expireList)
        {
            if (expireList.Count > 0)
            {
                var table = new List<string>();
                table.Add(@"<table>
                                <thead> 
                                    <tr> 
                                        <th>ID</th> 
                                        <th>ProviderID</th> 
                                        <th>ProviderType</th> 
                                        <th>ProviderEmail</th> 
                                    </tr>
                                </thead>
                                <tbody>");

                foreach (var item in expireList)
                {
                    table.Add(@"<tr><td>" + item.Id.ToString() + "</td><td>" + item.ProviderId.ToString() + "</td><td>" + EnumHelper<UserType>.GetDescription((UserType)item.ProviderType) + "</td><td>" + item.ProviderEmail + "</td></tr>");
                }

                table.Add("<table></tbody>");
                _log.Debug("Sending Offers Expiry to " + ADMIN_EMAIL);
                Task.Run(() =>
                {
                    try
                    {
                        var status = EmailManager.SendExpiryOfOffersToAdmin(ADMIN_EMAIL, string.Join("", table)); _log.Debug(status.ToString());
                    }
                    catch (Exception ex) { _log.Error("Offers expiry email sending failed:" + ex.Message); }
                });
               
            }
        }
    }
}
