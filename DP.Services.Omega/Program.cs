﻿using log4net.Config;
using System;
using System.IO;
using Topshelf;

namespace DP.Services.Omega
{
    class Program
    {
        static void Main(string[] args)
        {
                HostFactory.Run(hostConfigurator =>
                {
                    hostConfigurator.Service<OmegaService>(serviceConfigurator =>
                    {
                        serviceConfigurator.ConstructUsing(() => new OmegaService());
                        serviceConfigurator.WhenStarted(omegaSvc =>
                        {
                        // XmlConfigurator.ConfigureAndWatch(new FileInfo(".\\log4net.config"));
                        omegaSvc.Start();
                        });
                        serviceConfigurator.WhenStopped(omegaSvc => omegaSvc.Stop());
                    });

                    log4net.Config.XmlConfigurator.Configure();
                    hostConfigurator.UseLog4Net();
                    hostConfigurator.RunAsLocalSystem();

                    hostConfigurator.SetDisplayName("DoctorPursuit Omega");
                    hostConfigurator.SetDescription("DPOmega monitors expiration time of pda, offer, appointment");
                    hostConfigurator.SetServiceName("DpOmegaSvc");

                    hostConfigurator.StartAutomatically();
                    hostConfigurator.EnableServiceRecovery(rc =>
                    {
                        rc.RestartService(1);
                    });
                });
        }
    }
}
