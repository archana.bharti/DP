define([], function() {

    var config = {},
        baseUrl = "",
        api = "",
        fixtures = "",
        autoPlay = false;
        
   // api = "http://www.staging.doctorpursuit.com/api/";
    //beta

   api = "http://doctorpursuit.com/api/";

    //int
  // api = "http://dp-dev.gaween.com/api/";


    //local
  //api = "http://localhost:20921/api/";

    //azurr
    //api = "http://103.14.127.78:88/api/";
    

 
    /***************************************************
                SECURITY
    ****************************************************/

    config.security = {
        login: api + "auth/Login",
        isLoggedIn: api + "auth/isLoggedIn",
        logout: api + "auth/logout",
        forgotPassword: api + "auth/forgotPassword?email=",
        changePassword: api + "auth/changePassword"
    }


    /***************************************************
                ENUMS/COMMON
    ****************************************************/

    config.enums = {
        roles: api + "common/getRoles",
        states: api + "common/getStates",
        cities: api + "common/getCities",
        locations: api + "common/getLocations",
        specializations: api + "common/getSpecializations",
        collegeCities: api + "common/getCollegeCities",
        stdCodes: api + "common/getStdCodes"
    }

    /***************************************************
     SEARCH MODULES
     ****************************************************/

    config.search = {
        getResults: api + "search",
        zone: api + "search/zone",
        zoneAll : api + "search/zoneDashboard",
        college: api + "search/college",
        insurance: api + "insurance/search",
        collegeFeatured: api + "search/collegeFeatured",
        ConsolatedSearch: api + 'insurance/Consolatedsearch',
    }

    /***************************************************
     GOOGLE MAPS
     ****************************************************/

    config.gmaps = {
        key: 'AIzaSyA26KarT8wTUqmIsc_6qi2H5Q2gZbCeS9g'
    }

    /***************************************************
                DASHBOARD
    ****************************************************/

    config.dashboard = {
        contactus : api + "contactus"
    }

    /***************************************************
               USER
   ****************************************************/

    config.user = {
        create: api + "user",
        suggest: api + "user/userFinder",
        polls: api + "user/polls",
        addPoll: api + "user/addPoll",
        pollStats : api + "user/pollstats"
    }

    /***************************************************
               PROFILE
   ****************************************************/

    config.profile = {
        userProfile: api + "userProfile?uId=",
        update: api + "userProfile?uid=",
        updateSummary: api + "userProfile/updateSummary?uid=",
        userScore: api + "Score",
        userSpecializations: api + "Specialization",
        userEndorsements: api + "userprofile/endorsements",
        endorseSpecialization: api + 'endorsement',
        bookmarkProfile: api + "bookmark",
        topRated: api + "review/getTopRated?entityId={0}&entityType={1}",
        uploadImage: api + "userProfile/uploadImage",
        qualifications: {
            allLongShort: api + "userProfile/getUnifiedQualifications",
            allUserLongShort: api + "userProfile/GetUnifiedUserQualifications?userId=",
            addLong: api + "userProfile/addlongQualification",
            addShort: api + "userProfile/addShortQualification",
            deleteLong: api + "userProfile/deleteLongQualification",
            deleteShort: api + "userProfile/deleteShortQualification",
            addQualif: api + "userProfile/addQualification",
            removeQualif: api + "userProfile/removeQualification"
        }
    }

    /***************************************************
     PDA
     ****************************************************/

    config.pda = {
        advice: api + 'pda',
        get : api + "pda",
        answer: api + "pda/answer",
        specializations: api + "pda/Specializations"
    }

    /***************************************************
                RECOMMEND
  ****************************************************/
    config.recommend = {
        addOrUpdate: api + "recommend"
    }

  /***************************************************
                REPORTS
  ****************************************************/
    config.report = {
        getFeaturedReport: api + "report/getFeaturedReport"
    }

    /***************************************************
            REVIEW
    ****************************************************/
    config.review = {
        getParameters: api + "review/GetRatingParameters?type=",
        add: api + "review",
        update: api + "review",
        userReviews: api + "review/getUserReviews",
        myReviews: api + "userProfile/myReviews?uid="
    }

    /***************************************************
           OFFERS
   ****************************************************/

    config.offer = {
        get: api + "offer",
        getById: api + "offer",
        claim: api + "offer/claim",
        myOffers: api + "offer/myOffers",
    }

    /***************************************************
          APPOINTMENT
  ****************************************************/

    config.appointment = {
        save: api + "appointment",
        checkAvailability: api + "appointment/checkAvailability"
    }

    /***************************************************
         INSURANCE
 ****************************************************/
    config.insurance = {
        get: api + 'insurance/',
        featured: api + 'insurance/GetFeatured',
        update: api + 'insurance',
        updateSummary: api + 'insurance/updateSummary',
        getInsuraceType: api + 'insurance/InsuranceTypeFinder',
    }

    return config;
});
