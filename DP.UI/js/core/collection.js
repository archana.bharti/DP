define(['backbone','apiConfig'], function(Backbone,Config) {
    "use strict";
    var Collection = Backbone.Collection.extend({

        initialize: function (options) {
            if(this.url == undefined || this.url.trim().length ==0 )
            this.url = (options != undefined) ? options.url : null;
        },
        //parse: function (response, options) {
        //    return response.response.model;
        //}
        parse: function (obj) {

            var self = this;
            $.each(obj.response.model, function () {
                self.add(this)
            });
            return self.models;
        }
    });

    return Collection;

});
