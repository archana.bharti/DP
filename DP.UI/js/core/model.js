define(["jquery", "backbone", "apiConfig"], function ($, Backbone) {

    var Model = Backbone.Model.extend({

        initialize: function (options) {
            if (this.url == undefined || (typeof(this.url) == "string" && this.url.trim().length == 0))
                this.url = (options != undefined) ? options.url : null;
        },

        sync: function (method, model, options) {

            var params = {};

            switch (method) {
                case "create":
                    params = this._create(model, options);
                    break;
                case "read":
                    params = this._read(model, options);
                    break;
                case "update":
                    params = this._update(model, options);
                    break;
                case "delete":
                    params = this._delete(model, options);
                    break;
            }

            return params;
        },

        _create: function (model, options) {

            var params = _.extend({

                headers: { Accept: "*/*; charset=utf-8" },
                type: 'POST',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(model.attributes),
                url: model.url,
                processData: false,
                crossDomain: true

            }, options);

            return $.ajax(params);

        },

        _read: function (model, options) {

            if (model.attributes.id != null) {
                model.url = model.url + '/' + model.id;
            }

            var params = _.extend({

                headers: { Accept: "*/*; charset=utf-8" },
                type: 'GET',
                dataType: 'json',
                url: model.url,
                processData: false,
                crossDomain: true

            }, options);

            return $.ajax(params);

        },

        _update: function (model, options) {

            //if (model.attributes.id != null) {
            //    model.url = model.url + '/' + model.id;
            //}

            var params = _.extend({

                headers: { Accept: "*/*; charset=utf-8" },
                type: 'PUT',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(model.attributes),
                url: model.url,
                processData: false,
                crossDomain: true

            }, options);

            return $.ajax(params);

        },

        _delete: function (model, options) {

            var params = _.extend({

                headers: { Accept: "*/*; charset=utf-8" },
                type: 'DELETE',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(model.attributes),
                url: model.url,
                processData: false,
                crossDomain: true

            }, options);

            return $.ajax(params);

        },

        parse: function (obj) {

            if (obj.isJqueryDataGrid) {
                // throw ' Failed to parse model.Jquery Data Grid not compatible.';
                delete obj['isJqueryDataGrid'];
                return obj;
            } else {
                var self = this;

                //for each model in collection, calls this base parse and fails at .reponse.model. hence the check
                if (obj.response && obj.response.model) {
                    var _m = obj.response.model;
                    $.extend(true, this.attributes, _m);
                    return _m;
                }
            }
        },

        wrap: function (model) {

            var req = {
                request: {}
            }

            for (var a in model) {
                req.request[a] = model[a];
            }

            return req;

        },

        validate: function (attrs) {

        },

        postForm: function (callback) {

            this.save(null, {
                success: function (model, data, options) {
                    callback({ success: true, message: data.response.message, model: data.response });
                },
                error: function (model, data, options) {
                    app.trigger("error", data.statusText);
                }
            });

        },

        mapQueryString: function (paramsArray)
        {
            var self = this;
            var url = self.url;
            for (var i = 0; i <= paramsArray.length - 1; i++) {
                url = url.replace('{' + i + '}', paramsArray[i]);
            }
            self.url = url;
        }

    }, {

        // instance vars?

    });

    return Model;

});
