﻿define([
    "apiConfig",
    "viewBase",
    "text!modules/dashboard/templates/thankyou.html",
     "bootstrap-dialog",
    "bootstrapValidator"],
    function (Config, Base, thankyouTpl, BootstrapDialog) {

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.isInitialSearchLoaded = false;
                this.template = _.template(thankyouTpl);
            },

           
            render: function () {
                
                console.log("inside here render", thankyouTpl);
                var self = this;
                self.$el.html(_.template(thankyouTpl));
                self.isInitialSearchLoaded = false;
                
             
            },

         
        });

        return View;
    });