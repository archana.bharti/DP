﻿define([
    "apiConfig",
    "viewBase",
    "text!modules/dashboard/templates/banner.html",
     "bootstrap-dialog",
    "bootstrapValidator"],
    function (Config, Base, bannerTpl, BootstrapDialog) {

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.isInitialSearchLoaded = false;
                this.template = _.template(bannerTpl);
            },

           
            render: function () {
                
                console.log("inside here render", bannerTpl);
                var self = this;
                self.$el.html(_.template(bannerTpl));
                self.isInitialSearchLoaded = false;
                
             
            },

         
        });

        return View;
    });