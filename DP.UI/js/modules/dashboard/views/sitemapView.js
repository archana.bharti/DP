﻿define([
    "apiConfig",
    "viewBase",
    "text!modules/dashboard/templates/sitemap.html",
     "bootstrap-dialog",
    "bootstrapValidator"],
    function (Config, Base, sitemapTpl, BootstrapDialog) {

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.isInitialSearchLoaded = false;
                this.template = _.template(sitemapTpl);
            },

           
            render: function () {
                
                console.log("inside here render", sitemapTpl);
                var self = this;
                self.$el.html(_.template(sitemapTpl));
                self.isInitialSearchLoaded = false;
                
             
            },

         
        });

        return View;
    });