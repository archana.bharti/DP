define([
        "apiConfig",
        "viewBase"
    ],
    function (
        Config,
        Base
    ) {

        var View = Base.extend({
            formSetup: function (form) {
                var self = this;
                form.bootstrapValidator({
                    live: 'disabled',
                    submitButtons: 'button[type="submit"]',
                    fields: {
                        name: {
                            validators: {
                                notEmpty: {
                                    message: 'Please enter your user name'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'Password should not be empty.'
                                },
                                stringLength: {
                                    min: 4,
                                    message: 'Password must be of 4 or more characters'
                                },
                                identical: {
                                    field: 'confirmuserpassword',
                                    message: 'The password and its confirm are not the same'
                                }
                            }

                        },
                        confirmuserpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'Confirm password for security'
                                },
                                identical: {
                                    field: 'userpassword',
                                    message: 'The password and its confirm are not the same'
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'Email should not be empty.'
                                },
                                emailAddress: {
                                    message: 'The value is not a valid email address'
                                }
                            }
                        },
                        contact: {
                            validators: {
                                notEmpty: {
                                    message: 'Please provide your contact number.'
                                }
                            }
                        }
                    }
                })
            },

            validateForm: function (form) {
                form.data("bootstrapValidator").validate();
                return form.data("bootstrapValidator").isValid();
            },

            resetForm: function (form) {
                form.data("bootstrapValidator").resetForm();
            }
        });

        return View;
    });