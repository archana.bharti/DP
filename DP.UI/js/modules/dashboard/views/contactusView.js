﻿define([
    "apiConfig",
    "viewBase",
    "text!modules/dashboard/templates/contactus.html",
     "bootstrap-dialog",
    "bootstrapValidator"],
    function (Config, Base, ContactusTpl, BootstrapDialog) {

        var View = Base.extend({

            initialize: function () {
                this.template = _.template(ContactusTpl);
            },

            events: {
                "click .btn-submit": "send",
                "click .img-reason" : "toggleReason"
            },

            render: function () {
                var self = this;
                    self.form = {
                        selector: function () { return $(".form-contactus") },
                        setup: function () {
                            this.selector().bootstrapValidator({
                                //live: 'disabled',
                                fields: {
                                    name: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Please enter name'
                                            }
                                        }
                                    },
                                    message: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Please enter message'
                                            }
                                        }
                                    },
                                    email: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Please enter email'
                                            },
                                            emailAddress: {
                                                message: 'Not a valid email address'
                                            }
                                        }
                                    },
                                    mobile: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Phone number should not be empty.'
                                            },
                                            callback: {
                                                message: 'Not a valid phone number',
                                                callback: function (value, validator, $field) {

                                                    if ($.trim(value).length == 0) return true;

                                                    return /^\d{10}$/.test(value) == true;
                                                }
                                            }
                                        }
                                    }
                                }
                            })
                        },
                        validate: function () {
                            this.selector().data("bootstrapValidator").validate();
                            return this.selector().data("bootstrapValidator").isValid();
                        },
                        reset: function () {
                            this.selector().data("bootstrapValidator").resetForm();
                        },
                        getData: function () {
                           return self.serializeForm(this.selector())
                        }
                    }
                self.$el.html(self.template({}));
                self.form.setup();
            },

            toggleReason: function (e) {
                $(".img-reason.selected").removeClass("selected");
                $(e.currentTarget).addClass("selected");
                var reason = $(e.currentTarget).data("reason").toString();
                var txt = $("#txt-reason");
                switch (reason) {
                    case "1": txt.text('To be Featured'); break;
                    case "2": txt.text('To be Listed'); break;
                    case "3": txt.text('Info not Correct'); break;
                    case "4": txt.text('To Place an Offer'); break;
                    case "5": txt.text('General Queries'); break;
                    case "6": txt.text('Offer Renewal'); break;
                    case "7": txt.text('Careers'); break;
                }
            },

            send: function (e) {
                var ele = $(e.currentTarget),
                    self = this;
                if (self.form.validate()) {
                    var data = self.form.getData();
                    data.reason = self.$('i.img-reason.selected').attr("data-reason");
                    $.post(Config.dashboard.contactus, data).success(function () {
                        //BootstrapDialog.show({
                        //    title: 'Thank you!',
                        //    message: 'Your query will be resolved in 7 working days.'//'A activation link has been sent to your registered email id. Please click on the link to activate your account.'
                        //})
                        //app.router.redirect("home");
                        //  app.router.redirect("thankyou");
                       // console.log("data>>>>",data);
                      app.router.navigate('thank_you', true);
                    })
                }
            }
        });

        return View;
    });