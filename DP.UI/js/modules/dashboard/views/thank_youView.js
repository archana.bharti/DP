﻿define([
    "apiConfig",
    "viewBase",
    "text!modules/dashboard/templates/thank_you.html",
     "bootstrap-dialog",
    "bootstrapValidator"],
    function (Config, Base, thank_youTpl, BootstrapDialog) {

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.isInitialSearchLoaded = false;
                this.template = _.template(thank_youTpl);
            },

           
            render: function () {
                
                console.log("inside here render", thank_youTpl);
                var self = this;
                self.$el.html(_.template(thank_youTpl));
                self.isInitialSearchLoaded = false;
                
             
            },

         
        });

        return View;
    });