﻿
define([
    "apiConfig",
    "modules/dashboard/views/base",
    'core/model',
    'modules/shared/collections/states',
    'modules/shared/collections/specializations',
    'modules/shared/collections/cities',
    'modules/shared/collections/collegeCities',
    'modules/shared/collections/locations',
    "text!modules/dashboard/templates/dashboard.html",
    "text!modules/dashboard/templates/features-carousel-item.html",
   "text!modules/dashboard/templates/hospital.html",
    "bootstrap-dialog",
    "bootstrapSelect",
    "owl-carousel",
    "jqExt"
],

    function (Config, Base, Model, StatesColl, SpecializationsColl, CitiesColl, CollegeCitiesColl, LocationsColl, WelcomeTpl, FeaturesCarouselItemTpl, hospitalTpl, BootstrapDialog) {

        "use strict";

        var View = Base.extend({

            initialize: function (options) {
                var self = this;
                self.specializationsModel = new Model();
                self.specializationsModel.url = Config.enums.specializations;

                self.citiesModel = new Model();
                self.citiesModel.url = Config.enums.cities;

                self.statesModel = new Model();
                self.statesModel.url = Config.enums.states;
                this.template = _.template(hospitalTpl);
                if (app.sessionManager.get("DASHBOARD-DDL-LIST") == null) {
                    $.when(
                        self.specializationsModel.fetch(),
                        self.citiesModel.fetch(),
                        self.statesModel.fetch()
                        ).done(function () {

                            self.dashboardDdlList = {
                                cities: self.citiesModel.toJSON(),
                                states: self.statesModel.toJSON(),
                                hospitalSpzs: self.specializationsModel.toJSON().hospital,
                                specializations: self.specializationsModel.toJSON()
                            }
                            app.sessionManager.set("DASHBOARD-DDL-LIST", self.dashboardDdlList)
                            self.render();
                        })
                } else {
                    self.dashboardDdlList = app.sessionManager.get("DASHBOARD-DDL-LIST");

                    self.render();
                    self.initSpecializations(self.$('#doctor-clinic-search-form input:radio').first());
                    self.$('#doctor-clinic-search-form input:radio').first().click();
                }
            },

            events: {
                "click .btn-search": "search",
                "click .search-nav li": "onTabClick",
                "click #doctor-clinic-search-form input:radio": "initSpecializations",
                "change [data-city-location]": "loadLocations",
                "change [data-state-city]": "loadCitiesByState",
                "change #state-college": "loadCollegeCitiesByState",
                "click [data-zonal-item]": "redirect",
                "click input.radio-college-dental": "clearNameSearch",
                "click input.radio-college-medical": "clearNameSearch",
                "click input.radio-doc-medical": "clearNameSearch",
                "click input.radio-doc-dental": "clearNameSearch",
                "click input.radio-doc-altmed": "clearNameSearch",
            },

            render: function () {
                var self = this;

                self.$el.html(_.template(hospitalTpl, {
                    cities: self.dashboardDdlList.cities,
                    hospitalSpzs: self.dashboardDdlList.hospitalSpzs,
                    states: self.dashboardDdlList.states
                }));

                $('#spz-doctor-clinic, #spz-hospital').select2({
                    style: "width:100%"
                });

                self.$('#doctor-clinic-search-form input:radio').first().click();
                self.currentNavId = $('.search-nav li.active').data('panel');

                setTimeout(function () { self.initializeNameSelect() }, 250);
                self.initializeFormValidations(self.currentNavId)
                    .loadFeatures();
                $(".dp-footer").fadeIn(500);
            },

            initSpecializations: function (e) {
                var self = this,
                    ele = e.currentTarget != undefined ? $(e.currentTarget) : $(e),
                    val = ele.val(),
                    spzs = self.dashboardDdlList.specializations, // self.specializationsModel.toJSON(),
                    spzDdl = self.$('#spz-doctor-clinic');
                spzDdl.find("option").remove();
                spzDdl.append($("<option/>").val("null").text('Select Specialization'));

                switch (val.toString()) {
                    case "1":
                        $.each(spzs.medical, function (i, o) {
                            spzDdl.append($("<option/>").val(o.id).text(o.name));
                        })
                        break;
                    case "2":
                        $.each(spzs.dental, function (i, o) {
                            spzDdl.append($("<option/>").val(o.id).text(o.name));
                        })
                        break;
                    case "3":
                        $.each(spzs.alternativeMedicine, function (i, o) {
                            spzDdl.append($("<option/>").val(o.id).text(o.name));
                        })
                }

                spzDdl.select2().val("null").trigger("change");
            },

            loadLocations: function (e) {

                var self = this,
                    ele = $(e.currentTarget),
                    cityId = ele.val(),
                    target = $(ele.data("cityLocation"));

                if (cityId == "null") {
                    target.find("option").remove();
                    target.attr("disabled", "disabled").append($("<option/>").val("null").text('Select Location'));
                    return;
                }

                if (cityId == 3 || cityId == 4) {
                    target.find("option").remove();
                    target.attr("disabled", "disabled").append($("<option/>").val("null").text('No locations found'));
                    return;
                }

                new LocationsColl([], { id: cityId }).fetch({
                    success: function (m) {
                        var data = [];
                        data.push({ id: "null", text: 'Location' });
                        $.each(m.models, function (index, item) { // Iterates through a collection
                            data.push({ id: item.attributes.value, text: item.attributes.text });
                        });

                        if (m.model.length == 0) target.children().remove();
                        target.select2({
                            disabled: m.models.length == 0 ? true : false,
                            data: m.models.length == 0 ? null : data,
                            placeholder: "No locations found"
                        })


                        // ele.select2("open");
                    }
                });

                target.removeAttr("disabled")
            },

            /* Events handler */

            search: function (e) {

                var completeArray = [];
                var HospitalcompleteArray = [];
                var CollagecompleteArray = [];
                var self = this;
                var values = self.serializeForm($('#' + self.currentNavId + ' form'));

                values.SearchType = $('#' + self.currentNavId).data('searchtype');

                /////////     Doctor Clinic Tab   ////////////

                var allNames = $("#doctor-clinic-search-form .select2-selection__rendered");
                var doctorName = $("option", allNames).text();
                var locationId = $("#doctor-clinic-search-form .city-selection").val();
                var locationName = $("#doctor-clinic-search-form .city-selection option[value='" + locationId + "']").text();
                $.each(allNames, function (nameindex, name) {
                    completeArray.push($(name).text());
                });
                completeArray[0] = completeArray[0].substr(1);
                console.log("1>>>>>>", completeArray[0], "2>>>>", completeArray[1], "3>>>>", completeArray[2], "3>>>>", locationName);


                ///////////////     Hospital Tab    ///////////////////////////////

                var allHospitalNames = $("#hospital-search-form .select2-selection__rendered");
                var HospitalName = $("option", allHospitalNames).text();
                var HoslocationId = $("#hospital-search-form .city-selection").val();
                var HoslocationName = $("#hospital-search-form .city-selection option[value='" + HoslocationId + "']").text();

                $.each(allHospitalNames, function (nameindex, name) {
                    HospitalcompleteArray.push($(name).text());
                });
                HospitalcompleteArray[0] = HospitalcompleteArray[0].substr(1);
                console.log("11>>>>>>", HospitalcompleteArray[0], "12>>>>", HospitalcompleteArray[1], "13>>>>", HospitalcompleteArray[2], "131>>>>", HoslocationName);







                ///////////////     Collage  Tab    ///////////////////////////////

                var allCollageNames = $("#college-search-form .select2-selection__rendered");
                var CollageName = $("option", allCollageNames).text();
                var CollageStateId = $("#college-search-form .stateSelection").val();
                var CollageStateName = $("#college-search-form .stateSelection option[value='" + CollageStateId + "']").text();

                $.each(allCollageNames, function (nameindex, name) {
                    CollagecompleteArray.push($(name).text());
                });
                CollagecompleteArray[0] = CollagecompleteArray[0].substr(1);
                console.log("111>>>>>>", CollagecompleteArray[0], "131>>>>", CollageStateName);



                var FinalVlaues = {};






                var value1 = $("#select2-DoctorOrClinicId-to-container option").text();

                if (values.SearchType == 3) {
                    values.DepartmentId = $('input[name=optradio]:checked', '#' + self.currentNavId).val();
                    values.cityId = $('#city-college').val();
                } else {
                    values.DoctorOrClinicCategoryType = $('input[name=optradio]:checked', '#' + self.currentNavId).val()
                }

                if (values.SpecializationId == 'Select Specialization') {

                    values.SpecializationId = null;
                }
                if (values.locationId == 'Select Location') {
                    values.locationId = null;
                }
                values.ip = window.IP;
                //localStorage.clear();
                console.log("search params", values);

                localStorage.setItem("searchParams", JSON.stringify(values));

                Backbone.history.fromURL = "home";
                console.log("completeArray>>>>", completeArray);

                var routeParams = [];
                completeArray[0] !== 'Name Search' && completeArray[0] !== "ame Search" ? routeParams.push(completeArray[0].replace(' ', '')) : null;
                completeArray[1] !== 'Select Specialization' ? routeParams.push(completeArray[1].replace(' ', '')) : null;
                completeArray[2] !== undefined && completeArray[2] !== "Location" && completeArray[2] !== "No locations found"  ? routeParams.push(completeArray[2].replace(' ', '')) : null;
                locationName !== 'Metro City' ? routeParams.push(locationName.replace(' ', '')) : null;




                CollagecompleteArray[0] !== "null" && values.CollegeId !== undefined ? routeParams.push(CollagecompleteArray[0]) : null;
                CollageStateName !== "null" && values.stateId !== undefined ? routeParams.push(CollageStateName) : null;

                HospitalcompleteArray[0] !== 'Name Search' && completeArray[0] !== "ame Search" ? routeParams.push(HospitalcompleteArray[0].replace(' ', '')) : null;
                HospitalcompleteArray[1] !== 'Select Specialization' ? routeParams.push(HospitalcompleteArray[1].replace(' ', '')) : null;
                HospitalcompleteArray[2] !== undefined && HospitalcompleteArray[2] !== "Location" && HospitalcompleteArray[2] !== "No locations found" ? routeParams.push(HospitalcompleteArray[2].replace(' ', '')) : null;
                HoslocationName !== 'Metro City' ? routeParams.push(HoslocationName.replace(' ', '')) : null;


                if (values.SearchType == 1) {
                    var FinalVlaues = {
                        "Name": completeArray[0],
                        "Specialization": completeArray[1],
                        "City": locationName,
                        "Location": completeArray[2] !== undefined ? completeArray[2] : null,
                        "SearchType": values.SearchType

                    };
                }
                if (values.SearchType == 2) {
                    var FinalVlaues = {
                        "Name": HospitalcompleteArray[0],
                        "Specialization": HospitalcompleteArray[1],
                        "City": HoslocationName,
                        "Location": HospitalcompleteArray[2] !== undefined ? HospitalcompleteArray[2] : null,
                        "SearchType": values.SearchType
                    };
                }

                if (values.SearchType == 3) {
                    var FinalVlaues = {
                        "Name": CollagecompleteArray[0],
                        "Specialization": null,
                        "City": null,
                        "Location": CollageStateName !== "State" ? CollageStateName : null,
                        "SearchType": values.SearchType

                    };
                }
                localStorage.setItem("searchParamsValues", JSON.stringify(FinalVlaues));

                if (routeParams.length !== 0) app.router.navigate('search/' + routeParams.join('/'), true);
                else app.router.navigate('search/results', true);
                return false;
            },

            onTabClick: function (e) {
                var self = this;
                self.currentNavId = $(e.currentTarget).data('panel');
                //var Id = self.currentNavId.substr(0, self.currentNavId.search('-'));
                //$("> div", $("#major")).removeClass("hide").hide();
                //$("#" + Id).show();
                ////$("#doctor").hide();
                //app.router.navigate('/' + self.currentNavId.substr(0, self.currentNavId.search('-')), true);


                self.initializeFormValidations(self.currentNavId);
                return self;
            },

            /* Helper methods */

            initializeNameSelect: function () {
                var self = this;
                try {

                    var activePanel = $("#" + self.currentNavId);
                    var _formatUser = function (user) {
                        if (user.loading) return user.text;
                        var $user = $(
                            '<div id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</div>'
                        );
                        return $user;
                    }

                    var _selection = function (user) {

                        if (user.name == undefined) {
                            return '<option value="null" selected="selected">Name Search</option>';
                        } else {
                            user.name = user.name.replace('Dr.', '');
                        }

                        return '<div id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</div>'
                    }
                    self.$(".suggest-ddl").select2({
                        allowClear: true,
                        placeholder: "Name",

                        ajax: {
                            url: Config.user.suggest,
                            dataType: 'json',
                            type: "POST",
                            delay: 250,
                            data: function (params) {

                                var userType = parseInt($('#' + self.currentNavId).data('usertype'));
                                //for doctor and clinic the suggestion should get both merged types as DoctorsAndClinics(6)
                                if (userType == 1 || userType == 2) { userType = -1; }
                                return {

                                    searchTerm: params.term,
                                    userType: userType,
                                    subType: $('#' + self.currentNavId).find("[name='optradio']:checked").val()
                                };
                            },
                            processResults: function (data, page) {
                                return {
                                    results: data.response.model.results
                                };
                            },
                            cache: true
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        },
                        minimumInputLength: 1,
                        templateResult: _formatUser,
                        templateSelection: _selection
                    });
                }
                catch (e) {
                    console.log("FAILED TO INIT NAME SEARCH", e);
                }
                return self;
            },

            initializeFormValidations: function (currentNavId) {
                var self = this;
                self.formSetup($('#' + currentNavId + ' form'));
                return self;
            },

            loadFeatures: function () {

                var self = this,
                    locationId = (localStorage.getItem("searchParams") == undefined || JSON.parse(localStorage.getItem("searchParams")).locationId == undefined)
                                 ? null : JSON.parse(localStorage.getItem("searchParams")).locationId,
                    stateId = (localStorage.getItem("searchParams") == undefined || JSON.parse(localStorage.getItem("searchParams")).stateId == undefined)
                                 ? null : JSON.parse(localStorage.getItem("searchParams")).stateId;
                var model = new Model();
                model.url = Config.search.zoneAll;
                model.save({ locationId: locationId, stateId: stateId }, {
                    success: function (m, res) {
                        self.$('#features-carousel').append(_.template(FeaturesCarouselItemTpl, {
                            //this model is dependent on locationId it is been processed as a flat model rather as a collection
                            model: res.response.model
                        }));
                        self.$('#features-carousel').owlCarousel({
                            navigation: true, // Show next and prev buttons
                            slideSpeed: 10000,
                            singleItem: true,
                            //autoPlay: Config.autoPlay,
                            autoPlay: true,
                            stopOnHover: true,
                            pagination: false,
                            autoHeight: true,
                            navigationText: ["<i class='fa fa-backward' />", "<i class='fa fa-forward' />"],
                            transitionStyle: "fade"
                        });
                    }
                })
            },

            loadCitiesByState: function (e) {
                var self = this,
                  ele = $(e.currentTarget),
                  stateId = ele.val(),
                  target = $(ele.data("stateCity"));

                target.children().remove();

                if (stateId == "null") {
                    target.find("option").remove();
                    target.attr("disabled", "disabled").append($("<option/>").val("null").text('Metro City'));
                    return;
                }

                new CitiesColl([], { id: stateId }).fetch({
                    success: function (m) {
                        var data = [];
                        data.push({ id: "null", text: 'Metro City' });
                        $.each(m.models, function (index, item) { // Iterates through a collection
                            data.push({ id: item.attributes.value, text: item.attributes.text });
                        });
                        if (m.model.length == 0) target.children().remove();
                        target.select2({
                            disabled: m.models.length == 0 ? true : false,
                            data: m.models.length == 0 ? null : data,
                            placeholder: "No cities found"
                        })
                    }
                });
                target.removeAttr("disabled")
            },

            loadCollegeCitiesByState: function (e) {
                var self = this,
                   ele = $(e.currentTarget),
                   stateId = ele.val(),
                   target = $('#city-college');

                target.children().remove();

                if (stateId == "null") {
                    target.find("option").remove();
                    target.attr("disabled", "disabled").append($("<option/>").val("null").text('Metro City'));
                    return;
                }

                new CollegeCitiesColl([], { id: stateId }).fetch({
                    success: function (m) {
                        var data = [];
                        data.push({ id: "null", text: 'Metro City' });
                        $.each(m.models, function (index, item) { // Iterates through a collection
                            data.push({ id: item.attributes.value, text: item.attributes.text });
                        });
                        if (m.model.length == 0) target.children().remove();
                        target.select2({
                            disabled: m.models.length == 0 ? true : false,
                            data: m.models.length == 0 ? null : data,
                            placeholder: "No cities found"
                        })
                    }
                });
                target.removeAttr("disabled")
            },

            redirect: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    route = ele.attr('data-zonal-item');
                app.router.navigate(route, true);
            },

            clearNameSearch: function (e) {
                var ddl = $('.ddl-college-name-search'),
                    ddl2 = $('.ddl-doc-name-search');

                try { if (ddl.select2) { ddl.select2("val", "") } } catch (e) { }
                try { if (ddl2.select2) { ddl2.select2("val", "") } } catch (e) { }
            }


        });

        return View;

    });

