﻿define([
    "apiConfig",
    "viewBase",
    "text!modules/dashboard/templates/aboutus.html",
     "bootstrap-dialog",
    "bootstrapValidator"],
    function (Config, Base, aboutusTpl, BootstrapDialog) {

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.isInitialSearchLoaded = false;
                this.template = _.template(aboutusTpl);
            },

           
            render: function () {
                
             console.log("inside here render", aboutusTpl);
                var self = this;
                self.$el.html(_.template(aboutusTpl));
                self.isInitialSearchLoaded = false;
                
             
            },

         
        });

        return View;
    });