﻿define([
	"apiConfig",
    "constantsConfig",
	"core/controller",
	"modules/dashboard/views/dashboardView",
    "modules/dashboard/views/contactusView",
    "modules/dashboard/views/aboutusView",
    "modules/dashboard/views/sitemapView",
     "modules/dashboard/views/bannerView",
     "modules/dashboard/views/DoctorClinicView",
      "modules/dashboard/views/HospitalView",
     "modules/dashboard/views/CollegeView",
     "modules/dashboard/views/thankyouView",
     "modules/dashboard/views/thank_youView"
], function (
	Config,
    Constants,
	Controller,
	DashboardView,
    ContactUs,
    AboutUs,
   SiteMap,
   Banner,
   DoctorClinic,
   Hospital,
   College,
   ThankYou,
   Thank_You
) {

    "use strict";
   
    var Controller = Controller.extend({

        init: function () {
           
            
            return this;
          
           
        },

       
        events: function (args) {

            return this;
        },

        dashboard: function () {
           
            var self = this,
                view = new DashboardView({ controller: self, el: Constants.CONTENT });
                    $(document).ajaxStop(function () {
                        // place code to be executed on completion of last outstanding ajax call here
                        $(".loader").addClass("hide");
                      
                    });
                view.clear()
        },

        contactus: function () {
            var self = this,
             view = new ContactUs({ controller: self, el: Constants.CONTENT });
            console.log("inside here contact us");
            view.clear().render();
        },

        aboutus: function () {
            var self = this,
             
         view = new AboutUs({ controller: self, el: Constants.CONTENT });
            console.log("inside here");
        view.clear().render();
        },

        sitemap: function () {
        var self = this,
             
             view = new SiteMap({ controller: self, el: Constants.CONTENT });
                console.log("inside here");
                view.clear().render();
        },
        Promo: function () {
            var self = this,

                 view = new Banner({ controller: self, el: Constants.CONTENT });
            console.log("inside here");
            view.clear().render();
        },
        DoctorClinic: function () {
            var self = this,            
             view = new DoctorClinic({ controller: self, el: Constants.CONTENT });
            console.log("inside DoctorClinic");
            view.clear();
        },
        Hospital: function () {
            var self = this,
             view = new Hospital({ controller: self, el: Constants.CONTENT });
            console.log("inside Hospital");
            view.clear().render();
        },
        College: function () {
            var self = this,
             view = new College({ controller: self, el: Constants.CONTENT });
            console.log("inside College");
            view.clear().render();
        },
        thankyou: function () {
            var self = this,

                 view = new ThankYou({ controller: self, el: Constants.CONTENT });
            console.log("inside here");
            view.clear().render();
        },
        thank_you: function () {
            console.log("inside here thank_you");
            var self = this,
                  
            view = new Thank_You({ controller: self, el: Constants.CONTENT });
          
            view.clear().render();
        }

    });

    return Controller;

});
