﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "text!modules/pda/templates/conversations.html"
    ],

    function (Config, Constants, Base, Model, ConversationsTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function () {
                var self = this;
            },

            events : {
                "click .reply-btn" : "onReplyBtnClick",
                "click .submit-answer" : "onSubmittingAnswer"
            },

            render: function () {
                var self = this;
                var role = app.sessionManager.get('USER').type;
                var questionsModel = new Model();
                questionsModel.url = Config.pda.advice;
                questionsModel.fetch({
                    success: function(model, resp){
                        self.$el.html(_.template(ConversationsTpl, {
                            data: resp.response.model,
                            role: role
                        }));
                    }
                })
            },

            onReplyBtnClick: function(e){
                var self = this;
                var ele = $(e.currentTarget);
                ele.hide();
                ele.closest('.reply-section').find('.reply-to-question').removeClass('hide');
            },

            onSubmittingAnswer: function(e){
                var self = this, ele = $(e.currentTarget);
                var request = {answer: ele.closest('.reply-to-question').find('textarea').val(),
                                id: ele.closest('.question-item').data('question-id'),
                                userId: ele.closest('.question-item').data('userid')};

                if ($.trim(request.answer).length == 0) return;
                request.answer = request.answer + "\n\n " + "\"Doctor/Clinic\" is replying your questions based on the information (limited) you have provided. It's always good to get an in-person patient consultation";

                var answerModel = new Model({ id: request.id });
                answerModel.url = Config.pda.advice + '/' + app.sessionManager.get('USER').userId;
                answerModel.save(request, {
                    success: function(model, resp){
                        ele.closest('.reply-section').html('<span class="answer-text"><b>Answer : </b>' + resp.response.model.answer + '</span>');
                        ele.closest('.replied-date').html('<span><b>Replied </b>' + resp.response.model.repliedOn + '</span>');
                    }
                })
            }
        });

        return View;

    });