﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       'modules/pda/collections/specializations',
       'modules/shared/collections/cities',
       "text!modules/pda/templates/form.html",
       "text!modules/pda/templates/conversation-item.html",
       "bootstrapSelect"
], function (Config, Constants, Base, Model, SpzColl, CitiesColl, RequestTpl, ConversationItemTpl) {

    "use strict";

    var View = Base.extend({
        tagName: 'div',

        events :{
            "click .btn-submit-pda": "onPDASubmit"
        },

        initialize: function (options) {
            this.parentView = options.parent;
            this.$el.html(_.template(RequestTpl, {}));
            return this;
        },

        render: function () {
            var self = this;
            self.loadCities(self.$('#cities'));
            self.loadSpecializations(self.$('#spz'));
                      
            self.form = {
                _setup: function () {
                    var f = $('#pda-form');
                    f.bootstrapValidator({
                        live: 'disabled',
                        submitButtons: 'button[type="submit"]',
                        fields: {
                            subject: {
                                validators: {
                                    notEmpty: {
                                        message: 'Subject should not be empty.'
                                    }
                                }
                            },
                            question: {
                                validators: {
                                    notEmpty: {
                                        message: 'Question should not be empty.'
                                    }
                                }
                            }
                        }
                    });
                },
                _validate: function () {
                     $('#pda-form').data("bootstrapValidator").validate();
                    return $('#pda-form').data("bootstrapValidator").isValid();
                },
                _reset: function () {
                    $('#pda-form').data("bootstrapValidator").resetForm();
                    var self = this;
                    $("#subject, #question").val('');
                    $('#pdaTandQ').prop('checked', false);
                   // self.$el.find('#cities, #spz').select2("val", "-1");
                }
            }

            self.form._setup();

            return self;
        },

        loadSpecializations : function(ele){
            new SpzColl().fetch({
                success: function (m) {
                    var data = [];
                    $.each(m.models, function (index, item) {
                        data.push({ id: item.attributes.value, text: item.attributes.text });
                    });
                    ele.select2({
                        data: data
                    })
                }
            });
        },
        
        loadCities: function (ele) {
            new CitiesColl().fetch({
                success: function (m) {
                    var data = [];
                    $.each(m.models, function (index, item) {
                        data.push({ id: item.attributes.value, text: item.attributes.text });
                    });

                    ele.select2({
                        data: data
                    })
                }
            });
        },

        onPDASubmit: function (e) {
            var self = this, form = $('#pda-form'), ele = $(e.currentTarget);
            if (!self.form._validate()) { return false; }
            if (!self.$('#pdaTandQ').prop('checked')) { app.trigger("warning", "Please accept terms and conditions", "Terms and Conditions"); return false; }
         
            var values = self.serializeForm(form),
                model = new Model();
            console.log(values);
            values.userId = app.sessionManager.get('USER').userId;
          
            model.url = Config.pda.advice;
            ele.text("Saving...").addClass("disabled");
            self.form._reset();
            model.save(values, {
                success: function (m, res) {
                    var role = app.sessionManager.get('USER').subscriberTypeText;
                    self.parentView.$('#pda-conversations-list').prepend(_.template(ConversationItemTpl,{ model: m.toJSON(), role :role }));
                    self.unfreezeForm(form);
                    self.form._reset();
                    ele.text("Submit").removeClass("disabled");
                },
                error: function (model, data) {
                    app.trigger("response:error",data.responseJSON.response.error1 );
                    self.unfreezeForm(form);
                    self.form._reset();
                    ele.text("Submit").removeClass("disabled");
                }
            });
            return false;
        }

    });

    return View;
});