define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/pda/views/pda",
    "modules/pda/views/conversations"
], function (
    Config,
    Constants,
    Controller,
    PdaView,
    ConversationsView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        pda: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }

            var self = this,
                view = new PdaView({ controller: self, el: Constants.CONTENT, args: args });
            view.clear().render();
        }
    });

    return Controller;

});
