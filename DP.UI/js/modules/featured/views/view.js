﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "text!modules/featured/views/templates/featured.html",
        "text!modules/featured/views/templates/featured-list.html",
        "bootstrapSelect",
        "jqExt"
    ],

    function (Config, Constants, Base, Model, FeaturedTpl, FeaturedListTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function () {

                document.title = "Book Doctor's Appointment Online | Book Hospital/Clinic Appointment Online";
                $('meta[name=description]').attr('content', 'We are very responsive and we answer to your queries to the best of our capabilities. Doctorpursuit.com LLP lets you book an appointment with your doctor online.');
                var self = this;
                var d = new Date();
                self.months = self.getMonthsOfQuarter(Math.floor((d.getMonth() + 3) / 3));
                self.year = d.getFullYear();
                self.render();
            },

            events : {
                "click a.month": "onMonthSelect",
               // "change select.years": "onYearSelect"
            },

            render: function () {
                var self = this;
                self.$el.html(_.template(FeaturedTpl, {}));
                _.each(self.months, function (i) {
                    self.$el.find("a.month[data-value='" + i + "']").addClass('active');
                });
                self.$el.find("options[value='" + self.year + "']").attr('selected', 'selected');
                self.renderResults();
            },

            renderResults: function () {
                var self = this;
                var model = new Model();
                model.url = Config.report.getFeaturedReport;
             
                model.save({ months: self.months, year: self.year }, {
                    success: function (m, res) {
                        self.$("#featured-list").html('').html(_.template(FeaturedListTpl, { model: m.toJSON() }));
                    }
                });
            },
            
            onYearSelect: function (e) {
                var self = this;
                self.year = $(e.currentTarget).val();
                self.renderResults();
            },

            onMonthSelect: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.data('quarter');

                self.$el.find("a.month[data-value]").removeClass("active");
                self.months = self.getMonthsOfQuarter(val);
                _.each(self.months, function (i) {
                    self.$el.find("a.month[data-value='" + i + "']").addClass('active');
                });
                self.renderResults();
            },
            getMonthsOfQuarter: function (month) {
                var q = "Q" + month.toString();
                return Constants.YEAR_QUARTER[q];
            }
        });

        return View;

    });