define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/offers/views/view",
    "modules/offers/views/claim",
    "modules/offers/views/myoffers"
], function (
    Config,
    Constants,
    Controller,
    OffersView,
    ClaimView,
    MyOfferView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        offers: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
                view = new OffersView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().render();
        },


        myOffers: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
                view = new MyOfferView({ controller: self, el: Constants.CONTENT, args: args });
            view.clear().render();
        },


        claim: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
              view = new ClaimView($.extend({ controller: self, el: Constants.CONTENT }, args));
              view.clear()
        }
    });

    return Controller;

});
