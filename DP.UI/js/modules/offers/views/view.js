﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "text!modules/offers/views/templates/offers.html",
        "jqExt"
    ],

    function (Config, Constants, Base, Model, OffersTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function ()
            {
                document.title = "Treatment Offers - Doctorpursuit.com LLP";
                $('meta[name=description]').attr('content', 'Get the best offers online for various medical treatments only at Doctorpursuit.com LLP. Sign in today to get your free personal doctor advice.');
                var self = this;
            },

            events: {
                'click button.btn-claim': "claim",
                'mouseover [data-offer-desc]': "showDesc",
                "click .btn-fb-share": "shareOnFb",
                "click .btn-twitter-share": "shareOnTwitter",
                'keyup #bmr-age': "bmr",
                'keyup #bmr-body-height': "bmr",
                'keyup #bmr-body-weigth': "bmr",
                'keyup #pdd-first-data': "pdd",
                'keyup #bmi-body-height': "bmi",
                'keyup #bmi-body-weigth': "bmi"
            },

            render: function () {
            
                var self = this;
                var m = new Model();
                m.url = Config.offer.get;
             
                m.fetch(
                    {
                        success:
                        function (r) {
                            
                            self.$el.html(_.template(OffersTpl, {

                                model: r.toJSON()
                            }));
                            console.log("offers>>>", r.toJSON());
                        }
                    });

                //FB INIT
                FB.init({
                    appId: '598483290303155',
                    status: true,
                    cookie: true,
                    xfbml: true,
                    version: 'v2.6'
                });
            },
            claim: function (e) {
                var ele = $(e.currentTarget),
                    self = this,
                    data = {},
                    user = app.sessionManager.get("USER");
                data.ipAddress = window.IP;
                data.userEmail = user.email;
                data.userPhone = user.phone_1;
                data.OfferMasterId = ele.data("offer-id");
                ele.text("Saving...");
                $.post(Config.offer.claim, data, function (res) {
                    ele.text("Claim");
                    app.trigger("response:success", "You have successfully claimed this offer. Please wait for the further communication.");
                }).error(function () {
                    ele.text("Claim");
                    app.trigger("response:error", "Unable to process your request now. Please try after sometime");
                })
            },
            bmr: function () {
                var self = this,
                    height = $('#bmr-body-height').val(),
                    weight = $('#bmr-body-weigth').val(),
                    age = $('#bmr-age').val(),
                    res = $('#bmr-result'),
                    gender = $("[name='bmr-gender']:checked").val(),
                    rx = new RegExp(/^[\d]+$/);

                if (age.length == 0) {
                    res.val(0);
                }

                if (height.length > 0 && weight.length > 0 && age.length > 0 &&
                   rx.test(height) == true && rx.test(weight) == true && rx.test(age) == true) {
                    switch (gender) {
                        case 'm':
                            var r = 66 + (13.7 * parseFloat(weight)) + (5 * parseFloat(height)) - (6.8 * parseFloat(age));
                            res.val(r.toFixed(2));
                            break;
                        case 'f':
                            var r = 655 + (9.6 * parseFloat(weight)) + (1.8 * parseFloat(height)) - (4.7 * parseFloat(age));
                            res.val(r.toFixed(2));
                            break;
                    }
                }
            },
            bmi: function () {
                var self = this,
                    height = $('#bmi-body-height').val(),
                    weight = $('#bmi-body-weigth').val(),
                    res = $('#bmi-result'),
                    rx = new RegExp(/^[\d]+$/);

                res.val(0.0);
                if (height.length > 0 && weight.length > 0 &&
                   rx.test(height) == true && rx.test(weight) == true) {
                    height = parseFloat(height);
                    weight = parseFloat(weight);
                    var r = (weight / (height * height)) * parseFloat(10000);
                    res.val(r.toFixed(2));
                }
            },
            pdd: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.val(),
                    res = $('#pdd-pregnancy-duedata');
                res.val('');

                var fn_addDays = function (date, days) {
                    date.setDate(date.getDate() + parseInt(days));
                    return date.toDateString();
                };

                try {
                    if (val.length == 5) {
                        var split = val.split('-'),
                            date = split[0],
                            month = split[1],
                            year = new Date().getFullYear(),
                            rx = /^[0-9]{2}[-][0-9]{2}$/;
                        if (rx.test(val)) {
                            var _d = new Date(year + "-" + month + "-" + date);
                            res.val(fn_addDays(_d, 280));
                        }
                    }
                }
                catch (e) {
                    console.log(e);
                    res.val('Not a valid date-month');
                }
            },

            showDesc: function (e) {
                var ele = $(e.currentTarget),
                    desc = ele.attr("data-offer-desc");
                $(ele).popover({
                    container: "body",
                    content: desc,
                    trigger: "hover",
                    placement: "bottom"
                }).popover('show')
            },

            shareOnFb: function (e) {
                var ele = $(e.currentTarget),
                    link = ele.attr("data-link"),
                    image = ele.attr('data-image');
               // $("meta[property='og:image']").attr('content', image)
                FB.ui({
                    method: 'share',
                    href: link,
                    appId: '598483290303155'
                }, function (response) { });
            },

            shareOnTwitter: function (e) {
                var ele = $(e.currentTarget),
                   link = ele.attr("data-link"),
                   linkLength = link.length,
                   text = $.trim(ele.attr("data-text")),
                   image = ele.attr('data-image');
               // $("meta[name='twitter:image']").attr('content', image)
                window.open("https://twitter.com/intent/tweet?" +
                            $.param({
                                url: encodeURI(link),
                                text: text.substring(0, 140),
                                original_referer: "www.gaween.com"
                            }))
            }
        });

        return View;

    });