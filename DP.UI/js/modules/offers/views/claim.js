﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "text!modules/offers/views/templates/claim.html",
       "bootstrapValidator"
],
 function (Config, Constants, Base, Model,  ClaimTpl) {

     "use strict";
     var View = Base.extend({

         initialize: function (params) {
             var self = this;
             self.model = new Model();
             self.model.url = Config.offer.get + '/'+ params.id;
             self.model.fetch({ success: function () { self.render(); } });
             return this;
         },

         events: {
             'click .btn-claim-offer': "claim",
             'mouseover [data-offer-desc]': "showDesc",
             "click .btn-fb-share": "shareOnFb"
         },

         render: function () {
             var self = this,
                 _setupForm = function () {
                     $('#form-claim-offer').bootstrapValidator({
                         live: 'disabled',
                         submitButtons: 'button[type="submit"]',
                         fields: {
                             userEmail: {
                                 validators: {
                                     notEmpty: {
                                         message: 'Email should not be empty.'
                                     },
                                     emailAddress: {
                                         message: 'Not a valid email address'
                                     }
                                 }
                             },
                             userPhone: {
                                 validators: {
                                     notEmpty: {
                                         message: 'Contact number should not be empty.'
                                     },
                                     callback: {
                                         message: 'Not a valid phone number',
                                         callback: function (value, validator, $field) {

                                             if ($.trim(value).length == 0) return true;

                                             return /^\d{10}$/.test(value) == true;
                                         }
                                     }
                                 }
                             }
                         }
                     });
                 }

             self.$el.html(_.template(ClaimTpl, self.model.toJSON()));
             _setupForm();

             //FB INIT
             FB.init({
                 appId: Constants.FACEBOOK_APP_ID,
                 status: true,
                 cookie: true,
                 xfbml: true,
                 version: 'v2.6'
             });
         },

         claim: function (e) {
             var self = this,
                 ele = $(e.currentTarget),
                 form = $('#form-claim-offer');
                // form = $('#form-claim-offer'),
                //_validateForm = function () {
                //    form.data("bootstrapValidator").validate();
                //    return $('#form-claim-offer').data("bootstrapValidator").isValid();
                //},
                //_resetForm = function () {
                //    form.data("bootstrapValidator").resetForm();
                //},
                //_eleDisable = function () {
                //    ele.addClass("disabled").text("Saving...");
                //},
                //_eleEnable = function () {
                //    ele.removeClass("disabled").text("Submit");
                //}
                        
             if (true) { //{ if (_validateForm()) {
                 var data = self.model.toJSON();
                 data.ipAddress = window.IP;
                 data.OfferMasterId = ele.data("offer-id");
                 self.freezeForm(form);
                // _eleDisable();

                 $.post(Config.offer.claim, data, function (res) {
                     app.router.redirect("offers");
                     app.trigger("response:success", "Thanks for claiming the offer please refer your Email or SMS for the Offer ID.");
                 }).error(function () {
                     app.trigger("response:error", "Unable to process your request now. Please try after sometime");
                 })
             }

             return false;
         },

         showDesc: function (e) {
             var ele = $(e.currentTarget),
                 desc = ele.attr("data-offer-desc");
             $(ele).popover({
                 container: "body",
                 content: desc,
                 trigger: "hover",
                 placement: "bottom"
             }).popover('show')
         },

         shareOnFb: function (e) {
             var ele = $(e.currentTarget),
                 link = ele.attr("data-link");
             FB.ui({
                 method: 'share',
                 href: link
             }, function (response) { });
         }
     });

     return View;

 });