﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "text!modules/offers/views/templates/myoffers.html",
       "text!modules/offers/views/templates/offerTrans.html",
       "bootstrap-dialog",
       "jqExt"
],

   function (Config, Constants, Base, Model, OffersTpl, OfferTrans, BootstrapDialog) {

       "use strict";

       var View = Base.extend({

           initialize: function () {
               var self = this;
               self.offers = null;
           },

           events: {
               'click button.btn-offer-more-details': "transactions",
              
           },

           render: function () {
               
               var self = this;
               var m = new Model();
               m.url = Config.offer.myOffers;
               m.fetch(
                   {
                       success:
                       function (r) {
                           self.offers = r.toJSON();
                          
                           self.$el.html(_.template(OffersTpl, {
                              
                               model: r.toJSON()
                           }));
                        
                       }
                   })
           },

           transactions: function (e) {
               var self = this,
                   ele = $(e.currentTarget),
                   id = parseInt(ele.data("offerId"));
               
               var offer = _.find(self.offers, { id: id });
               if (offer != undefined) {
                   BootstrapDialog.show({
                       size: BootstrapDialog.SIZE_WIDE,
                       cssClass : "offerTransactionDetails",
                       message: $('<div></div>').html(_.template(OfferTrans, {model:offer.offerTransactions})),
                       title: "Offer Transaction Details",
                       closable: true,
                   });
               }
           }
       });

       return View;

   });