define([
        "apiConfig",
        "modules/register/views/base",
        'core/model',
        'modules/shared/collections/states',
        'modules/shared/collections/cities',
        'modules/shared/collections/locations',
        'modules/shared/collections/roles',
        "text!modules/register/templates/register.html",
        "text!modules/register/templates/doctor-form.html",
        "text!modules/register/templates/user-form.html",
        "constantsConfig",
        "bootstrap-dialog",
        "bootstrapSelect",
        "bootstrapValidator"
    ],
    function (
        Config,
        ViewBase,
        Model,
        StatesColl,
        CitiesColl,
        LocationsColl,
        RolesColl,
        RegisterTpl,
        DoctorFormTpl,
        UserFormTpl,
        Constants,
        BootstrapDialog
    ) {
        
        "use strict";

        var view = ViewBase.extend({

            initialize: function () {

                var self = this;
                
                self.registerTemplate = _.template(RegisterTpl, {
                    doctorForm: _.template(DoctorFormTpl, {}),
                    userForm: _.template(UserFormTpl, {})
                });

                self.statesLoaded = false;

                return this;
            },

            render: function () {

                var self = this;
                self.$el.html(self.registerTemplate);
                self.initializeSelect2();
                self.getRoles();
                self.formSetup();
                $('.nav-tabs li').click();
                return self;
            },

            events: {
                "click .btn-register" : "register",
                "click .nav-tabs li"  : "onTabClick"
            },

            /* START - Register flow  - START*/
            register: function (e)
            {
                var self = this,
                    ele = $(e.currentTarget),
                    form = self.$el.find('.form-register');
              
                if (self.validateForm())
                {
                 
                    if (!$('#agreeterms').prop('checked')) {
                        BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_WARNING,
                            title: 'Agreement',
                            message: 'Please agree to the terms and conditions before signing up.'
                        })
                        return;
                    }
                    self.createUser(ele, form);
                }
              
            },

            createUser: function (ele, form) {
                var self = this,
                    values = self.serializeForm($('.form-register')),
                    activeTabData = $('.register-tabs > li.active').data('role'),
                    roleInfo = self.roles.where({ text: Constants.USER_ROLES[activeTabData] });

                
                $(".suggest-ddl").removeClass('select2-validation-error');
                values.roleId = roleInfo[0].get('value');
                values.registrationType = activeTabData;


                if (activeTabData != "5") {
                    var _data = $('.suggest-ddl').select2('data')
                    values["name"] = _data[0].name;
                    values["userId"] = _data[0].id;
                    
                    //custom validation for select2
                    if (
                     values.subscriberId == '-1' ||
                       values.userId == -1 ||
                       values.stateId == 'Select State') {
                        $(".btn-register").hide().show('fade');
                        $(".suggest-ddl").addClass('select2-validation-error');
                        return false
                    };
                }

                self.freezeForm(form);
                ele.text("Processing...");

                var model = new Model(); //cannot pass url in the constructor
                model.url = Config.user.create;
             
                model.save(values, {
                    success: function () {
                        //app.sessionManager.set("USER", model.toJSON());
                        //BootstrapDialog.show({
                        //    title: 'Success',
                        //    message: 'Please follow the email that has been sent to your email address entered during registration.'//'A activation link has been sent to your registered email id. Please click on the link to activate your account.'
                        //})
                     
                        app.router.navigate('thankyou', true);
                    },
                    error: function (model, data) {
                        BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_DANGER,
                            title: 'Error',
                            message: data.responseJSON.response.error1
                        })
                        self.unfreezeForm(form);
                        self.resetForm();
                        ele.text("Submit");
                    }
                });
            },

            /* END - Register flow - END */

            initializeSelect2: function () {
                var self = this;

                self.$('.stateSelection').select2();
                self.$('.citySelection').select2({style: 'width:100%', disabled:true});
                self.$('.locationSelection').select2({ disabled: true });

                var _formatUser = function (user) {
                    if (user.loading) return user.text;
                    var $user = $(
                        '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>'
                      );
                    return $user;
                }

                var _selection = function (user) {

                    if (user.name == undefined) {
                        return '<option value="-1" selected="selected">Search and Select</option>';
                    }else{
                        user.name = user.name.replace('Dr.', '');
                    }

                    console.log("selected subscriber", user);

                    //select2 bug
                    if (self.selectedSubscribed && user.id == self.selectedSubscribed.id) { return '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>' }

                    self.selectedSubscribed = user;
                    self.loadStates(function () {
                        self.$el.find(".stateSelection").select2('val',user.stateId);
                        self.$el.find("#cellPhone").val(user.mobileNo);
                    }, user);

                    return '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>'
                }

                self.$(".suggest-ddl").select2({
                    ajax: {
                        url: Config.user.suggest,
                        dataType: 'json',
                        type : "POST",
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term,
                                userType: $('.register-tabs > li.active').data('role'),
                                excludeSubscribers: true
                            };
                        },
                        processResults: function (data, page) {
                            // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data                          
                            return {
                               
                                results: data.response.model.results
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    minimumInputLength: 0,
                    templateResult: _formatUser, // omitted for brevity, see the source of this page
                    templateSelection: _selection // omitted for brevity, see the source of this page
                });
            },

            /* Events Handler */
            onStateSelection: function(e){
                var self = this;
                if(!self.statesLoaded){
                    self.$el.find(".stateSelection option").remove();
                    self.$el.find(".stateSelection").append($("<option></option>") // Yes you can do this.
                        .text('Loading states ...')
                        .val(0));
                    self.loadStates();
                }
            },

            onTabClick: function (e)
            {
                console.log("e>>>",e);
                var self = this;
               
                self.currentRole = $(e.currentTarget).data('role');
                console.log("e>>>", self.currentRole, self.$el);
                //alert(self.currentRole);

                self.$el.find('.extra-section, .suggest-ddl-row').show();
                if (parseInt(self.currentRole) == 5) {
                    self.$el.find('.extra-section, .suggest-ddl-row').hide();
                    self.$el.find('.User-Phone').show(); 
                    self.$el.find('.All-Phone').hide();
                }
                else if (parseInt(self.currentRole) == 6) {
                    self.$el.find('.extra-section').hide();
                    self.$el.find('.All-Phone').show();
                    self.$el.find('.User-Phone').hide();
                }
                else {
                    self.$el.find('.extra-section, .suggest-ddl-row').show();
                    self.$el.find('.All-Phone').show();
                    self.$el.find('.User-Phone').hide();
                }
            },

            onChangingState: function (e) {
                var self = this;
                var stateId = $(e.currentTarget).val();
                self.populateCitiesByStateId(stateId);
            },

            onChangingCity: function (e) {
                var self = this;
                var cityId = $(e.currentTarget).val();
                self.populateLocationsByCityId(cityId);                
            },
            
            getRoles: function(){
                var self = this;
                self.roles = new RolesColl();
                self.roles.fetch();
            },

            loadStates: function(callback){
                var self = this;
                if (self.statesLoaded) {
                    if (callback) {
                        callback();
                        self.populateCitiesByStateId(self.selectedSubscribed.stateId)
                    } return;
                }

                self.states = new StatesColl();
                self.states.fetch({
                    success: function () {

                        self.statesLoaded = true;
                        self.$el.find(".stateSelection option").remove(); // Remove all <option> child tags.
                        var data = [];
                        $.each(self.states.models, function(index, item) { // Iterates through a collection
                            data.push({id: item.attributes.value, text: item.attributes.text});
                        });

                        self.$el.find(".stateSelection").select2({
                            data: data
                        });
                        self.statesLoaded = true;
                        if (callback) {
                            callback();
                            self.populateCitiesByStateId(self.selectedSubscribed.stateId)
                        } 
                    }
                });
            },

            populateCitiesByStateId: function (stateId) {
                var self = this;

                self.cities = new CitiesColl([], {id: stateId});
                self.cities.fetch({
                    success: function(){
                        self.$el.find(".citySelection option").remove(); // Remove all <option> child tags.
                        var data = [];
                        $.each(self.cities.models, function(index, item) { // Iterates through a collection
                            data.push({id: item.attributes.value, text: item.attributes.text});
                        });
                        self.$el.find(".citySelection").select2({
                            //disabled: false,
                            data: data
                        });

                        self.populateLocationsByCityId(self.selectedSubscribed.cityId)
                    }
                });
            },

            populateLocationsByCityId: function (cityId) {
                var self = this;

                self.locations = new LocationsColl([], {id: cityId});
                self.locations.fetch({
                    success: function(){
                        self.$el.find(".locationSelection option").remove(); // Remove all <option> child tags.
                        var data = [];
                        $.each(self.locations.models, function(index, item) { // Iterates through a collection
                            data.push({id: item.attributes.value, text: item.attributes.text});
                        });
                        self.$el.find(".locationSelection").select2({
                            //disabled: false,
                            data:data
                        })
                       
                        // self.$el.find(".locationSelection").trigger('change', options);

                        self.$el.find(".locationSelection").select2("val", self.selectedSubscribed.locationId)
                    }
                });
            }
        });

        return view;

    })