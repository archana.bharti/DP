define([
        "apiConfig",
        "viewBase"
    ],
    function (
        Config,
        Base
    ) {

        var View = Base.extend({
            formSetup: function () {
                var self = this;
                var form = self.$el.find('.form-register')
                form.bootstrapValidator({
                    live: 'disabled',
                    submitButtons: 'button[type="submit"]',
                    fields: {
                        name: {
                            validators: {
                                notEmpty: {
                                    message: 'Please enter name'
                                }
                            }
                        },
                        displayName: {
                            validators: {
                                notEmpty: {
                                    message: 'Please enter display name'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'Password should not be empty.'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Password must be of 6 or more characters'
                                },
                                identical: {
                                    field: 'confirmuserpassword',
                                    message: 'The password and its confirm are not the same'
                                }
                            }
                        },
                        confirmuserpassword: {
                            validators: {
                                notEmpty: {
                                    message: 'Confirm password for security'
                                },
                                identical: {
                                    field: 'password',
                                    message: 'The password and its confirm are not the same'
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'Email should not be empty.'
                                },
                                emailAddress: {
                                    message: 'Not a valid email address'
                                }
                            }
                        },
                        contact: {
                            validators: {
                                notEmpty: {
                                   message: 'Phone number should not be empty.'
                                },
                                callback: {
                                    message: 'Not a valid phone number',
                                    callback: function (value, validator, $field) {

                                        if ($.trim(value).length == 0) return true;

                                        return /^\d{10}$/.test(value) == true;
                                    }
                                }
                            }
                        }
                    }
                })
            },

            validateForm: function () {
                $('.form-register').data("bootstrapValidator").validate();
                return $('.form-register').data("bootstrapValidator").isValid();
            },

            resetForm: function () {
                $('.form-register').data("bootstrapValidator").resetForm();
            }
        });

        return View;
    });