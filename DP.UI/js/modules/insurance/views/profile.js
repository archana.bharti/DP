﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "text!modules/insurance/views/templates/profile.html",
       "text!modules/user/templates/change-password.html",
       "text!modules/user/templates/upload-image.html",
       "text!modules/user/templates/user-reviews.html",
       "text!modules/user/templates/write-review.html",
       "text!modules/user/templates/my-reviews.html",
       "text!modules/user/templates/category-score-report.html",
       "text!modules/search/templates/appointment.html",
       "moment",
       "hcm",
       "bootstrap-dialog",
       "bootstrapValidator",
      
], function (Config, Constants, Base, Model, ProfileTpl, ChangePasswordTpl, UploadImageTpl, UserReviewsTpl, WriteReviewTpl, MyReviewsTpl, ScoreReportTpl, AppointmentFormTpl, moment, HCM,BootstrapDialog) {

    var planAttr = [
                        { key: "cashlessFacility", text: "Cashless Facility" },
                        { key: "maternityBenefits", text: "Maternity Benefits" },
                        { key: "ambulanceCharges", text: "Ambulance Charges" },
                        { key: "dayCareProcedures", text: "DayCare Procedures" },
                        { key: "healthCheckup", text: "Health Checkup" },
                        { key: "taxBenefits", text: "Tax Benefits" },
                        { key: "hospitalizationExpenses", text: "Hospitalization Expenses" },
                        { key: "freelookPeriod", text: "Freelook Period" },
                        { key: "alternativeTreatment", text: "Alternative Treatment" },
                        { key: "cumulativeBonus", text: "Cumulative Bonus" },
                        { key: "organDonorExpenses", text: "OrganDonor Expenses" },
                        { key: "prePostHospitalizationCharges", text: "PrePost HospitalizationCharges" },
                        { key: "portability", text: "Portability" }]

    var setupForm = {
        validation: function () {
            var form = $('.form-insurance-edit');
            form.bootstrapValidator({
                live: 'enabled',
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Name should not be empty.'
                            }
                        }
                    },
                    displayName: {
                        validators: {
                            notEmpty: {
                                message: 'Display Name should not be empty.'
                            }
                        }
                    },
                    phone_1: {
                        validators: {
                            notEmpty: {
                                message: ' '
                            },
                            callback: {
                                message: 'Not a valid phone number',
                                callback: function (value, validator, $field) {

                                    if ($.trim(value).length == 0) return true;

                                    return /^\d{10}$/.test(value) == true;
                                }
                            }
                        }
                    },
                }
            });

        },
        validate: function () {
            var form = $('.form-insurance-edit');
            form.data("bootstrapValidator").validate();
            return form.data("bootstrapValidator").isValid();
        },
        reset: function () {
            $('.form-insurance-edit').data("bootstrapValidator").resetForm();
        }
    }

    var ChangePasswordView = Base.extend({

        template: _.template(ChangePasswordTpl),

        tagName: "div",

        initialize: function (options) {
            this.dialogRef = options.dialogRef
        },

        events: {
            'click .btn-change-password': "changePassword"
        },
        render: function () {
            var self = this;
            var _initFormValidator = function () {
                $('#form-change-password').bootstrapValidator({
                    live: 'disabled',
                    submitButtons: 'button[type="submit"]',
                    fields: {
                        oldPassword: {
                            validators: {
                                notEmpty: {
                                    message: 'Password should not be empty.'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Password must be of 6 or more characters'
                                },
                                callback: {
                                    message: 'Old password and new password should not be same',
                                    callback: function (value, validator, $field) {
                                        var _oldp = $field.val();
                                        var _newP = $("[name='newPassword']").val();
                                        if (btoa(_newP) === btoa(_oldp)) {
                                            return {
                                                valid: false,       // or true
                                                message: 'Old password and new password should not be same'
                                            }
                                        }

                                        return {
                                            valid: true
                                        }
                                    }
                                }
                            }
                        },
                        newPassword: {
                            validators: {
                                notEmpty: {
                                    message: 'New password should not be empty.'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Password must be of 6 or more characters'
                                },
                                identical: {
                                    field: 'newPassword2',
                                    message: 'The new password and password entered again are not identical.'
                                }
                            }
                        },
                        newPassword2: {
                            validators: {
                                notEmpty: {
                                    message: 'Confirm password again for security'
                                },
                                stringLength: {
                                    min: 6,
                                    message: 'Password must be of 6 or more characters'
                                },
                                identical: {
                                    field: 'newPassword',
                                    message: 'The new password and password entered again are not identical.'
                                }
                            }
                        }
                    }
                })
            }
            self.$el.html(self.template(self.model));
            _initFormValidator();
            return this;
        },

        changePassword: function (e) {
            var self = this, ele = $(e.currentTarget), form = $('#form-change-password')
            var _validateForm = function () {
                form.data("bootstrapValidator").validate();
                return $('#form-change-password').data("bootstrapValidator").isValid();
            },
            _resetForm = function () {
                form.data("bootstrapValidator").resetForm();
            },
            _eleDisable = function () {
                ele.addClass("disabled").text("Saving...");
            },
            _eleEnable = function () {
                ele.removeClass("disabled").text("Submit");
            }

            if (_validateForm()) {

                var data = self.serializeForm(form);
                self.freezeForm(form);
                _eleDisable();

                data.oldPassword = btoa(data.oldPassword);
                data.newPassword = btoa(data.newPassword);
                delete data.newPassword2;

                $.post(Config.security.changePassword, data, function (res) {
                    app.trigger("response:success", "Password changed successfully.");
                    self.dialogRef.close();
                    self.remove();
                }).error(function () {
                    _eleEnable();
                    self.unfreezeForm(form);
                    _resetForm();
                })
            }

            return false;
        }
    });

    var UploadImageView = Base.extend({

        template: _.template(UploadImageTpl),

        tagName: "div",

        initialize: function (options) {
            this.dialogRef = options.dialogRef;
            this.parentView = options.parentView;
        },

        events: {
            'click .btn-upload-image': "uploadImage"
        },
        render: function () {
            var self = this;
            self.$el.html(self.template());
            return this;
        },
        uploadImage: function (e) {
            var self = this,
                form = self.$('#form-user-image-upload'),
                ele = $(e.currentTarget),
                pbar = form.find('.progress');

            ele.addClass("disabled").text("Saving...");
            self.imageUpload(
                           { id: 0 },
                           form.find('input[type="file"]'),
                           Config.profile.uploadImage,
                           pbar,
                           function (res, data) {
                               if (res == true) {
                                   self.parentView.$('.user-img')
                                       .find('img')
                                       .attr("src", "")
                                       .attr("src", data.response.model)
                               }
                               self.dialogRef.close();
                           });
            pbar.show();
        }
    });

    var View = Base.extend({

        initialize: function (options) {
            var self = this;
            self.template = _.template(ProfileTpl);
            self.subscriber = options.subscriber;
            self.subscriber.readOnly = !(self.subscriber.account)
            self.subscriber.showWriteReview = true;
            self.subscriber.showMyReviews = false;
            self.subscriber.showScoreReport = false;
            self.displayPhones = [];
            self.reviewsInfo = new Model();
            self.render();
        },

        events: {
            "click .btn-edit-summary": "editSummary",
            "click .btn-show-edit": "initEdit",
            'click .btn-hide-edit': "hideEdit",
            'click .btn-update': "update",
            'click .btn-change-password-init': "showChangePassword",
            'click .img-edit-icon': "showUploadImage",
            'click .chkbox_displayPhone': "updateDisplayPhone",
            'click .btn-save-reviews': "submitReview",
            'click .btn-contact': "contact"
        },

        render: function () {
          
            var self = this;
            $.get(Config.insurance.get + self.subscriber.subscriberId, function (res) {
                self.subscriber = $.extend({},
                    res.response.model,
                    self.subscriber,
                    {
                        isAccount: (self.subscriber.account == true),
                        isPhoneVisible: function (p) {
                            if (this.displayPhone != null && this.displayPhone != '') {
                                var l = this.displayPhone.split(',');
                                if (l.length > 0) {
                                    var f = _.contains(l, p.toString());
                                    if (f == true) { self.displayPhones = _.without(self.displayPhones, parseInt(p)); self.displayPhones.push(parseInt(p)); }
                                    return f;
                                }
                            }
                            return false;
                        },
                        getRatingTitle: function (key) {
                            return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                        }
                    });
               
                var trueList = [];
                _.each(planAttr, function (attr, i) {
                    if (res.response.model[attr.key] == true) trueList.push('<label class="label label-primary" style="display:inline-block">' + attr.text.toUpperCase() + '</label>');
                })
                self.subscriber.trueList = trueList.join(' ');

                if (app.isAuthenticated == true && app.sessionManager.get("USER").subscriberType == 5) {
                    if (self.subscriber.account == false) self.subscriber.showWriteReview = true;
                    if (self.subscriber.account == true) self.subscriber.showMyReviews = true;
                }

                if (app.isAuthenticated == true && app.sessionManager.get("USER").subscriberType == 6) {
                    self.subscriber.showScoreReport = true;
                }

                ////TO ALLOW NON-AUTHANTICATED USERS////////
                if (app.isAuthenticated == false)
                {
                    self.subscriber.showScoreReport = true;
                }

                self.$el.html(self.template(self.subscriber));

                $(".rating").rating({ showClear: false, showCaption: false });
                setupForm.validation();
                self.$el.find(".wyswg").summernote(
                    {
                        height: 200,
                        focus: true,
                        tooltip: ""
                    })

             
                if (self.subscriber.showMyReviews) { try { self.renderMyReviews(); } catch (e) { } }
                if (self.subscriber.showWriteReview) { try { self.renderWriteReviews(); } catch (e) { } }


                /////CALL TO RENDER SHOW SCORE REPORT/////
                if (self.subscriber.showScoreReport) { try { self.renderScoreReport(); } catch (e) { } }

               // if (!self.subscriber.showScoreReport) { try { self.renderScoreReportWithoutLogin(); } catch (e) { } }
                try { self.renderUserReviews();} catch (e) { } 

                var tab = self.$el.find(".reviews-section > ul > li:first-child");
                tab.addClass("active");
                $(tab.find("a").attr('href')).addClass('active').addClass('in')
            })
        },

        editSummary: function (e) {
            var self = this,
                editPanel = $("#edit-summary"),
                viewPanel = $("#view-summary"),
                ele = $(e.currentTarget);
            if (viewPanel.data("toggle") == "show") {
                editPanel.hide();
                viewPanel.show();
                viewPanel.data("toggle", "hide");
                ele.find(".fa").removeClass("fa-save").addClass("fa-edit");
                self.subscriber.summary = self.$el.find(".wyswg").summernote('code');
                $.post(Config.insurance.updateSummary,
                    { id: self.subscriber.subscriberId, summary: self.subscriber.summary }, function (resp) {
                        self.subscriber.summary = self.$el.find(".wyswg").summernote('code');
                        self.render();
                    });
            }
            else {
                editPanel.show();
                viewPanel.hide();
                viewPanel.data("toggle", "show");
                ele.find(".fa").removeClass("fa-edit").addClass("fa-save");
            }
        },

        initEdit: function (e) {
            $(".edit-profile").slideDown();
        },

        hideEdit: function (e) {
            $(".edit-profile").slideUp();
        },

        updateDisplayPhone: function (e) {
            var self = this,
                ele = $(e.currentTarget),
                val = ele.data("phone");
            if (ele.prop("checked") == true) {
                self.displayPhones = _.without(self.displayPhones, parseInt(val));
                self.displayPhones.push(parseInt(val));
            }
            else {
                self.displayPhones = _.without(self.displayPhones, parseInt(val));
            }
        },

        update: function (e) {
            var self = this,
                form = $('.form-insurance-edit');
            ele = $(e.currentTarget);

            if (setupForm.validate()) {
                var params = self.serializeForm(form);
                params.displayPhone = self.displayPhones.join(',');
                $.post(Config.insurance.update, params, function (res) {
                    $(".edit-profile").slideUp();
                    $.extend(self.subscriber, res.response.model);
                    self.render();
                })
            }
            return false;
        },

        showChangePassword: function () {
            var self = this;
            if (self.changePasswordView != null) self.changePasswordView.remove();
            BootstrapDialog.show({
                title: 'Change Password',
                message: '<div id="change-password-view"></div>',
                closeByBackdrop: false,
                onshown: function (dialogRef) {
                    self.changePasswordView = new ChangePasswordView($.extend({ el: '#change-password-view', parentView: self, dialogRef: dialogRef }));
                    self.changePasswordView.render();
                },
            });
        },

        showUploadImage: function () {
            var self = this;
            if (self.uploadImageView != null) self.uploadImageView.remove();
            BootstrapDialog.show({
                title: 'Profile Image',
                message: '<div id="upload-image-view"></div>',
                closeByBackdrop: false,
                onshown: function (dialogRef) {
                    self.uploadImageView = new UploadImageView($.extend({ el: '#upload-image-view', parentView: self, dialogRef: dialogRef }));
                    self.uploadImageView.render();
                },
            });
        },

        renderMyReviews: function () {
            var self = this,
                   m = new Model();
            m.url = Config.review.myReviews + app.sessionManager.get("USER").userId;
            m.fetch({
                success: function (model, resp) {
                    var data = resp.response.model;

                    self.$el.find("#my-reviews").html(
                       _.template(MyReviewsTpl, {
                           model: data,
                           getTitle: function (key) {
                               return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                           },
                           omit: function (r) {
                               return _.omit(r, "id", "userId", "userName", "subscriberName", "reviewedDate", "insuranceId", "hospitalId", "collegeId");
                           }
                       }));
                    $(".rating").rating({ showClear: false, showCaption: false });
                }
            })
        },

        renderWriteReviews: function () {
            var self = this, m = new Model();
            m.url = Config.review.getParameters + '6';
            m.fetch({
                success: function (m, res) {
                    self.writeReviewParameters = res.response.model;
                    var model = {
                        role: self.role,
                        getTitle: function (key) {
                            return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                        },
                        parameters: self.writeReviewParameters
                    }
                    self.$el.find("#write-review").html(
                        _.template(WriteReviewTpl, {
                            model: model
                        }));
                    $(".rating").rating({ showClear: false, showCaption: false });
                }
            })
        },

        renderUserReviews: function () {
            var self = this;
            var qs = "?id=" + self.subscriber.subscriberId + "&type=6";
            var m = new Model();
            m.url = Config.review.userReviews + qs;
            m.fetch({
                success: function (m) {
                    self.$el.find("#user-reviews").html(
                        _.template(UserReviewsTpl, {
                            model: m.toJSON(),
                            getTitle: function (key) {
                                return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                            },
                            omit: function (r) {
                                return _.omit(r, "id", "userId", "userName", "subscriberName", "reviewedDate", "clinicOrDoctorId", "insuranceId", "hospitalId", "collegeId", "yourExperience");
                            }
                        }));
                    $(".rating").rating({
                        size: 'sm',
                        symbol: "\uf0c8 ",
                        glyphicon: false,
                        ratingClass: "rating-fa", showClear: false, showCaption: false
                    });
                }
            });
        },

        renderScoreReport: function () {
          
            var self = this;
            $.get(Config.profile.userScore + "?id=" + self.subscriber.subscriberId + "&type=6", function (resp) {
                var ele = self.$("#insurance-score-report"),
                body = ele;

                var fn_mapParams = function (data) {
                    var _d = [];
                    for (var key in data) {
                        _d.push({
                            title: Constants.REVIEW_PARAMETER_MAP.getMap(key.replace('Percentage', '')),
                            param: key
                        })
                    }

                    return _d;
                }

                var data = resp.response.model;
                data = _.omit(data, ['id', 'userId']);

                body.html(_.template(ScoreReportTpl, {
                    model: fn_mapParams(data)
                }));

                var _hcModel;
                for (var r in data) {
                    _hcModel = {
                        selector: $("#hc_" + r),
                        seriesName: r.capitalizeFirstLetter(),
                        title: r.capitalizeFirstLetter(),
                        value: [
                            { name: data[r], y: data[r] },
                            { name: (100 - data[r]), y: (100 - data[r]), dataLabels: { enabled: false, connectorWidth: 0 } }]
                    }
                    HCM.pie(_hcModel.selector, _hcModel);
                }

              ele.fadeIn();
            });
        },




        //renderScoreReportWithoutLogin: function () {
        //    alert(Config.profile.userScore);
        //    var self = this;
        //    $.get(Config.profile.userScore + "?id=" + self.subscriber.subscriberId + "&type=6", function (resp) {
        //        var ele = self.$("#insurance-score-report"),
        //        body = ele;

        //        var fn_mapParams = function (data) {
        //            var _d = [];
        //            for (var key in data) {
        //                _d.push({
        //                    title: Constants.REVIEW_PARAMETER_MAP.getMap(key.replace('Percentage', '')),
        //                    param: key
        //                })
        //            }

        //            return _d;
        //        }

        //        var data = resp.response.model;
        //        data = _.omit(data, ['id', 'userId']);

        //        body.html(_.template(ScoreReportTpl, {
        //            model: fn_mapParams(data)
        //        }));

        //        var _hcModel;
        //        for (var r in data) {
        //            _hcModel = {
        //                selector: $("#hc_" + r),
        //                seriesName: r.capitalizeFirstLetter(),
        //                title: r.capitalizeFirstLetter(),
        //                value: [
        //                    { name: data[r], y: data[r] },
        //                    { name: (100 - data[r]), y: (100 - data[r]), dataLabels: { enabled: false, connectorWidth: 0 } }]
        //            }
        //            HCM.pie(_hcModel.selector, _hcModel);
        //        }

        //        ele.fadeIn();
        //    });
        //},

        submitReview: function (e) {
            var self = this,
                   ele = $(e.currentTarget),
                   request = {},
                   parameters = {},
                   val;

            if (!app.isAuthenticated) {
                app.trigger("warning", "Please Sign In or Register.", "Authentication");
                ele.addClass("disabled");
                return;
            }
            else if (app.isAuthenticated && app.sessionManager.get('USER').subscriberType != '5') {
                app.trigger("warning", "You are not authorized to submit reviews", "Authorization");
                ele.addClass("disabled");
                return;
            }

            _.each(self.$el.find("[data-rv-param]"), function (p) {
                val = parseInt($(p).rating().val());
                val = (val == 0) ? 1 : val; //default val set to 1
                parameters[$(p).data("rv-param")] = val;
            });

            request["insurance"] = parameters;
            request.insurance.insuranceId = self.subscriber.subscriberId;
            request.insurance.userId = app.sessionManager.get('USER').userId;

            request.userType = 6;
            var reviewModel = new Model();
            reviewModel.url = Config.review.add;
            reviewModel.save(request, {
                success: function (model, resp) {
                    $(e.currentTarget).addClass('disabled');
                    app.trigger("response:success", "Your review is submitted.");
                    self.renderUserReviews();
                }
            });

            e.stopPropagation();
            return false;
        },

        contact: function (e) {
            var _sdate = new Date();
            var _edate = new Date();
            _edate.setMinutes(_sdate.getMinutes() + 60);
            var dateFormat = 'YYYY-MM-DD HH:mm',
                params = {
                    from: moment(_sdate).format(dateFormat),
                    to: moment(_edate).format(dateFormat)
                }
            var self = this,
              ele = $(e.currentTarget),


              fn_save = function (btn, parentBtn, form, entityId, entityType, entityName, callback) {
                  ele.addClass("disabled");
                  var data = self.serializeForm(form);

                  if ($.trim(data.email).length == 0 ||
                   //   $.trim(data.startTime).length == 0 ||
                      $.trim(data.mobile).length == 0 ||
                      $.trim(data.name).length == 0) { return false; }

                  var rx = /^[0-9]{10}$/;
                  if (rx.test($.trim(data.mobile)) == false) { return false; }
                  btn.text('Saving...').addClass("disabled");
                  data.entityId = entityId;
                  data.entityType = Constants.USER_ROLES_ID[entityType.toUpperCase()];
                  data.entityName = entityName;
                  data.startTime = params.to

                  var model = new Model();
                  model.url = Config.appointment.save;

                  model.save(data, {
                      success: function () {
                          parentBtn.addClass("disabled").attr("disabled", "disabled");
                          callback();
                      },
                      error: function (a) {
                          app.trigger("response:error", arguments[1].responseJSON);
                          btn.text('Save').removeClass("disabled");
                      }
                  })
              },

              fn_chkAvailability = function () {
                  $.get(Config.appointment.checkAvailability, function (res) {
                      if (res && res.response.model === true) {
                          BootstrapDialog.show({
                              message: $('<div></div>').html(_.template(AppointmentFormTpl)({hideDateTime: true})),
                              title: "Contact Agent",
                              closable: true,
                              cssClass: "appoinment-dialog",
                              data: {
                                  'entityId': self.subscriber.subscriberId,
                                  'entityType': self.subscriber.subscriberTypeText.toLowerCase(),
                                  'entityName': self.subscriber.name
                              },
                              onshown: function (dialogRef) {
                                  //$('#datetimepicker-appointment').datetimepicker({
                                  //    format: 'YYYY-MM-DD HH:mm',
                                  //    defaultDate: params.to
                                  //});
                                  $(".btn-save-appointment").on('click', function () {
                                      fn_save($(this),
                                           ele,
                                           $('.form-appointment'),
                                           dialogRef.getData('entityId'),
                                           dialogRef.getData('entityType'),
                                           dialogRef.getData('entityName'),
                                           function () {
                                               dialogRef.close();
                                           })
                                  })
                              }
                          });
                      }
                  })
              };

            fn_chkAvailability();
        },

    });

    return View;
});
