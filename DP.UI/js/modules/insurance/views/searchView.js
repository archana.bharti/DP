﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
       "core/model",
       "modules/insurance/views/searchView",
       "text!modules/insurance/views/templates/insurance.html",
       "text!modules/insurance/views/templates/search-result-item.html",
       "text!modules/insurance/views/templates/featured-carousel-item.html",
       "text!modules/search/templates/appointment.html",
       "text!modules/insurance/views/templates/insuranceResults.html",
       "moment",
       "bootstrapSelect",
       "owl-carousel",
       "jqExt"
],

   function (Config, Constants, Base, Model, InsuranceView, InsuranceTpl, SearchResultItem, FeaturedCarouselItemTpl, AppointmentFormTpl, insuranceResultsTpl, moment) {

       "use strict";

       var insuranceType = {
           "any": { value: 0, text: "Any" },
           "individual": { value: 1, text: "Individual Plans" },
           "familyFloater": { value: 2, text: "Family Floater Plans" },
           "topup": { value: 3, text: "Top up" },
           "individualAndFloater": { value: 4, text: "Individual and Floater Plans" },
           "criticalIllness": { value: 5, text: "Critical Illness" },
           "seniorCitizen": { value: 6, text: "Senior Citizen" }
       }

       var View = Base.extend({

          
           
           initialize: function ()
           {
              
               document.title = "Online Health Insurance Plans | Health Calculators - BMR and Pregnancy Due Date";
               $('meta[name=description]').attr('content', 'Find the best online health insurance plans that meet your requirements. Find out when the baby is due and the key dates for your pregnancy. ');
               var self = this;
           },
           events: {
               'change #ddl-insurnace-types': "onInsuranceDdlChange",
               'click .btn-search': "search",
               'click .btn-ComSearch': "Overallsearch",
               'click .btn-Reset': 'ResetValue',
               'click .btn-recommend': 'recommend',
               'click .btn-save-bookmark': 'bookmark',
               'keyup #bmr-age': "bmr",
               'keyup #bmr-body-height': "bmr",
               'keyup #bmr-body-weigth': "bmr",
               'keyup #pdd-first-data': "pdd"
           },
           render: function () {
               var self = this;
               var params = localStorage.getItem("searchParams");
               console.log("agye params?>>>", params);

               self.$el.html(insuranceResultsTpl, {});

               self.FGrid = $("#grid-insurnace-featured")
               self.FGridLoaded = false;
               self.NFGrid = $("#grid-insurnace-non-featured");
               self.NFGridLoaded = false;
               // self.$(".btn-search").trigger('click');    
               if(JSON.parse(localStorage.getItem("NFparams")!=null))
               {
                   console.log("not null>>>>>",JSON.parse(localStorage.getItem("NFparams")));
                   self.Overallsearch();
               }

               if (JSON.parse(localStorage.getItem("SearchNFparams") != null)) {
                   console.log("not null>>>>>", JSON.parse(localStorage.getItem("NFparams")));
                   self.search();
               }
               //setTimeout(function () { self.Overallsearch() }, 250);
               try { self.renderFeatured(); } catch (e) { console.log(e); }
           },

           onInsuranceDdlChange: function (e) {
               var self = this,
                   ele = $(e.currentTarget),
                   val = ele.val();
               if (val == insuranceType.topup.value || val == insuranceType.criticalIllness.value) {
                   self.$('.insurnace-search-options').find("input:checkbox").prop("checked", false).attr("disabled", "disabled");
                   return;
               }

               self.$('.insurnace-search-options').find("input:checkbox").removeAttr("disabled");
           },

           search: function (e) {
               var self = this,
                   planAttr = [
                        { key: "cashlessFacility", text: "Cashless Facility" },
                        { key: "maternityBenefits", text: "Maternity Benefits" },
                        { key: "ambulanceCharges", text: "Ambulance Charges" },
                        { key: "dayCareProcedures", text: "DayCare Procedures" },
                        { key: "healthCheckup", text: "Health Checkup" },
                        { key: "taxBenefits", text: "Tax Benefits" },
                        { key: "hospitalizationExpenses", text: "Hospitalization Expenses" },
                        { key: "freelookPeriod", text: "Freelook Period" },
                        { key: "alternativeTreatment", text: "Alternative Treatment" },
                        { key: "cumulativeBonus", text: "Cumulative Bonus" },
                        { key: "organDonorExpenses", text: "OrganDonor Expenses" },
                        { key: "prePostHospitalizationCharges", text: "PrePost HospitalizationCharges" },
                        { key: "portability", text: "Portability" }]

               var fnItemFormatter = function (value, row, index) {
                   var view = this,
                       trueList = [];
                   _.each(planAttr, function (attr, i) {
                       if (row[attr.key] == true) trueList.push('<label class="label label-primary" style="display:inline-block">' + attr.text.toUpperCase() + '</label>');
                   })
                   row.trueList = trueList.join(' ');
                   return _.template(SearchResultItem)(row)
               }

               //self.$("#grid-insurnace-featured").closest('.bootstrap-table').show();

               if (self.NFGridLoaded == true) {
                   self.NFGrid.bootstrapTable('refresh', { query: self.getFilters(false) });
               }
               else {
                   self.NFGrid.bootstrapTable({
                       url: Config.search.insurance + '?ip=' + window.IP,
                       sidePagination: 'server',
                       method: "post",
                       cache: false,
                       pagination: true,
                       pageSize: 5,
                       showLoading: true,
                       clickToSelect: false,
                       sortable: false,
                       showHeader: false,
                       showPaginationSwitch: false,
                       queryParams: function (params) {
                           // $.extend(params, self.getFilters(false));
                           var params1 = {};
                           params1 = JSON.parse(localStorage.getItem("SearchNFparams"));

                           params1.limit = params.limit;
                           params1.offset = params.offset;

                           params1.order = params.order;
                           params1.sort = params.sort;
                           console.log("inside Search queryParams >>>>>", params1);
                         


                          
                           return params1;

                          // return params;
                       },
                       columns: [
                           {
                               field: 'providerName',
                               title: 'Provider',
                               valign: 'middle',
                               align: 'left',
                               formatter: fnItemFormatter
                           }]
                   });

                   self.NFGrid.on("load-success.bs.table", function () {
                       self.NFGridLoaded = true;
                   })
               }

               if (self.FGridLoaded == true) {
                   self.FGrid.bootstrapTable('refresh', { query: self.getFilters(true) });
               }
               else {
                   self.FGrid.bootstrapTable({
                       url: Config.search.insurance + '?ip=' + window.IP,
                       sidePagination: 'server',
                       method: "post",
                       cache: false,
                       pagination: false,
                       showLoading: true,
                       clickToSelect: false,
                       sortable: false,
                       showHeader: false,
                       showPaginationSwitch: false,
                       queryParams: function (params) {
                          // $.extend(params, self.getFilters(true));
                           var params1 = {};
                           params1 = JSON.parse(localStorage.getItem("SearchNFparams"));
                           params1.insurance.isFeatured = true;
                           params1.limit = params.limit;
                           params1.offset = params.offset;

                           params1.order = params.order;
                           params1.sort = params.sort;
                           console.log("inside Search features true >>>>>", params);



                           return params1;
                       },
                       columns: [
                           {
                               field: 'providerName',
                               title: 'Provider',
                               valign: 'middle',
                               align: 'left',
                               formatter: fnItemFormatter
                           }]
                   });
                   self.FGrid.on("load-success.bs.table", function () {
                       self.FGridLoaded = true;
                       var rows = $(this).bootstrapTable('getData');
                       if (rows.length == 0) {
                           self.$("#grid-insurnace-featured").closest('.bootstrap-table').hide();
                       }
                   })
               }

               $(".panel-footer").show();

           },

           Overallsearch: function (e) {
            
          
               var self = this,
                   planAttr = [
                        { key: "cashlessFacility", text: "Cashless Facility" },
                        { key: "maternityBenefits", text: "Maternity Benefits" },
                        { key: "ambulanceCharges", text: "Ambulance Charges" },
                        { key: "dayCareProcedures", text: "DayCare Procedures" },
                        { key: "healthCheckup", text: "Health Checkup" },
                        { key: "taxBenefits", text: "Tax Benefits" },
                        { key: "hospitalizationExpenses", text: "Hospitalization Expenses" },
                        { key: "freelookPeriod", text: "Freelook Period" },
                        { key: "alternativeTreatment", text: "Alternative Treatment" },
                        { key: "cumulativeBonus", text: "Cumulative Bonus" },
                        { key: "organDonorExpenses", text: "OrganDonor Expenses" },
                        { key: "prePostHospitalizationCharges", text: "PrePost HospitalizationCharges" },
                        { key: "portability", text: "Portability" }]


              

               var fnItemFormatter = function (value, row, index) {
                   var view = this,
                       trueList = [];

                   row.trueList = trueList.join(' ');
                   return _.template(SearchResultItem)(row)
               }
              
               self.$("#grid-insurnace-featured").closest('.bootstrap-table').show();

               if (self.NFGridLoaded == true) {
                   self.NFGrid.bootstrapTable('refresh', { query: self.getConsolatedFilters(false) });
               }

               else {
                   self.NFGrid.bootstrapTable({
                       url: Config.search.ConsolatedSearch,
                       sidePagination: 'server',
                       method: "post",
                       cache: false,
                       pagination: true,
                       pageSize: 5,
                       showLoading: true,
                       clickToSelect: false,
                       sortable: false,
                       showHeader: false,
                       showPaginationSwitch: false,
                       queryParams: function (params) {
                           var params1 = {};
                           params1 = JSON.parse(localStorage.getItem("NFparams"));

                          params1.limit = params.limit;
                           params1.offset = params.offset;

                           params1.order = params.order;
                           params1.sort = params.sort;
                           console.log("searchParamsFeaturedFalse + Upper", params);


                          
                           return params1;
                       },
                       columns: [
                           {
                               field: 'providerName',
                               title: 'Provider',
                               valign: 'middle',
                               align: 'left',
                               formatter: fnItemFormatter
                           }]
                   });

                   self.NFGrid.on("load-success.bs.table", function () {

                       self.NFGridLoaded = true;
                   })
               }

               //// Code removed due to stop filterination on bases of featured and non featured///

               //if (self.FGridLoaded == true) {
               //    self.FGrid.bootstrapTable('refresh', { query: self.getConsolatedFilters(true) });
               //}
               //else {
               //    self.FGrid.bootstrapTable({
               //        url: Config.search.ConsolatedSearch,
               //        sidePagination: 'server',
               //        method: "post",
               //        cache: false,
               //        pagination: false,
               //        showLoading: true,
               //        clickToSelect: false,
               //        sortable: false,
               //        showHeader: false,
               //        showPaginationSwitch: false,
               //        queryParams: function (params) {

               //            var params1 = {};
               //            params1 = JSON.parse(localStorage.getItem("Second"));
               //            params1.insurance.isFeatured = true;
               //            console.log("searchParamsFeaturedTalse + Lowwer", params1);
               //            return params1;

               //        },
               //        columns: [
               //            {
               //                field: 'providerName',
               //                title: 'Provider',
               //                valign: 'middle',
               //                align: 'left',
               //                formatter: fnItemFormatter
               //            }]
               //    });
               //    self.FGrid.on("load-success.bs.table", function () {
               //        self.FGridLoaded = true;
               //        var rows = $(this).bootstrapTable('getData');
               //        if (rows.length == 0) {
               //            self.$("#grid-insurnace-featured").closest('.bootstrap-table').hide();
               //        }
               //    })
               //}

               $(".panel-footer").show();
               //app.router.navigate('insurance/results', { trigger: true });

           },
           ResetValue: function (e) {

               $("#consolatedSearch").val('');

           },

           getFilters: function (isFeatured) {
               var self = this,
                   data = self.serializeForm($('#form-insurance')),
                   lenghtOfSelections = $('#form-insurance').find(":checkbox:checked").length;

               for (var key in data) {
                   if (data[key] == "on") {
                       data[key] = 1;
                   }
                   else if (data[key] == "off") {
                       data[key] = 0;
                   }
               }
               data.planType = _.filter(insuranceType, { value: parseInt($("#ddl-insurnace-types").val()) })[0].text
               data.isFeatured = isFeatured;
               return { insurance: data };
           },

           getConsolatedFilters: function (isFeatured) {
               var self = this,
                   data = self.serializeForm($('#form-insurance')),
                   lenghtOfSelections = $('#form-insurance').find(":checkbox:checked").length;

               for (var key in data) {
                   if (data[key] == "on") {
                       data[key] = 1;
                   }
                   else if (data[key] == "off") {
                       data[key] = 0;
                   }
               }
               data.planType = _.filter(insuranceType, { value: parseInt($("#ddl-insurnace-types").val()) })[0].text
               data.isFeatured = isFeatured;
               return { insurance: data };
           },

           recommend: function (e) {
               var self = this,
                    ele = $(e.currentTarget),
                    spnRm = ele.closest(".insurance-result-item").find(".spnRecommendCount"),
                    eid = ele.data("entityId"),
                    request = { entityId: eid, entityType: 6, remove: false, ipAddress: window.IP },
                    cnt = parseInt(spnRm.text().trim()),
                    model = new Model();

               model.url = Config.recommend.addOrUpdate;
               request.remove = false;


               model.save(request, {
                   success: function () {
                       spnRm.text(cnt + 1);
                       ele.replaceWith('<i class="fa fa-thumbs-up fa-lg clr-green"></i>');
                   }
               })
           },

           bookmark: function (e) {

               var self = this,
                   ele = $(e.currentTarget);
               if (app.isAuthenticated) {
                   var request = {
                       entityId: app.sessionManager.get('USER').subscriberId,
                       entityType: app.sessionManager.get('USER').subscriberType,
                       bookmarkEntityId: $(e.currentTarget).data('user-id'),
                       bookmarkEntityType: '6'
                   };
                   var saveContactModel = new Model();
                   saveContactModel.url = Config.profile.bookmarkProfile;
                   saveContactModel.save(request, {
                       success: function (model, resp) {
                           $(e.currentTarget).text('Saved');
                       }
                   });
                   ele.addClass("disabled");
               }
           },

           renderFeatured: function () {
               var m = new Model();
               var fnCarousel = function (flist) {
                   var mod = flist.length % 5, i = 0;
                   var splits = [], size = 5;

                   while (flist.length > 0)
                       splits.push(flist.splice(0, size));

                   var parent = self.$('.carousel-featured-items'), owlWrapper, firstChild, owlItemStyle;

                   $.each(splits, function (i, d) {
                       self.$('.carousel-featured-items').append(_.template(FeaturedCarouselItemTpl, {
                           item: d
                       }))
                   });


                   $(".carousel-featured-items").owlCarousel({
                       navigation: true,
                       slideSpeed: 40,
                       paginationSpeed: 55,
                       singleItem: true,
                       autoPlay: Config.autoPlay,
                       stopOnHover: true,
                       pagination: false,
                       autoHeight: true,
                       navigationText: ["<i class='fa fa-backward' />", "<i class='fa fa-forward' />"],
                       transitionStyle: "backSlide"
                   });
               }

               m.url = Config.insurance.featured;
               m.fetch({
                   success: function () {
                       fnCarousel(arguments[1].response.model)
                   }
               })
           },

           bmr: function () {
               var self = this,
                   height = $('#bmr-body-height').val(),
                   weight = $('#bmr-body-weigth').val(),
                   age = $('#bmr-age').val(),
                   res = $('#bmr-result'),
                   gender = $("[name='bmr-gender']:checked").val(),
                   rx = new RegExp(/^[\d]+$/);

               if (age.length == 0) {
                   res.val(0);
               }

               if (height.length > 0 && weight.length > 0 && age.length > 0 &&
                  rx.test(height) == true && rx.test(weight) == true && rx.test(age) == true) {
                   switch (gender) {
                       case 'm':
                           var r = 66 + (13.7 * parseFloat(weight)) + (5 * parseFloat(height)) - (6.8 * parseFloat(age));
                           res.val(r.toFixed(2));
                           break;
                       case 'f':
                           var r = 655 + (9.6 * parseFloat(weight)) + (1.8 * parseFloat(height)) - (4.7 * parseFloat(age));
                           res.val(r.toFixed(2));
                           break;
                   }
               }
           },

           pdd: function (e) {
               var self = this,
                   ele = $(e.currentTarget),
                   val = ele.val(),
                   res = $('#pdd-pregnancy-duedata');
               res.val('');

               var fn_addDays = function (date, days) {
                   date.setDate(date.getDate() + parseInt(days));
                   return date.toDateString();
               };

               try {
                   if (val.length == 5) {
                       var split = val.split('-'),
                           date = split[0],
                           month = split[1],
                           year = new Date().getFullYear(),
                           rx = /^[0-9]{2}[-][0-9]{2}$/;
                       if (rx.test(val)) {
                           var _d = new Date(year + "-" + month + "-" + date);
                           res.val(fn_addDays(_d, 280));
                       }
                   }
               }
               catch (e) {
                   console.log(e);
                   res.val('Not a valid date-month');
               }
           },

           contact: function (e) {
               var _sdate = new Date();
               var _edate = new Date();
               _edate.setMinutes(_sdate.getMinutes() + 60);
               var dateFormat = 'YYYY-MM-DD HH:mm',
                   params = {
                       from: moment(_sdate).format(dateFormat),
                       to: moment(_edate).format(dateFormat)
                   }
               var self = this,
                 ele = $(e.currentTarget),


                 fn_save = function (btn, parentBtn, form, entityId, entityType, entityName, callback) {
                     ele.addClass("disabled");
                     var data = self.serializeForm(form);

                     if ($.trim(data.email).length == 0 ||
                         //   $.trim(data.startTime).length == 0 ||
                         $.trim(data.mobile).length == 0 ||
                         $.trim(data.name).length == 0) { return false; }

                     var rx = /^[0-9]{10}$/;
                     if (rx.test($.trim(data.mobile)) == false) { return false; }
                     btn.text('Saving...').addClass("disabled");
                     data.entityId = entityId;
                     data.entityType = Constants.USER_ROLES_ID[entityType.toUpperCase()];
                     data.entityName = entityName;
                     data.startTime = params.to

                     var model = new Model();
                     model.url = Config.appointment.save;

                     model.save(data, {
                         success: function () {
                             parentBtn.addClass("disabled").attr("disabled", "disabled");
                             callback();
                         },
                         error: function (a) {
                             app.trigger("response:error", arguments[1].responseJSON);
                             btn.text('Save').removeClass("disabled");
                         }
                     })
                 },

                 fn_chkAvailability = function () {
                     $.get(Config.appointment.checkAvailability, function (res) {
                         if (res && res.response.model === true) {
                             BootstrapDialog.show({
                                 message: $('<div></div>').html(_.template(AppointmentFormTpl)({ hideDateTime: true })),
                                 title: "Contact Agent",
                                 closable: true,
                                 cssClass: "appoinment-dialog",
                                 data: {
                                     'entityId': self.subscriber.subscriberId,
                                     'entityType': self.subscriber.subscriberTypeText.toLowerCase(),
                                     'entityName': self.subscriber.name
                                 },
                                 onshown: function (dialogRef) {
                                     //$('#datetimepicker-appointment').datetimepicker({
                                     //    format: 'YYYY-MM-DD HH:mm',
                                     //    defaultDate: params.to
                                     //});
                                     $(".btn-save-appointment").on('click', function () {
                                         fn_save($(this),
                                              ele,
                                              $('.form-appointment'),
                                              dialogRef.getData('entityId'),
                                              dialogRef.getData('entityType'),
                                              dialogRef.getData('entityName'),
                                              function () {
                                                  dialogRef.close();
                                              })
                                     })
                                 }
                             });
                         }
                     })
                 };

               fn_chkAvailability();
           },

       });

       return View;

   });