define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/insurance/views/view",
     "modules/insurance/views/searchView"
], function (
    Config,
    Constants,
    Controller,
    InsuranceView,
    InsuranceSearchView
) {

    "use strict";
 
    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        insurance: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
                view = new InsuranceView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().render();
          
        },
        insuranceResults: function ()
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
                    var self = this,
                        view = new InsuranceSearchView({ controller: self, el: Constants.CONTENT });
                    view.clear().render();
        }
    });

    return Controller;

});
