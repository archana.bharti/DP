﻿define([
       "apiConfig",
       "constantsConfig",
       "modules/user/views/base",
       "core/model",
       "text!modules/user/templates/qualifications.html",
], function (Config, Constants, Base, Model, QualifsTpl) {

    "use strict";

    var View = Base.extend({
        initialize: function (obj) {
            var self = this;
            self.parent = obj.view;
            self.subscriber = obj.subscriber;
            self.$el = obj.container;
            self.allLongShort = new Model();
            self.allLongShort.url = Config.profile.qualifications.allLongShort;

            self.allUserLongShort = new Model();
            self.allUserLongShort.url = Config.profile.qualifications.allUserLongShort + self.subscriber.subscriberId;
        },
        events :{
            "click [data-long-qualif-id]": "updateLongQualif",
            "click [data-short-qualif-id]": "updateShortQualif",
            "click .btn-addAsNewLongQualif": "addAsNewLongQualif",
            "click .btn-addAsNewShortQualif": "addAsNewShortQualif"
        },

        render: function () {
            var self = this;
            var allowToUpdate = self.subscriber.account;
            if (!allowToUpdate) { self.$el.find('.user-qualifications').remove(); return;}
            $.when(self.allLongShort.fetch(), self.allUserLongShort.fetch()).done(function (r1, r2) {
                self.$el.find('.user-qualifications').html(_.template(QualifsTpl, {
                    model: {
                        allowToUpdate: allowToUpdate,
                        allLongShort: self.allLongShort.toJSON(),
                        allUserLongShort: self.allUserLongShort.toJSON(),
                        hasLongQualifcation: function (id) {
                            return _.find(self.allUserLongShort.toJSON().longQualifications, { id: id }) != undefined;
                        },
                        hasShortQualifcation: function (id) {
                            return _.find(self.allUserLongShort.toJSON().shortQualifications, { id: id }) != undefined;
                        }
                    }
                }));
                self.sortByCheck($('.longQualif-list'));
                self.sortByCheck($('.shortQualif-list'))
            })
        },

        sortByCheck : function(listContainer){
            var checkdListed = listContainer.find('input:checked').closest('li');
            listContainer.prepend(checkdListed);
        },

        sortByunCheck: function (listContainer, eleToMovetoIndex) {
            var index = eleToMovetoIndex.data('index');
            var prevSiblg = listContainer.find("input[data-index='" + (index - 1) + "']").closest('li');
            eleToMovetoIndex.closest('li').insertAfter(prevSiblg);
        },

        addAsNewLongQualif : function(e){
            var self = this,
                ele = $(e.currentTarget),
                container = ele.closest('.panel-addas-new-qualif'),
                isLongQualif = container.find("input:checked").val() == 0,
                qualif = $.trim($('#txt-new-qualif').val()),
                url = Config.profile.qualifications.addLong,
                model = new Model();

            if (qualif.length == 0) { return;}
            if (!isLongQualif) {
                url = Config.profile.qualifications.addShort;
            }

            var params = {
                LongEndDegree: qualif,
                ShortEndDegree: qualif
            }
            model.url = url;
            model.save(params, {
                success: function (res) {
                    var qualifId = res.toJSON().id;
                    var m = new Model();
                    m.url = Config.profile.qualifications.addQualif;
                    if (isLongQualif) {
                        params = {
                            doctorQualification: {
                                longQualificationId: qualifId
                            }
                        }
                    }
                    else {
                        params = {
                            doctorQualification: {
                                shortQualificationId: qualifId
                            }
                        }
                    }

                    m.save(params, {
                        success: function () {
                            self.render();
                        }
                    })
                }
            })
        },

        addAsNewShortQualif: function (e) {
            this.render();
        },

        updateLongQualif: function (e) {
            var ele = $(e.currentTarget),
                li = ele.closest("li"),
                self = this,
                m = new Model(),
                params = {},
                qualifId =  _.find(self.allLongShort.toJSON().longQualifications, { id: ele.data('longQualifId') }).id

            if (ele.prop("checked") == true) {
                params = {
                    doctorQualification : {
                        longQualificationId: qualifId
                    }
                }
                m.url = Config.profile.qualifications.addQualif;
                m.save(params, {
                    success: function (res) {
                        self.sortByCheck($('.longQualif-list'))
                    }
                });
            }
            else {
                params = {
                    doctorQualification: {
                        longQualificationId: qualifId
                    }
                }
                m.url = Config.profile.qualifications.removeQualif;
                m.save(params, {
                    success: function (res) {
                        self.sortByunCheck($('.longQualif-list'), ele)
                    }
                });
            }
        },

        updateShortQualif: function (e) {
            var ele = $(e.currentTarget),
                li = ele.closest("li"),
                self = this,
                m = new Model(),
                params = {},
                qualifId =  _.find(self.allLongShort.toJSON().shortQualifications, { id: ele.data('shortQualifId') }).id;

            if (ele.prop("checked") == true) {
                params = {
                    doctorQualification: {
                        shortQualificationId: qualifId
                    }
                }
                m.url = Config.profile.qualifications.addQualif;
                m.save(params, {
                    success: function (res) {
                        self.sortByCheck($('.shortQualif-list'))
                    }
                });
            }
            else {
                params = {
                    doctorQualification: {
                        shortQualificationId: qualifId
                    }
                }
                m.url = Config.profile.qualifications.removeQualif;
                m.save(params, {
                    success: function (res) {
                        self.sortByunCheck($('.shortQualif-list'), ele)
                    }
                });
            }
        }

    })

    return View;
});