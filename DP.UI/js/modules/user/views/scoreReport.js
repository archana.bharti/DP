﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/category-score-report.html",
        "hcm"
    ],

    function (Config, Constants, Base, Model, ScoreReportTpl, HCM) {

        "use strict";

        var View = Base.extend({

            initialize: function (obj) {
                var self = this
                self.parent = obj.view;
                self.subscriber = obj.subscriber;
                self.$el = obj.container;
                self.scoreModel = new Model();
            },

            events : {

            },

            render: function () {
                var self = this;
             console.log("inside score report");
              // alert("self.subscriber.subscriberType ", self.subscriber.subscriberType);
                self.scoreModel.url = Config.profile.userScore + '/' + self.subscriber.subscriberId + '?type=' + self.subscriber.subscriberType;
                console.log("inside score report", self.scoreModel.url);
                var fn_mapParams = function (data) {
                    var _d = [];
                    for (var key in data) {
                        _d.push({
                            title: Constants.REVIEW_PARAMETER_MAP.getMap(key.replace('Percentage', '')),
                            param :  key
                        })
                    }

                    return _d;
                }

                self.scoreModel.fetch({
                    success: function (model, resp) {
                        var data = resp.response.model;
                        data = _.omit(data, ['id', 'userId']);
                        self.$el.find('.user-score-report').html(_.template(ScoreReportTpl, {
                            model: fn_mapParams(data)
                        }));
                        var _hcModel;
                        for (var r in data) {
                            _hcModel = {
                                selector: $("#hc_" + r),
                                seriesName: r.capitalizeFirstLetter(),
                                title: r.capitalizeFirstLetter(),
                                value: [
                                    { name: data[r], y: data[r] },
                                    { name: (100 - data[r]), y: (100 - data[r]), dataLabels: { enabled: false, connectorWidth: 0 } }]
                            }
                            HCM.pie(_hcModel.selector, _hcModel);
                        }
                    }
                });
            }
        });

        return View;

    });
