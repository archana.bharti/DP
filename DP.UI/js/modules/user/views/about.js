﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/profile.html",
        "text!modules/user/templates/change-password.html",
        "text!modules/user/templates/upload-image.html",
        "text!modules/search/templates/appointment.html",
        "bootstrap-dialog",
        "bootstrapValidator"
    ],

    function (Config, Constants, Base, Model, ProfileTpl, ChangePasswordTpl, UploadImageTpl, AppointmentFormTpl, BootstrapDialog) {

        "use strict";


        var ChangePasswordView = Base.extend({

            template: _.template(ChangePasswordTpl),

            tagName : "div",

            initialize: function (options) {
                this.dialogRef = options.dialogRef
            },

            events: {
                'click .btn-change-password' : "changePassword"
            },
            render: function () {
                var self = this;
                var _initFormValidator = function () {
                    $('#form-change-password').bootstrapValidator({
                        live: 'disabled',
                        submitButtons: 'button[type="submit"]',
                        fields: {
                            oldPassword: {
                                validators: {
                                    notEmpty: {
                                        message: 'Password should not be empty.'
                                    },
                                    stringLength: {
                                        min: 6,
                                        message: 'Password must be of 6 or more characters'
                                    },
                                    callback: {
                                        message: 'Old password and new password should not be same',
                                        callback: function (value, validator, $field) {
                                            var _oldp = $field.val();
                                            var _newP = $("[name='newPassword']").val();
                                            if (btoa(_newP) === btoa(_oldp)) {
                                                return {
                                                    valid: false,       // or true
                                                    message: 'Old password and new password should not be same'
                                                }
                                            }

                                            return {
                                                valid:  true
                                            }
                                        }
                                    }
                                }
                            },
                            newPassword: {
                                validators: {
                                    notEmpty: {
                                        message: 'New password should not be empty.'
                                    },
                                    stringLength: {
                                        min: 6,
                                        message: 'Password must be of 6 or more characters'
                                    },
                                    identical: {
                                        field: 'newPassword2',
                                        message: 'The new password and password entered again are not identical.'
                                    }
                                }
                            },
                            newPassword2: {
                                validators: {
                                    notEmpty: {
                                        message: 'Confirm password again for security'
                                    },
                                    stringLength: {
                                        min: 6,
                                        message: 'Password must be of 6 or more characters'
                                    },
                                    identical: {
                                        field: 'newPassword',
                                        message: 'The new password and password entered again are not identical.'
                                    }
                                }
                            }
                        }
                    })
                }
                self.$el.html(self.template(self.model));
                _initFormValidator();
                return this;
            },

            changePassword: function (e) {
                var self = this, ele = $(e.currentTarget), form = $('#form-change-password')
                var _validateForm = function () {
                    form.data("bootstrapValidator").validate();
                    return $('#form-change-password').data("bootstrapValidator").isValid();
                },
                _resetForm = function () {
                    form.data("bootstrapValidator").resetForm();
                },
                _eleDisable = function () {
                    ele.addClass("disabled").text("Saving...");
                },
                _eleEnable = function () {
                    ele.removeClass("disabled").text("Submit");
                }

                if (_validateForm()) {
                   
                    var data = self.serializeForm(form);
                    self.freezeForm(form);
                    _eleDisable();

                    data.oldPassword = btoa(data.oldPassword);
                    data.newPassword = btoa(data.newPassword);
                    delete data.newPassword2;

                    $.post(Config.security.changePassword, data, function (res) {
                        app.trigger("response:success", "Password changed successfully.");
                        self.dialogRef.close();
                        self.remove();
                    }).error(function () {
                        _eleEnable();
                        self.unfreezeForm(form);
                        _resetForm();
                    })
                }

                return false;
            }
        });

        var UploadImageView = Base.extend({

            template: _.template(UploadImageTpl),

            tagName: "div",

            initialize: function (options) {
                this.dialogRef = options.dialogRef;
                this.picNo = options.dialogRef.getData("picNo");
                this.parentView = options.parentView;
            },

            events: {
                'click .btn-upload-image': "uploadImage"
            },
            render: function () {
                var self = this;
                self.$el.html(self.template());
                return this;
            },
            uploadImage: function (e) {
                var self = this,
                    form = self.$('#form-user-image-upload'),
                    ele = $(e.currentTarget),
                    pbar = form.find('.progress');

                ele.addClass("disabled").text("Saving...");
                self.imageUpload(
                               { id: 0, pictureNo : self.picNo },
                               form.find('input[type="file"]'),
                               Config.profile.uploadImage,
                               pbar,
                               function (res, data) {
                                   self.parentView.$('.user-img')
                                       .find('.pic' + self.picNo)
                                       .attr("src", "")
                                       .attr("src", data.response.model + "?nocache=1")
                                   self.dialogRef.close();
                               });
                pbar.show();
            }
        });

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.parent = obj.view;
                self.subscriber = obj.subscriber;
                self.$el = obj.container;
                self.roleId = self.subscriber.subscriberType;
                self.profileInfo = new Model();
                self.changePasswordView = null;
                self.uploadImageView = null;
                self.displayPhones = [];
                self.stdCodesModel = [];
                console.log("About View Loaded");
            },

            events : {
                "click .edit-profile": "showForm",
                "click .btn-cancel-user-profile": "hideForm",
                'click .btn-save-user-profile': "save",
                'click .btn-change-password-init': "showChangePassword",
                'click .img-edit-icon': "showUploadImage",
                'click .chkbox_displayPhone': "updateDisplayPhone",
                'click .btn-bookmark': "bookmark",
                'click .btn-contact': "contact",
                'click .img-fullview': "imageFullView",
                'keyup [data-validate-phone]': "validatePhone",
                'click .btn-share-facebook': "shareOnFb"
            },

            render: function () {
                var self = this;
                self.renderProfile(self.subscriber);

                if(app.sessionManager.get("STD-CODES") == null){
                    $.get(Config.enums.stdCodes, function (res) {
                        app.sessionManager.set("STD-CODES", res.response.model);
                        self.renderStdCodes(res.response.model);
                    })
                } else {
                    self.renderStdCodes(app.sessionManager.get("STD-CODES"));
                }

                //FB INIT
                FB.init({
                    appId: Constants.FACEBOOK_APP_ID,
                    status: true,
                    cookie: true,
                    xfbml: true,
                    version: 'v2.6'
                });

                //GA
                ga('send', 'pageview');
            },

            renderStdCodes : function(data){
                var stdDll = $(".ddl-std");
                _.each(stdDll, function (s) {
                    s = $(s);
                    _.each(data, function (item) {
                        s.append($("<option></option>").attr("value", item.value).text([item.value,item.text].join('-')));
                    })
                })
            },

            showChangePassword : function(){
                var self = this;
                if (self.changePasswordView != null) self.changePasswordView.remove();
                BootstrapDialog.show({
                    title: 'Change Password',
                    message: '<div id="change-password-view"></div>',
                    closeByBackdrop: false,
                    onshown: function (dialogRef) {
                        self.changePasswordView = new ChangePasswordView($.extend({ el: '#change-password-view', parentView: self, dialogRef: dialogRef }));
                        self.changePasswordView.render();
                    },
                });
            },

            showUploadImage : function(e){
                var self = this, picno = $(e.currentTarget).attr("data-profile-pic");
                if (self.uploadImageView != null) self.uploadImageView.remove();
                BootstrapDialog.show({
                    title: 'Profile Image',
                    message: '<div id="upload-image-view"></div>',
                    closeByBackdrop: false,
                    data: {
                        'picNo': picno
                    },
                    onshown: function (dialogRef) {
                        self.uploadImageView = new UploadImageView($.extend({ el: '#upload-image-view', parentView: self, dialogRef: dialogRef }));
                        self.uploadImageView.render();
                    },
                });
            },

            renderProfile: function (model)
            {
              
                var self = this;                
                var fnGetQualifications = function (arr, key) {
                    var temp = [];
                    $.each(arr, function (idx, q) {
                        temp.push(q[key]);
                    });
                    return temp.join(', ');
                }
                self.$el.find('.user-profile').html(_.template(ProfileTpl, $.extend(model,{
                    readOnly: !(self.subscriber.account),
                    roleId: self.subscriber.subscriberType,
                    longQualifications: model.longQualifications == undefined ? [] : fnGetQualifications(model.longQualifications, 'longEndDegree'),
                    shortQualifications: model.shortQualifications == undefined ? [] : fnGetQualifications(model.shortQualifications, 'shortEndDegree'),
                    email: (self.subscriber.account == true) ? self.subscriber.email : null,
                    isAccount: (self.subscriber.account == true),
                    getPhoneNumber : function(val){
                        if (val != undefined && val != '0' && val != null) {
                            return val.split('-')[1];
                        }
                    },
                    isPhoneVisible: function (p) {
                        if (this.displayPhone != null && this.displayPhone != '') {
                            var l = this.displayPhone.split(',');
                            if (l.length > 0) {
                                var f = _.contains(l, p.toString());
                                if (f == true) { self.displayPhones = _.without(self.displayPhones, parseInt(p)); self.displayPhones.push(parseInt(p)); }
                                return f;
                            }
                        }
                        return false;
                    },
                    getYouTubeUrl: function (url) {
                        if ($.trim(url).length === 0) return '';
                        url = url.replace("watch?v=", "embed/");
                        var hasEmbed = new RegExp('\\bembed\\b').test(url);
                        if (hasEmbed) return url;
                        else {
                            return "www.youtube.com/embed" + url.substring(url.lastIndexOf('/'))
                        }
                    }
                })));
                if (app.sessionManager.get("STD-CODES") != null) {
                    self.renderStdCodes(app.sessionManager.get("STD-CODES"));
                }
            },

            updateDisplayPhone: function (e)
            {
             
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.data("phone");
                if (ele.prop("checked") == true) {
                    self.displayPhones = _.without(self.displayPhones, parseInt(val));
                    self.displayPhones.push(parseInt(val));
                }
                else {
                    self.displayPhones = _.without(self.displayPhones, parseInt(val));
                }
            },

            showForm: function (e)
            {
               
                var self = this;
                self.$('.user-edit-row').show();
            },

            hideForm: function(e){
                var self = this;
                self.$('.user-edit-row').hide();
            },

            save: function (e) {
             
                e.stopPropagation();

                var self = this,
                    ele = $(e.currentTarget),
                    form = self.$(".form-user-profile");
                var request = self.serializeForm(form);
                request.subscriberType = self.subscriber.subscriberType;
                request.subscriberId = self.subscriber.subscriberId;
                request.displayPhone = self.displayPhones.join(',');
                request.phone_3 = $('.ddl-landline1').val() + '-'+ request.phone_3;
                request.phone_4 = $('.ddl-landline2').val() + '-'+ request.phone_4;
                console.log(request);
                var m = new Model({id : self.subscriber.userId})
                m.url = Config.profile.update + self.subscriber.userId;
               
                m.save(request, {
                    success: function (model, resp)
                    {
                        //subscriberType
                      
                        if (request.subscriberType == "5") {
                           
                            self.subscriber.phone_1 = request.phone_1;
                        }
                        self.subscriber.phone_1 = request.phone_1;
                        self.subscriber.displayName = request.displayName
                        self.subscriber.name = request.displayName;
                        self.$('.user-edit-row').hide();
                      
                        ///// dome by me*
                        self.renderProfile(self.subscriber);
                        //this commented code implemented by other developer
                        //self.renderProfile(resp.response.model);
                    
                      
                     
                    }
                });
                
                return false;
            },

            bookmark: function (e) {
                var self = this,
                    ele = $(e.currentTarget);
                if (app.isAuthenticated) {
                    var request = {
                        entityId: app.sessionManager.get('USER').subscriberId,
                        entityType: app.sessionManager.get('USER').subscriberType,
                        bookmarkEntityId: self.subscriber.subscriberId,
                        bookmarkEntityType: self.subscriber.subscriberType
                    };
                    var saveContactModel = new Model();
                    saveContactModel.url = Config.profile.bookmarkProfile;
                    saveContactModel.save(request, {
                        success: function (model, resp) {
                            ele.addClass("disabled").attr("disabled", "disabled");
                        },
                        error: function (a,b,c) {
                            ele.addClass("disabled").attr("disabled","disabled");
                        }
                    });
                }
            },

            contact: function (e) {
                var self = this,
                  ele = $(e.currentTarget),

                  fn_save = function (btn, parentBtn, form, entityId, entityType, entityName, callback) {
                      ele.addClass("disabled");
                      var data = self.serializeForm(form);

                      if ($.trim(data.email).length == 0 ||
                          $.trim(data.startTime).length == 0 ||
                          $.trim(data.mobile).length == 0 ||
                          $.trim(data.name).length == 0) { return false; }

                      var rx = /^[0-9]{10}$/;
                      if (rx.test($.trim(data.mobile)) == false) { return false; }
                      btn.text('Saving...').addClass("disabled");
                      data.entityId = entityId;
                      data.entityType = Constants.USER_ROLES_ID[entityType.toUpperCase()];
                      data.entityName = entityName;

                      var model = new Model();
                      model.url = Config.appointment.save;

                      model.save(data, {
                          success: function () {
                              parentBtn.addClass("disabled").attr("disabled", "disabled");
                              callback();
                          },
                          error: function (a) {
                              app.trigger("response:error", arguments[1].responseJSON);
                              btn.text('Save').removeClass("disabled");
                          }
                      })
                  },

                  fn_chkAvailability = function () {
                      $.get(Config.appointment.checkAvailability, function (res) {
                          if (res && res.response.model === true) {
                              BootstrapDialog.show({
                                  message: $('<div></div>').html(_.template(AppointmentFormTpl)({ hideDateTime: false })),
                                  title: "Appointment",
                                  closable: true,
                                  cssClass: "appoinment-dialog",
                                  data: {
                                      'entityId': self.subscriber.subscriberId,
                                      'entityType': self.subscriber.subscriberTypeText.toLowerCase(),
                                      'entityName': self.subscriber.name
                                  },
                                  onshown: function (dialogRef) {
                                      $('#datetimepicker-appointment').datetimepicker({
                                          format: 'YYYY-MM-DD HH:mm'
                                      });
                                      $(".btn-save-appointment").on('click', function () {
                                          fn_save($(this),
                                               ele,
                                               $('.form-appointment'),
                                               dialogRef.getData('entityId'),
                                               dialogRef.getData('entityType'),
                                               dialogRef.getData('entityName'),
                                               function () {
                                                   dialogRef.close();
                                               })
                                      })
                                  }
                              });
                          }
                      })
                  };

                fn_chkAvailability();
            },

            imageFullView: function (e) {
                var ele = $(e.currentTarget),
                clone = ele.clone();
               var dialog = BootstrapDialog.show({
                   title: "Gallery",
                   cssClass : "modal-dialog-image-preview",
                    message: function () { return $("<div></div>").html(clone); },
                    closeByBackdrop: true
               });
               //dialog.getModalHeader().hide();
            },

            validatePhone: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.val(),
                    validation = ele.attr("data-validate-phone");

                if (validation === 'mobile') {
                    var rx1 = /^[\d]+$/;
                    if (rx1.test(val) == false) {
                        ele.val(val.substring(0, val.length - 1));
                    }
                }
                else if (validation === 'landline') {
                    var rx1 = /^[\d -]+$/;
                    if (rx1.test(val) == false) {
                        ele.val(val.substring(0, val.length - 1));
                    }
                }
            },

            shareOnFb: function (e) {
                var ele = $(e.currentTarget),
                    self = this,
                    link = window.location.origin + '/profile/' + self.subscriber.subscriberTypeText.toLowerCase() + '/' + self.subscriber.subscriberId;
                FB.ui({
                    method: 'share',
                    href: link + "?app_id=" + Constants.FACEBOOK_APP_ID
                }, function (response) { });
            }

        });

        return View;

    });