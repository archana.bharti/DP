﻿define([
       "apiConfig",
       "constantsConfig",
       "viewBase",
], function (Config, Constants, Base) {

    var view = Base.extend({

        getOptions: function (options) {
            var self = this;
            var _opts = {}
            if (app.isAuthenticated) {
                _opts = app.sessionManager.get("USER");
                if (parseInt(options.id) === parseInt(_opts.userId) || options.id === _opts.id) {
                    _opts["readOnly"] = false;
                }
                else {
                    _opts["readOnly"] = true;
                    _opts = { userId: options.id, type: options.type, readOnly: true };
                }
            } else {
                _opts = { userId: options.id, type: options.type, readOnly: true };
            }

            return _opts;
        },

        getSectionsVisibility: function (type, accountUser) {
            var self = this;
            var isAuthenticated = app.isAuthenticated;
            var isAccountUser = accountUser;

            var _v = {
                showSummary: true,
                showReports: false,
                showSpecializations: true,
                showQualifications: true,
                allowToWriteReview: false,
                showReviews: true
            }
            switch (type.toLowerCase()) {
                case 'user':
                    _v.allowToWriteReview = (isAuthenticated === true && isAccountUser == false) ? true : false;
                    _v.showSpecializations = false;
                    _v.showQualifications = false;
                    _v.showSummary = false;
                    break;
                case 'doctor':
                    _v.showReports = (isAuthenticated === true && isAccountUser == true) ? true : false;
                    break;
                case 'clinic':
                    _v.showReports = (isAuthenticated === true && isAccountUser == true) ? true : false;
                    _v.showQualifications = false;
                    // showReports: false
                    break;
                case 'hospital':
                    _v.showReports = (isAuthenticated === true && isAccountUser == true) ? true : false;
                    _v.showQualifications = false;
                    break;
                case 'college':
                    //_v.showSpecializations = false;
                    _v.showQualifications = false;
                    break;
                case 'insurance':
                    _v.showSpecializations = false;
                    _v.showQualifications = false;
                    break;
            }
            return _v;
        }

    });
    return view;
});
