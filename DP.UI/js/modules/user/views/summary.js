﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/summary.html"
    ],

    function (Config, Constants, Base, Model, SummaryTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.parent = obj.view;
                self.subscriber = obj.subscriber;
                self.$el = obj.container;
                self.summaryInfo = new Model();
            },

            events : {
                "click .btn-edit-summary": "edit"
            },


            render: function () {
                var self = this;
                self.$el.find('.user-summary').html(_.template(SummaryTpl, {
                    summary: self.subscriber.summary,
                    showEdit: self.subscriber.account,
                    topRated : self.subscriber.topRated,
                    showTopRating: (self.subscriber.subscriberType != '5' && self.subscriber.topRated != null),
                    getRatingTitle: function (key) {
                        return Constants.REVIEW_PARAMETER_MAP.getMap(key);
                    }
                }));
                $(".rating").rating({ showClear: false, showCaption: false });
                self.$el.find(".wyswg").summernote(
                    {
                        height: 200,
                        focus: true,
                        tooltip:""
                    })
            },

            edit: function (e) {
                var self = this,
                    editPanel = $("#edit-summary"),
                    viewPanel = $("#view-summary"),
                    ele = $(e.currentTarget);
                if (viewPanel.data("toggle") == "show") {
                    editPanel.hide();
                    viewPanel.show();
                    viewPanel.data("toggle", "hide");
                    ele.find(".fa").removeClass("fa-save").addClass("fa-edit");
                    self.subscriber.summary = self.$el.find(".wyswg").summernote('code');

                    var m = new Model({ id: self.subscriber.subscriberId })
                    m.url = Config.profile.updateSummary + btoa(self.subscriber.subscriberId + ':' + self.subscriber.subscriberType);
                    m.save({ summary: self.subscriber.summary }, {
                        success: function (model, resp) {
                            self.subscriber.summary = self.$el.find(".wyswg").summernote('code');
                            self.render();
                        }
                    });
                }
                else {
                    editPanel.show();
                    viewPanel.hide();
                    viewPanel.data("toggle", "show");
                    ele.find(".fa").removeClass("fa-edit").addClass("fa-save");
                }
            }
        });

        return View;

    });