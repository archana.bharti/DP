﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "modules/user/views/base",
        "core/model",
        "text!modules/user/templates/specializations.html",
        "text!modules/user/templates/endorsements.html",
         "text!modules/user/templates/endorsers.html",
        'modules/shared/collections/specializations'
    ],

    function (Config, Constants, Base, Model, SpecializationsTpl, EndorsementsTpl, EndorsersTpl, SpecializationsColl) {

        "use strict";

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.parent = obj.view;
                self.subscriber = obj.subscriber;
                self.$el = obj.container;
                self.specializationsInfo = new Model();
                self.endorsementsInfo = new Model();
            },

            events: {

                "keyup .search-specialization": 'onSearchList',
                "click .btn-endorse": "onEndorse",
                "click a.has-endorsers": "showEndorsers",
                "click input.chkUpdateSpecialization" : "updateSpecialization"
            },

            render: function () {
                var self = this;
                self.getUserSpecializations(function (userSpzs) {
                    self.getEndorsements(function (endorsedSpzs) {
                        self.renderSpecializations(endorsedSpzs, userSpzs);
                    });
                });
            },
            

            getUserSpecializations : function(callback){
                var self= this;
                self.specializationsInfo.url = Config.profile.userSpecializations + '/' + self.subscriber.subscriberId + '?type=' + self.subscriber.subscriberType;
                self.specializationsInfo.fetch({
                    success: function (model, resp) {
                        var spz = model.toJSON();
                        _.bind(callback(spz), self);
                    }
                });
            },

            renderSpecializations: function (endorsedSpzs, userSpzs) {
                var self = this;

                var _isCurrentUserEndorsed = function (spzId, endorsement) {
                    var yes = false;
                    if (!app.isAuthenticated) { return false; }
                    else {

                        if (endorsement == undefined) { return false;}
                        var endorserType = app.sessionManager.get('USER').subscriberType,
                          endorserId = app.sessionManager.get('USER').subscriberId;
                        _.each(endorsement.endorsers, function (en) {
                            if (en.endorserId == endorserId) { //&& en.endorserType == endorserType
                                yes = true;
                                return false;
                            }
                        })
                    }
                    return yes;
                }

                var _sort = function () {
                    var hasSpz = self.$(".spz-list").find("input:checked").closest(".list-group-item");
                   
                    self.$(".spz-list").find("input:checked").closest(".list-group-item").remove();
                    hasSpz.insertAfter(self.$("li.list-group-item.header"))
                }

                var splUrl =  (self.subscriber.subscriberType.toString() == '4') ?
                                Config.enums.specializations + '?id=' + self.subscriber.subscriberId + '&type=' + self.subscriber.subscriberType :
                                Config.enums.specializations;
                $.getJSON(splUrl, function (res) {
                    var data = [],_model;
                    switch(self.subscriber.subscriberType.toString()){
                        case '1':
                        case '2':
                            _model = res.response.model.medical
                                .concat(res.response.model.dental)
                                .concat(res.response.model.alternativeMedicine)
                            break;
                        case '3':
                            _model =  res.response.model.hospital;
                            break;
                        case '4':
                            _model =  res.response.model.college;
                            break;
                    }


                    $.each(_model, function (index, item) {
                        data.push({ id: item.id, text: item.name });
                    });

                    self.$el.find('.user-specializations').html(_.template(SpecializationsTpl, {
                        model: {
                            specializations: self.subscriber.subscriberType != 4 ? data.splice(1) : data,
                            endorsements: endorsedSpzs,
                            title : self.subscriber.subscriberType == "4" ? "DEPARTMENTS & ENDORSEMENTS" : "SPECIALIZATIONS & ENDORSEMENTS",
                            showEndorsement: true,
                            allowToEndorse: self.canEndorse(),
                            allowToUpdateSpecializations: (self.subscriber.account && self.subscriber.subscriberType != 4),
                            hasSpecialization: function (spzId) {
                                var spz = _.findWhere(this.endorsements, { specializationId: parseInt(spzId) });
                                return (spz == undefined) ? false : true;
                            },
                            getEndorsements: function (spzId) {
                                var _endorsement = _.findWhere(this.endorsements, { specializationId: parseInt(spzId) });
                                return _.template(EndorsementsTpl,
                                    {
                                        model:
                                          {
                                              endorsement: _endorsement,
                                              canEndorse: this.allowToEndorse,
                                              specializationId: spzId,
                                              isCurrentUserEndorsed: _isCurrentUserEndorsed(spzId, _endorsement)
                                          }
                                    });
                            }
                        }
                    }));

                    _sort();
                })
               
            },

            getEndorsements: function(callback){
                var self = this;
                self.endorsementsInfo.url = Config.profile.userEndorsements + '?id=' + self.subscriber.subscriberId + '&type=' + self.subscriber.subscriberType;
                self.endorsementsInfo.fetch({
                    success : function(model, resp){
                       _.bind(callback(model.toJSON()), self);
                    }
                })
            },

            showEndorsers : function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    data = JSON.parse(ele.next().val());
                ele.popover({
                    content: _.template(EndorsersTpl, { model: data }),
                    title: "Endorsers",
                    animation: true,
                    container: 'body',
                    placement: 'top',
                    html:true
                }).popover('show');
            },

            onEndorse: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    parent = ele.closest("li"),
                    spzId = ele.data("specialization-id"),
                    endorserType = app.sessionManager.get('USER').subscriberType,
                    endorserId = app.sessionManager.get('USER').subscriberId,
                    endorseeId = self.subscriber.subscriberId,
                    endorseeType = self.subscriber.subscriberType,

                    specializations = [];
                    specializations.push(spzId);

                var request = {EndorserId: endorserId, EndorserType: endorserType, EndorseeId: endorseeId, EndorseeType: endorseeType, Specializations: specializations};

                var endorseModel = new Model();
                endorseModel.url = Config.profile.endorseSpecialization;
                endorseModel.save(request, {
                    success: function () {
                        ele.addClass("disabled");
                        $.when(parent.effect("highlight", {}, 100)).done(function () {
                            setTimeout(function (view) {
                                view.render();
                            }, 10, self);
                        });
                    }
                })
            },

            canEndorse: function(){
                var self = this,
                    searchRole, accountRole;

                var searchRole = self.subscriber.subscriberTypeText.toLowerCase();

                if (self.subscriber.account == true) {
                    return false;
                }

                if (app.isAuthenticated)
                    accountRole = app.sessionManager.get("USER").subscriberTypeText.toLowerCase();
                else {
                    return false;
                }

                switch (searchRole) {
                    case "doctor":
                    case "clinic":
                        if (accountRole == 'doctor' || accountRole == 'clinic')
                            return true;
                        else return false;
                    case "hospital":
                        if (accountRole == 'hospital' || accountRole == 'doctor' || accountRole == 'clinic')
                            return true;
                        else return false;
                    case "college":
                        if (accountRole == 'college' || accountRole == 'doctor' || accountRole == 'clinic')
                            return true;
                        else return false;
                }

                return false;
            },

            onSearchList: function (e)
            {
              
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.val().trim(),
                    header = self.$(".spz-list").find(".header"),
                    items = self.$(".spz-list").find(".list-group-item");
                function sentenceCase(str) {
                    if ((str === null) || (str === ''))
                        return false;
                    else
                        str = str.toString();

                    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
                }
                var val1 = sentenceCase(val)
                items.show();
                if (val.length > 0) {
                    var itemsToShow = items.find("div.checkbox:contains('" + val1 + "')").closest(".list-group-item");
                    items.not(itemsToShow).not(header).hide();
                }
            },

            updateSpecialization: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    isChecked = ele.prop("checked"),
                    parent = ele.closest("li"),
                    header = self.$(".spz-list").find(".list-group-item.header"),
                    spzId = ele.val(),
                    userId = self.subscriber.id;
                
                var m = new Model();
                if (isChecked) {
                    //add
                    var request = { specializationId: spzId, subscriberId: userId };
                    m.url = Config.profile.userSpecializations + '?type=' + self.subscriber.subscriberType;
                    m.save(request, {
                        success: function () {
                            parent.insertAfter(header)
                        }
                    });
                }
                else {
                    //destroy
                    m.url = Config.profile.userSpecializations + '/' + spzId + '?type=' + self.subscriber.subscriberType;
                    m.set("id", spzId);
                    m.destroy({
                        success: function () {
                        }
                    });
                }
            }
        });

        return View;

    });