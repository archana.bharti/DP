define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/user/views/view",
    "modules/user/views/savedProfiles"
], function (
    Config,
    Constants,
    Controller,
    UserView,
    SavedProfiles
) {

    "use strict";

  
    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        profile: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
              
             view = new UserView({ controller: self, el: Constants.CONTENT, args: args });
           
            //view.clear();//.render();
            //view.clear().render();
        },

        bookmarks: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
                view = new SavedProfiles({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().render();
        },

        editProfile: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
                view = new UserView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().renderEditProfile();
        }
    });

    return Controller;

});
