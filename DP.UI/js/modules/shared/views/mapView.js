define([
        "apiConfig",
        "viewBase",
        'core/model'
        //'async!http://maps.google.com/maps/api/js?sensor=false'
    ],

    function (Config,
              Base,
              Model) {

        "use strict";

        var View = Base.extend({
            _map: null,

            render: function(markersArr){
                var self = this;
                var bounds = new google.maps.LatLngBounds();
                this.$el.css('width','100%').css('height','700px');

                var locations = [];
                for(var i = 0; i < markersArr.length; i++){
                    locations.push([markersArr[i].name, markersArr[i].latitude, markersArr[i].longitude, markersArr[i].isFeatured])
                }

                // Setup the different icons and shadows
                var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

                var icons = [
                    iconURLPrefix + 'red-dot.png',
                    iconURLPrefix + 'blue-dot.png',
                ]
                var iconsLength = icons.length;

                var map = new google.maps.Map(this.el, {
                    zoom: 11,
                    center: new window.google.maps.LatLng(12.9255, 77.5874),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                self.infowindow = new google.maps.InfoWindow({
                    maxWidth: 160
                });

                self.markers = new Array();

                var iconCounter = 0;

                // Add the markers and infowindows to the map
                for (var i = 0; i < locations.length; i++) {
                    var iconPath = (locations[i][3]) ? icons[1] : icons[0];
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        icon: iconPath
                    });

                    self.markers.push(marker);

                    google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                        return function() {
                            self.infowindow.setContent(locations[i][0]);
                            self.infowindow.open(map, marker);
                        }
                    })(marker, i));

                    iconCounter++;
                    // We only have a limited number of possible icon colors, so we may have to restart the counter
                    if(iconCounter >= iconsLength) {
                        iconCounter = 0;
                    }1
                }

                function autoCenter() {
                    //  Create a new viewpoint bound
                    var bounds = new google.maps.LatLngBounds();
                    //  Go through each...
                    for (var i = 0; i < self.markers.length; i++) {
                        bounds.extend(self.markers[i].position);
                    }
                    //  Fit these bounds to the map
                    //map.fitBounds(bounds);
                    self.pos = bounds.getCenter();
                    ///map.setCenter(new window.google.maps.LatLng(self.pos.J, self.pos.M));
                }
                google.maps.event.addListenerOnce(map, 'idle', function() {
                    google.maps.event.trigger(map, 'resize');
                });
                self.map = map;
                autoCenter();
                return this;

            },

            getLocationDetails: function(address, isFeatured){
                var self = this;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode( { 'address': address + ',Bangalore'}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK)
                    {
                        var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

                        var icons = [
                            iconURLPrefix + 'red-dot.png',
                            iconURLPrefix + 'blue-dot.png',
                        ]
                        var latLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                        var marker = new google.maps.Marker({
                            position: latLng,
                            map: self.map,
                            icon: (isFeatured) ? icons[1] : icons[0]
                        });
                        google.maps.event.addListener(marker, 'mouseover', function() {
                            self.infowindow.setContent(address);
                            self.infowindow.open(self.map,marker);
                        });
                        new google.maps.event.trigger( marker, 'mouseover' );
                        self.markers.push(marker);
                    }
            });
        }
        });

        return View;

    });