define([
        "apiConfig",
        'core/collection',
        'modules/shared/models/textValue'],
    function (
        Config,
        Base,
        TextValueModel) {
        "use strict";
        var Collection = Base.extend({
            model: TextValueModel,
            url: Config.enums.specializations
        });

        return Collection;
    });
