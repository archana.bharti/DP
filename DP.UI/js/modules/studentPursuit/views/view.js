﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "modules/studentPursuit/views/map/view",
        "text!modules/studentPursuit/views/templates/main.html",
        "text!modules/studentPursuit/views/templates/college-features-item.html",
        "bootstrapSelect",
        "owl-carousel"
    ],

    function (Config, Constants, Base, Model, MapView, MainTpl, FeaturesCarouselItemTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function () {
               
                document.title = "Find Best Medical & Dental Colleges in India";
                $('meta[name=description]').attr('content', 'Find the best medical colleges in India by the state. Find valuable information like contact info, reviews based on different factors, departments in the college and more.');
                var self = this;
            },

            events : {
                'click .btn-find' : "find"
            },

            render: function () {
                var self = this;
                self.$el.html(_.template(MainTpl, {

                }));
                self.initializeNameSelect();
                self.loadFeatures();
                self.renderIndiaMap();
            },

            initializeNameSelect: function(){
                var self = this;
                var _formatUser = function (user) {
                    if (user.loading) return user.text;
                    var $user = $(
                        '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>'
                    );
                    return $user;
                }

                var _selection = function (user) {

                    if (user.name == undefined) {
                        return '<option value="-1" selected="selected">Search name</option>';
                    }else{
                        user.name = user.name.replace('Dr.', '');
                    }

                    console.log("selected subscriber", user);

                    //select2 bug
                    if (self.selectedSubscribed && user.id == self.selectedSubscribed.id) { return '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>' }

                    self.selectedSubscribed = user;

                    return '<option id="' + user.id + '"  data-uid="' + user.id + '" data-cid="' + user.cityId + '" data-sid="' + user.stateId + '" data-lid="' + user.locationId + '" data-phone="' + user.mobileNo + '">' + user.name + '</options>'
                }
                self.$(".suggest-ddl").select2({
                    ajax: {
                        url: Config.user.suggest,
                        dataType: 'json',
                        type : "POST",
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term,
                                userType: '4'
                            };
                        },
                        processResults: function (data, page) {
                            // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data
                            return {

                                results: data.response.model.results
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    minimumInputLength: 1,
                    templateResult: _formatUser, // omitted for brevity, see the source of this page
                    templateSelection: _selection // omitted for brevity, see the source of this page
                });
            },

            renderIndiaMap: function(){
                var self = this;
                try{
                    var mapView = new MapView({container: self.$el, view: self});
                    mapView.render();
                }
                catch (e) {
                    console.log(e);
                }
            },

            loadFeatures: function ()
            {
               
                var self = this;
                var model = new Model();
                model.url = Config.search.collegeFeatured;
                model.fetch({
                    success: function (m, res) {
                        self.$el.find('#features-carousel').append(_.template(FeaturesCarouselItemTpl, {
                            model: res.response.model.rows
                        }));
                        self.$el.find('#features-carousel').owlCarousel({
                            navigation: true, // Show next and prev buttons
                            slideSpeed: 50,
                            paginationSpeed: 75,
                            singleItem: true,
                            autoPlay: true,
                            stopOnHover: true,
                            pagination: false,
                            autoHeight: true,
                            navigationText: ["<i class='fa fa-backward' />", "<i class='fa fa-forward' />"],
                            transitionStyle: "backSlide"
                        });
                    }
                })
            },

            find: function (e) {
             
                var self = this,
                    data = self.$(".suggest-ddl").select2("data");
                app.router.redirect('profile/college/' + data[0].id);
            }
        });

        return View;

    });