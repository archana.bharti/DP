﻿var map_config = {
	'default':{
		'borderColor':'#9CA8B6', //region outlines
		'mapShadow':'#000', //shadow color below the map
		'shadowOpacity':'50', //shadow opacity, value, 0-100
	},
	'map_1':{
		'hover': 'ANDHRA PRADESH',  //region hover content
		'upColor':'#5FBD86', //region color when page loads
		'overColor':'#FFEE88', //region color when mouse hover
		'downColor':'#993366',//region color when mouse clicking
		'enable':true,//true/false to enable/disable this region
		'stateId': 1
	},
	'map_2':{
		'hover': 'ARUNACHAL PRADESH',
		'upColor':'#59C3E0',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 2
	},
	'map_3':{
		'hover': 'ASSAM',
		'upColor':'#F7F076',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 3
	},
	'map_4':{
		'hover': 'BIHAR',
		'upColor':'#54C5DC',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 4
	},
	'map_5':{
		'hover': 'CHHATTISGARH',
		'upColor':'#D987B9',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 5
	},
	'map_6':{
		'hover': 'GOA',
		'upColor':'#5FBD86',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 6
	},
	'map_7':{
		'hover': 'GUJARAT',
		'upColor':'#5FBD86',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 7
	},
	'map_8':{
		'hover': 'HARYANA',
		'upColor':'#54C5DC',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 8
	},
	'map_9':{
		'hover': 'HIMACHAL PRADESH',
		'upColor':'#FBBD85',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 9
	},
	'map_10':{
		'hover': 'JAMMU AND KASHMIR',
		'upColor':'#F5F07A',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 10
	},
	'map_11':{
		'hover': 'JHARKHAND',
		'upColor':'#AEAE50',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 11
	},
	'map_12':{
		'hover': 'KARNATAKA',
		'upColor':'#54C5DC',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 12
	},
	'map_13':{
		'hover': 'KERALA',
		'upColor':'#D987B9',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 13
	},
	'map_14':{
		'hover': 'MADHYA PRADESH',
		'upColor':'#FBBD85',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 14
	},
	'map_15':{
		'hover': 'MAHARASHTRA',
		'upColor':'#F5F07A',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 15
	},
	'map_16':{
		'hover': 'MANIPUR',
		'upColor':'#EBECED',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 16
	},
	'map_17':{
		'hover': 'MEGHALAYA',
		'upColor':'#5FBD86',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 17
	},
	'map_18':{
		'hover': 'MIZORAM',
		'upColor':'#D987B9',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 18
	},
	'map_19':{
		'hover': 'NAGALAND',
		'upColor':'#FBBD85',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 19
	},
	'map_20':{
		'hover': 'ODISHA',
		'upColor':'#54C5DC',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 20
	},
	'map_21':{
		'hover': 'PUNJAB',
		'upColor':'#5FBD86',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 21
	},
	'map_22':{
		'hover': 'RAJASTHAN',
		'upColor':'#D987B9',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 22
	},
	'map_23':{
		'hover': 'SIKKIM',
		'upColor':'#D987B9',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 23
	},
	'map_24':{
		'hover': 'TAMIL NADU',
		'upColor':'#F5F07A',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 24
	},
	'map_25':{
		'hover': 'TELANGANA',
		'upColor':'#5FBD86',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 25
	},
	'map_26':{
		'hover': 'TRIPURA',
		'upColor':'#EBECED',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 26
	},
	'map_27':{
		'hover': 'UTTAR PRADESH',
		'upColor':'#F5F07A',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 27
	},
	'map_28':{
		'hover': 'UTTARAKHAND',
		'upColor':'#FBBD85',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':false,
		'stateId': 36
	},
	'map_29':{
		'hover': 'PONDICHERRY',
		'upColor':'#EBECED',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 30
	},
	'map_30':{
		'hover': 'DELHI',
		'upColor':'#EBECED',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 31
	},
	'map_31':{
		'hover': 'UTTARAKHAND',
		'upColor':'#D987B9',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 32
	},
	'map_32':{
		'hover': 'UTTARANCHAL',
		'upColor':'#EBECED',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 34
	},
	'map_33':{
		'hover': 'DAMAN AND DIU',
		'upColor':'#EBECED',
		'overColor':'#FFEE88',
		'downColor':'#993366',
		'enable':true,
		'stateId': 35
	}
}
