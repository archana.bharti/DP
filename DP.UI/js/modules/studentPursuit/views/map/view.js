﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "text!modules/studentPursuit/views/templates/map.html"
    ],

    function (Config, Constants, Base, Model, MapTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function (obj) {
                var self = this;
                self.$el = obj.container;
            },

            events : {
                'click .btn-college-submit' : 'submitCollegeSearchQuery'
            },

            render: function () {
                var self = this;
                self.$el.find('.india-map-view').html(_.template(MapTpl, {}));
                self.configureMap();
                self.interactMap();

            },

            configureMap: function(){
                var self = this;
                self.map_config = {
                    'default':{
                        'borderColor':'#9CA8B6', //region outlines
                        'mapShadow':'#000', //shadow color below the map
                        'shadowOpacity':'50', //shadow opacity, value, 0-100
                    },
                    'map_1':{
                        'hover': 'ANDHRA PRADESH',  //region hover content
                        'upColor':'#5FBD86', //region color when page loads
                        'overColor':'#FFEE88', //region color when mouse hover
                        'downColor':'#993366',//region color when mouse clicking
                        'enable':true,//true/false to enable/disable this region
                        'stateId': 1
                    },
                    'map_2':{
                        'hover': 'ARUNACHAL PRADESH',
                        'upColor':'#59C3E0',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 2
                    },
                    'map_3':{
                        'hover': 'ASSAM',
                        'upColor':'#F7F076',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 3
                    },
                    'map_4':{
                        'hover': 'BIHAR',
                        'upColor':'#54C5DC',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 4
                    },
                    'map_5':{
                        'hover': 'CHHATTISGARH',
                        'upColor':'#D987B9',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 5
                    },
                    'map_6':{
                        'hover': 'GOA',
                        'upColor':'#5FBD86',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 6
                    },
                    'map_7':{
                        'hover': 'GUJARAT',
                        'upColor':'#5FBD86',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 7
                    },
                    'map_8':{
                        'hover': 'HARYANA',
                        'upColor':'#54C5DC',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 8
                    },
                    'map_9':{
                        'hover': 'HIMACHAL PRADESH',
                        'upColor':'#FBBD85',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 9
                    },
                    'map_10':{
                        'hover': 'JAMMU AND KASHMIR',
                        'upColor':'#F5F07A',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 10
                    },
                    'map_11':{
                        'hover': 'JHARKHAND',
                        'upColor':'#AEAE50',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 11
                    },
                    'map_12':{
                        'hover': 'KARNATAKA',
                        'upColor':'#54C5DC',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 12
                    },
                    'map_13':{
                        'hover': 'KERALA',
                        'upColor':'#D987B9',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 13
                    },
                    'map_14':{
                        'hover': 'MADHYA PRADESH',
                        'upColor':'#FBBD85',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 14
                    },
                    'map_15':{
                        'hover': 'MAHARASHTRA',
                        'upColor':'#F5F07A',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 15
                    },
                    'map_16':{
                        'hover': 'MANIPUR',
                        'upColor':'#EBECED',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 16
                    },
                    'map_17':{
                        'hover': 'MEGHALAYA',
                        'upColor':'#5FBD86',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 17
                    },
                    'map_18':{
                        'hover': 'MIZORAM',
                        'upColor':'#D987B9',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 18
                    },
                    'map_19':{
                        'hover': 'NAGALAND',
                        'upColor':'#FBBD85',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 19
                    },
                    'map_20':{
                        'hover': 'ODISHA',
                        'upColor':'#54C5DC',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 20
                    },
                    'map_21':{
                        'hover': 'PUNJAB',
                        'upColor':'#5FBD86',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 21
                    },
                    'map_22':{
                        'hover': 'RAJASTHAN',
                        'upColor':'#D987B9',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 22
                    },
                    'map_23':{
                        'hover': 'SIKKIM',
                        'upColor':'#D987B9',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 23
                    },
                    'map_24':{
                        'hover': 'TAMILNADU',
                        'upColor':'#F5F07A',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 24
                    },
                    'map_25':{
                        'hover': 'TELANGANA',
                        'upColor':'#5FBD86',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 25
                    },
                    'map_26':{
                        'hover': 'TRIPURA',
                        'upColor':'#EBECED',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 26
                    },
                    'map_27':{
                        'hover': 'UTTAR PRADESH',
                        'upColor':'#F5F07A',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 27
                    },
                    'map_28':{
                        'hover': 'UTTARAKHAND',
                        'upColor':'#FBBD85',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 32
                    },
                    'map_29':{
                        'hover': 'WEST BENGAL',
                        'upColor':'#EBECED',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 29
                    },
                    'map_30':{
                        'hover': 'DELHI',
                        'upColor':'#EBECED',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 31
                    },
                    'map_31':{
                        'hover': 'UTTARAKHAND',
                        'upColor':'#D987B9',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 32
                    },
                    'map_32':{
                        'hover': 'UTTARANCHAL',
                        'upColor':'#EBECED',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 34
                    },
                    'map_33':{
                        'hover': 'DAMAN AND DIU',
                        'upColor':'#EBECED',
                        'overColor':'#FFEE88',
                        'downColor':'#993366',
                        'enable':true,
                        'stateId': 35
                    }
                }

            },

            interactMap: function () {
                try{
                    var self = this;
                    var map_config = self.map_config;
                    for(var i = 1; i < 34; i++){
                        self.addEvent('map_'+i);
                    }
                    if($('#shadow').find('path').eq(0).attr('fill') != 'undefined'){
                        var shadowOpacity = map_config['default']['shadowOpacity'];
                        var shadowOpacity = parseInt(shadowOpacity);
                        if (shadowOpacity >=100){shadowOpacity = 1;}else if(shadowOpacity <=0){shadowOpacity =0;}else{shadowOpacity = shadowOpacity/100;}

                        $('#shadow').find('path').attr({'fill':map_config['default']['mapShadow']}).css({'fill-opacity':shadowOpacity})
                    }
                }
                catch (e) {
                    console.log(e);
                }
            },

            addEvent: function(id,relationId){
                var self = this;
                var map_config = self.map_config;
                var _obj = $('#'+id);
                _obj.attr({'fill':map_config[id].upColor,'stroke':map_config['default']['borderColor']});
                _obj.attr({'cursor':'default'});
                if(map_config[id]['enable'] == true){
                    if (self.isTouchEnabled()) {
                        _obj.on('touchstart', function(e){
                            var touch = e.originalEvent.touches[0];
                            var x=touch.pageX+10, y=touch.pageY+15;
                            var tipw=$('#map-tip').outerWidth(), tiph=$('#map-tip').outerHeight(),
                                x=(x+tipw>$(document).scrollLeft()+$(window).width())? x-tipw-(20*2) : x
                            y=(y+tiph>$(document).scrollTop()+$(window).height())? $(document).scrollTop()+$(window).height()-tiph-10 : y
                            $('#'+id).css({'fill':map_config[id]['downColor']});
                            $('#map-tip').show().html(map_config[id]['hover']);
                            $('#map-tip').css({left:x, top:y})
                        })
                        _obj.on('touchend', function(){
                            $('#' + id).css({ 'fill': map_config[id].upColor });
                            if(map_config[id]['target'] == 'new_window'){
                                window.open(map_config[id]['url']);
                            }else if(map_config[id]['target'] == 'same_window'){
                                window.parent.location.href=map_config[id]['url'];
                            }
                        })
                    }
                    _obj.attr({'cursor':'pointer'});
                    _obj.hover(function(){
                        $('#map-tip').show().html(map_config[id]['hover']);
                        _obj.css({'fill':map_config[id]['overColor']})
                    },function(){
                        $('#map-tip').hide();
                        $('#'+id).css({'fill':map_config[id]['upColor']});
                    })
                    _obj.click(function(){
                        $('#'+id).css({'fill':map_config[id]['downColor']});
                        $('.state-name h3').html(map_config[id]['hover']);
                        $('.state-name').find('.map-state-id').html(map_config[id]['stateId']);
                    })
                    _obj.mouseup(function(){
                        //$('#'+id).css({'fill':map_config[id]['overColor']});
                        if(map_config[id]['target'] == 'new_window'){
                            window.open(map_config[id]['url']);
                        }else if(map_config[id]['target'] == 'same_window'){
                            window.parent.location.href=map_config[id]['url'];
                        }
                    })
                    _obj.mousemove(function(e){
                        var x=e.pageX+10, y=e.pageY+15;
                        var tipw=$('#map-tip').outerWidth(), tiph=$('#map-tip').outerHeight(),
                            x=(x+tipw>$(document).scrollLeft()+$(window).width())? x-tipw-(20*2) : x
                        y=(y+tiph>$(document).scrollTop()+$(window).height())? $(document).scrollTop()+$(window).height()-tiph-10 : y
                        $('#map-tip').css({left:x, top:y})
                    })
                }
            },

            isTouchEnabled: function(){
                return (('ontouchstart' in window)
                || (navigator.MaxTouchPoints > 0)
                || (navigator.msMaxTouchPoints > 0));
            },

            submitCollegeSearchQuery: function(e){
                var self = this;
                var collegeType = self.$el.find('input[name=optradio]:checked').val();
                var stateId = self.$el.find('.map-state-id').html();
                var values = {};
                values.DepartmentId = collegeType;
                values.SearchType = 3;
                values.ip = window.IP;
                values.stateId = stateId;
                localStorage.clear();
                localStorage.setItem("searchParams", JSON.stringify(values));
                var pageValue = {
                    "Value": true
                };
                //localStorage.setItem("PageValue", JSON.stringify(pageValue));
                $("#loading-control").show();
                Backbone.history.fromURL = "studentpursuit";
                app.router.navigate('search/results', true);
            }
        });

        return View;

    });