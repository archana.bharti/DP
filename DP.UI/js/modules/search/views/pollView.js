﻿define([
        "apiConfig",
        "viewBase",
        "constantsConfig",
        'core/model',
        "text!modules/search/templates/poll.html"],

    function (Config,
              Base,
              Constants,
              Model,
              PollTpl) {

        var View = Base.extend({

            initialize: function () {
                var self = this;
                self.template = _.template(PollTpl);
            },

            events: {
                'click .rad-poll-option': 'poll'
            },

            render: function () {
                var self = this;
                var m = new Model();
                m.url = Config.user.polls;
                m.fetch({
                    success: function (res) {
                        self.$el.html(self.template(res.toJSON()))
                    }
                })
            },
            poll: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    item = ele.closest(".poll-item"),
                    nextItem = item.next(),
                    masterId = item.attr("data-master-id"),
                    val = item.find("input:checked").val(),
                    poll = {
                        yes: false,
                        no: false,
                        pollMasterId: masterId,
                        IpAddress : window.IP
                    }
                if (val === 'yes') {
                    poll.yes = true;
                } else if (val === 'no') {
                    poll.no = true;
                }
                
                var m = new Model();
                m.url = Config.user.addPoll;
                m.save(poll, {
                    success: function () {
                        if (nextItem.length > 0) {
                            item.hide("slide", { direction: "left" }, 500, function () {
                                nextItem.show("slide", { direction: "left" });
                            });;
                        }
                        else {
                            self.$el.html("<h5>Thank you. We appreciate your time.</h5>")
                        }
                    }
                })
            }
        });

        return View;

    })