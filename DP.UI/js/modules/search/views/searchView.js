define([
        "apiConfig",
        "viewBase",
        "constantsConfig",
        'core/model',
        "modules/shared/models/map",
        "modules/shared/views/mapView",
        "modules/search/views/pollView",
        "text!modules/search/templates/searchResults.html",
        "text!modules/search/templates/featured-item.html",
        "text!modules/search/templates/non-featured-item.html",
        "text!modules/search/templates/mapListItems.html",
        "text!modules/search/templates/featured-carousel-item.html",
        "text!modules/shared/templates/left-sidebar.html",
        "text!modules/shared/templates/right-sidebar.html",
        "text!modules/search/templates/appointment.html",
        "bootstrap-dialog",
        "owl-carousel",
        "slimScroll",
        "bootstrapDatetimePicker",
        "jqExt"
    ],

    function (Config,
              Base,
              Constants,
              Model,
              Map,
              MapView,
              PollView,
              SearchResultsTpl,
              FeaturedItemTpl,
              NonFeaturedItemTpl,
              MapListItemTpl,
              FeaturedCarouselItemTpl,
              LeftSideBarTpl,
              RightSideBarTpl,
              AppointmentFormTpl,
              BootstrapDialog) {

        "use strict";
        
        var View = Base.extend({

            initialize: function (options) {

                
                
                var self = this;
                self.isInitialSearchLoaded = false;
                $.extend(self, {
                    mapLoaded: false,
                    markers: [],
                    featured: [],
                    carouselRendered : false,
                    searchParams: JSON.parse(localStorage.getItem('searchParams'))
                });
              
            },

            events:{
                'click li[data-tab-view]': 'switchTab',
                'click .search-btn': 'search',
                'click .map-item-name' : 'onMapItemClick',
                'click .btn-save-bookmark': 'bookmark',
                'click .btn-recommend': 'recommend',
                'click .btn-book-appoinment': 'initAppointment',
                'keyup #bmi-body-height': "bmi",
                'keyup #bmi-body-weigth': "bmi"
            },

            render: function () {
                var self = this;
                self.$el.html(_.template(SearchResultsTpl, {
                    leftSideBar: _.template(LeftSideBarTpl,{}),
                    rightSideBar: _.template(RightSideBarTpl, {}),
                    showDistance: (self.searchParams != 3),
                    searchType: self.searchParams.SearchType
                }));


               
                if(self.searchParams.locationId == null){
                    $('.search-by-distance-col').remove();
                }

                self.FGrid = self.$el.find("#grid-featured")
                self.NFGrid = self.$el.find("#grid-non-featured");
                setTimeout(function (v) {
                    v.getSearchResults();
                    v.initZonal();
                    v.loadPolls();
                }, 1000, self);
            },
          
            //searching: function () {
                 //    var self = this;
            //    self.isInitialSearchLoaded = true;
            //    // self.FGrid.bootstrapTable('refresh', { query: self.getFilters(true, true) });
            //    self.NFGrid.bootstrapTable('refresh', { query: self.getFilters(false, true) });
            //},

            search: function (e) {
              
           
                var self = this;
                self.isInitialSearchLoaded = true;
               // self.FGrid.bootstrapTable('refresh', { query: self.getFilters(true, true) });
                self.NFGrid.bootstrapTable('refresh', { query: self.getFilters(false, true) });
            },

            switchTab: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    ctx = ele.data('tab-view');
                
                switch (ctx) {
                    
                    case 'map':
                        self.$el.find('.left-side-column, .right-side-column, .search-sidebar-main, .middle-column, #listView').hide();
                        self.$el.find('.middle-column').removeClass('col-md-6').addClass('col-md-12').show();
                        self.$el.find("#mapView").show();
                        self.renderGmap();
                       
                        break;
                    case 'list':
                        self.$el.find('.left-side-column, .right-side-column, .search-sidebar-main, #listView').show();
                        self.$el.find('.middle-column').removeClass('col-md-12').addClass('col-md-6').show();
                        self.$el.find("#mapView").hide();
                        break;
                }
            },

            bookmark: function(e){
                var self = this,
                    ele = $(e.currentTarget);
                if (app.isAuthenticated) {
                    var request = {
                        entityId: app.sessionManager.get('USER').subscriberId,
                        entityType: app.sessionManager.get('USER').subscriberType,
                        bookmarkEntityId: $(e.currentTarget).data('user-id'),
                        bookmarkEntityType : Constants.USER_ROLES_ID[$(e.currentTarget).data('profile').toLowerCase()]
                    };
                    var saveContactModel = new Model();
                    saveContactModel.url = Config.profile.bookmarkProfile;
                    saveContactModel.save(request, {
                        success: function (model, resp) {
                            $(e.currentTarget).text('Saved');
                        }
                    });
                    ele.addClass("disabled");
                }
            },

            renderGmap: function(){
                var self = this;
                if (self.mapView) {
                    //TODO: dont know i have commented this line
                    //self.mapView.destroy();
                    self.$el.find("#gmaps-container").html('');
                }
                self.mapView = new MapView();
                self.$el.find("#gmaps-container").append(self.mapView.render(self.markers).el);
            },

            onMapItemClick: function(e){
                var self = this,
                    ele = $(e.currentTarget),
                    name = ele.data("name"),
                    loc = ele.data("location"),
                    featured = ele.data('featured');
                var address = name + ", " + loc;
                var isFeatured = (featured == 'featured') ? true : false;
                self.mapView.getLocationDetails(address, isFeatured);
            },

            getSearchResults: function ()
            {
                //var PageVal = JSON.parse(localStorage.getItem('PageValue'));
              
                //if (JSON.stringify(PageVal) != "null")
                //{
                //    if(PageVal.Value!="false")
                //    {
                //        alert(JSON.stringify(PageVal));
                //        $(".btn btn-xs btn-success btn-book-appoinment").hide();
                //    }
                //}
              
                var self = this;
                self.$('.featured-ul, .non-featured-ul, .carousel-featured-items,.map-list-ul').html('');
               self.getNonFeaturedData(false, false);
               self.getFeaturedData(true, false);

                var pageValue = {
                    "Value": false
                };
                localStorage.setItem("PageValue", JSON.stringify(pageValue));
            },

            getFeaturedData: function ()
            {
               
                var getva = (JSON.parse(localStorage.getItem('searchParamsValues')));

                if (getva.SearchType != 3) {


                    if (getva.City === "Metro City" && getva.Specialization === "Select Specialization") {
                        $(".DisTextCity").text("Displaying result around Bengaluru city");
                    }

                    if (getva.Specialization !== "Select Specialization" && getva.City === "Metro City") {
                        $(".DisTextSpec").text("Displaying " + getva.Specialization + " Specialization  in  Bengaluru");
                    }


                    if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ")) {
                        $(".search-by-distance-col").hide();
                        if (getva.SearchType == 1) {
                            $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                        }
                        else {
                            $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                        }
                    }


                    if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ") && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found")) {
                        $(".search-by-distance-col").show();
                        if (getva.SearchType == 1) {
                            $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within 1 KM of " + getva.Location);
                        }
                        else {
                            $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within 2 KM of " + getva.Location);
                        }
                    }

                    if (getva.City != "Metro City" && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found") && getva.Specialization === "Select Specialization") {
                        $(".search-by-distance-col").show();
                        if (getva.SearchType == 1) {
                            $(".DisTextLoc").text("Displaying Specialization for " + getva.Specialization + " within 1 KM of  " + getva.Location);
                        }
                        else {
                            $(".DisTextLoc").text("Displaying Specialization for " + getva.Specialization + " within 1 KM of  " + getva.Location);
                        }
                    }



                    if (getva.City != "Metro City" && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found") && getva.Specialization === "Select Specialization") {
                        $(".search-by-distance-col").show();
                        if (getva.SearchType == 1) {
                            $(".DisTextLoc").text("Displaying search result around 1km radius of  " + getva.Location);
                        }
                        else {
                            $(".DisTextLoc").text("Displaying search result around 2km radius of  " + getva.Location);
                        }
                    }


                    if (( getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai " && getva.City != "Metro City") && (getva.Location == null || getva.Location == "Location" || getva.Location == "No locations found") && getva.Specialization === "Select Specialization") {

                        $(".DisTextLoc").text("Displaying result around" + getva.City + "city");
                    }


                }


           
                console.log("getva>>>> getFeaturedData", getva);
                var txt = $('.name').html();

                var self = this;
                self.FGrid.bootstrapTable({
                    url: Config.search.getResults,
                    sidePagination: 'server',
                    method : "post",
                    cache: false,
                    pagination: false,
                    showLoading: true,
                    clickToSelect: false,
                    sortable: false,
                    showHeader: false,
                    showPaginationSwitch: false,
                    queryParams: function (params) {
                        $.extend(params, self.getFilters(true, false));
                        if (getva.SearchType != 3) {
                            if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ")) {
                                $(".search-by-distance-col").hide();
                                 $(".DisTextNoRes").text("");
                                if (getva.SearchType == 1) {
                                    $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                                }
                                else {
                                    $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                                }
                            }

                            if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ") && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found")) {
                                $(".search-by-distance-col").show();
                                $(".DisTextNoRes").text("");
                                if (getva.SearchType == 1) {
                                    $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within " + params.distance + " KM of " + getva.Location);
                                }
                                else {
                                    $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within " + params.distance + " KM of " + getva.Location);
                                }
                            }

                            if (getva.City != "Metro City" && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found") && getva.Specialization === "Select Specialization") {
                                $(".search-by-distance-col").show();
                                $(".DisTextNoRes").text("");
                                if (getva.SearchType == 1) {
                                    $(".DisTextLoc").text("Displaying search result around" + params.distance + "KM radius of  " + getva.Location);
                                }
                                else {
                                    $(".DisTextLoc").text("Displaying search result around " + params.distance + "KM radius of  " + getva.Location);
                                }
                            }
                        }
                        console.log("Distance>>> getFeaturedData", params);
                        return params;
                    },
                    columns: [
                        {
                            field: 'name',
                            title: 'UID',
                            valign: 'middle',
                            align: 'left',
                            formatter: _.bind(self.itemFormatter,self, true)
                        }]
                });

                self.FGrid.on("load-success.bs.table", function (e, data) {
                    if (data.total == 0) {
                       
                        $(this).closest('.featured-list-panel').hide();
                    }
                    else {
                       
                        $(this).closest('.featured-list-panel').show();
                    }
                }).on("page-change.bs.table", function () { })
            },

            getNonFeaturedData: function () {
                var self = this;
                var getva = (JSON.parse(localStorage.getItem('searchParamsValues')));

                if (getva.SearchType != 3) {
                    if (getva.City === "Metro City" && getva.Specialization === "Select Specialization") {
                        $(".DisTextCity").text("Displaying result around Bengaluru city");
                    }

                    if (getva.Specialization !== "Select Specialization" && getva.City === "Metro City") {
                        $(".DisTextSpec").text("Displaying " + getva.Specialization + " Specialization  in  Bengaluru");
                    }


                    if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ")) {
                        $(".DisTextNoRes").text("");
                        $(".search-by-distance-col").hide();
                        if (getva.SearchType == 1) {
                            $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                        }
                        else {
                            $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                        }
                    }

                    if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ") && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found")) {
                        $(".search-by-distance-col").show();
                        if (getva.SearchType == 1) {
                            $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within 1 KM of " + getva.Location);
                        }
                        else {
                            $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within 2 KM of " + getva.Location);
                        }
                    }

                    if (getva.City != "Metro City" && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found") && getva.Specialization === "Select Specialization") {
                        $(".search-by-distance-col").show();
                        if (getva.SearchType == 1) {
                            $(".DisTextLoc").text("Displaying search result around 1km radius of  " + getva.Location);
                        }
                        else {
                            $(".DisTextLoc").text("Displaying search result around 2km radius of  " + getva.Location);
                        }
                    }
                    if (( getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai " && getva.City != "Metro City" ) && (getva.Location == null || getva.Location == "Location" || getva.Location == "No locations found") && getva.Specialization === "Select Specialization") {

                        $(".DisTextLoc").text("Displaying result around" + getva.City + "city");
                    }
                }
                console.log("getva>>>>getNonFeaturedData", getva);
                var txt = $('.name').html();
             //////// to display searched Item//////////

                //if (getva.Name != "Name Search")
                //$(".name").text(txt+getva.Name);
               
                //var txt = $('.specialisation').html();
                //if (getva.Specialization != "Select Specialization")
                //$(".specialisation").text(txt + getva.Specialization);
               
                //var txt = $('.city').html();
                //if (getva.City != "Metro City")
                //$(".city").text(txt + getva.City);

                //var txt = $('.location').html();
                //if (getva.Location != undefined) {
                //    $(".location").text(txt + getva.Location);
                //}
                
               
                var self = this;
                self.NFGrid.bootstrapTable({
                    url: Config.search.getResults,
                    sidePagination: 'server',
                    method: "post",
                    cache: false,
                    pagination: true,
                    pageSize: 5,
                    showLoading: true,
                    clickToSelect: false,
                    sortable: false,
                    showHeader: false,
                    showPaginationSwitch: false,
                    //formatNoMatches : function(){
                    //    return ""
                    //},
                    queryParams: function (params) {
                        $.extend(params, self.getFilters(false, false));
                        if (getva.SearchType != 3) {
                            if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ")) {
                               
                                $(".search-by-distance-col").hide();
                                $(".DisTextNoRes").text("");
                                if (getva.SearchType == 1) {
                                    $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                                  
                                }
                                else {
                                    $(".DisTextSpec").text("Displaying  " + getva.Specialization + " Specialization  in  Bengaluru")
                                    
                                }
                            }
                            if (getva.Specialization !== "Select Specialization" && (getva.City !== "Metro City" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ") && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found")) {
                                $(".search-by-distance-col").show();
                                $(".DisTextNoRes").text("");
                                if (getva.SearchType == 1) {
                                    $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within " + params.distance + " KM of " + getva.Location);
                                   
                                }
                                else {
                                    $(".DisTextSpec").text("Displaying Specialization for " + getva.Specialization + " within " + params.distance + " KM of " + getva.Location);
                                   
                                }
                            }

                            if (getva.City != "Metro City" && (getva.Location != null && getva.Location != "Location" && getva.Location != "No locations found") && getva.Specialization === "Select Specialization") {
                                $(".search-by-distance-col").show();
                                $(".DisTextNoRes").text("");
                                if (getva.SearchType == 1) {
                                    $(".DisTextLoc").text("Displaying search result around"+ params.distance+"KM radius of  " + getva.Location);
                                }
                                else {
                                    $(".DisTextLoc").text("Displaying search result around "+params.distance +"KM radius of  " + getva.Location);
                                }
                            }

                           
                        }
                        console.log("Distance>>> getFeaturedData", params);
                        return params; 
                    },
                    onPageChange: function (number, size) {
                        if (number <=  1) {
                            self.FGrid.closest('.featured-list-panel').show();
                        }
                        else {
                            self.FGrid.closest('.featured-list-panel').hide();
                        }
                    },
                    columns: [
                        {
                            field: 'name',
                            title: 'UID',
                            valign: 'middle',
                            align: 'left',
                            formatter: _.bind(self.itemFormatter, self, false)
                        }]
                });
                                
                self.NFGrid.on("load-success.bs.table", function (e, data) {
                    if (data.total == 0) {
                        if (getva.Name === "Name Search" && getva.City != " Hyderabad " && getva.City != " Mumbai " && getva.City != " Chennai ") {
                            $(".DisTextNoRes").text("No results found please increase the search radius");
                        }
                        else {
                            $(".DisTextNoRes").text("No results found ");
                        }
                        $(".DisTextLoc").text("");
                        $(".DisTextLoc").text("");
                        $(".DisTextSpec").text("");
                        $(this).closest('.non-featured-list-panel').hide();
                                           }
                    else {
                        //$(".DisTextNoRes").text("");
                       // $(".DisTextLoc").text("Displaying search result within 1KM radius of" + getva.Location);
                        $(this).closest('.non-featured-list-panel').show();
                    }
                })
            },

            getFilters: function (featured, resetPagination) {

                var self = this;
               
                var fnGetDistanace = function () {
                    if (self.searchParams.SearchType == '1') {
                        return Constants.SEARCH_CLINIC_FEATURED_DEFAULT_INITIAL_RADIUS;
                    }
                    else if (self.searchParams.SearchType == '2') {
                        return Constants.SEARCH_HOSPITAL_FEATURED_DEFAULT_INITIAL_RADIUS;
                    }
                }

                var distanceVal, reviewVal = self.$el.find(".reviews-select").val(),
                recommendVal = self.$el.find(".recommended-select").val(),
                sparams = $.extend(true, {}, self.searchParams);

                if(featured == true){
                    if(self.isInitialSearchLoaded == false){
                        distanceVal = fnGetDistanace();
                    }
                    else {
                        if (self.searchParams.locationId == undefined || self.searchParams.locationId == null) {
                            distanceVal = fnGetDistanace();
                        }
                        else { //distance dropdown is visible
                            distanceVal = self.$el.find(".distance-select").val();
                        }
                    }
                } else {
                    if (self.isInitialSearchLoaded == true) {
                        if (self.searchParams.locationId != undefined) {
                            distanceVal = self.$el.find(".distance-select").val();
                        }
                    }
                }

                //if (featured == true) {
                //    distanceVal = fnGetDistanace();
                //} else {
                //    distanceVal = self.$el.find(".distance-select").val();
                //} 

                var p = $.extend(sparams, { byFeatured: featured, cityId: (self.searchParams.SearchType == '3') ? self.searchParams.cityId :  '2', distance: distanceVal, byRecommended: recommendVal, byReview: reviewVal });
                return p;
            },

            itemFormatter: function (isFeatured, value, row, index) {
                var self = this;
                var tpl = isFeatured == true ? FeaturedItemTpl : NonFeaturedItemTpl;
                var model = row;
                switch (row.subType.toLowerCase()) {
                    case 'clinic':
                        model.iconName = 'Hospital.png';
                        model.sDisplay = 'block';
                        break;
                    case 'doctor':
                        model.iconName = 'Doctor.png'
                        model.sDisplay = 'block';
                        break;
                    case 'hospital':
                        model.iconName = 'Hospital.png'
                        model.sDisplay = 'block';
                        break;
                    case 'college':
                        model.iconName = 'College.png'
                        model.sDisplay = 'none';
                        break;
                    default:
                        model.iconName = 'college-icon-new.jpg'
                        model.sDisplay = 'none';
                        break;
                }

                self.$('.map-list-ul').append(_.template(MapListItemTpl, {
                    item: row,
                    iconName: model.iconName,
                    featured: isFeatured == true ?  'featured' : '',
                    iconPath: isFeatured == true ? 'css/img/gmap-marker-yellow.png' : 'css/img/gmap-marker-red.png',
                    css: isFeatured == true ? 'clr-yellow' : 'clr-snuf'
                }));

                if (_.where(self.markers, { name: row.name }).length == 0) {
                    self.markers.push({ latitude: row.latitude, longitude: row.longitude, name: row.name, isFatured: isFeatured });
                }

                //if (isFeatured == true && _.where(self.featured, { name: row.name }).length == 0) {
                   // self.featured.push(row);
                //}


                return _.template(tpl, {
                    item: model
                })
            },

            initZonal: function () {

                var self = this,
                    locationId = (self.searchParams == undefined || self.searchParams.locationId == undefined)
                                 ? null : self.searchParams.locationId,
                    stateId = (self.searchParams == undefined || self.searchParams.stateId == undefined)
                                 ? null : self.searchParams.stateId;
                
                var model = new Model();
                model.url = self.searchParams.SearchType == "3" ? Config.search.college : Config.search.zone;
                model.save({ locationId: locationId, stateId: stateId }, {
                    success: function (m, res) {
                        _.each(res.response.model, function (i) {
                            self.featured.push(i);
                        });
                        self.renderZonal();
                    }
                })
            },

            renderZonal: function () {
                var self = this;
                //  if (!self.carouselRendered) {
                var flist = self.featured.slice(0);
                var mod = flist.length % 5, i = 0;
                //if (mod != 0) {
                //    i = 0;
                //    while (flist.length % 5 != 0) {
                //        flist.push(flist[i]);
                //        i++;
                //    }
                //}

                var splits = [], size = 5;

                while (flist.length > 0)
                    splits.push(flist.splice(0, size));

                var parent = self.$('.carousel-featured-items'), owlWrapper, firstChild, owlItemStyle;

                $.each(splits, function (i, d) {
                    self.$('.carousel-featured-items').append(_.template(FeaturedCarouselItemTpl, {
                        item: d
                    }))
                });


                $(".carousel-featured-items").owlCarousel({
                    navigation: true, // Show next and prev buttons
                    slideSpeed: 10000000,
                    //paginationSpeed: 55,
                    singleItem: true,
                    autoPlay: true,
                    stopOnHover: true,
                    pagination: false,
                    autoHeight: true,
                    navigationText: ["<i class='fa fa-backward' />", "<i class='fa fa-forward' />"],
                    transitionStyle: "backSlide"
                });
                // }
            },

            recommend: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    spnRm = ele.closest(".search-result-item").find(".spnRecommendCount"),
                    data = ele.data("entity").split(':'),
                    request = { entityId: data[0], entityType: Constants.USER_ROLES_ID[data[1].toUpperCase()], remove: false, ipAddress: window.IP },
                    cnt = parseInt(spnRm.text().trim()),
                    model = new Model();

                model.url = Config.recommend.addOrUpdate;
                request.remove = false;


                model.save(request, {
                    success: function () {
                        spnRm.text(cnt + 1);
                        ele.replaceWith('<i class="fa fa-thumbs-up fa-lg clr-green"></i>');
                    }
                })

            },

            initAppointment: function (e) {

                var self = this,
                   ele = $(e.currentTarget),
                   data = ele.data("entity").split(':'),

                   fn_save = function (btn, form, entityId, entityType, entityName, callback) {
                       ele.addClass("disabled");
                       var data = self.serializeForm(form);

                       if ($.trim(data.email).length == 0 ||
                           $.trim(data.startTime).length == 0 ||
                           $.trim(data.mobile).length == 0 ||
                           $.trim(data.name).length == 0) { return false; }

                       var rx = /^[0-9]{10}$/;
                       if (rx.test($.trim(data.mobile)) == false) { return false; }
                       btn.text('Saving...').addClass("disabled");
                       data.entityId = entityId;
                       data.entityType = Constants.USER_ROLES_ID[entityType.toUpperCase()];
                       data.entityName = entityName;

                       var model = new Model();
                       model.url = Config.appointment.save;

                       model.save(data, {
                           success: function () {
                               callback();
                           },
                           error: function (a) {
                               app.trigger("response:error", arguments[1].responseJSON);
                               btn.text('Save').removeClass("disabled");
                           }
                       })
                   },

                   fn_chkAvailability = function () {
                       $.get(Config.appointment.checkAvailability, function (res) {
                           if (res && res.response.model === true) {
                               BootstrapDialog.show({
                                   message: $('<div></div>').html(_.template(AppointmentFormTpl)({ hideDateTime: false })),
                                   title: "Appointment",
                                   closable: true,
                                   cssClass: "appoinment-dialog",
                                   data: {
                                       'entityId': data[0],
                                       'entityType': data[1],
                                       'entityName': data[2]
                                   },
                                   onshown: function (dialogRef) {
                                       $('#datetimepicker-appointment').datetimepicker({
                                           format: 'YYYY-MM-DD HH:mm'
                                       });
                                       $(".btn-save-appointment").on('click', function () {
                                           fn_save($(this),
                                                $('.form-appointment'),
                                                dialogRef.getData('entityId'),
                                                dialogRef.getData('entityType'),
                                                dialogRef.getData('entityName'),
                                                function () {
                                                    dialogRef.close();
                                                })
                                       })
                                   }
                               });
                           }
                       })
                   };

                fn_chkAvailability();
            },

            bmi: function () {
                var self = this,
                    height = $('#bmi-body-height').val(),
                    weight = $('#bmi-body-weigth').val(),
                    res = $('#bmi-result'),
                    rx = new RegExp(/^[\d]+$/);

                res.val(0.0);
                if (height.length > 0 && weight.length > 0 &&
                   rx.test(height) == true && rx.test(weight) == true) {
                    height = parseFloat(height);
                    weight = parseFloat(weight);
                    var r = (weight / (height * height)) * parseFloat(10000);
                    res.val(r.toFixed(2));
                }
            },

            loadPolls: function () {
                var pollView = new PollView({ el: '#poll-view' });
                pollView.clear().render();
            }
        });

        return View;

    });