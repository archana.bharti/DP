﻿﻿define([
        "apiConfig",
        "constantsConfig",
        "viewBase",
        "core/model",
        "modules/healthyWay/views/view",
        "text!modules/healthyWay/views/templates/under-construction.html",
        "bootstrapSelect"
    ],

    function (Config, Constants, Base, Model, HealthyWayView, HealthyWayTpl) {

        "use strict";

        var View = Base.extend({

            initialize: function () {
                var self = this;
            },

            events : {
                'keyup #bmr-age': "bmr",
                'keyup #bmr-body-height': "bmr",
                'keyup #bmr-body-weigth': "bmr",
                'keyup #pdd-first-data' : "pdd"
            },

            render: function () {
                var self = this;
                self.$el.html(_.template(HealthyWayTpl, {

                }));

                //FB INIT
                //FB.init({
                //    appId: '598483290303155',
                //    status: true,
                //    cookie: true,
                //    xfbml: true,
                //    version: 'v2.6'
                //});
            },

            bmr: function () {
                var self = this,
                    height = $('#bmr-body-height').val(),
                    weight = $('#bmr-body-weigth').val(),
                    age = $('#bmr-age').val(),
                    res = $('#bmr-result'),
                    gender = $("[name='bmr-gender']:checked").val(),
                    rx = new RegExp(/^[\d]+$/);

                if (age.length == 0) {
                    res.val(0);
                }

                if(height.length > 0 && weight.length >0 && age.length > 0 &&
                   rx.test(height) == true && rx.test(weight) == true && rx.test(age) == true) {
                    switch (gender) {
                        case 'm':
                            var r = 66 + (13.7 * parseFloat(weight)) + (5 * parseFloat(height)) - (6.8 * parseFloat(age));
                            res.val(r);
                            break;
                        case 'f':
                            var r = 655 + (9.6 * parseFloat(weight)) + (1.8 * parseFloat(height)) - (4.7 * parseFloat(age));
                            res.val(r);
                            break;
                    }
                }  
            },

            pdd: function (e) {
                var self = this,
                    ele = $(e.currentTarget),
                    val = ele.val(),
                    res = $('#pdd-pregnancy-duedata');
                res.val('');

                var fn_addDays =  function (date, days) {
                    date.setDate(date.getDate() + parseInt(days));
                    return date.toDateString();
                };

                try{
                    if (val.length == 5) {
                        var split = val.split('-'),
                            date = split[0],
                            month = split[1],
                            year = new Date().getFullYear(),
                            rx = /^[0-9]{2}[-][0-9]{2}$/;
                        if (rx.test(val)) {
                            var _d = new Date(year + "-" + month + "-" + date);
                            res.val(fn_addDays(_d, 280));
                        }
                    }
                }
                catch(e){
                    console.log(e);
                    res.val('Not a valid date-month');
                }
            }
        });

        return View;

    });