define([
    "apiConfig",
    "constantsConfig",
    "core/controller",
    "modules/healthyWay/views/view"
], function (
    Config,
    Constants,
    Controller,
    HealthyWayView
) {

    "use strict";

    var Controller = Controller.extend({

        init: function () {

            return this;
        },


        events: function (args) {

            return this;
        },

        healthyWay: function (args)
        {
            if (!$(".loader").hasClass("hide")) {
                $(".loader").addClass("hide");
            }
            var self = this,
                view = new HealthyWayView({ controller : self, el: Constants.CONTENT, args: args });
            view.clear().render();
        }
    });

    return Controller;

});
