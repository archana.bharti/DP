﻿({
   waitSeconds : 120,
   paths: {

        //jquery
        "jquery": "libs/jquery/jquery.min",
        "jqueryUi": "libs/jquery/jquery-ui.min",

        //underscore & backbone
        "underscore": "libs/backbone/lodash",
        "backbone": "libs/backbone/backbone",
        
        "backbone-gmaps": "libs/backbone/backbone.googlemaps",
        "text": "libs/require/text",
        "bar-rating": "libs/jquery/jquery.barrating.min",
        "summernote": "libs/summernote.min",
        "codemirror": "libs/codemirror.min",
        "circliful": "libs/jquery/jquery.circliful.min",

        "gmaps": "libs/google/gmaps",
        "z-tabs": "libs/plugins/tabs",
        "owl-carousel": "libs/plugins/owl.carousel",

        //bootstrap
        "bootstrap": "libs/bootstrap/bootstrap.min",
        "bootstrapValidator": "libs/bootstrap/bootstrap-validator",
        "bootstrapSelect": "libs/bootstrap/select2.min",
        "bootstrap-dialog": "libs/bootstrap/bootstrap-dialog",
        "bootstrapTable": "libs/bootstrap/bootstrap-table.min",
        "bootstrapDatetimePicker": "libs/bootstrap/bootstrap-datepicker",
        "bootstrap-rating": "libs/bootstrap/bootstrap-star-rating",
        "metis-menu": "libs/bootstrap/metis-menu.min",

       
        "highcharts": "libs/highcharts/highcharts",
        "highcharts-more": "libs/highcharts/highcharts-more",
        "highcharts-guage": "libs/highcharts/highcharts-guage",

       
        "chain": "libs/plugins/chain",
        "convert": "libs/plugins/convert",
        "jqExt": "libs/plugins/jqExtensions",
        "cookie": "libs/plugins/cookie.min",
        "slimScroll": "libs/plugins/slim-scroll",
        "hcm": "libs/plugins/hcm",
        "moment": "libs/plugins/moment",

       
        "apiConfig": "config/api",
        "constantsConfig": "config/constants",

       
        "modelBase": "core/model",
        "collectionBase": "core/collection",
        "viewBase": "core/view",
        "controllerBase": "core/controller",

    },
    shim: {
        "jqueryUi": ["jquery"],
        "bootstrap": ["jquery"],
        "bootstrapValidator": ["jquery"],
        "bootstrapTable": ["jquery"],
        "bootstrap-rating": ["jquery"],
        "bootstrapDatetimePicker": ["jquery", "moment"],
        "metis-menu": ["jquery", "bootstrap"],
        "bootstrapSelect": ["jquery"],
        "highcharts": ["jquery"],
        "highcharts-more": ["jquery", "highcharts"],
        "highcharts-guage": ["jquery", "highcharts", "highcharts-more"],
        "tooltip": ["jquery"],
        "cookie": ["jquery"],
        "convert": ["jquery"],
        "gmaps": ["jquery"],
        "async": ["jquery"],
        "jqExt": ["jquery"],
        "chain": ["backbone"],
        "slimScroll": ["jquery"],
        "bar-rating": ["jquery"],
        "summernote": ["jquery"],
        "z-tabs": ["jquery"],
        "owl-carousel": ['jquery'],
        "backbone-gmaps": ["jquery"],
        'bootstrap-dialog': {
            deps: ["jquery", "underscore", "backbone", "bootstrap"]
        },
        "backbone": {
            "deps": ["underscore", "jquery"],
            "exports": "Backbone"
        },
        "underscore": {
            "exports": '_'
        },
    },
    appDir: '',
    baseUrl: './js',
    dir: './dist',
    modules: [
        {
            name: 'main',
        }
    ],
    fileExclusionRegExp: /^(r|build)\.js$/,
    //optimizeCss: 'standard',
    removeCombined: true
})