﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public partial class ModelFactory
    {
        public static List<ClinicViewModel> ConvertToClinicVMs(List<ClinicAndDoctor> dbClinics)
        {
            List<ClinicViewModel> vms = new List<ClinicViewModel>();
            foreach(ClinicAndDoctor dbModel in dbClinics)
            {
                vms.Add(new ClinicViewModel()
                {
                    Id = dbModel.Id,
                    Name = dbModel.Name,
                    City_Name = (dbModel.City == null)? "-not defined-" : dbModel.City.Name,
                    State_Name = (dbModel.State == null) ? "-not defined-" : dbModel.State.Name,
                    Location_Name = (dbModel.Location == null) ? "-not defined-" : dbModel.Location.Name,
                });
            }
            return vms;
        }
    }
}
