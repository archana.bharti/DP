﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class ClinicViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Summary { get; set; }
        public string State_Name { get; set; }
        public string City_Name { get; set; }
        public string Location_Name { get; set; }
        public string Street { get; set; }
        public int Category { get; set; }
        public int Review_Count { get; set; }
        public int Recommended_Count { get; set; }
    }
}
