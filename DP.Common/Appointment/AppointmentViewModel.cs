﻿using System;

namespace DP.Common.Models
{
    public class AppointmentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int EntityId { get; set; }
        public int EntityType { get; set; }
        public string Token { get; set; }
    }
}
