﻿using DP.Common.Enums;
using DP.Common.Utilities;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;

namespace DP.Common.Models
{
    public partial class ModelFactory
    {
        public static Appointment ConvertAppointmentDbm(AppointmentViewModel viewModel)
        {
            Random rnd = new Random();
            var v = rnd.Next(9999999, 999999999);
            Appointment model = new Appointment()
            {
                Name = viewModel.Name,
                Email = viewModel.Email,
                Mobile = viewModel.Mobile,
                EntityId = viewModel.EntityId,
                EntityType = viewModel.EntityType,
                StartTime = viewModel.StartTime,
                EndTime = viewModel.EndTime
            };
            return model;
        }
    }
}
