﻿using DP.Services.Data.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DP.Common.Utilities;
using DP.Common.Enums;

namespace DP.Common.Models
{
    public partial class ModelFactory
    {
        public static ApplicationUser ConvertUserVmToDbm(UserRegistrationViewModel vm)
        {
            ApplicationUser dbModel = new ApplicationUser();

            dbModel.CityId = vm.CityId;
            dbModel.CreatedOn = DateTime.Now;
            dbModel.LocationId = vm.LocationId;
            dbModel.StateId = vm.StateId;
            dbModel.UserType = EnumHelper<UserType>.GetDescription((UserType)Convert.ToInt32(vm.RegistrationType));
            dbModel.PhoneNumber = vm.Contact;
            dbModel.UserName = vm.Email;
            dbModel.Email = vm.Email;
            dbModel.DisplayName = vm.DisplayName;
            dbModel.SubscriberId = string.IsNullOrEmpty(vm.UserId) ? null : ( Convert.ToInt32(vm.UserId) > 0 ? (int?)Convert.ToInt32(vm.UserId) : null );

            return dbModel;
        }

        public static SubscriberViewModel ConvertToSubscriberViewModel(SearchEntityBase dbModel)
        {
            SubscriberViewModel vm = new SubscriberViewModel();
            vm.City = dbModel.City;
            vm.DisplayPhone = dbModel.Display_Phone;
            vm.ImageUrl = dbModel.Profile_Pic_1;
            vm.ImageUrl2 = dbModel.Profile_Pic_2;
            vm.ImageUrl3 = dbModel.Profile_Pic_3;
            vm.Website = dbModel.Website;
            vm.VideoUrl = dbModel.VideoUrl;
            vm.Location = dbModel.Location;
            vm.Name = dbModel.Name;
            vm.FullAddress = dbModel.FullAddress;
            vm.Phone_1 = dbModel.Phone_No1;
            vm.Phone_2 = dbModel.Phone_No2;
            vm.Phone_3 = dbModel.Phone_No3;
            vm.Phone_4 = dbModel.Phone_No4;
            vm.State = dbModel.State;
            vm.SubscriberId = dbModel.Id;
            vm.SubscriberType = (int) EnumHelper<UserType>.GetValueFromDescription(dbModel.Type);
            vm.Summary = dbModel.Summary;
            return vm;
        }

        public static SubscriberViewModel ExtendSubscriberViewModel(SubscriberViewModel viewModel, ApplicationUser identityUser = null, Feature feature = null)
        {
            if (identityUser != null)
            {
                viewModel.DisplayName = Convert.ToString(identityUser.DisplayName);
                viewModel.Email = Convert.ToString(identityUser.Email);
                viewModel.IsFeatured = true;
                viewModel.IsTerminated = identityUser.IsTerminated;
                viewModel.UserId = identityUser.Id;
            }
            
            if(feature != null)
            {
                viewModel.Plan = new SubscriberPlanViewModel();
                viewModel.Plan.EnableBasic = feature.EnableBasic;
                viewModel.Plan.EnableFeatured = feature.EnableFeatured;
                viewModel.Plan.EnableTextual = feature.EnableTextual;
                viewModel.Plan.EndDate = feature.EndDate;
                viewModel.Plan.EntityId = viewModel.SubscriberId.Value;
                viewModel.Plan.EntityType =(UserType) viewModel.SubscriberType;
                viewModel.Plan.Id = feature.Id;
                viewModel.IsFeatured = true;
                viewModel.Plan.IsActive = (feature.Status.Id == (int)UserStatus.Active);
            }
            return viewModel;
        }

        public static SubscriberViewModel ConvertToSubscriberViewModel(ApplicationUser appUser)
        {
            SubscriberViewModel vm = new SubscriberViewModel();
            vm.Name = appUser.DisplayName;
            vm.DisplayName = appUser.DisplayName;
            vm.City = appUser.City;
            vm.Email = appUser.Email;
            vm.Phone_1 = appUser.PhoneNumber;
            vm.UserId = appUser.Id;

            return vm;
        }



        public static Feature ConvertToFeatureDbm(ApplicationUser user)
        {
            return new Feature()
            {
                EntityId = user.SubscriberId.Value,
                EntityType = (int)EnumHelper<UserType>.GetValueFromDescription(user.UserType),
                StartDate = new DateTime(),
                StatusValue = 4
            };
        }

        public static UserFinderViewModel ConvertToUserSuggestionVM(dynamic model)
        {
            return new UserFinderViewModel()
            {
                Id = model.Id,
                Name =  model.Name,
                LocationId = (model.LocationId != null) ? model.LocationId : 0,
                StateId = (model.StateId != null) ? model.StateId : 0,
                CityId = (model.CityId != null) ? model.CityId : 0,
                MobileNo = model.Phone_No1
            };
        }

        public static Feature ConvertToFeatureDbm(SubscriberPlanViewModel vm)
        {
            return new Feature()
            {
                EntityId = vm.EntityId,
                EntityType = (int)vm.EntityType,
                StartDate = vm.StartDate,
                EndDate = vm.EndDate,
                EnableBasic = vm.EnableBasic,
                EnableFeatured = vm.EnableFeatured,
                EnableTextual = vm.EnableTextual
            };
        }


        public static InsuranceTypeFinderViewModel ConvertToPlanNameSuggestionVM(dynamic model)
        {
            return new InsuranceTypeFinderViewModel()
            {
                Id = model.Id,
                Name = model.Name,
                
            };
        }

    }
}
