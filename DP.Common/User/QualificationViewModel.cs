﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common
{
    public class QualificationViewModel
    {
        public QualificationDoctor DoctorQualification { get; set; }
        public QualificationLong LongQualification { get; set; }
        public QualificationShort ShortQualification { get; set; }

        public QualificationViewModel()
        {
            this.DoctorQualification = new QualificationDoctor();
            this.LongQualification = new QualificationLong();
            this.ShortQualification = new QualificationShort();
        }
    }

}
