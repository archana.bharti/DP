﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DP.Common.Models
{
    public class UserRegistrationViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string DisplayName { get; set; }

        private int? _stateId;
        public int? StateId
        {
            get
            {
                return (_stateId.HasValue && _stateId.Value == 0) ? null : _stateId;
            }
            set
            {
                _stateId = value;
            }
        }

        private int? _cityId;
        public int? CityId
        {
            get
            {
                return (_cityId.HasValue && _cityId.Value == 0) ? null : _cityId;
            }
            set
            {
                _cityId = value;
            }
        }

        private int? _locationId;
        public int? LocationId
        {
            get
            {
                return (_locationId.HasValue && _locationId.Value == 0) ? null : _locationId;
            }
            set
            {
                _locationId = value;
            }
        }

        [Required]
        public string Contact { get; set; }

        
        public string UserPhone { get; set; }

        public bool ShowContactOnProfileView { get; set; }
        [Required]
        public string RegistrationType { get; set; }
        [Required]
        public string RoleId { get; set; }
       
        public string UserId { get; set; }
    }
}
