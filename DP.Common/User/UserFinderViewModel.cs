﻿using DP.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace DP.Common.Models
{
    public class UserFinderViewModel
    {
        [Required]
        public UserType UserType { get; set; }
        public int? SubType { get; set; }
        [Required]
        public string SearchTerm { get; set; }
        public bool ExcludeSubscribers { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public int LocationId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string MobileNo { get; set; }
    }
}
