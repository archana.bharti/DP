﻿using DP.Common.Enums;
using DP.Common.Utilities;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class SubscriberViewModel
    {
        public string UserId { get; set; }
        public int? SubscriberId { get; set; }
        public int SubscriberType { get; set; }
        public string SubscriberTypeText {
            get
            {
                return EnumHelper<UserType>.GetDescription((UserType)this.SubscriberType);
            }
        }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool IsTerminated { get; set; }
        public bool IsFeatured { get; set; }
        public string ImageUrl { get; set; }
        public string VideoUrl { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Summary { get; set; }
        public string Phone_1 { get; set; }
        public string Phone_2 { get; set; }
        public string Phone_3 { get; set; }
        public string Phone_4 { get; set; }
        public string FullAddress { get; set; }
        public string DisplayPhone { get; set; }
        public City City { get; set; }
        public State State { get; set; }
        public Location Location { get; set; }
        public SubscriberPlanViewModel Plan { get; set; }
        public List<SpecializationViewModel> Specializations { get; set; }
        public List<QualificationLong> LongQualifications;
        public List<QualificationShort> ShortQualifications;
        public int ReviewsCount { get; set; }
        public int RecommendationsCount { get; set; }
        public object TopRated { get; set; }

        public string ImageUrl2 { get; set; }
        public string ImageUrl3 { get; set; }

        public SubscriberViewModel()
        {
            this.LongQualifications = new List<QualificationLong>();
            this.ShortQualifications = new List<QualificationShort>();
        }
    }
}
