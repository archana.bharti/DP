﻿using DP.Common.Enums;
using DP.Common.Utilities;
using System;

namespace DP.Common.Models
{
    public class SubscriberPlanViewModel
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public UserType EntityType { get; set; }
        public string EntityTypeText { get
            {
                return EnumHelper<UserType>.GetDescription(this.EntityType);
            }
        }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }
        public bool EnableBasic { get; set; }
        public bool EnableTextual { get; set; }
        public bool EnableFeatured { get; set; }
    }
}
