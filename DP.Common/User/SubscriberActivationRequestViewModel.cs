﻿using DP.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class SubscriberActivationRequestViewModel
    {
        public int SubscriberId { get; set; }
        public UserType UserType { get; set; }
        public UserStatus Status { get; set; }
    }
}
