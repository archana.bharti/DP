﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class PdaViewModel : PdaBase
    {
        public string UserId { get; set; }
        public int CityId { get; set; }
        public int DoctorId { get; set; }
        public int SpecializationId { get; set; }
        public string SpecializationIds { get; set; }
    }
}
