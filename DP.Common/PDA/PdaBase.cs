﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public abstract class PdaBase
    {
        public int? Id { get; set; }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string UserEmailStatus { get; set; }
        public string DoctorEmail { get; set; }
        public string DoctorName { get; set; }
        public string SpecializationName { get; set; }
        public string CityName { get; set; }
        public string DoctorEmailStatus { get; set; }
        public string Subject { get; set; }
        public string Answer { get; set; }
        public string Question { get; set; }
        public string RepliedOn { get; set; }
        public string SentOn { get; set; }
        public bool IsAnswered
        {
            get
            {
                return (!string.IsNullOrEmpty(this.Answer) && this.Answer.Trim().Length > 0);
            }
        }
    }
}
