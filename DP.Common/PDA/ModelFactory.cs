﻿using DP.Common.Enums;
using DP.Services.Data.Models;
using System;

namespace DP.Common.Models
{
    public partial class ModelFactory
    {
        public static PersonalAdvice ConvertToPdaDbm(PdaViewModel viewModel)
        {
            PersonalAdvice dbModel = new PersonalAdvice()
            {
                Id = (viewModel.Id.HasValue && viewModel.Id.Value > 0) ? viewModel.Id.Value : 0,
                CityId = viewModel.CityId,
                Question = viewModel.Question,
                SpecializationId = viewModel.SpecializationId,
                Subject = viewModel.Subject,
                Answer = viewModel.Answer,
                SubmittedDate = DateTime.Now,
                UserId = viewModel.UserId,
                UserEmail = viewModel.UserEmail,
                DoctorEmailStatusValue = (int)MailStatus.Pending,
                UserEmailStatusValue = (int)MailStatus.Pending
            };
            return dbModel;
        }

        public static PdaViewModel ConvertToPdaVm(PersonalAdvice dbModel)
        {
            PdaViewModel vm = new PdaViewModel();
            vm.Id = dbModel.Id;
            vm.Answer = dbModel.Answer;
            vm.CityId = dbModel.CityId.HasValue ? dbModel.City.Id : 0;
            vm.CityName = dbModel.CityId.HasValue ? dbModel.City.Name : string.Empty;
            vm.DoctorEmailStatus = dbModel.DoctorEmailStatus.Text;
            vm.DoctorId = dbModel.DoctorId;
            vm.DoctorName = dbModel.Doctor.Name;
             vm.Question = dbModel.Question;
            vm.RepliedOn = dbModel.RepliedDate.HasValue ? dbModel.RepliedDate.Value.ToString("yyyy-MM-dd HH:mm:ss") : string.Empty;
            vm.SentOn = dbModel.SubmittedDate.ToString("yyyy-MM-dd HH:mm:ss");
            vm.SpecializationId = dbModel.SpecializationId;
            vm.SpecializationName = dbModel.Specialization.Category;
            vm.Subject = dbModel.Subject;
            vm.UserEmail = dbModel.UserEmail;
            vm.UserEmailStatus = dbModel.UserEmailStatus.Text;
             vm.UserName = dbModel.User.UserName;
            vm.DoctorEmail = dbModel.DoctorEmail;
            vm.UserId = dbModel.UserId;

            return vm;
        }
    }
}
