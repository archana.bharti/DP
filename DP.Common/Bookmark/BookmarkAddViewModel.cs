﻿namespace DP.Common.Models
{
    public class BookmarkAddViewModel
    {
        public string UserId { get; set; }
        public int BookmarkId { get; set; }
        public string BookmarkName { get; set; }
        public int Type { get; set; }
    }
}
