﻿namespace DP.Common.Models
{
    public class BookmarkViewModel
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int BookmarkId { get; set; }
        public string BookmarkName { get; set; }
        public int Type { get; set; }
        public string TypeText { get; set; }
    }
}
