﻿namespace DP.Common.Models
{
    public class FeaturedEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
