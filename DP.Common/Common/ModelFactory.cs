﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public partial class ModelFactory
    {
        public static List<TextValueViewModel> ConvertToTextValuePair<T>(IList<T> source, string value, string text, string[] others = null)
        {
            List<TextValueViewModel> list = new List<TextValueViewModel>();
            foreach (var item in source)
            {
                Type t = item.GetType();
                var val = Convert.ToString(t.GetProperty(value).GetValue(item));
                var txt = Convert.ToString(t.GetProperty(text).GetValue(item));
                Dictionary<string, object> othersDictionary = null;
                if (others != null && others.Length > 0)
                {
                    othersDictionary = new Dictionary<string, object>();
                    foreach (string s in others)
                    {
                        var oVal = Convert.ToString(t.GetProperty(s).GetValue(item));
                        var oText = s;
                        if (!othersDictionary.ContainsKey(oText))
                        {
                            othersDictionary.Add(oText, oVal);
                        }
                    }
                }
                list.Add(new TextValueViewModel() { Text = txt, Value = val, Others = othersDictionary });
            }

            return list;
        }

        public static FeaturedEntity ConvertToFeaturedEntity(dynamic model)
        {
            return new FeaturedEntity()
            {
                Id = model.Id,
                Name = model.Name,
                Type = model.Type
            };
        }
    }
}
