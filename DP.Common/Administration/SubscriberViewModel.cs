﻿using DP.Common.Enums;
using DP.Common.Utilities;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models.Administration
{
    public class SubscriberViewModel
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public bool IsTerminated { get; set; }
        public int SubscriberId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Type { get; set; }
        public UserType UserType
        {
            get
            {
                return EnumHelper<UserType>.GetValueFromDescription(this.Type);
            }

        }
        public string Location { get; set; }
        public string Specializations { get; set; }
        public int RecommendationCount { get; set; }
        public int RatingCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool EnableBasic { get; set; }
        public bool EnableTextual { get; set; }
        public bool EnableFeatured { get; set; }
        public int Active { get; set; }
    }
}
