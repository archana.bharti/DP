﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class DatabaseTableViewModel
    {
        public string TableName { get; set; }
        public string EntityName { get; set; }
        public string Type { get; set; }
    }

    public class GridColumn
    {
        public string Field { get; set; }
        public string Title { get; set; }
    }
}
