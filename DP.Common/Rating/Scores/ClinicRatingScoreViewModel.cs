﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class ClinicRatingScoreViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public float CleanlinessPercentage { get; set; }
        public float FeesChargedPercentage { get; set; }
        public float WaitingHoursPercentage { get; set; }
        public float ListenPercentage { get; set; }
        public float ComfortPercentage { get; set; }
        public float FriendlyStaffPercentage { get; set; }
        public float CustomerSatisfactionPercentage { get; set; }
        public float QualityOfServicePercentage { get; set; }
        public float OnCallPercentage { get; set; }
        public float LatestTechnologyPercentage { get; set; }
    }

    public class InsuranceRatingScoreViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public float CustomerServicePercentage { get; set; }
        public float InNetworkFacilitiesPercentage { get; set; }
        public float EaseOfClaimFillingPercentage { get; set; }
        public float MeetsPromisedBenefitsPercentage { get; set; }
        public float ValueForMoneyPercentage { get; set; }
        public float ClaimSettlementPercentage { get; set; }
        public float OverallSatisfactionPercentage { get; set; }
    }
}
