﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class HospitalRatingScoreViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public float InfrastructurePercentage { get; set; }
        public float SafetyCleanlinessPercentage { get; set; }
        public float CourteousStaffPercentage { get; set; }
        public float CafeFoodPercentage { get; set; }
        public float QualityOfCarePercentage { get; set; }
        public float VistingHoursPercentage { get; set; }
        public float OvernightStayPercentage { get; set; }
        public float EmergencyCarePercentage { get; set; }
        public float OverallSatisfactionPercentage { get; set; }
    }
}
