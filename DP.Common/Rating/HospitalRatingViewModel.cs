﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class HospitalRatingViewModel : BaseRatingViewModel
    {
        public HospitalRatingViewModel()
        {
            Infrastructure = 1;
            SafetyCleanliness = 1;
            CourteousStaff = 1;
            CafeFood = 1;
            QualityOfCare = 1;
            VistingHours = 1;
            OvernightStay = 1;
            EmergencyCare = 1;
        }
        public int HospitalId { get; set; }
        public int Infrastructure { get; set; }
        public int SafetyCleanliness { get; set; }
        public int CourteousStaff { get; set; }
        public int CafeFood { get; set; }
        public int QualityOfCare { get; set; }
        public int VistingHours { get; set; }
        public int OvernightStay { get; set; }
        public int EmergencyCare { get; set; }
        public int OverallSatisfaction { get; set; }
        public string LastVisited { get; set; }
    }
}
