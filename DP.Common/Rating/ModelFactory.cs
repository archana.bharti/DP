﻿// ***********************************************************************
// Assembly         : DP.Common
// Author           : GWE
// Created          : 10-28-2015
//
// Last Modified By : GWE
// Last Modified On : 10-31-2015
// ***********************************************************************
// <copyright file="ModelFactory.cs" company="">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Models namespace.
/// </summary>
namespace DP.Common.Models
{
    /// <summary>
    /// Class ModelFactory.
    /// </summary>
    public partial class ModelFactory
    {
        /// <summary>
        /// Converts to rating database model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>ClinicRating.</returns>
        public static ClinicRating ConvertToRatingDbModel(ClinicRatingViewModel viewModel)
        {
            ClinicRating dbModel = new ClinicRating();
            MapToDbModel(viewModel, ref dbModel);
            dbModel.Id = viewModel.Id.HasValue ? viewModel.Id.Value : 0;
            dbModel.UserId = viewModel.UserId;
            dbModel.ReviewedDate = DateTime.Now;
            return dbModel;
        }

        public static InsuranceRating ConvertToRatingDbModel(InsuranceRatingViewModel viewModel)
        {
            InsuranceRating dbModel = new InsuranceRating();
            MapToDbModel(viewModel, ref dbModel);
            dbModel.Id = viewModel.Id.HasValue ? viewModel.Id.Value : 0;
            dbModel.UserId = viewModel.UserId;
            dbModel.ReviewedDate = DateTime.Now;
            return dbModel;
        }

        public static HospitalRating ConvertToRatingDbModel(HospitalRatingViewModel viewModel)
        {
            HospitalRating dbModel = new HospitalRating();
            MapToDbModel(viewModel, ref dbModel);
            dbModel.Id = viewModel.Id.HasValue ? viewModel.Id.Value : 0;
            dbModel.UserId = viewModel.UserId;
            dbModel.ReviewedDate = DateTime.Now;
            return dbModel;
        }

        public static CollegeRating ConvertToRatingDbModel(CollegeRatingViewModel viewModel)
        {
            CollegeRating dbModel = new CollegeRating();
            MapToDbModel(viewModel, ref dbModel);
            dbModel.Id = viewModel.Id.HasValue ? viewModel.Id.Value : 0;
            dbModel.UserId = viewModel.UserId;            
            dbModel.ReviewedDate = DateTime.Now;
            return dbModel;
        }


        /// <summary>
        /// Converts to rating view model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>ClinicRatingAddOrUpdateViewModel.</returns>
        public static ClinicRatingViewModel ConvertToRatingViewModel(ClinicRating model)
        {
            ClinicRatingViewModel viewModel = new ClinicRatingViewModel();
            if (model != null)
            {
                MapToModel<ClinicRating, ClinicRatingViewModel>(model, ref viewModel);
                if (model.User != null)
                {
                    if (!string.IsNullOrEmpty(model.User.DisplayName))
                    {
                        viewModel.UserName = model.User.DisplayName.Contains('@') ? model.User.DisplayName.Substring(0, model.User.DisplayName.IndexOf('@')) : model.User.DisplayName;
                    }
                    else
                    {
                        viewModel.UserName = model.User.UserName.Substring(0, model.User.UserName.IndexOf('@'));
                    }
                }
                viewModel.SubscriberName = model.ClinicOrDoctor.Name;
            }
            return viewModel;
        }

        /// <summary>
        /// Converts to scores view model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>ClinicRatingScoreViewModel.</returns>
        public static ClinicRatingScoreViewModel ConvertToScoresViewModel(ClinicRating model)
        {
            ClinicRatingScoreViewModel viewModel = new ClinicRatingScoreViewModel();

            if (model != null)
                MapToModel<ClinicRating, ClinicRatingScoreViewModel>(model, ref viewModel);

            return viewModel;
        }
        
        /// <summary>
        /// Converts to rating view model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>HospitalRatingAddOrUpdateViewModel.</returns>
        public static HospitalRatingViewModel ConvertToRatingViewModel(HospitalRating model)
        {
            HospitalRatingViewModel viewModel = new HospitalRatingViewModel();
            if (model != null)
            {
                MapToModel(model, ref viewModel);
                if (model.User != null)
                {
                    if (!string.IsNullOrEmpty(model.User.DisplayName))
                    {
                        viewModel.UserName = model.User.DisplayName;
                    }
                    else
                    {
                        viewModel.UserName = model.User.UserName.Substring(0, model.User.UserName.IndexOf('@'));
                    }
                }
                viewModel.SubscriberName = model.Hospital.Name;
            }
            return viewModel;
        }

        public static InsuranceRatingViewModel ConvertToRatingViewModel(InsuranceRating model)
        {
            InsuranceRatingViewModel viewModel = new InsuranceRatingViewModel();
            if (model != null)
            {
                MapToModel<InsuranceRating, InsuranceRatingViewModel>(model, ref viewModel);
                if (model.User != null)
                {
                    if (!string.IsNullOrEmpty(model.User.DisplayName))
                    {
                        viewModel.UserName = model.User.DisplayName;
                    }
                    else
                    {
                        viewModel.UserName = model.User.UserName.Substring(0, model.User.UserName.IndexOf('@'));
                    }
                }
                viewModel.SubscriberName = model.Insurance.Name;
            }
            return viewModel;
        }

        /// <summary>
        /// Converts to scores view model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>HospitalRatingScoreViewModel.</returns>
        public static HospitalRatingScoreViewModel ConvertToScoresViewModel(HospitalRating model)
        {
            HospitalRatingScoreViewModel viewModel = new HospitalRatingScoreViewModel();

            if (model != null)
                MapToModel<HospitalRating, HospitalRatingScoreViewModel>(model, ref viewModel);

            return viewModel;
        }

        public static InsuranceRatingScoreViewModel ConvertToScoresViewModel(InsuranceRating model)
        {
            InsuranceRatingScoreViewModel viewModel = new InsuranceRatingScoreViewModel();

            if (model != null)
                MapToModel<InsuranceRating, InsuranceRatingScoreViewModel>(model, ref viewModel);

            return viewModel;
        }

        /// <summary>
        /// Converts to rating database model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>CollegeRating.</returns>
       

        /// <summary>
        /// Converts to rating view model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>CollegeRatingAddOrUpdateViewModel.</returns>
        public static CollegeRatingViewModel ConvertToRatingViewModel(CollegeRating model)
        {
            CollegeRatingViewModel viewModel = new CollegeRatingViewModel();
            if (model != null)
            {
                MapToModel(model, ref viewModel);
                if (model.User != null)
                {
                    if (!string.IsNullOrEmpty(model.User.DisplayName))
                    {
                        viewModel.UserName = model.User.DisplayName;
                    }
                    else
                    {
                        viewModel.UserName = model.User.UserName.Substring(0, model.User.UserName.IndexOf('@'));
                    }
                }
                viewModel.SubscriberName = model.College.Name;
            }
            return viewModel;
        }

        /// <summary>
        /// Maps to database model.
        /// </summary>
        /// <typeparam name="T1">The type of the t1.</typeparam>
        /// <typeparam name="T2">The type of the t2.</typeparam>
        /// <param name="viewModel">The view model.</param>
        /// <param name="dbModel">The database model.</param>
        private static void MapToDbModel<T1, T2>(T1 viewModel, ref T2 dbModel)
        {
            var viewModelProperties = viewModel.GetType().GetProperties();
            var dbModelProperties = dbModel.GetType().GetProperties();

            foreach (var dbProp in dbModelProperties)
            {
                var prop = viewModelProperties
                                    .Where(x => x.Name == dbProp.Name)
                                    .SingleOrDefault();
                if (prop != null)
                    dbProp.SetValue(dbModel, prop.GetValue(viewModel));
            }
        }

        /// <summary>
        /// Maps to model.
        /// </summary>
        /// <typeparam name="T1">The type of the t1.</typeparam>
        /// <typeparam name="T2">The type of the t2.</typeparam>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        private static void MapToModel<T1, T2>(T1 from, ref T2 to)
        {
            var fromProperties = from.GetType().GetProperties();
            var toProperties = to.GetType().GetProperties();

            foreach (var prop in toProperties)
            {
                var property = fromProperties
                                    .Where(x => x.Name == prop.Name)
                                    .SingleOrDefault();
                if (property != null)
                    prop.SetValue(to, property.GetValue(from));
            }
        }
    }



}
