﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class ClinicRatingViewModel : BaseRatingViewModel
    {
        public ClinicRatingViewModel()
        {
            Cleanliness = 1;
            FeesCharged = 1;
            WaitingHours = 1;
            Listen = 1;
            Comfort = 1;
            FriendlyStaff = 1;
            CustomerSatisfaction = 1;
            QualityOfService = 1;
            OnCall = 1;
            LatestTechnology = 1;
        }
        public int ClinicOrDoctorId { get; set; }
        public int Cleanliness { get; set; }
        public int FeesCharged { get; set; }
        public int WaitingHours { get; set; }
        public int Listen { get; set; }
        public int Comfort { get; set; }
        public int FriendlyStaff { get; set; }
        public int CustomerSatisfaction { get; set; }
        public int QualityOfService { get; set; }
        public int OnCall { get; set; }
        public int LatestTechnology { get; set; }
        public string LastVisited { get; set; }
        //public string YourExperience { get; set; }
    }
}
