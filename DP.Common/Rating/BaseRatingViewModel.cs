﻿using DP.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public abstract class BaseRatingViewModel
    {
        public int? Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string SubscriberName { get; set; }
        public DateTime ReviewedDate { get; set; }
    }
}
