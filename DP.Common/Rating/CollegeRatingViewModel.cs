﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class CollegeRatingViewModel : BaseRatingViewModel
    {
        public CollegeRatingViewModel()
        {
            QualityOfEducation = 1;
            Location = 1;
            Library = 1;
            Campus = 1;
            Food = 1;
            SocialActivities = 1;
            IntenetConnectivity = 1;
            OverallSatisfaction = 1;
        }
        public int CollegeId { get; set; }
        public int QualityOfEducation { get; set; }
        public int Location { get; set; }
        public int Library { get; set; }
        public int Campus { get; set; }
        public int Food { get; set; }
        public int SocialActivities { get; set; }
        public int IntenetConnectivity { get; set; }
        public int OverallSatisfaction { get; set; }
    }
}
