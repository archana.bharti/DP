﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class InsuranceRatingViewModel : BaseRatingViewModel
    {
        public InsuranceRatingViewModel()
        {
            InsuranceId = 1;
            CustomerService = 1;
            InNetworkFacilities = 1;
            EaseOfClaimFilling = 1;
            MeetsPromisedBenefits = 1;
            ValueForMoney = 1;
            ClaimSettlement = 1;
            OverallSatisfaction = 1;
        }

        public int InsuranceId { get; set; }
        public int CustomerService { get; set; }
        public int InNetworkFacilities { get; set; }
        public int EaseOfClaimFilling { get; set; }
        public int MeetsPromisedBenefits { get; set; }
        public int ValueForMoney { get; set; }
        public int ClaimSettlement { get; set; }
        public int OverallSatisfaction { get; set; }
    }
}
