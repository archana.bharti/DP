﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class UserRatingAddOrUpdateViewModel : BaseRatingViewModel
    {
        public List<ClinicRatingViewModel> ClinicRatings { get; set; }
        public List<CollegeRatingViewModel> CollegeRatings { get; set; }
        public List<HospitalRatingViewModel> HospitalRatings { get; set; }
        public List<InsuranceRatingViewModel> InsuranceRatings { get; set; }
    }
}
