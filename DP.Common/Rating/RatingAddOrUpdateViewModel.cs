﻿using DP.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class RatingAddOrUpdateViewModel
    {
        public UserType UserType { get; set; }
        public ClinicRatingViewModel Clinic { get; set; }
        public CollegeRatingViewModel College { get; set; }
        public HospitalRatingViewModel Hospital { get; set; }
        public InsuranceRatingViewModel Insurance { get; set; }
    }
}
