﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class ZoneSearchResponseViewModel
    {
        public int EntityId { get; set; }
        public string EntityType { get; set; }
        public string Name { get; set; }
        public string Summary { get; set; }
        public string ImageUrl { get; set; }
        public string LocationName { get; set; }
    }
}
