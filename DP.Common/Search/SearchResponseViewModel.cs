﻿using DP.Common.Constants;
using DP.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class SearchResponseViewModel
    {
        public List<SearchResultViewModel> Rows { get; set; }
        public int Total { get; set; }
        public SearchResponseViewModel()
        {
            Rows = new List<SearchResultViewModel>();
        }
    }

    public class SearchResultViewModel
    {
        public string UID { get; set; }
        public int? EntityId { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Name { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string SpecializationIds { get; set; }
        public string SpecializationNames { get; set; }
        public string LongQualifications { get; set; }
        public string ShortQualifications { get; set; }
        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public double Distance { get; set; }
        public int ReviewCount { get; set; }
        public int RecommendedCount { get; set; }
        public bool IsFeatured { get; set; }
        public int Radius { get; set; }
        public bool IsRecommended { get; set; }

        private string _imageUrl;
        public string ImageUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_imageUrl))
                {
                    CacheManager cacheManager = new CacheManager();
                    Dictionary<string, object> fromCache = cacheManager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object>;
                    var rootPath = Convert.ToString(fromCache[GlobalSettings.S3_ROOT_PATH]);
                    _imageUrl = rootPath + Type.ToLower() + "-default-300x300.png";
                    return _imageUrl;
                }
                return _imageUrl;
            }
            set
            {
                _imageUrl = value;
            }
        }

    }
}
