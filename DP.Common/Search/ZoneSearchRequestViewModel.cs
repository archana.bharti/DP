﻿using DP.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class ZoneSearchRequestViewModel
    {
        public int? LocationId { get; set; }
        public int? StateId { get; set; }
        public UserType? userType { get; set; }
    }
}
