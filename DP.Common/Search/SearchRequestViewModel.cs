﻿using DP.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class SearchRequestViewModel
    {
        [Required]
        public SearchType SearchType { get; set; }
        public DoctorOrClinicCategoryType DoctorOrClinicCategoryType { get; set; }
        public int? DoctorOrClinicId { get; set; }
        public int? LocationId { get; set; }
        public int? CityId { get; set; }
        public int? StateId { get; set; }
        public int? CollegeId { get; set; }
        public int? HospitalId { get; set; }
        public int? SpecializationId { get; set; }
        public int? DepartmentId { get; set; }
        public bool ByFeatured { get; set; }
        public int Distance { get; set; }
        public int ByReview { get; set; }
        public int ByRecommended { get; set; }
        public int ByDistance { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string Ip { get; set; }
        public int Take
        {
            get
            {
                return Limit;
            }
        }
        public int Skip
        {
            get
            {
                return Offset;
            }
        }
    }
}
