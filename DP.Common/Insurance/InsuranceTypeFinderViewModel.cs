﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class InsuranceTypeFinderViewModel
    {
        public string searchTerm { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string PlanType { get; set; }
    }
}
