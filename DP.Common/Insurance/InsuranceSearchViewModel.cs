﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class InsuranceSearchViewModel
    {
        public Insurance insurance;
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string Ip { get; set; }
        public string Search { get; set; }
        public int Take
        {
            get
            {
                return Limit;
            }
        }
        public int Skip
        {
            get
            {
                return Offset;
            }
        }
    }
}
