﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class EndorsementAddOrUpdateViewModel
    {
        public int? Id { get; set; }
        public int EndorserId { get; set; }
        public int EndorserType { get; set; }
        public int EndorseeId { get; set; }
        public int EndorseeType { get; set; }
        public int[] Specializations { get; set; }
    }
}
