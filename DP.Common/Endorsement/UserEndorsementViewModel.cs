﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class UserEndorsementViewModel
    {
        public int SpecializationId { get; set; }
        public string SpecializationName { get; set; }
        public int Count { get; set; }
        public string Users { get; set; }
        public string Type { get; set; }
        public List<EndorserViewModel> Endorsers { get; set; }
    }
}
