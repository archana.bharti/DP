﻿using DP.Common.Enums;
using DP.Common.Utilities;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;

namespace DP.Common.Models
{
    public partial class ModelFactory
    {
        public static List<Endorsement> ConvertToEndorsementDbm(EndorsementAddOrUpdateViewModel viewModel)
        {
            var model = new List<Endorsement>();

            if(viewModel.Specializations.Length > 0)
            {
                Array.ForEach(viewModel.Specializations, s =>
                {
                    model.Add(new Endorsement()
                    {
                        Id = (viewModel.Id.HasValue) ? viewModel.Id.Value : 0,
                        EndorseeId = viewModel.EndorseeId,
                        EndorseeType = viewModel.EndorseeType,
                        EndorserId = viewModel.EndorserId,
                        EndorserType = viewModel.EndorserType,
                        SpecializationId = s
                    });
                });
            }

            return model;
        }
    }
}
