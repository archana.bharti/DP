﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class EndorserViewModel
    {
        public string EndorserId { get; set; }
        public string EndorserType { get; set; }
        public string EndorserName { get; set; }
        public string EndorserImageUrl { get; set; }
    }
}