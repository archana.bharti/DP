﻿using DP.Common.Enums;
using DP.Services.Data.Models;

namespace DP.Common.Models
{
    public partial class ModelFactory
    {
        public static SpecializationViewModel ConvertToSpecializationVM(SpecializationClinic model)
        {
            return new SpecializationViewModel()
            {
                Id =model.Id,
                SpecializationId = model.SpecializationId,
                Name = model.Specialization.Category,
                Group = model.Specialization.Group,
                SubscriberId = model.ClinicId
            };
        }

        public static SpecializationViewModel ConvertToSpecializationVM(SpecializationHospital model)
        {
            return new SpecializationViewModel()
            {
                Id = model.Id,
                SpecializationId = model.SpecializationId,
                Name = model.Specialization.Category,
                Group = model.Specialization.Group,
                SubscriberId = model.HospitalId
            };
        }

        public static T ConvertSpecializationVmToDbm<T>(SpecializationViewModel viewModel, UserType type) where T : new()
        {
            dynamic model = new T();

            model.SpecializationId = viewModel.SpecializationId;

            if (type == UserType.Clinic || type == UserType.Doctor)
                model.ClinicId = viewModel.SubscriberId;
            else if (type == UserType.Hospital)
                model.HospitalId = viewModel.SubscriberId;

            return (T)model;

        }
    }
}
