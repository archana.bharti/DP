﻿namespace DP.Common.Models
{
    public class SpecializationViewModel
    {
        public int Id { get; set; }
        public int SpecializationId { get; set; }
        public int SubscriberId { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
    }
}
