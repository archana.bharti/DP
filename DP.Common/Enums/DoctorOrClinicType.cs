﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum DoctorOrClinicType
    {
        [Description("Clinic")]
        Clinic = 1,

        [Description("Doctor")]
        Doctor = 2
    }

    public enum DoctorOrClinicCategoryType
    {
        [Description("Medicine")]
        Medical = 1,

        [Description("Dental")]
        Dental = 2,

        [Description("Alternative Medicine")]
        AlternativeMedicine = 3
    }
}
