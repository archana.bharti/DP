﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum UserStatus
    {
        [Description("Active")]
        Active = 4,

        [Description("Deactive")]
        Deactive = 5,
    }
}
