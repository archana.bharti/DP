﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum UserType
    {
        ClinicAndDoctor = -1,
        [Description("CLINIC")]
        Clinic =1,
        [Description("DOCTOR")]
        Doctor = 2,
        [Description("HOSPITAL")]
        Hospital = 3,
        [Description("COLLEGE")]
        College = 4,
        [Description("USER")]
        User = 5,
        [Description("INSURANCE")]
        Insurance = 6
    }
}
