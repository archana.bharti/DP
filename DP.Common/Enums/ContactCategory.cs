﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum ContactCategory
    {
        [Description("To be featured")]
        ToBeFeatured = 1,

        [Description("To be listed")]
        ToBeListed = 2,

        [Description("Information not correct")]
        InfoNotCorrect = 3,

        [Description("To place an offer")]
        PlaceOffer = 4,

        [Description("General query")]
        GeneralQuery = 5,

        [Description("Offer Renewal")]
        OfferRenewal = 6,

        [Description("Careers")]
        Careers = 7

    }    
}
