﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum MailStatus
    {
        [Description("Success")]
        Success = 1,

        [Description("Failure")]
        Failure = 2,

        [Description("Pending")]
        Pending = 3
    }    
}
