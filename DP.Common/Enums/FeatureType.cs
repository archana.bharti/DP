﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum FeatureType
    {
        [Description("Textual")]
        Textual = 1,

        [Description("Zonal")]
        Zonal = 2,

        [Description("Basic")]
        Basic = 3
    }
}
