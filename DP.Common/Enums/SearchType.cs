﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum SearchType
    {
        [Description("DoctorOrClinic")]
        DoctorOrClinic =1,

        [Description("Hospital")]
        Hospital = 2,

        [Description("College")]
        College = 3,

        [Description("All")]
        All = 4
    }
}
