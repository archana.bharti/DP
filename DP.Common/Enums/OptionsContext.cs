﻿namespace DP.Common.Enums
{
    public enum OptionsContextType
    {
        Global = 0,
        Appointment = 1,
        Pda = 2,
        Offer = 3,
        Review =4
    }
}
