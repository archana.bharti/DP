﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum SignInStatus
    {
        [Description("Success")]
        Success,

        [Description("LockedOut")]
        LockedOut,

        [Description("Login Requires Two Factor Authentication")]
        RequiresTwoFactorAuthentication,

        [Description("Login Failure")]
        Failure,

        [Description("Inactive User")]
        Inactive,

        [Description("User Requires Verification")]
        RequireVerification
    }
}
