﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public enum CollegeType
    {
        [Description("Medical College")]
        MedicalCollege =1,

        [Description("Dental College")]
        DentalCollege =2
    }
}
