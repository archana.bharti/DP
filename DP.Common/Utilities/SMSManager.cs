﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GWE.SmsProvider;
using log4net;

namespace DP.Common.Utilities
{
    public class SMSManager
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SMSManager));
        private ISmsProvider smsProvider { get { return TextLocalSmsFactory.Get; } }
        private static SMSManager _smsMgr;

        public static SMSManager Instance
        {
            get
            {
                if (_smsMgr == null)
                {
                    _smsMgr = new SMSManager();
                }
                return _smsMgr;
            }
        }

        private SMSManager()
        {
        }

        public async Task SendActivationSuccessAsync(string receipent, string userName)
        {
            await Task.Run(() =>
            {
               var response = smsProvider.Send(TextLocalSmsTempalteNames.USER_ACTIVATION_SUCCESS_DIRECT, new string[] { userName }, receipent).FirstOrDefault();
                
               Log.Info(response);
            });
        }

        public async Task SendUnderVerificationSuccessAsync(string receipent, string user)
        {
            await Task.Run(() =>
            {
                var response = smsProvider.Send(TextLocalSmsTempalteNames.USER_ACTIVATION_SUCCESS_VERIFICATION, new string[] { user }, receipent);
                 Log.Info(response);
            });
        }

        public async Task SendOfferClaimToEntityUserAsync(string receipent, string user, string token)
        {
            await Task.Run(() =>
            {
                var response = smsProvider.Send(TextLocalSmsTempalteNames.OFFER_CLIAM_ENTITY_USER, new string[] { user, token }, receipent);
                Log.Info(response);
            });
        }

        public async Task SendOfferClaimToExternalUserAsync(string receipent, string user, string token, string provider)
        {
            await Task.Run(() =>
            {
                var response = smsProvider.Send(TextLocalSmsTempalteNames.OFFER_CLIAM_EXTERNAL_USER, new string[] { user, token, provider }, receipent);
                Log.Info(response);
            });
        }

        public async Task SendAppointmentToExternalUserAsync(string receipent, string appo, string des, string dial)
        {
            await Task.Run(() =>
            {
                var response = smsProvider.Send(TextLocalSmsTempalteNames.APPOINTMENT_EXTERNAL_USER, new string[] { appo, des, dial }, receipent);
                Log.Info(response);
            });
        }

        public async Task SendAppointmentToEntityUserAsync(string user, string phone, string date, string token, string receipent)
        {
            await Task.Run(() =>
            {
                var response = smsProvider.Send(TextLocalSmsTempalteNames.APPOINTMENT_ENTITY_USER, new string[] { user, phone, date, token }, receipent);
                Log.Info(response);
            });
        }

        public async Task SendPdaDoctorAsync(string receipent, string user)
        {
            await Task.Run(() =>
            {
                var response = smsProvider.Send(TextLocalSmsTempalteNames.PDA_QUESTION_DOCTOR, new string[] { user }, receipent);
                Log.Info(response);
            });
        }
    }
}
