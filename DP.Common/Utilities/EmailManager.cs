﻿using DP.Common.Enums;
using DP.Common.Models;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using DP.Common.Constants;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DP.Common.Utilities
{
    public class MailInfo
    {   
        public string ToAddress { get; set; }
        public string FromAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

    public class SendTokenObject
    {
        public MailMessage MailMessageObject { get; set; }
        public string Token { get; set; }
        public SmtpClient SmtpClientObject { get; set; }
    }
    
    public static class EmailManager
    {
        public static MailStatus Send(MailInfo info)
        {
            SmtpSection smtp = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            try
            {
                MailMessage mail = new MailMessage(smtp.From, info.ToAddress);
                //using (MailMessage mail = new MailMessage())
                //{
                    mail.IsBodyHtml = true;
                    mail.Priority = MailPriority.High;
                    mail.BodyEncoding = Encoding.UTF8;
                    mail.SubjectEncoding = Encoding.UTF8;

                  //  mail.From = new MailAddress(smtp.From);
                  //  mail.To.Add(info.ToAddress);
                    mail.Subject = info.Subject;
                    mail.Body = info.Body;

                SmtpClient client = new SmtpClient(smtp.Network.Host, smtp.Network.Port);
                //using (SmtpClient client = new SmtpClient(smtp.Network.Host, smtp.Network.Port))
                //{
                    client.DeliveryMethod = smtp.DeliveryMethod;
                    client.EnableSsl = smtp.Network.EnableSsl;
                    client.UseDefaultCredentials = smtp.Network.DefaultCredentials;
                    client.Credentials = new NetworkCredential(smtp.Network.UserName, smtp.Network.Password);
                    client.Port = smtp.Network.Port;
                    string userToken = info.ToAddress;
                    client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                SendTokenObject tokenObject = new SendTokenObject();
                tokenObject.MailMessageObject = mail;
                tokenObject.SmtpClientObject = client;
                tokenObject.Token = userToken;

                    client.SendAsync(mail, tokenObject);
                    //client.Dispose();
                    //mail.Dispose();
                // }
                // }
            }
            catch (SmtpException ex)
            {
                //TODO:log failed exception
                return MailStatus.Failure;
            }

            return MailStatus.Success;
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            SendTokenObject tokenObject = e.UserState as SendTokenObject;
            tokenObject.SmtpClientObject.Dispose();
            tokenObject.MailMessageObject.Dispose();
        }

        public static string GetTemplate(string messageTemplate)
        {
            /*https://support.microsoft.com/en-us/kb/319292*/
            var t = string.Empty;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Format("DP.Common.Utilities.EmailTemplates.{0}.html", messageTemplate)))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                     t = reader.ReadToEnd();
                }
            }

            return t;
        }

        public static void ReplaceTemplateParameters(ref string html, params string[] parameters)
        {
            for (var i = 0; i <= parameters.Length - 1; i++)
            {
                html = html.Replace("[p" + i + "]", parameters[i]);
            }
        }


        //public static MailInfo CreateActivationMail(string template, string activationLink, string dearName, string welcomeTo, string toAddress)
        //{
        //    MailInfo info = new MailInfo();
        //    info.Body = template
        //                .Replace(EmailTemplates.ActivationLink_PlaceHolder_UserName, dearName)
        //                .Replace(EmailTemplates.ActivationLink_PlaceHolder_UrlLink, activationLink)
        //                .Replace(EmailTemplates.ActivationLink_PlaceHolder_WelcomeTo, welcomeTo);
        //    info.Subject = "Doctor Pursuit User Activation";
        //    info.ToAddress = toAddress;
        //    return info;
        //}

        public static MailStatus SendActivationLink(string receipent, params string[] parameters)
        {
            string _html = GetTemplate(EmailTemplates.Template_ActivationLink);
            ReplaceTemplateParameters(ref _html, parameters);
            MailInfo info = new MailInfo();
            info.Body = _html;
            info.Subject = "DoctorPursuit User Activation Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendPDA(string receipent, string template, params string[] parameters)
        {
            string _html = GetTemplate(template);
            ReplaceTemplateParameters(ref _html, parameters);
            MailInfo info = new MailInfo();
            info.Body = _html;
            info.Subject = "DoctorPursuit Personal Doctor Advice Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendApprovalSuccess(string receipent, string appUrl)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_ActivationSuccess);
            EmailManager.ReplaceTemplateParameters(ref template, appUrl);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit User Activation Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendVerificationPending(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_EmailVerified_L2Verification);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit User Activation Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendAppointmentToEntity(string receipent, params string[] parameters)
        {
           // receipent = "tarakesh@gaween.com";
            var template = EmailManager.GetTemplate(EmailTemplates.Template_Appointment_ToEntity);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Appointment Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendAppointmentToUser(string receipent, params string[] parameters)
        {
           // receipent = "tarakesh@gaween.com";

            var template = EmailManager.GetTemplate(EmailTemplates.Template_Appointment_ToUser);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Appointment Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendOfferClaim(string receipent, params string[] parameters)
        {
            // receipent = "tarakesh@gaween.com";

            var template = EmailManager.GetTemplate(EmailTemplates.Template_OfferClaim);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Offer Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendOfferIntimation(string receipent, params string[] parameters)
        {
            // receipent = "tarakesh@gaween.com";

            var template = EmailManager.GetTemplate(EmailTemplates.Template_OfferIntimation);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Offer Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendOfferDetailsToAdmin(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_OfferDetailsToAdmin);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Offer Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendForgotPassword(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_ForgotPassword);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Forgot Password Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendContactEmailToUser(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_ContactUsToUser);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendContactEmailToAdmin(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_ContactUsToAdmin);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendExpiryOfFeaturedToAdmin(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_ContactUsToAdmin);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Expiry Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendExpiryOfOffersToAdmin(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_ExpiryOfOffersToAdmin);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Expiry Notification";
            info.ToAddress = receipent;
            return Send(info);
        }

        public static MailStatus SendExpiryOfPDAToAdmin(string receipent, params string[] parameters)
        {
            var template = EmailManager.GetTemplate(EmailTemplates.Template_ExpiryOfPDAToAdmin);
            ReplaceTemplateParameters(ref template, parameters);
            MailInfo info = new MailInfo();
            info.Body = template;
            info.Subject = "DoctorPursuit Expiry Notification";
            info.ToAddress = receipent;
            return Send(info);
        }
    }
}
