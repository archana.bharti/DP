﻿using GWE.SmsProvider;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Utilities
{
    public class ConfigHelper
    {
       public static T Get<T>(string key)
        {
            return (T)Convert.ChangeType(System.Configuration.ConfigurationManager.AppSettings[key], typeof(T));
        }
    }

    public class DistanceConfig
    {
        private RetrieverSection _Config;

        public DistanceConfig(string section)
        {
            _Config = ConfigurationManager.GetSection(section)  as RetrieverSection;
        }

        public ElementCollection GetFeatured()
        {
            return _Config.Featured;
        }

        public ElementCollection GetNonFeatured()
        {
            return _Config.NonFeatured;
        }
    }

    public class RetrieverSection : ConfigurationSection
    {
        [ConfigurationProperty("Featured")]
        public ElementCollection Featured
        {
            get { return (ElementCollection)this["Featured"]; }
        }

        [ConfigurationProperty("NonFeatured")]
        public ElementCollection NonFeatured
        {
            get { return (ElementCollection)this["NonFeatured"]; }
        }
    }

    [ConfigurationCollection(typeof(Element))]
    public class ElementCollection : ConfigurationElementCollection
    {
        public Element this[int index]
        {
            get { return (Element)BaseGet(index); }
        }

        public new Element this[string key]
        {
            get { return (Element)BaseGet(key); }
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Element)element).Key;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Element();
        }
    }

    public class Element : ConfigurationElement
    {
        public Element() { }

        [ConfigurationProperty("key", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Key
        {
            get { return (string)this["key"]; }
        }

        [ConfigurationProperty("value", DefaultValue = "", IsRequired = true)]
        public string Value
        {
            get { return (string)this["value"]; }
        }
    }
}
