﻿// ***********************************************************************
// Assembly         : DP.Common
// Author           : GWE
// Created          : 10-21-2015
//
// Last Modified By : GWE
// Last Modified On : 10-28-2015
// ***********************************************************************
// <copyright file="EnumHelper.cs" company="">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The Utilities namespace.
/// </summary>
namespace DP.Common.Utilities
{
    /// <summary>
    /// Class EnumHelper.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EnumHelper<T>
    {
        //public static string GetDescription(int t)
        //{
        //    return EnumHelper<T>.GetDescription((T)t);
        //}

        /// <summary>
        /// Retrieve the description on the enum
        /// </summary>
        /// <param name="en">The Enumeration</param>
        /// <returns>A string representing the friendly name</returns>
        public static string GetDescription(T en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }

        /// <summary>
        /// Gets the value from description.
        /// </summary>
        /// <param name="description">The description.</param>
        /// <returns>T.</returns>
        /// <exception cref="System.InvalidOperationException"></exception>
        /// <exception cref="System.ArgumentException">Not found.;description</exception>
        public static T GetValueFromDescription(string description)
        {
            var type = typeof(T);

            if (!type.IsEnum) throw new InvalidOperationException();

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;

                if (attribute != null)
                {
                    if (attribute.Description.ToUpper() == description.ToUpper())
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name.ToUpper() == description.ToUpper())
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentException("Not found.", "description");
        }
    }
}
