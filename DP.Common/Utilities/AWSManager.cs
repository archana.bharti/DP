﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.IO;
using System.Globalization;
using Amazon.Runtime;

namespace DP.Common.Utilities
{
    public class AwsS3Manager
    {
        string _awsAccesKey = ConfigHelper.Get<string>("AWSAccessKey");
        string _awsSecretKey = ConfigHelper.Get<string>("AWSSecretKey");
        string _bucket = ConfigHelper.Get<string>("AWSS3Bucket");

        public AwsS3Manager()
        {
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task UploadAsync(byte[] file, string fileName)
        {
            var results3url = string.Empty;
            using (var client = new AmazonS3Client())
            {
                using (var transferUtility = new TransferUtility(client))
                {
                    using (var fileStream = new MemoryStream(file))
                    {
                        await transferUtility.UploadAsync(fileName, _bucket, _awsSecretKey);
                    }
                }
            }
        }

        public async Task<Exception> Upload2Async(byte[] file, string fileName, bool deleteIfExists = false)
        {
            try {
                var config = new AmazonS3Config();
                var endPoint = config.RegionEndpoint.GetEndpointForService("s3");
                IAmazonS3 client = AWSClientFactory.CreateAmazonS3Client(_awsAccesKey, _awsSecretKey, config);
                if (deleteIfExists) {
                    await DeleteBeforeUpload(fileName);
                }
                var putRequest = new PutObjectRequest();
                putRequest.BucketName = _bucket;
                putRequest.CannedACL = S3CannedACL.PublicRead;
                putRequest.StorageClass = S3StorageClass.Standard;
                putRequest.Key = fileName;
                using (var fileStream = new MemoryStream(file))
                {
                    putRequest.InputStream = fileStream;
                    var response = await client.PutObjectAsync(putRequest);
                }
            }
            catch(Exception ex)
            {
                return ex;
            }

            return null;
        }

        public async Task DeleteBeforeUpload(string fileName)
        {
            try {
                IAmazonS3 client = AWSClientFactory.CreateAmazonS3Client(_awsAccesKey, _awsSecretKey);
                DeleteObjectRequest request = new DeleteObjectRequest();
                request.BucketName = _bucket;
                request.Key = fileName;
                await client.DeleteObjectAsync(request);
            }
            catch(AmazonServiceException ex)
            {

            }
            catch (AmazonClientException ex)
            {

            }
        }

        public string GetS3Url(string fileName)
        {
            var config = new AmazonS3Config();
            var endPoint = config.RegionEndpoint.GetEndpointForService("s3");
            return new Uri(string.Format(CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}", endPoint.HTTPS ? "https://" : "http://", endPoint, _bucket, fileName)).AbsoluteUri;
        }
    }
}
