﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;

namespace DP.Common.Utilities
{
    public class CacheManager
    {
        static MemoryCache memCache;
        static readonly object padlock = new object();
        static CacheManager(){
             memCache =  new MemoryCache("DP_CACHE");
        }

        public void Add(string key, object value, DateTimeOffset? offset =null)
        {
           lock (padlock)
           {
               memCache.Add(key, value, (offset == null ? DateTimeOffset.MaxValue :  offset.Value));
           }
        }

        public virtual void RemoveItem(string key)
        {
            lock (padlock)
            {
                memCache.Remove(key);
            }
        }

        public virtual object GetItem(string key)
        {
            lock (padlock)
            {
                var res = memCache[key];
                return res;
            }
        }

        public virtual void RemoveAll()
        {
            lock (padlock)
            {
                foreach (var obj in memCache)
                {
                    memCache.Remove(obj.Key);
                }
            }
        }
    }
}
