﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Utilities
{
    public static class SecurityHelper
    {
        public static string key = System.Configuration.ConfigurationManager.AppSettings["ENCRYPT_DECRYPT_KEY"].ToString();  
        public static string Encrypt(string value)
        {
            byte[] EnctArray = UTF8Encoding.UTF8.GetBytes(value);
            var objcrpt = new MD5CryptoServiceProvider();
            byte[] SrctArray = objcrpt.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            
            objcrpt.Clear();

            var objt = new TripleDESCryptoServiceProvider();
            objt.Key = SrctArray;
            objt.Mode = CipherMode.ECB;
            objt.Padding = PaddingMode.PKCS7;
            
            var crptotrns = objt.CreateEncryptor();
            byte[] resArray = crptotrns.TransformFinalBlock(EnctArray, 0, EnctArray.Length);

            objt.Clear();

            return Convert.ToBase64String(resArray, 0, resArray.Length);
        }
        public static string Decrypt(string DecryptText)
        {
            byte[] DrctArray = Convert.FromBase64String(DecryptText.Replace(' ','+'));
            
            var objmdcript = new MD5CryptoServiceProvider();
            byte[] SrctArray = objmdcript.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            objmdcript.Clear();

            var objt = new TripleDESCryptoServiceProvider();
            objt.Key = SrctArray;
            objt.Mode = CipherMode.ECB;
            objt.Padding = PaddingMode.PKCS7;

            var crptotrns = objt.CreateDecryptor();
            byte[] resArray = crptotrns.TransformFinalBlock(DrctArray, 0, DrctArray.Length);
            objt.Clear();

            return UTF8Encoding.UTF8.GetString(resArray);
        }
    }
}
