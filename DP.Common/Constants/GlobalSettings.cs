﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DP.Common.Constants
{
    public abstract class GlobalSettings
    {
        public const string S3_ROOT_PATH = "S3RootPath";
        public const string GRID_PAGE_SIZE = "GridPageSize";
        public const string GLOBAL_SETTINGS = "GLOBAL_SETTINGS";
        public const string DATE_FORMAT_1 = "yyyy-MM-dd HH:mm:ss";
        public static string APP_URL
        {
            get {
                //var url = (HttpContext.Current.Request.UrlReferrer == null) ? HttpContext.Current.Request.Url.Authority : HttpContext.Current.Request.UrlReferrer.Authority;
                //just staticly changed it to check url on local server By (Reena Maan)
                var url =  HttpContext.Current.Request.Url.Authority;
                var http = System.Web.HttpContext.Current.Request.IsLocal == true ? "http://" : System.Web.HttpContext.Current.Request.IsSecureConnection == true ? "https://" : "http://";
                return http + url;
            }
        }
        public static string CLIENT_URL
        {
            get
            {
              return System.Configuration.ConfigurationManager.AppSettings["CLIENT_URL"].ToString();
            }
        }

        public static string ADMIN_EMAIL
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["ADMIN_EMAIL"].ToString();
            }
        }

        public const string EMAILCAT_FEATURED = "EMAILCAT_FEATURED";
        public const string EMAILCAT_LISTED = "EMAILCAT_LISTED";
        public const string EMAILCAT_INFORMATION_NOT_CORRECT = "EMAILCAT_INFORMATION_NOT_CORRECT";
        public const string EMAILCAT_PLACE_OFFER = "EMAILCAT_PLACE_OFFER";
        public const string EMAILCAT_QUERIES = "EMAILCAT_QUERIES";
        public const string EMAILCAT_RENEWAL_OFFER = "EMAILCAT_RENEWAL_OFFER";
        public const string EMAILCAT_CAREERS = "EMAILCAT_CAREERS";

        public static string GetEmailByCategory(string emailCat)
        {
            return System.Configuration.ConfigurationManager.AppSettings[emailCat].ToString();
        }
    }
}
