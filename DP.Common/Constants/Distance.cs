﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Constants
{
    public static class Distance
    {
        public const string Default = "default";
        public const string Slide = "slide";
        public const string Max = "max";
    }

    public static class ConfigSection
    {
        public const string DoctorsOrClinic = "DoctorsOrClinics";
        public const string Hospital = "Hospital";
        public const string College = "College";
    }
}
