﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Constants
{
    public abstract class EntityType
    {
        public const string Clinic = "CLINIC";
        public const string Doctor = "DOCTOR";
        public const string Hospital = "HOSPITAL";
        public const string College = "COLLEGE";

        public const int Clinic_Value = 1;
        public const int Doctor_Value = 2;
        public const int Hospital_Value = 3;
        public const int College_Value = 4;

        public static string GetText(int value)
        {
            var text = string.Empty;
            switch(value)
            {
                case 1: text = EntityType.Clinic; break;
                case 2: text = EntityType.Doctor; break;
                case 3: text = EntityType.Hospital; break;
                case 4: text = EntityType.College; break;
            }
            return text;
        }

        public static int GetValue(string text)
        {
            var value = 0;
            switch (text.ToUpper())
            {
                case EntityType.Clinic: value = EntityType.Clinic_Value; break;
                case EntityType.Doctor: value = EntityType.Doctor_Value; break;
                case EntityType.Hospital: value = EntityType.Hospital_Value; break;
                case EntityType.College: value = EntityType.College_Value; break;
            }
            return value;
        }
    }
}
