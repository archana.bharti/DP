﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Enums
{
    public abstract class EmailTemplates
    {
        public const string ActivationLink_PlaceHolder_UserName = "[USER_NAME]";
        public const string ActivationLink_PlaceHolder_UrlLink = "[URL_LINK]";
        public const string ActivationLink_PlaceHolder_WelcomeTo = "[WELCOME_TO]";
        public const string Template_PdaDoctor = "PdaDoctor";
        public const string Template_PdaUser = "PdaUser";
        public const string Template_PdaAnswer = "PdaAnswer";
        public const string Template_ActivationLink = "ActivationLink";
        public const string Template_ActivationSuccess = "ActivationSuccess";
        public const string Template_EmailVerified = "ActivationSuccess";
        public const string Template_EmailVerified_L2Verification = "EmailVerifiedAndL2Verification";
        public const string Template_Appointment_ToEntity = "AppointmentToEntity";
        public const string Template_Appointment_ToUser = "AppointmentToUser";
        public const string Template_ForgotPassword = "ForgotPassword";
        public const string Template_OfferClaim = "OfferClaim";
        public const string Template_OfferIntimation = "OfferIntimation";
        public const string Template_OfferDetailsToAdmin = "OfferDetailsToAdmin";
        public const string Template_ContactUsToUser = "ContactUsToUser";
        public const string Template_ContactUsToAdmin = "ContactUsToAdmin";
        public const string Template_ExpiryOfFeaturedToAdmin = "ExpiryOfFeaturedToAdmin";
        public const string Template_ExpiryOfOffersToAdmin = "ExpiryOfOffersToAdmin";
        public const string Template_ExpiryOfPDAToAdmin = "ExpiryOfPDAToAdmin";
    }
}
