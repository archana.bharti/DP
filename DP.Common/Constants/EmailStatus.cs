﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Constants
{
    public abstract class EmailStatus
    {
        public const string Sent = "SENT";
        public const string Delivered = "DELIVERED";
        public const string Failed = "FAILED";
    }
}
