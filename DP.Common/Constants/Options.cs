﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Constants
{
    public abstract class OptionKeys
    { 
        public const string REVIEW_THRESHOLD = "USER_REVIEW_THRESHOLD";
        public const string APPOINTMENT_START_TIME = "START_TIME";
        public const string APPOINTMENT_END_TIME = "END_TIME";
        public const string APPOINTMENT_WEEK_OFF = "WEEK_OFF";
        public const string APPOINTMENT_VALIDITY_PERIOD = "VALIDITY_PERIOD";
    }
}
