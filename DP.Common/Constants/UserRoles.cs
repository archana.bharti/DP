﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Constants
{
    public static class Roles
    {
        public const string Administrator = "ADMINISTRATOR";
        public const string Doctor = "DOCTOR"; 
        public const string Clinic = "CLINIC"; 
        public const string Hospital = "HOSPITAL"; 
        public const string College = "COLLEGE"; 
        public const string User = "USER"; 
    }
}
