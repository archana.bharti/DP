﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class FeaturedReportViewModel
    {
        public string Month { get; set; }
        public int Count { get; set; }
        public int Year { get; set; }
        public List<EntityViewModel> Subscribers { get; set; }

        public FeaturedReportViewModel()
        {
            Subscribers = new List<EntityViewModel>();
        }
    }
}
