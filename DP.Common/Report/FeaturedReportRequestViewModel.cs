﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class FeaturedReportRequestViewModel
    {
        public int Year { get; set; }
        public int[] Months { get; set; }
    }
}
