﻿using DP.Common.Enums;
using DP.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Common.Models
{
    public class EntityViewModel
    {
        public int EntityId { get; set; }
        public int EntityType { get; set; }
        public string EntityTypeText { get; set; }
        public string EntityName { get; set; }
        public string ImageUrl { get; set; }
        public DateTime RegisteredOn { get; set; }
    }
}
