﻿namespace Topshelf.WindowsService
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(hostConfigurator =>
            {
                hostConfigurator.Service<TimerService>(serviceConfigurator =>
                {
                    serviceConfigurator.ConstructUsing(() => new TimerService());
                    serviceConfigurator.WhenStarted(myService => myService.Start());
                    serviceConfigurator.WhenStopped(myService => myService.Stop());
                });

                hostConfigurator.RunAsLocalSystem();

                hostConfigurator.SetDisplayName("TimerService");
                hostConfigurator.SetDescription("TimerService using Topshelf");
                hostConfigurator.SetServiceName("TimerService");
            });
        }
    }
}
