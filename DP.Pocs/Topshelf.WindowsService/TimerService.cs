﻿using System;
using System.IO;
using System.Timers;

public class TimerService
{
    private Timer _timer = null;

    public TimerService()
    {
        double interval = 60000;
        _timer = new Timer(interval);
        _timer.Elapsed += new ElapsedEventHandler(OnTick);
    }

    protected virtual void OnTick(object sender, ElapsedEventArgs e)
    {
        write("Tick:" + DateTime.Now.ToLongTimeString());
    }

    public void Start()
    {
        write("Info: TimerService is Started");

        _timer.AutoReset = true;
        _timer.Enabled = true;
        _timer.Start();
    }

    public void Stop()
    {
        write("Info: TimerService is Stopped");

        _timer.AutoReset = false;
        _timer.Enabled = false;
    }

    private void write(string text)
    {
        //var path = @"C:\Logger\ServiceLog.txt";

        //using (StreamWriter sw = (File.Exists(path)) ? File.AppendText(path) : File.CreateText(path))
        //{
        //    sw.WriteLine(text);
        //}
    }
}