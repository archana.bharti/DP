﻿using DP.Common.Enums;
using DP.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DP.Administration.Models
{
    public class SubscriberRating
    {
        public List<CollegeRatingViewModel> CollegeRatings { get; set; }
        public List<ClinicRatingViewModel> ClinicRatings { get; set; }
        public List<HospitalRatingViewModel> HospitalRatings { get; set; }
        public List<InsuranceRatingViewModel> InsuranceRatings { get; set; }
        public int SubscriberId { get; set; }
        public UserType UserType { get; set; }
    }
}