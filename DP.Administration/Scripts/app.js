﻿window.app = {};
(function ($) {

    String.prototype.capitalizeFirstLetter = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    $.ajaxSetup({

        statusCode: {
            401: function () {
                self.login();
            }
            ,
            400: function (a, b, c) {
                app.showLoader(a.statusText);
            },

            403: function (a, b, c) {
                app.showLoader(a.statusText);
            },

            500: function (a, b, c) {
                app.showLoader(a.statusText);
            }
        }
    });


    $.date = {
        isDateGreater: function (fromDate, toDate) {
            return Date.parse(fromDate) > Date.parse(toDate);
        },
        format: function () {

        }
    }

    app = {

        showLoader : function (message) {
            var l = $("#loader");
            l.find('span.message').text(message)
            l.fadeIn();
            setTimeout(function () { l.find('span.message').text(''); l.hide(); }, 3500)
        },

        serializeForm: function (form) {

            var serializedArray = form.serializeArray();

            var object = {};
            $.each(serializedArray, function (idx, item) {
                object[item.name] = item.value;
            })

            return object;
        },

        ajaxFormPost: function (form, url, progressbar, callback) {
            var self = this;
            var formData = new FormData(form[0]);
            $.ajax({
                url: url,
                async: true,
                type: 'POST',
                xhr: function () {
                    var jqXhr = $.ajaxSettings.xhr();
                    progressbar.attr('aria-valuenow', 0);
                    if (jqXhr.upload) { // Check if upload property exists
                        jqXhr.upload.addEventListener('progress', function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                progressbar.attr('aria-valuenow', percentComplete);
                            }
                        }, false);
                    }
                    return jqXhr;
                },
                success: function (a) {
                    progressbar.hide();
                    callback(true)
                },
                error: function (a, b, c) {
                    progressbar.hide();
                    callback(false, a)
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                crossDomain: true
            })
        },

        imageUpload: function (data, image, url, progressbar, callback) {

            var form = $("<form/>");
            for (var key in data) {
                $("<input/>").attr({ name: key, value: data[key] }).appendTo(form);
            }
            form.append(image);
            progressbar.show();
            this.ajaxFormPost(form, url, progressbar, callback);
        },

        showImage: function (input, image) {

            input = input[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    image.attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

                image.show();
            }
        },

        freezeForm: function (form) {
            form.find("a,input,select").attr("disabled", "disabled");
        },

        unfreezeForm: function (form) {
            form.find("a,input,select").removeAttr("disabled");
        }
    }

    app["user"] = {};
    app["offer"] = {};

})(jQuery)