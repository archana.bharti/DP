﻿(function ($) {

    var poll = {};

    $(document).ready(function () {
        var formCreate = $('#form-poll-create');
        var name_notext = "[name='noText']",
            name_yestext = "[name='yesText']",
            name_question = "[name='question']",
            name_active = "[name='active']";

        $('.btn-toggle-poll').on("click", function () {
            formCreate.toggle();
        });

        $('.btn-create-poll-master').on("click", function () {
            var data = {
                noText: formCreate.find(name_notext).val(),
                yesText: formCreate.find(name_yestext).val(),
                question: formCreate.find(name_question).val(),
                active: formCreate.find(name_active).prop("checked")
            }

            if ($.trim(data.question).length > 0) {
                $.post(window.ApiRoot + 'poll', data, function (res) {
                    app.showLoader("Poll data added successfully");
                    setTimeout(function () { window.location.href = window.location.pathname + window.location.search; }, 500)
                })
            }
        });

        $(".btn-save-poll-master").on("click", function () {
            var ele = $(this),
                masterId = ele.attr("data-pollmaster-id"),
                form = $("#poll-master-form-" + masterId);

            var data = {
                id: masterId,
                noText: form.find(name_notext).val(),
                yesText: form.find(name_yestext).val(),
                question: form.find(name_question).val(),
                active: form.find(name_active).prop("checked")
            }

            if ($.trim(data.question).length > 0) {
                $.post(window.ApiRoot + 'poll/update', data, function (res) {
                    app.showLoader("Poll data updated successfully");
                })
            }
        });
    });

})(jQuery);