﻿var insurance = {};
(function ($) {

    insurance.update = function (data) {
        var apiUrl = window.ApiRoot + 'insurance/update'
       return $.post(apiUrl, data);
    }

    $(document).ready(function () {
        $(".btn-insurance-update").on("click", function (e) {
            var ele = $(this),
                row = ele.closest("tr"),
                id = row.data("id"),
                checkboxes = row.find("input[type='checkbox']"),
                data = {};
            data.id = id;
            $.each(checkboxes, function (idx, checkbox) {
                checkbox = $(checkbox);
                var field = checkbox.data("field"),
                    isChecked = checkbox.prop("checked");
                data[field] = isChecked;
            })

            ele.text("Saving...").addClass("disabled");
            insurance.update(data).success(function (res) {
                ele.text("Update").removeClass("disabled");
            }).error(function () {
                ele.text("Update").removeClass("disabled");
            })

            console.log(data);
        })
    });
}
)(jQuery);