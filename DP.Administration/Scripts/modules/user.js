﻿(function ($) {
    $(document).ready(function () {

        $(".subscriber-tabs .search input").on("keyup", function (e) {
            var val = $(this).val().trim();
            var panels = $("#doctors-tab-pane, #hospitals-tab-pane, #colleges-tab-pane, #users-tab-pane").find(".subscriber-panel-item");
            panels.show();
            if (val.length > 0) {
                var visible = panels.find('.panel-subscriber:contains("' + val + '")').closest(".subscriber-panel-item");
                var nonvisible = panels.not(visible);
                nonvisible.hide();
            }
        });

        var _exUsersGrid = {

            formatter : {
                status : function(value, row, index){
                    if (row.isTerminated == true) { return "<label class='label label-danger'>DEACTIVE</label>"; }
                    else return "<label class='label label-success'>ACTIVE</label>";
                }
            },

            actions : function (value, row, index) {
                var arr =
                 [
                '<div class="btn-group">',
                    '<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">',
                    '<i class="fa fa-bars" />',
                    '</button>',
                    '<ul class="dropdown-menu">',
                 ],
                 arr2 = [],
                 arr3 = ['</ul>',
                 '</div>'];
                if(row.isTerminated)
                    arr2.push('<li><a href="javascript:void(0)" data-row-action="activate">Activate</a></li>')
                else
                    arr2.push('<li><a href="javascript:void(0)" data-row-action="terminate">Terminate</a></li>')

                return arr.concat(arr2).concat(arr3).join('');
            }
        }

        $('#tblExternalUsers').bootstrapTable({
            url: window.ApiRoot + 'user/externalUsers',
            pagination: true,
            pageNumber : 1,
            pageSize: 20,
            sidePagination : 'server',
            pageList: [10, 25, 50, 100, 250],
            columns: [
                {
                    field: 'actions',
                    title: 'Actions',
                    valign: 'middle',
                    align: 'left',
                    width: "50",
                    formatter: _exUsersGrid.actions,
                    //TODO: handle the row action from there instead of from view. such that we will have more information about the row
                    events: {
                        'click a[data-row-action="terminate"]': function (e, value, row, index) {
                            var btn = $(e.currentTarget);
                            $.post(window.ApiRoot + 'user/terminate', { userId: row.id }, function () {
                                app.showLoader("User terminated.");
                                $('#tblExternalUsers').bootstrapTable('refresh')
                            });

                            e.stopPropagation();
                        },
                        'click a[data-row-action="activate"]': function (e, value, row, index) {
                            var btn = $(e.currentTarget);
                            $.post(window.ApiRoot + 'user/activate', { userId: row.id }, function () {
                                app.showLoader("User activated.");
                                $('#tblExternalUsers').bootstrapTable('refresh')
                            });

                            e.stopPropagation();
                        }
                    }
                }, {
                    field: 'id',
                    title: '#', visible: false
                }, {
                    field: 'email',
                    title: 'Email',
                }, {
                    field: 'displayName',
                    title: 'Display Name',
                }, {
                    field: 'isTerminated',
                    title: 'Status',
                    formatter: _exUsersGrid.formatter.status,
                }, {
                    field: 'phoneNumber',
                    title: 'Contact',
                }, {
                    field: 'createdOn2',
                    title: 'Created On',
                }]
        });

        var datepicOpt = app.user.hasPlan == false ? {  format: "YYYY-MM-DD",  minDate: new Date()} : { format: "YYYY-MM-DD"}
        $(".datepicker").datetimepicker(datepicOpt);

        $('.btn-user-activate').on('click', function () {
            var self = $(this),
                 uid = self.data("uid"),
                 parent = self.closest('.user-basic-info'),
                 img = parent.find('.image');

            $.post(window.ApiRoot + 'user/activate', { userId: uid }, function () {
                app.showLoader("User activate.");
                img.removeClass('deactive active').addClass('active');
                setTimeout(function () { window.location.href = window.location.pathname + window.location.search; }, 1000)
            })
        });

        $("#btnSavePlan").on("click", function () {
            var self = $(this),
                form = $("#form-subscriber-plan"),
                data = form.serializeObject(),
                img = $(".image");

            if ($.date.isDateGreater(data.startDate, data.endDate)) { return app.showLoader("Start date should not be greater than End date."); }

            data.enableTextual = data.enableTextual === "on" ? true : false;
            data.enableBasic = data.enableBasic === "on" ? true : false;
            data.enableFeatured = data.enableFeatured === "on" ? true : false;
            self.addClass("disabled").text("Saving...");
            $.post("/user/subscribe", data, function () {
                app.showLoader("Plan saved.");
                setTimeout(function () { window.location.href = window.location.pathname + window.location.search; }, 1000)
                if (!$("#lblVerified").is(":visible")) {
                    $("#lblVerified").fadeIn();
                }
            }).done(function () {
                self.removeClass("disabled").text("Save");
                if (!img.hasClass('active')) {
                    img.addClass("active");
                }
            })
        });

        $(".btn-user-terminate").on("click", function () {
            var self = $(this),
                uid = self.data("uid"),
                parent = self.closest('.user-basic-info'),
                img = parent.find('.image');

            $.post(window.ApiRoot + 'user/terminate', { userId: uid }, function () {
                app.showLoader("User terminated.");
                img.removeClass('deactive active').addClass('deactive');
                setTimeout(function () { window.location.href = window.location.pathname + window.location.search;; }, 1000)
            })
        });

    });
})(jQuery)