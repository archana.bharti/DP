﻿(function ($) {

    var pda = {};

    pda.renderSubscriptions = function renderSubscriptions(doctorOrClinicId) {

        var panel = $('#panel-pda-subscriptions');
        $('#hdndoctorOrClinicId').val(doctorOrClinicId);

        panel.html('');

        $.get("/pda/PdaSubscriptions?doctorOrClinicId=" + doctorOrClinicId, function (res) {
            panel.html(res);
            //initActions
        })
    },

    pda.renderSpecializations = function renderSpecializations(doctorOrClinicId) {
        var splList = $('#specializations-list');
        splList.html('');
        $.get("/pda/specializations?doctorOrClinicId=" + doctorOrClinicId, function (res) {
            splList.html(res);
            //initActions
        })
    }

    $(document).ready(function () {
        var form = $('#form-pda-subscription');

        pda.renderSubscriptions($("#ddl-providers").val());
        pda.renderSpecializations($("#ddl-providers").val())

        $(".datepicker").datetimepicker({
            format: "YYYY-MM-DD",
            minDate: new Date()
        });

        $("#ddl-providers").on("change", function () {
            var _val = $(this).val();
            pda.renderSubscriptions(_val);
            pda.renderSpecializations(_val);
        })

        $('.btn-create-subscription').on("click", function () {
            $('#form-pda-subscription').toggle();
        })

        $('.btn-cancel').on("click", function () {
            $('#form-pda-subscription').hide();
        })

        $('.btn-save').on("click", function () {
            var form = $('#form-pda-subscription');
            var data = form.serializeObject();
            console.log(data);
            $.post("/pda/create", data, function (res) {
                app.showLoader(res.success == true ? "Pda subscription saved." : res.message);
                pda.renderSubscriptions(data.doctorOrClinicId)
            })
            .fail(function (a, b, c) {

            })
        });

        $(document).on("click", '.btn-pda-deactivate', function () {
            var id = $(this).attr('data-pda-id');
            var data = { id: id };
            $.post(window.ApiRoot + 'pda/deactivate', data, function (res) {
                app.showLoader("Pda subscription deactivated");
                pda.renderSubscriptions($('#hdndoctorOrClinicId').val());
            })
        });

    })
})(jQuery);