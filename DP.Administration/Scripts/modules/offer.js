﻿(function ($) {

    $(document).ready(function () {
        var form = $(".form-create-offer");
        $(".datepicker").datetimepicker({
            format: "YYYY-MM-DD",
            minDate: new Date()
        });
        $("#ddl-providers").select2();

        form.bootstrapValidator({
            // live: 'disabled',
            fields: {
                providerEmail: {
                    validators: {
                        notEmpty: {
                            message: 'Provider email should not be empty'
                        },
                        emailAddress: {
                            message: "Please enter valid provider email"
                        }
                    }
                },
                content: {
                    validators: {
                        notEmpty: {
                            message: 'Offer content should not be empty'
                        }
                        //},
                        //stringLength: {
                        //    max : 512
                        //}
                    }
                },
                startDateTime: {
                    validators: {
                        notEmpty: {
                            message: 'Start date should not be empty'
                        }
                    }
                },
                expiryDateTime: {
                    validators: {
                        notEmpty: {
                            message: 'Expiry date should not be empty'
                        }
                    }
                },
                expiryDateTime: {
                    validators: {
                        notEmpty: {
                            message: 'Expiry date should not be empty'
                        }
                    }
                },
                expiryDateTime: {
                    validators: {
                        notEmpty: {
                            message: 'Expiry date should not be empty'
                        }
                    }
                },
                currentPrice: {
                    validators: {
                        notEmpty: {
                            message: 'Current Price should not be empty.'
                        },
                        integer: {
                            message: 'Current should be a number'
                        }
                    }
                },
                discount: {
                    validators: {
                        notEmpty: {
                            message: 'Discount should not be empty.'
                        },
                        integer: {
                            message: 'Discount should be a number'
                        }
                    }
                },
                quantity: {
                    validators: {
                        notEmpty: {
                            message: 'Quantity should not be empty.'
                        },
                        integer: {
                            message: 'Quantity should be a number'
                        }
                    }
                }
            }
        });

        $("#txtDiscount, #txtCurrentPrice").on("keyup", function () {
            //if (form.data("bootstrapValidator").isValid()) {
            var _lbl = $("#lblNetPrice"),
                _cp = parseInt($("#txtCurrentPrice").val()),
                _dis = parseInt($("#txtDiscount").val()),
                _np = parseFloat(_cp - (_cp * (_dis / 100)));
            _lbl.val(isNaN(_np) ? "0.0" : _np);
            // }
        });

        $(".btn-create-offer").on("click", function (e) {
            form.data("bootstrapValidator").validate();
            if (form.data("bootstrapValidator").isValid()) {
                var formData = form.serializeObject();
                formData.providerType = $("#ddl-providers").find("option:selected").data().userType
                $.post('/offer/create', formData, function (res) {
                    app.imageUpload(
                               { id: res.id },
                               $("input[type='file']"), window.ApiRoot + 'offer/UploadLogo',
                               $('.progress'),
                               function (res) {
                                   app.showLoader("Offer saved successfully.");
                                   setTimeout(function () { window.location.href = "/offer/index"; }, 1000)
                               });

                });
            }
        });

        $("input[type='file']").on("change", function (e) {
            var self = $(this),
                  img = self.next();
            app.showImage(self, img);
        })
    })

})(jQuery);