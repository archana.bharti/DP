﻿using DP.Administration.Models;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Business;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DP.Administration.Controllers
{
    public class UserController : BaseController
    {
        private SubscriberManager subscriberManager;
        private ClinicRatingManager clinicRatingMgr;
        private HospitalRatingManager hospitalRatingMgr;
        private UserManager userManager;
        private CollegeRatingManager collegeRatingMgr;
        private OfferManager offerManager;
        private FeatureManager featureManager;
        private InsuranceManager insuranceManager;
        private InsuranceRatingManager insuranceRatingManager;

        public UserController(
            SubscriberManager subscriberManager,
            UserManager userManager, 
            ClinicRatingManager clinicRatingMgr, 
            HospitalRatingManager hospitalRatingMgr, 
            CollegeRatingManager collegeRatingMgr,
            OfferManager offerManager,
            FeatureManager featureManager,
            InsuranceManager insuranceManager,
            InsuranceRatingManager insuranceRatingManager)
        {
            this.subscriberManager = subscriberManager;
            this.clinicRatingMgr = clinicRatingMgr;
            this.hospitalRatingMgr = hospitalRatingMgr;
            this.collegeRatingMgr = collegeRatingMgr;
            this.userManager = userManager;
            this.offerManager = offerManager;
            this.featureManager = featureManager;
            this.insuranceManager = insuranceManager;
            this.insuranceRatingManager = insuranceRatingManager;
        }

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

      
        // GET: User/Details/5
        public ActionResult Details(string id, UserType type)
        {
            return View("Details", this.subscriberManager.Details(id, type));
        }

        public ActionResult Doctors()
        {
            return PartialView("_Doctors", this.subscriberManager.GetClinicOrDoctorSubscribers());
        }

        public ActionResult Hospitals()
        {
            return PartialView("_Hospitals", this.subscriberManager.GetHospitalSubscribers());
        }

        public ActionResult Colleges()
        {
            return PartialView("_Colleges", this.subscriberManager.GetCollegeSubscribers());
        }

        public ActionResult Insurances()
        {
            return PartialView("_Insurances", this.subscriberManager.GetInsuranceSubscribers());
        }

        [HttpPost]
        public ActionResult Subscribe(SubscriberPlanViewModel viewModel)
        {
            try
            {
                return Json(subscriberManager.SubscribeToPlan(viewModel));
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult> GetUserReviews(int sid, UserType type)
        {
            try
            {
                SubscriberRating results = new SubscriberRating() { SubscriberId = sid, UserType = type };
                switch (type)
                {
                    case UserType.Clinic:
                    case UserType.Doctor:
                        results.ClinicRatings = await clinicRatingMgr.Get(sid);
                        break;
                    case UserType.Hospital:
                        results.HospitalRatings = await hospitalRatingMgr.Get(sid);
                        break;
                    case UserType.College:
                        results.CollegeRatings = await collegeRatingMgr.Get(sid);
                        break;
                    case UserType.Insurance:
                        results.InsuranceRatings = await insuranceRatingManager.Get(sid);
                        break;
                }

                return PartialView("_UserRatings", results);
            }
            catch (Exception ex)
            {
                return View("../Shared/_ViewError", new { Message = ex.Message });
            }
        }

        [HttpGet]
        public ActionResult Offers(int sid, int type)
        {
            List<OfferMaster> offers = this.offerManager.Get(sid, type);
            return PartialView("_Offers", offers);
        }


    }
}
