﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DP.Administration.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        protected JsonResult JsonResult(object data)
        {
            return new JsonResult(data);
        }
    }
}