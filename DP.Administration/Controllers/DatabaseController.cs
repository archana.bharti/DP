﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using DP.Administration.Models;
using DP.Services.Business;
using DP.Common;
using DP.Services.Business.Administration;

namespace DP.Administration.Controllers
{
    public class DatabaseController : BaseController
    {
        private DatabaseManager databaseManager;
        private CommonManager commonManager;
        public string APP_URL
        {
            get
            {
                var url = (HttpContext.Request.UrlReferrer == null) ? HttpContext.Request.Url.Authority : HttpContext.Request.UrlReferrer.Authority;
                return HttpContext.Request.Url.Scheme + "://" + url;

            }
        }
        public DatabaseController(DatabaseManager databaseManager, CommonManager commonManager)
        {
            this.databaseManager = databaseManager;
            this.commonManager = commonManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        [System.Web.Mvc.Authorize]
        public ActionResult MySqlAdmin()
        {
            //Server.TransferRequest(APP_URL + "/phpMyAdmin", true);
            //Server.Transfer("~\\phpMyAdmin", false);
            //return new EmptyResult();
            return Redirect(APP_URL + "/phpMyAdmin" );
        }

        public ActionResult Tables()
        {
            return PartialView("_Tables", this.databaseManager.GetTableNames());
        }
    }

}
