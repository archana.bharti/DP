﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DP.Administration.Models;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Business;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;
using System.Threading.Tasks;
using DP.Services.Data.UnitOfWork;

namespace DP.Administration.Controllers
{
    public class PollController : BaseController
    {
        private IPollDao pollDao;
        public PollController(IPollDao pollDao)
        {
            this.pollDao = pollDao;
        }

        public async Task<ActionResult> Index()
        {
            var list = await this.pollDao.GetPollMasterAllAsync();
            return View(list);
        }
    }
}