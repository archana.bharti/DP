﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DP.Administration.Models;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Business;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;
using System.Threading.Tasks;

namespace DP.Administration.Controllers
{
    public class PdaController : BaseController
    {
        private PDAManager pdaManager;
        private PdaSubscriptionManager pdaSubscriptionManager;

        public PdaController(PDAManager pdaManager, PdaSubscriptionManager pdaSubscriptionManager)
        {
            this.pdaManager = pdaManager;
            this.pdaSubscriptionManager = pdaSubscriptionManager;
        }
        // GET: Offer
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult FeaturedDoctorsAndClinics()
        {
            return PartialView("_FeaturedDoctorsAndClinics", this.pdaManager.Subscribers());
        }

        [HttpGet]
        public async Task<ActionResult> PdaSubscriptions(int doctorOrClinicId)
        {
            if(doctorOrClinicId == 0000)
                return PartialView("_PdaSubscriptions", await this.pdaSubscriptionManager.Get());
            return  PartialView("_PdaSubscriptions", await this.pdaSubscriptionManager.Get(doctorOrClinicId));
        }

        [HttpGet]
        public ActionResult Specializations(int doctorOrClinicId)
        {
            return PartialView("_Specializations", this.pdaSubscriptionManager.GetSpecializations(doctorOrClinicId));
        }

        [HttpPost]
        public ActionResult Create(PdaSubscription model)
        {
            try
            {
                if(this.pdaSubscriptionManager.HasSubscriptionWithSpecializationActive(model.DoctorOrClinicId, model.SpecializationId))
                {
                    throw new Exception("Doctor with this specialiazation is already active. To modify, please deactivate the existing and create as new.");
                }
                var result = this.pdaSubscriptionManager.Add(model).Result;
                return base.JsonResult(new { Success =  true });
            }
            
            catch(Exception ex)
            {
                return JsonResult(new { Success = false, Message = ex.Message });
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Deactivate(int pdaSubscriptionId)
        {
            try
            {
                await this.pdaSubscriptionManager.Deactivate(pdaSubscriptionId);
                return JsonResult(new { Success = true });
            }
            catch (Exception ex)
            {
                return JsonResult(new { Success = false, Message = ex.Message });
            }
        }
    }
}
