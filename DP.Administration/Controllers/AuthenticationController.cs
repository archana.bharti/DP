﻿using Google.Authenticator;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DP.Administration.Controllers
{
    public class AuthenticationController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            var isGoogleAuthenticatorEnabled = System.Configuration.ConfigurationManager.AppSettings["GOOGLE-AUTHENTICATOR-ENABLED"] == "1";
            ViewData["IsGoogleAuthenticatorEnabled"] = isGoogleAuthenticatorEnabled;
            return View("Login");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(AdminUserLogin user)
        {
            var isGoogleAuthenticatorEnabled = System.Configuration.ConfigurationManager.AppSettings["GOOGLE-AUTHENTICATOR-ENABLED"] == "1";
            var googleAccount = System.Configuration.ConfigurationManager.AppSettings["GOOGLE-AUTHENTICATOR-ACCOUNT"];
            var password = System.Configuration.ConfigurationManager.AppSettings["PORTAL-PASSWORD"];
            RedirectToRouteResult routeResult = null;
            if (googleAccount == user.UserName)
            {
                bool isCorrectPIN = false;
                if (isGoogleAuthenticatorEnabled)
                {
                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                    isCorrectPIN = tfa.ValidateTwoFactorPIN(System.Configuration.ConfigurationManager.AppSettings["GOOGLE-AUTHENTICATOR-KEY"], user.Passcode, TimeSpan.FromHours(1));
                    if (isCorrectPIN)
                    {
                        CreateSession(user.UserName);
                        routeResult = RedirectToAction("index", "user");
                    }
                }
                else if(user.Password == password)
                {
                    CreateSession(user.UserName);
                    routeResult = RedirectToAction("index", "user");
                }
                else
                {
                    routeResult = RedirectToAction("login", "authentication");
                }
            }
            else
            {
                return RedirectToAction("login", "authentication");
            }

            return routeResult;
        }


        private void CreateSession(string userName)
        {
            FormsAuthentication.SetAuthCookie(userName, false);
            var claims = new List<Claim>() {
                                             new Claim(ClaimTypes.Name, userName)
                                           };

            var id = new ClaimsIdentity(claims,
                                        DefaultAuthenticationTypes.ApplicationCookie);

            Request.GetOwinContext().Authentication.SignIn(id);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            ClearOwinContext();
            return RedirectToAction("login", "authentication");
        }

        private void ClearOwinContext()
        {
            Request.Cookies["DPAD-COOKIE"].Expires = DateTime.Now.AddYears(-10);

            Request.GetOwinContext().Authentication.SignOut(Microsoft.Owin.Security.Cookies.CookieAuthenticationDefaults.AuthenticationType);

            Request.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);

            Request.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);

            Request.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);

            Request.GetOwinContext().Authentication.SignOut(Microsoft.Owin.Security.Cookies.CookieAuthenticationDefaults.AuthenticationType);
        }

    }

    public class AdminUserLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Passcode { get; set; }
    }
} 