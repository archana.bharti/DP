﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DP.Administration.Models;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Business;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;
using System.Threading.Tasks;
using System.Web.Http;

namespace DP.Administration.Controllers.Api
{
    public class PdaController : BaseApiController
    {
        private PDAManager pdaManager;
        private PdaSubscriptionManager pdaSubscriptionManager;

        public PdaController(PDAManager pdaManager, PdaSubscriptionManager pdaSubscriptionManager)
        {
            this.pdaManager = pdaManager;
            this.pdaSubscriptionManager = pdaSubscriptionManager;

        }

        [HttpPost]
        public async Task<IHttpActionResult> Deactivate(PdaSubscription subscription)
        {
            await this.pdaSubscriptionManager.Deactivate(subscription.Id);
            return Ok(true);
        }
    }
}