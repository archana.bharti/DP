﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DP.Administration.Models;
using DP.Services.Business;
using DP.Common;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;

namespace DP.Administration.Controllers.Api
{
    public class DatabaseController : BaseApiController
    {
        private DatabaseManager databaseManager;
        private CommonManager commonManager;
        public DatabaseController(DatabaseManager databaseManager, CommonManager commonManager)
        {
            this.databaseManager = databaseManager;
            this.commonManager = commonManager;
        }

        [HttpGet]
        public IHttpActionResult Rows(string dbTable = "")
        {
            var result = this.databaseManager.GetTableRows(dbTable);
            return Ok(result);
        }
    }
}
