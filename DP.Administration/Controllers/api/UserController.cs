﻿using DP.Common.Models;
using DP.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DP.Administration.Controllers.Api
{
    public class UserController : BaseApiController
    {
        private UserManager userManager;
        private FeatureManager featureManager;
        public UserController(UserManager userManager, FeatureManager featureManager)
        {
            this.userManager = userManager;
            this.featureManager = featureManager;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Activate(SubscriberViewModel viewModel)
        {
            try
            {
                await this.userManager.Restart(viewModel.UserId.ToString());
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest("Failed to activate user");
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Deactivate(SubscriberActivationRequestViewModel viewModel)
        {
            try
            {
                await this.userManager.Deactivate(viewModel.SubscriberId, (int)viewModel.UserType);
                return Ok(true);
            }
            catch(Exception ex)
            {
                return BadRequest("Failed to deactivate user");
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Terminate(SubscriberViewModel viewModel)
        {
            try
            {
                await this.userManager.Terminate(viewModel.UserId.ToString());
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest("Failed to terminate user");
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> ExternalUsers(int offset, int limit, string order, string search =null)
        {
            try
            {
                offset = limit * (offset - 1);
                var rows = await this.userManager.GetExternalUsers(offset, limit);
                return Ok(new { total = rows.Count, rows = rows });
            }
            catch (Exception ex)
            {
                return BadRequest("Failed to fetch external users");
            }
        }

    }
}
