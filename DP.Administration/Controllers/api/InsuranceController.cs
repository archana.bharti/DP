﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DP.Administration.Controllers.Api
{
    public class InsuranceController : BaseApiController
    {
        private IInsuranceDao insuranceDao;
        public InsuranceController(IInsuranceDao insuranceDao)
        {
            this.insuranceDao = insuranceDao;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Update(Insurance insurance)
        {
            var propertiesToUpdate = new string[]
            {
                "CashlessFacility",
                "MaternityBenefits",
                "AmbulanceCharges",
                "DayCareProcedures",
                "HealthCheckup",
                "TaxBenefits",
                "HospitalizationExpenses",
                "FreelookPeriod",
                "AlternativeTreatment",
                "CumulativeBonus",
                "OrganDonorExpenses",
                "PrePostHospitalizationCharges",
                "CumulativeBonus",
                "Portability"
            };

            return Ok(await this.insuranceDao.UpdateAsync(insurance, propertiesToUpdate));
        }
    }
}
