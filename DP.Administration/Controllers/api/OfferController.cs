﻿using DP.Common.Utilities;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DP.Administration.Controllers.Api
{
    public class OfferController : BaseApiController
    {
        private OfferManager offerManager;
        public OfferController(OfferManager offerManager)
        {
            this.offerManager = offerManager;
        }

        [HttpPost]
        public async Task<IHttpActionResult> UploadLogo()
        {   
            if (!Request.Content.IsMimeMultipartContent())
            {
                return InternalServerError(new HttpResponseException(HttpStatusCode.UnsupportedMediaType));
            }

            var dataDictionary = await base.ParseEncodedForm(Request);

            if (dataDictionary != null)
            {
                AwsS3Manager s3Manager = new AwsS3Manager();
                OfferMaster offerMaster = this.offerManager.GetById(Convert.ToInt32(dataDictionary["id"]));
                string fileName = "offerid_" + offerMaster.Id.ToString() + "_eid" + offerMaster.ProviderId.ToString() + "_etype" + offerMaster.ProviderType.ToString() + Convert.ToString(dataDictionary["fileType"]);
                await s3Manager.Upload2Async(dataDictionary["file"] as byte[], fileName, true);

                string url = s3Manager.GetS3Url(fileName);
                offerMaster.ImageUrl = url;
                this.offerManager.Update(offerMaster);
                return Ok(true);
            }

            return BadRequest("Unable to upload image. Try again.");
        }
    }
}
