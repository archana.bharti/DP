﻿using DP.Common.Models;
using DP.Services.Business;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace DP.Administration.Controllers.Api
{
    public class PollController : BaseApiController
    {
        private IPollDao pollDao;
        public PollController(IPollDao pollDao)
        {
            this.pollDao = pollDao;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post(PollMaster model)
        {
            await this.pollDao.AddPollMaster(model);
            return Ok(true);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Update(PollMaster model)
        {
            await this.pollDao.UpdatePollMaster(model);
            return Ok(true);
        }
    }
}
