﻿using DP.Services.Business;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DP.Administration.Controllers
{
    public class InsuranceController : BaseController
    {
        private IInsuranceDao insuranceDao;
        public InsuranceController(IInsuranceDao insuranceDao)
        {
            this.insuranceDao = insuranceDao;
        }


        // GET: Insurance
        public async Task<ActionResult> Index()
        {
            return View(await this.insuranceDao.GetAll(false));
        }
    }
}