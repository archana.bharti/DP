﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DP.Administration.Models;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Business;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;

namespace DP.Administration.Controllers
{
    public class OfferController : BaseController
    {
        private OfferManager offerManager;
        public OfferController( OfferManager offerManager)
        {
            this.offerManager = offerManager;
        }
        // GET: Offer
        public ActionResult Index()
        {
            List<OfferMaster> offers = this.offerManager.Get();
            return View(offers);
        }

        // GET: Create
        public ActionResult Create()
        {
            return View();
        }

        // GET: Offer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Offer/Details/5
        public ActionResult Subscribers()
        {  
            return PartialView("_Subscribers", this.offerManager.Subscribers());
        }

        // POST: Offer/Create
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(OfferMaster model,HtmlHelper content)
        {
            try
            {
                model.OfferStatusValue = (int)OfferStatus.Active;
                var item =  this.offerManager.Add(model);
                return JsonResult(item);
            }
            catch(Exception ex)
            {
                return View("../Shared/_ViewError", new { Message = ex.Message });
            }
        }
    }
}
