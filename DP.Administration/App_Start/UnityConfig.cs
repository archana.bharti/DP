using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using DP.Services.Business;
using System.Web.Http;

namespace DP.Administration
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            //http://www.devtrends.co.uk/blog/using-unity.mvc5-and-unity.webapi-together-in-a-project

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
            IocBusiness.Register(container);
        }
    }
}