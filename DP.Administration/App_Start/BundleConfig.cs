﻿using System.Web;
using System.Web.Optimization;

namespace DP.Administration
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                            "~/Scripts/bootstrap.js",
                            "~/Scripts/moment.js",
                            "~/Scripts/bootstrap-modal.min.js",
                            "~/Scripts/bootstrap-select.min.js",
                            "~/Scripts/bootstrap-table.min.js",
                            "~/Scripts/bootstrap-datepicker.min.js",
                            "~/Scripts/bootstrap-validator.min.js",
                            "~/Scripts/bootstrap-star-rating.min.js",
                            "~/Scripts/metisMenu.min.js",
                            "~/Scripts/sb-admin.js",
                            "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/dpadmin").Include(
                             "~/Scripts/app.js",
                            "~/Scripts/views/extensions.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/metisMenu.min.css",
                      "~/Content/sb-admin-2.css",
                      "~/Content/font-awesome/css/font-awesome.min.css",
                      "~/Content/bootstrap-select.css",
                      "~/Content/bootstrap-datetimepicker.css",
                      "~/Content/bootstrap-star-rating.css",
                      "~/Content/bootstrap-table.css",
                      "~/Content/bootstrap-dialog.min.css",
                      "~/Content/bootstrap-checkbox.css",
                      "~/Content/bootstrap-star-rating.css",
                      "~/Content/hover.min.css",
                      "~/Content/app.css"));
        }
    }
}
