﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using DP.Services.Business.Identity;
using System;

[assembly: OwinStartupAttribute(typeof(DP.Administration.Startup))]
namespace DP.Administration
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var httpConfig = new HttpConfiguration();

            //Use cookie based authentication
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Authentication/Login"),
                CookieName = "DPAD-COOKIE",
                SlidingExpiration = false,
                ExpireTimeSpan = TimeSpan.FromDays(1),
                ReturnUrlParameter = ""
            });

            app.UseWebApi(httpConfig);
            DP.Services.Business.IocBusiness.SetAppBuilder(app);
            ConfigureAuth(app);
        }
    }
}
