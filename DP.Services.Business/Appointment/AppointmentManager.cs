﻿using System.Collections.Generic;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using System.Threading.Tasks;
using DP.Common.Utilities;
using DP.Common.Enums;
using System;
using System.Linq;
using DP.Services.Data.Models;
using DP.Services.Business.Administration;
using DP.Common.Constants;

namespace DP.Services.Business
{
    public sealed class AppointmentManager : BaseManager
    {
        private IAppointmentDao appointmentDao;
        private IUserDao userDao;
        private IOptionDao optionDao;

        public AppointmentManager(IAppointmentDao appointmentDao, IUserDao userDao, IOptionDao optionDao)
        {
            this.appointmentDao = appointmentDao;
            this.userDao = userDao;
            this.optionDao = optionDao;
        }

        public async Task<int> Add(Appointment model)
        {
            var _sdate = model.StartTime;
            var _edate = model.EndTime.AddHours(3);

            var conflict = appointmentDao.Appointments().Any(a =>
                                            a.EntityId == model.EntityId &&
                                            a.EntityType == model.EntityType &&
                                            // a.Email == model.Email &&
                                            a.Mobile == model.Mobile &&
                                            a.StatusValue == (int)AppointmentStatus.Active &&
                                           _sdate >= a.StartTime &&
                                           _sdate <= a.EndTime);
            if (!conflict)
            {
                var appUser = await this.userDao.GetBySubscriptionAsync(model.EntityId, EnumHelper<UserType>.GetDescription((UserType)model.EntityType));
                if (appUser == null) throw new Exception("This user is not available to take appoinments.");

                //TODO: Number of maximum bookings allowed per day
                Random rnd = new Random();
                model.Token = Convert.ToString(rnd.Next(9999999, 999999999));
                model.EndTime = model.StartTime.AddHours(3);
                model.StatusValue = (int)AppointmentStatus.Active;
                var id = await appointmentDao.Add(model);
                await SendSMS(model, appUser);
                await SendEmail(model, appUser);

                return id;
            }
            throw new Exception("There is a conflict with another appointment. Please select new time slot.");
        }

        private async Task SendSMS(Appointment model, ApplicationUser appUser)
        {
            await SMSManager.Instance.SendAppointmentToExternalUserAsync(model.Mobile, model.StartTime.ToString(GlobalSettings.DATE_FORMAT_1), model.EntityName, appUser.PhoneNumber);
            await SMSManager.Instance.SendAppointmentToEntityUserAsync(model.Name, model.Mobile, model.StartTime.ToString(GlobalSettings.DATE_FORMAT_1), model.Token, appUser.PhoneNumber);
        }

        private async Task SendEmail(Appointment model, ApplicationUser appUser)
        {
            await Task.Run(() => EmailManager.SendAppointmentToEntity(appUser.Email, model.EntityName, model.Name, model.StartTime.ToString(GlobalSettings.DATE_FORMAT_1), model.Token, model.Mobile) );
            await Task.Run(() => EmailManager.SendAppointmentToUser(model.Email, model.Name, model.EntityName, model.StartTime.ToString(GlobalSettings.DATE_FORMAT_1), model.Token, appUser.PhoneNumber) );
        }

        public async Task<Tuple<bool, string, string, string, string, Exception>> CheckAvailability()
        {
            Option weekOffDays, startTime, endTime;
            var keyList = new List<string>()
            {
                OptionKeys.APPOINTMENT_WEEK_OFF,
                OptionKeys.APPOINTMENT_START_TIME,
                OptionKeys.APPOINTMENT_END_TIME
            };
            var options = await this.optionDao.Get(keyList);

            weekOffDays = options.Where(x => x.Key == OptionKeys.APPOINTMENT_WEEK_OFF).Single();
            startTime = options.Where(x => x.Key == OptionKeys.APPOINTMENT_START_TIME).Single();
            endTime = options.Where(x => x.Key == OptionKeys.APPOINTMENT_END_TIME).Single();

            if (weekOffDays.Value.Contains(((int)DateTime.Now.DayOfWeek).ToString()))
                return new Tuple<bool, string, string, string, string, Exception>(false, startTime.Value, endTime.Value, weekOffDays.Value, DateTime.Now.TimeOfDay.ToString(), new Exception("Appointments can not be booked on week off days"));
                //throw new Exception("Appointments can not be booked on week off days");

            //time validation

            var sTime = new TimeSpan(Convert.ToInt16(startTime.Value), 0, 0);
            var eTime = new TimeSpan(Convert.ToInt16(endTime.Value), 0, 0);
            var now = DateTime.Now.TimeOfDay;

            if (now < sTime || now > eTime)
                return new Tuple<bool, string, string, string, string, Exception>(false, startTime.Value, endTime.Value, weekOffDays.Value, DateTime.Now.TimeOfDay.ToString(), new Exception(String.Format("Appointments can not be booked before {0} or after {1}", Get24HoursFormat(startTime.Value), Get24HoursFormat(endTime.Value))));
             //throw new Exception(String.Format("Appointments can not be booked before {0} or after {1}", Get24HoursFormat(startTime.Value), Get24HoursFormat(endTime.Value)));

            return  new Tuple<bool, string, string, string, string, Exception>(true, "", "", "", "", null);
        }
        

        private string Get24HoursFormat(string value)
        {

            value = value.Length == 1 ? "0" + value : value;
            switch (value)
            {
                case "24": return "12a.m";
                case "00": return "12a.m";
                case "01": return "1a.m.";
                case "02": return "2a.m.";
                case "03": return "3a.m.";
                case "04": return "4a.m.";
                case "05": return "5a.m.";
                case "06": return "6a.m.";
                case "07": return "7a.m.";
                case "08": return "8a.m.";
                case "09": return "9a.m.";
                case "10": return "10a.m.";
                case "11": return "11a.m.";
                case "12": return "12p.m.";
                case "13": return "1p.m.";
                case "14": return "2p.m.";
                case "15": return "3p.m.";
                case "16": return "4p.m.";
                case "17": return "5p.m.";
                case "18": return "6p.m.";
                case "19": return "7p.m.";
                case "20": return "8p.m.";
                case "21": return "9p.m.";
                case "22": return "10p.m.";
                case "23": return "11p.m.";
            }
            return value;
        }
    }
}
