﻿using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class CommonManager
    {
        private ICommonDao commonDao;
        public CommonManager(ICommonDao dao)
        {
            this.commonDao = dao;
        }

        //public List<object> GetSearchParameters()
        //{
        //    var ctx = commonDao.GetDpContext();
        //    Dictionary<string, object> specializationContext = new Dictionary<string, object>();
        //    var doctor = ctx.Specializations.Where(x => x.Specific == 1).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
        //    var dental = ctx.Specializations.Where(x => x.Specific == 2).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
        //    var alternativeMedicine = ctx.Specializations.Where(x => x.Specific == 3).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
        //    var hospital =   ctx.Specializations.Where(x => x.Specific == 4).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
        //    specializationContext.Add("Doctor", doctor);
        //    specializationContext.Add("dental", dental);
        //    specializationContext.Add("AlternativeMedicine", alternativeMedicine);
        //    specializationContext.Add("Hospital", hospital);

        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<State> GetStates()
        {
            return this.commonDao.GetStates();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns></returns>
        public List<City> GetCities(int? stateId = null)
        {
            if (stateId.HasValue)
            {
                
                return this.commonDao.Cities().Where(x => x.StateId == stateId.Value).ToList();
            }
            else
            {
                return this.commonDao.GetCities();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns></returns>
        public List<CollegeLocation> GetCollegeCities(int? stateId = null)
        {
            if (stateId.HasValue)
            {
                var ctx = this.commonDao.GetDpContext();
                return ctx.CollegeLocations.Where(x => x.StateId == stateId.Value).ToList();
            }
            else
            {
                return new List<CollegeLocation>();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public List<Location> GetLocations(int? cityId = null)
        {
            if (cityId.HasValue)
            {
                return this.commonDao.Locations().Where(x => x.CityId == cityId.Value).ToList();
            }
            else
            {
                return this.commonDao.GetLocations();
            }
        }

        public List<STDCode> GetStdCodes()
        {
           return this.commonDao.GetDpContext().STDCodes.ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public Dictionary<string, object>  GetSpecializations(int? id =null, int? type = null)
        {
            //return this.commonDao.GetSpecializations();
            var ctx = commonDao.GetDpContext();
            Dictionary<string, object> specializationContext = new Dictionary<string, object>();
            //var doctor = ctx.Specializations.Where(x => x.Specific == 1).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
            //var dental = ctx.Specializations.Where(x => x.Specific == 2).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
            //var alternativeMedicine = ctx.Specializations.Where(x => x.Specific == 3).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
            //var hospital = ctx.Specializations.Where(x => x.Specific == 4).Select(x => new { Name = x.Category, Id = x.Id }).ToList();

            ///////     Change By Reena Due to Client Request/////////////////////////////////
           // var allSpzCds = (from spz_cd in ctx.SpecializationClinics join spz in ctx.Specializations on spz_cd.SpecializationId equals spz.Id select spz).Distinct().ToList();
            var allSpzCds = (from spz in ctx.Specializations  select spz).Distinct().ToList();
            var medical = allSpzCds.Where(x => x.Specific == 1).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
            var dental = allSpzCds.Where(x => x.Specific == 2).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
            var alternativeMedicine = allSpzCds.Where(x => x.Specific == 3).Select(x => new { Name = x.Category, Id = x.Id }).ToList();
            var hospital = (from spz_cd in ctx.SpecializationHospitals join spz in ctx.Specializations on spz_cd.SpecializationId equals spz.Id select new { Name = spz.Category, Id = spz.Id }).Distinct().ToList();
          
            specializationContext.Add("Medical", medical);
            specializationContext.Add("Dental", dental);
            specializationContext.Add("AlternativeMedicine", alternativeMedicine);
            specializationContext.Add("Hospital", hospital);
            if (id.HasValue && id.Value != 0 && type.HasValue && type != 0)
            {
                switch (type)
                {
                    case 1:
                    case 2:
                    case 3: break;
                    case 4:
                        var user = ctx.Colleges.Where(x => x.Id == id.Value).SingleOrDefault();
                        if(user != null){
                            var departmenType = user.DepartmentType.Value;
                            var collegDeparts = ctx.CollegeDepartments
                                .Where(x => x.Category == departmenType)
                                .Select(x => new { Name = x.Name, Id = x.Id }).ToList();
                            specializationContext.Add("College", collegDeparts);
                        }
                        break;
                }
            }
            else
            {
                var collegDeparts = ctx.CollegeDepartments.Select(x => new { Name = x.Name, Id = x.Id }).ToList();
                specializationContext.Add("College", collegDeparts);
            }

            return specializationContext;
        }

        public List<ClinicAndDoctor> GetClinicsAndDoctors(DoctorOrClinicType? type, DoctorOrClinicCategoryType? category)
        {
            var query = this.commonDao.ClinicsAndDoctors();
            if (category.HasValue) query = query.Where(x => x.Category == (int)category.Value);
            if (type.HasValue) query = query = query.Where(x => x.Type.ToUpper() == EnumHelper<DoctorOrClinicType>.GetDescription(type.Value).ToUpper());
            return query.ToList();
        }

        public List<FeaturedEntity> GetFeaturedEntities(int type)
        {
            string userType = EnumHelper<UserType>.GetDescription((UserType)type);
            var featuredEntities = this.commonDao.GetFeatureEntities(type);

            if (featuredEntities.Count > 0)
            {
                List<int> ids = featuredEntities.Select(f => f.EntityId).ToList();

                switch ((UserType)type)
                {
                    case UserType.Doctor:
                    case UserType.Clinic:
                        return commonDao.ClinicsAndDoctors().Where(h => ids.Contains(h.Id)).ToList().Select(e => ModelFactory.ConvertToFeaturedEntity(e)).ToList();
                    case UserType.Hospital:
                        return commonDao.Hospitals().Where(h => ids.Contains(h.Id)).ToList().Select(e => ModelFactory.ConvertToFeaturedEntity(e)).ToList();
                    case UserType.College:
                        return commonDao.Colleges().Where(c => ids.Contains(c.Id)).ToList().Select(e => ModelFactory.ConvertToFeaturedEntity(e)).ToList();
                }
            }

            return null;
        }

        public SubscriberViewModel GetOfferProviderDetails(int subscriberId, UserType type)
        {
            var ctx = this.commonDao.GetDpContext();

            switch (type)
            {
                case UserType.Doctor:
                case UserType.Clinic:
                    var dc = ctx.ClinicsAndDoctors.Where(x => x.Id == subscriberId).Single();
                    var dcIden = ctx.Users.Where(x => x.SubscriberId == subscriberId && (x.UserType == "DOCTOR" || x.UserType == "CLINIC")).Single();

                    return new SubscriberViewModel() { Email = dcIden.Email, Name = dc.Name };
                case UserType.Hospital:
                    var hos = ctx.Hospitals.Where(x => x.Id == subscriberId).Single();
                    var hosIden = ctx.Users.Where(x => x.SubscriberId == subscriberId && x.UserType == "HOSPITAL").Single();

                    return new SubscriberViewModel() { Email = hosIden.Email, Name = hos.Name };
                case UserType.College:
                    var col = ctx.Colleges.Where(x => x.Id == subscriberId).Single();
                    var colIden = ctx.Users.Where(x => x.SubscriberId == subscriberId && x.UserType == "COLLEGE").Single();

                    return new SubscriberViewModel() { Email = colIden.Email, Name = col.Name };
            }
            return null;
        }
    }
}
