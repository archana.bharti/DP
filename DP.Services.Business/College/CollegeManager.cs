﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class CollegeManager : BaseManager
    {
        private ICollegeDao collegeCtx;
        public CollegeManager(ICollegeDao collegeDao)
        {
            this.collegeCtx = collegeDao;
        }

        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <returns>Task<List<College>>.</returns>
        public async Task<List<College>> GetAllAsync()
        {
            return await this.collegeCtx.GetAllAsync();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<College>.</returns>
        public async Task<College> GetByIdAsync(int id)
        {
            return await this.collegeCtx.GetByIdAsync(id);
        }

        public async Task<CollegeLocation> GetCollegeLocation(int collegeId)
        {
            return await this.collegeCtx.GetCollegeLocation(collegeId);
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="propertiesToUpdate">The properties to update.</param>
        /// <returns>Task<College>.</returns>
        public async Task<College> UpdateAsync(College model, params string[] propertiesToUpdate)
        {
            return await this.collegeCtx.UpdateAsync(model, propertiesToUpdate);
        }

        /// <summary>
        /// update rating as an asynchronous operation.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <returns>Task<CollegeRating>.</returns>
        public async Task<CollegeRating> UpdateRatingAsync(CollegeRating rating)
        {
            return await this.collegeCtx.UpdateRatingAsync(rating);
        }

        public async Task<List<CollegeRating>> GetRatingAsync(string userId)
        {
            return await this.collegeCtx.GetRatingAsync(userId);
        }
    }
}
