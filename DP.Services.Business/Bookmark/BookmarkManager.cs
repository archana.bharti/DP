﻿using System.Collections.Generic;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using System.Threading.Tasks;
using DP.Common.Utilities;
using DP.Common.Enums;
using System;
using System.Linq;
using DP.Services.Data.Models;
using DP.Common.Constants;

namespace DP.Services.Business
{
    public sealed class BookmarkManager
    {
        private IBookmarkDao bookmarkDao;
        private IClinicAndDoctorDao clinicAndDoctorDao;
        private ICollegeDao collegeDao;
        private IHospitalDao hospitalDao;
        private IInsuranceDao insuranceDao;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookmarkDao"></param>
        public BookmarkManager(IBookmarkDao bookmarkDao, IClinicAndDoctorDao clinicAndDoctorDao, ICollegeDao collegeDao, IHospitalDao hospitalDao, IInsuranceDao insuranceDao)
        {
            this.bookmarkDao = bookmarkDao;
            this.clinicAndDoctorDao = clinicAndDoctorDao;
            this.collegeDao = collegeDao;
            this.hospitalDao = hospitalDao;
            this.insuranceDao = insuranceDao;
        }

        /// <summary>
        /// Gets the specified user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Task<List<Bookmark>>.</returns>
        public async Task<List<Bookmark>> Get(int entityId, int entityType)
        {
            var results = new List<Bookmark>();
            var bookmarks = await bookmarkDao.GetByUserAsync(new Bookmark() { EntityId = entityId, EntityType = entityType } );
            var doctorsIdList = new List<int>();
            var hospitalIdList = new List<int>();
            var collegeIdList = new List<int>();
            var insuranceIdList = new List<int>();
            CacheManager cacheManager = new CacheManager();
            Dictionary<string, object> fromCache = cacheManager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object>;
            var rootPath = Convert.ToString(fromCache[GlobalSettings.S3_ROOT_PATH]);

            doctorsIdList = bookmarks.Where(x => x.BookmarkEntityType == (int)UserType.Clinic || x.BookmarkEntityType == (int)UserType.Doctor).Select(x => x.BookmarkEntityId).ToList();
            hospitalIdList = bookmarks.Where(x => x.BookmarkEntityType == (int)UserType.Hospital).Select(x => x.BookmarkEntityId).ToList();
            collegeIdList = bookmarks.Where(x => x.BookmarkEntityType == (int)UserType.College).Select(x => x.BookmarkEntityId).ToList();
            insuranceIdList = bookmarks.Where(x => x.BookmarkEntityType == (int)UserType.Insurance).Select(x => x.BookmarkEntityId).ToList();

            if(doctorsIdList.Count > 0)
            {
                var doctors = this.clinicAndDoctorDao.Get().Where(x => doctorsIdList.Contains(x.Id)).ToList();
                if(doctors.Count > 0)
                {
                    doctors.ForEach(x =>
                    {
                        var btype =  x.Type.ToUpper() == "CLINIC" ? 1 : 2;
                        results.Add(new Bookmark()
                        {
                            BookmarkEntityId = x.Id,
                            BookmarkEntityType = btype,
                            EntityId = entityId,
                            EntityType = entityType,
                            BookmarkEntityName = x.Name,
                            BookmarkEntityTypeText = EnumHelper<UserType>.GetDescription((UserType)btype),
                            BookmarkImageUrl =  string.IsNullOrEmpty(x.Profile_Pic_1) ? rootPath + "doctorOrClinic".ToLower() + "-default-300x300.png" : x.Profile_Pic_1,
                            Id = bookmarks.Where(z=>z.EntityId == entityId &&
                                                    z.EntityType == entityType &&
                                                    z.BookmarkEntityId == x.Id &&
                                                    z.BookmarkEntityType == btype).Single().Id
                        });
                    });
                }
            }

            if (hospitalIdList.Count > 0)
            {
                var hospitals = this.hospitalDao.Get().Where(x => hospitalIdList.Contains(x.Id)).ToList();
                if (hospitals.Count > 0)
                {
                    hospitals.ForEach(x =>
                    {
                        results.Add(new Bookmark()
                        {
                            BookmarkEntityId = x.Id,
                            BookmarkEntityType = 3,
                            EntityId = entityId,
                            EntityType = entityType,
                            BookmarkEntityName = x.Name,
                            BookmarkImageUrl = string.IsNullOrEmpty(x.Profile_Pic_1) ? rootPath + "hospital".ToLower() + "-default-300x300.png" : x.Profile_Pic_1,
                            BookmarkEntityTypeText = EnumHelper<UserType>.GetDescription(UserType.Hospital),
                            Id = bookmarks.Where(z => z.EntityId == entityId &&
                                                    z.EntityType == entityType &&
                                                    z.BookmarkEntityId == x.Id &&
                                                    z.BookmarkEntityType == (int)UserType.Hospital).Single().Id
                        });
                    });
                }
            }

            if (collegeIdList.Count > 0)
            {
                var colleges = this.collegeDao.Get().Where(x => collegeIdList.Contains(x.Id)).ToList();
                if (colleges.Count > 0)
                {
                    colleges.ForEach(x =>
                    {
                        results.Add(new Bookmark()
                        {
                            BookmarkEntityId = x.Id,
                            BookmarkEntityType = 3,
                            EntityId = entityId,
                            EntityType = entityType,
                            BookmarkEntityName = x.Name,
                            BookmarkEntityTypeText = EnumHelper<UserType>.GetDescription(UserType.College),
                            BookmarkImageUrl = string.IsNullOrEmpty(x.Profile_Pic_1) ? rootPath + "college".ToLower() + "-default-300x300.png" : x.Profile_Pic_1,
                            Id = bookmarks.Where(z => z.EntityId == entityId &&
                                                    z.EntityType == entityType &&
                                                    z.BookmarkEntityId == x.Id &&
                                                    z.BookmarkEntityType == (int)UserType.College).Single().Id
                        });
                    });
                }
            }

            if (insuranceIdList.Count > 0)
            {
                var insurances = this.insuranceDao.Get.Where(x => insuranceIdList.Contains(x.Id)).ToList();
                if (insurances.Count > 0)
                {
                    insurances.ForEach(x =>
                    {
                        results.Add(new Bookmark()
                        {
                            BookmarkEntityId = x.Id,
                            BookmarkEntityType = 3,
                            EntityId = entityId,
                            EntityType = entityType,
                            BookmarkEntityName = x.Name,
                            BookmarkEntityTypeText = EnumHelper<UserType>.GetDescription(UserType.Insurance),
                            BookmarkImageUrl = string.IsNullOrEmpty(x.ImageUrl) ? rootPath + "inusrance".ToLower() + "-default-300x300.png" : x.ImageUrl,
                            Id = bookmarks.Where(z => z.EntityId == entityId &&
                                                    z.EntityType == entityType &&
                                                    z.BookmarkEntityId == x.Id &&
                                                    z.BookmarkEntityType == (int)UserType.Insurance).Single().Id
                        });
                    });
                }
            }
            return results;
        }

        /// <summary>
        /// Adds the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Task<System.Int32>.</returns>
        /// <exception cref="System.Exception">Bookmark already saved for this user</exception>
        public async Task<int> Add(Bookmark bookmark)
        {

            var item = bookmarkDao.Bookmarks()
                                      .Where(b => b.EntityId == bookmark.EntityId &&
                                                  b.EntityType == bookmark.EntityType &&
                                                  b.BookmarkEntityId == bookmark.BookmarkEntityId &&
                                                  b.BookmarkEntityType == bookmark.BookmarkEntityType).SingleOrDefault();
            if (item != null)
            {
                throw new Exception("Bookmark already saved for this user");
            }
            return await bookmarkDao.Add(bookmark);
        }

        /// <summary>
        /// Deletes the bookmark
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<int></returns>
        public async Task<int> Delete(int id)
        {
            return await bookmarkDao.Delete(id);
        }
    }
}
