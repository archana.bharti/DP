﻿using DP.Common.Constants;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DP.Services.Business
{
    public abstract class BaseRatingManager : BaseManager
    {
        protected ICommonDao commonDao;
        protected IOptionDao optionDao;
        public BaseRatingManager(ICommonDao commonDao, IOptionDao optionDao)
        {
            this.optionDao = optionDao;
            this.commonDao = commonDao;
        }

        /// <summary>
        /// Gets the score calcuation query.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>System.String.</returns>
        protected string GetScoreCalcuationQuery(ClinicRatingViewModel viewModel)
        {
            return BuildQuery(viewModel, "dp_clinic_rating");
        }

        /// <summary>
        /// Gets the score calcuation query.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>System.String.</returns>
        protected string GetScoreCalcuationQuery(HospitalRatingViewModel viewModel)
        {
            return BuildQuery(viewModel, "dp_hospital_rating");
        }

        /// <summary>
        /// Gets the score calcuation query.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>System.String.</returns>
        protected string GetScoreCalcuationQuery(CollegeRatingViewModel viewModel)
        {
            return BuildQuery(viewModel, "dp_college_rating");
        }

        protected string GetScoreCalcuationQuery(InsuranceRatingViewModel viewModel)
        {
            return BuildQuery(viewModel, "dp_insurance_rating");
        }

        /// <summary>
        /// Builds the query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModel">The view model.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <returns>System.String.</returns>
        private string BuildQuery<T>(T viewModel, string tableName)
        {
            StringBuilder builder = new StringBuilder();
            Dictionary<string, string> sumAvgPer = new Dictionary<string, string>();
            List<string> excludeProperties = new List<string>() { "ClinicOrDoctorId", "LastVisited", "HospitalId", "CollegeId", "UserId", "YourExperience", "ReviewedOn", "Id", "UserName" };

            sumAvgPer.Add("SumOfRating", "sum({0}) as {1}");
            sumAvgPer.Add("Average", "cast(avg({0})AS decimal(10,2)) as {1}");
            sumAvgPer.Add("Percentage", "cast((avg({0})/ 5)*100 AS decimal(10,2)) as {1}"); //max({0})


            var properties = viewModel.GetType().GetProperties().Where(x =>
                !excludeProperties.Contains(x.Name) &&
                 (x.PropertyType == typeof(int) || x.PropertyType == typeof(float))).ToList();

            foreach (var calParam in sumAvgPer)
            {
                foreach (var prop in properties)
                {
                    builder.Append(string.Format(calParam.Value, prop.Name, prop.Name + calParam.Key));
                    builder.Append(", ");
                }
            }

            var query = builder.ToString().Trim();
            query = query.Substring(0, query.LastIndexOf(','));

            return "SELECT " + query + " FROM " + tableName;
        }

        /// <summary>
        /// Gets the column names.
        /// </summary>
        /// <param name="columnCollection">The column collection.</param>
        /// <returns>List<System.String>.</returns>
        protected List<string> GetColumnNames(DataColumnCollection columnCollection)
        {
            List<string> colNames = new List<string>();
            for (var i = 0; i <= columnCollection.Count - 1; i++)
            {
                colNames.Add(columnCollection[i].ColumnName);
            }

            return colNames;
        }

        protected void CheckReviewEligibility(DateTime reviewedDate)
        {
            DateTime eligibleDate = reviewedDate.AddDays(Convert.ToInt32(
                   this.optionDao.Get(OptionKeys.REVIEW_THRESHOLD).Result.Value
                ));

            if (DateTime.Now < eligibleDate)
            {
                int daysLeftToReview = (int)(eligibleDate - DateTime.Now).TotalDays;
                throw new Exception("You have recently rated this user.You can rate again after " + daysLeftToReview + " days.");
            }
        }
    }
}
