﻿using System.Linq;
using DP.Services.Data.Models;
using System.Threading.Tasks;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using System.Data;
using System.Data.Entity;
using System.Data.Linq;
using System.Collections.Generic;
using System;
using MoreLinq;

namespace DP.Services.Business
{
    public class CollegeRatingManager : BaseRatingManager
    {
        private ICollegeDao collegeDao;
        private IUserDao userDao;
        public CollegeRatingManager(ICommonDao commonDao, IOptionDao optionDao, ICollegeDao collegeDao, IUserDao userDao) : base(commonDao, optionDao)
        {
            this.commonDao = commonDao;
            this.collegeDao = collegeDao;
            this.userDao = userDao;
        }

        /// <summary>
        /// Adds the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Task<System.Int32>.</returns>
        public async Task<int> AddOrUpdate(CollegeRatingViewModel viewModel)
        {
            var rating = await collegeDao.GetRatingAsync(viewModel.CollegeId, viewModel.UserId);

            if (rating != null)
                CheckReviewEligibility(rating.ReviewedDate);

            CollegeRating dbModel = await this.collegeDao.Add(ModelFactory.ConvertToRatingDbModel(viewModel));
            var dt = commonDao.GetDpContext().GetDataTable(string.Format("{0} {1}={2}", GetScoreCalcuationQuery(viewModel), "where CollegeId", viewModel.CollegeId));
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                var dbModelProperties = dbModel.GetType().GetProperties().Where(x => GetColumnNames(dt.Columns).Contains(x.Name)).ToList();

                dbModelProperties.ForEach(prop =>
                {
                    var value = (prop.PropertyType.Name == "Single" && row[prop.Name].GetType().Name == "Decimal") ? Convert.ToSingle(row[prop.Name]) : row[prop.Name];

                    if (value != DBNull.Value)
                    {
                        prop.SetValue(dbModel, value);
                    }
                });

                dbModel = await this.collegeDao.UpdateRatingAsync(dbModel);
            }
            return dbModel.Id;
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<List<CollegeRatingAddOrUpdateViewModel>>.</returns>
        public async Task<List<CollegeRatingViewModel>> Get(int id)
        {
            var results = await collegeDao.GetRatingAsync(id);
            return results.Select(x => ModelFactory.ConvertToRatingViewModel(x)).ToList();
        }

        public async Task<object> GetTopRated(int id)
        {
            var query = this.collegeDao.GetRating();
            query = query.Where(x => x.CollegeId == id).OrderByDescending(x => x.ReviewedDate);
            var notList = new List<string>() { "User", "College", "CollegeId", "ReviewedDate", "Type", "YourExperience", "UserId", "Id" };
            var result = await query.FirstOrDefaultAsync();
            if (result == null) { return null; }

            Dictionary<string, int> dict = new Dictionary<string, int>();
            var properties = result.GetType().GetProperties().Where(
                 x =>
                !x.Name.Contains("Percentage") &&
                !x.Name.Contains("SumOfRating") &&
                !x.Name.Contains("Average") &&
                !notList.Contains(x.Name));
            foreach (var prop in properties)
            {
                dict.Add(prop.Name, int.Parse(prop.GetValue(result).ToString()));
            }

            var topRated = dict.MaxBy(x => x.Value);
            if (topRated.Value == 5)
            {
                return new
                {
                    Parameter = topRated.Key,
                    Value = topRated.Value
                };
            }
            return null;
        }

        public async Task<int> GetRatingsCount(int userId)
        {
            var appUser = await this.userDao.GetBySubcriberId(userId, new List<string> { "COLLEGE" });
            return appUser == null ? 0 : await this.collegeDao.GetRatingCount(appUser.Id);
        }
    }
}
