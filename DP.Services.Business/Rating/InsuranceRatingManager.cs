﻿using System;
using System.Linq;
using DP.Services.Data.Models;
using System.Threading.Tasks;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using System.Data;
using System.Data.Entity;
using System.Data.Linq;
using System.Collections.Generic;
using DP.Common.Constants;
using MoreLinq;

namespace DP.Services.Business
{
    public class InsuranceRatingManager : BaseRatingManager
    {
        private IInsuranceDao dao;
        private IUserDao userDao;
        public InsuranceRatingManager(ICommonDao commonDao, IOptionDao optionDao, IInsuranceDao dao, IUserDao userDao) : base(commonDao, optionDao)
        {
            this.dao = dao;
            this.userDao = userDao;
        }

        public async Task<int> AddOrUpdate(InsuranceRatingViewModel viewModel)
        {
            var rating = await dao.GetRatingAsync(viewModel.InsuranceId, viewModel.UserId);

            if (rating != null)
                CheckReviewEligibility(rating.ReviewedDate);

            InsuranceRating dbModel = await this.dao.AddRating(ModelFactory.ConvertToRatingDbModel(viewModel));
            var dt = commonDao.GetDpContext().GetDataTable(string.Format("{0} {1}={2}", GetScoreCalcuationQuery(viewModel), "where InsuranceId", viewModel.InsuranceId));
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                var dbModelProperties = dbModel.GetType().GetProperties().Where(x => GetColumnNames(dt.Columns).Contains(x.Name)).ToList();

                dbModelProperties.ForEach(prop =>
                {
                    var value = (prop.PropertyType.Name == "Single" && row[prop.Name].GetType().Name == "Decimal") ? Convert.ToSingle(row[prop.Name]) : row[prop.Name];

                    if (value != DBNull.Value)
                    {
                        prop.SetValue(dbModel, value);
                    }
                });

                dbModel = await this.dao.UpdateRatingAsync(dbModel);
            }
            return dbModel.Id;
        }

        public async Task<List<InsuranceRatingViewModel>> Get(int id)
        {
            var results = await this.dao.GetRatingAsync(id);
            return results.Select(x => ModelFactory.ConvertToRatingViewModel(x)).ToList();
        }

        public async Task<InsuranceRatingScoreViewModel> GetScores(int id)
        {
            var result = await dao.GetRatingScores(id);
            return ModelFactory.ConvertToScoresViewModel(result);
        }
    }
}
