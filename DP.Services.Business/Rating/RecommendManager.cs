﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class RecommendManager : BaseManager
    {
        IRecommendDao recommendDao;
        public RecommendManager(IRecommendDao recommendDao)
        {
            this.recommendDao = recommendDao;
        }

        public async Task AddOrRemove(Recommend dbModel)
        {
            var hasRecommended = await this.recommendDao.Get(dbModel);
            if (hasRecommended == null)
            {
                await this.recommendDao.AddOrRemove(dbModel);
            }
        }

        public async Task<List<Recommend>> Get(int entityId, int type)
        {
            return await this.recommendDao.Get(entityId, type);
        }

        public async Task<int> Count(int entityId, int type)
        {
            return await this.recommendDao.Count(entityId, type);
        }
    }
}
