﻿using DP.Common.Models;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using DP.Common.Enums;
using DP.Common.Utilities;
using DP.Common.Constants;

namespace DP.Services.Business
{
    public class EndorsementManager : BaseManager
    {
        private const string ENDORSEEID = "[ID]";
        private const string ENDORSEETYPE = "[TYPE]";

        IEndorsementDao endorsementDao;
        ICommonDao commonDao;
        SpecializationManager specializationMgr;
        CacheManager cacheManager = new CacheManager();
        Dictionary<string, object> fromCache;
        string rootPath;
        UserType userType;

        public EndorsementManager(IEndorsementDao endorsementDao, ICommonDao commonDao, SpecializationManager specializationMgr)
        {
            this.commonDao = commonDao;
            this.endorsementDao = endorsementDao;
            this.specializationMgr = specializationMgr;

            fromCache= cacheManager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object>;
            rootPath = Convert.ToString(fromCache[GlobalSettings.S3_ROOT_PATH]);
        }

        public async Task<int> AddOrUpdate(EndorsementAddOrUpdateViewModel viewModel)
        {
            return await endorsementDao.Add(ModelFactory.ConvertToEndorsementDbm(viewModel).First());
        }
                
        public async Task<List<UserEndorsementViewModel>> Get(int endorseeId, int endorseeType)
        {
            userType = (UserType)endorseeType;
            string baseQuery = string.Empty;

            List<SpecializationViewModel> specializations = await specializationMgr.Get(endorseeId, (UserType)endorseeType);

            if (specializations.Count > 0)
            {
                switch (userType)
                {
                    case UserType.Clinic:
                    case UserType.Doctor:
                        baseQuery = endorsementDao.GetClinicOrDoctorQuery;
                        break;
                    case UserType.Hospital:
                        baseQuery = endorsementDao.GetHospitalQuery;
                        break;
                    case UserType.College:
                        baseQuery = endorsementDao.GetCollegeQuery;
                        break;
                }

                baseQuery = baseQuery.Replace(ENDORSEEID, endorseeId.ToString())
                                     .Replace(ENDORSEETYPE, endorseeType.ToString());

                var endorsements = commonDao.ExecuteQuery<UserEndorsementViewModel>(baseQuery);

                endorsements.ForEach(m =>
                {
                    m.Endorsers = MapEndorsers(m.Users);
                });

                specializations.ForEach(s =>
                {
                    if (!endorsements.Any(e => e.SpecializationId == s.SpecializationId))
                    {
                        endorsements.Add(new UserEndorsementViewModel()
                        {
                            SpecializationId = s.SpecializationId,
                            SpecializationName = s.Name,
                            Count = 0,
                            Users = string.Empty,
                            Endorsers = new List<EndorserViewModel>()
                        });
                    }
                });

                return endorsements.OrderBy(e => e.SpecializationId).ToList();
            }

            return null;

        }

        private List<TextValueViewModel> GetSpecializations(string specializationIds)
        {
            int[] ids = specializationIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToArray();

            return ModelFactory.ConvertToTextValuePair<Specialization>(
                commonDao.Specializations().Where(s => ids.Contains(s.Id)).ToList(), "Id", "Category");
        }

        private List<EndorserViewModel> MapEndorsers(string users)
        {
            List<EndorserViewModel> Endorsers = new List<EndorserViewModel>();
            users = users.Replace("http:", "").Replace("https:", "");
            Array.ForEach(users.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(sValue => sValue.Trim()).Select(s => s.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries)).ToArray(), a =>
            {
                Endorsers.Add(new EndorserViewModel()
                {
                    EndorserName = a[0],
                    EndorserImageUrl = (a[1].ToLower() == "default") ? rootPath + "default-user-100x100.png" : a[1],
                    EndorserId = a[2],
                    EndorserType = a[3]
                });
            });

            return Endorsers;
        }
    }
}
