﻿using DP.Services.Business.Administration;
using DP.Services.Data;
using DP.Services.Security;
using Microsoft.Practices.Unity;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class IocBusiness
    {
        private static IAppBuilder app{get; set;}
        public static IUnityContainer Container { get; set; }
        public static void SetAppBuilder(IAppBuilder appBuilder)
        {
            app = appBuilder;
            
            //Create one DbContext per request
            app.CreatePerOwinContext(DPContext.Create);
        }

        public static void Register(IUnityContainer container)
        {
            IocBusiness.Container = container;
            IocData.Register(container);
            //BA Layer
            container.RegisterType<SecurityManager>(new HierarchicalLifetimeManager());
            container.RegisterType<ClinicManager>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager>(new HierarchicalLifetimeManager());
            container.RegisterType<CommonManager>(new HierarchicalLifetimeManager());
            container.RegisterType<DoctorOrClinicSearchManager>(new HierarchicalLifetimeManager());
            container.RegisterType<HospitalSearchManager>(new HierarchicalLifetimeManager());
            container.RegisterType<CollegeSearchManager>(new HierarchicalLifetimeManager());
            container.RegisterType<FeatureManager>(new HierarchicalLifetimeManager());
            container.RegisterType<HospitalManager>(new HierarchicalLifetimeManager());
            container.RegisterType<CollegeManager>(new HierarchicalLifetimeManager());
            container.RegisterType<ClinicRatingManager>(new HierarchicalLifetimeManager());
            container.RegisterType<HospitalRatingManager>(new HierarchicalLifetimeManager());
          //  container.RegisterType<SummaryManager>(new HierarchicalLifetimeManager());
            container.RegisterType<UserProfileManager>(new HierarchicalLifetimeManager());
            container.RegisterType<SpecializationManager>(new HierarchicalLifetimeManager());
            container.RegisterType<PDAManager>(new HierarchicalLifetimeManager());
            container.RegisterType<BookmarkManager>(new HierarchicalLifetimeManager());
            container.RegisterType<EndorsementManager>(new HierarchicalLifetimeManager());
            container.RegisterType<ZoneSearchManager>(new HierarchicalLifetimeManager());
            container.RegisterType<RecommendManager>(new HierarchicalLifetimeManager());
            container.RegisterType<AppointmentManager>(new HierarchicalLifetimeManager());
            container.RegisterType<DatabaseManager>(new HierarchicalLifetimeManager());
            container.RegisterType<SubscriberManager>(new HierarchicalLifetimeManager());
            container.RegisterType<OfferManager>(new HierarchicalLifetimeManager());
            container.RegisterType<PdaSubscriptionManager>(new HierarchicalLifetimeManager());
            container.RegisterType<QualificationManager>(new HierarchicalLifetimeManager());
            container.RegisterType<InsuranceManager>(new HierarchicalLifetimeManager());
            //container.RegisterType<IZoneSearchFactory, ZoneSearchFactory>(new HierarchicalLifetimeManager());
            //container.RegisterType<IZoneSearch, DoctorOrClinicZoneSearch>("DoctorOrClinic", new HierarchicalLifetimeManager());
            //container.RegisterType<IZoneSearch, HospitalZoneSearch>("Hospital", new HierarchicalLifetimeManager());
        }  
    }
}
