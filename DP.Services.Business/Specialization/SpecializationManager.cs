﻿using System.Linq;
using System.Threading.Tasks;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using System.Data;
using DP.Common.Enums;
using System.Collections.Generic;
using DP.Services.Data.Models;

namespace DP.Services.Business
{
    public class SpecializationManager : BaseManager
    {
        private ICommonDao commonDao;
        private IClinicAndDoctorDao clinicAndDoctorDao;
        private IHospitalDao hospitalDao;
        private ICollegeDao collegeDao;
        private const string text = "Category";
        private const string value = "Id";
        public SpecializationManager(ICommonDao commonDao, IClinicAndDoctorDao clinicAndDoctorDao, IHospitalDao hospitalDao, ICollegeDao collegeDao)
        {
            this.commonDao = commonDao;
            this.clinicAndDoctorDao = clinicAndDoctorDao;
            this.hospitalDao = hospitalDao;
            this.collegeDao = collegeDao;
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns>Task<List<TextValueViewModel>>.</returns>
        public async Task<List<TextValueViewModel>> Get()
        {
            var results = commonDao.GetSpecializations();
            return ModelFactory.ConvertToTextValuePair<Specialization>(results, value, text);

        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <returns>Task<List<SpecializationViewModel>>.</returns>
        public async Task<List<SpecializationViewModel>> Get(int id, UserType type)
        {
            switch(type)
            {
                case UserType.Clinic:
                case UserType.Doctor:
                    var clinicSpl = await clinicAndDoctorDao.GetSpecializations(id);
                    return clinicSpl.Select(x => ModelFactory.ConvertToSpecializationVM(x)).ToList();
                case UserType.Hospital:
                    var hospitalSpl = await hospitalDao.GetSpecializations(id);
                    return hospitalSpl.Select(x => ModelFactory.ConvertToSpecializationVM(x)).ToList();
                case UserType.College:
                    var collegeSpl = commonDao.GetDpContext().CollegeDepartments.ToList();
                    List<SpecializationViewModel> splVms = new List<SpecializationViewModel>();
                    foreach (var model in collegeSpl)
                    {
                        splVms.Add(new SpecializationViewModel()
                                    {
                                        Id = model.Id,
                                        SpecializationId = model.Id,
                                        Name = model.Name,
                                        SubscriberId = id
                        });
                    }
                    return splVms;
            }

            return new List<SpecializationViewModel>();
        }

        /// <summary>
        /// Adds the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="type">The type.</param>
        /// <returns>Task<SpecializationViewModel>.</returns>
        public async Task<SpecializationViewModel> Add(SpecializationViewModel viewModel, UserType type)
        {
            switch(type)
            {
                case UserType.Clinic:
                case UserType.Doctor:
                    var clinic = await clinicAndDoctorDao.AddSpecialization(ModelFactory.ConvertSpecializationVmToDbm<SpecializationClinic>(viewModel, type));
                    return ModelFactory.ConvertToSpecializationVM(clinic);
                case UserType.Hospital:
                    var hospital = await hospitalDao.AddSpecialization(ModelFactory.ConvertSpecializationVmToDbm<SpecializationHospital>(viewModel, type));
                    return ModelFactory.ConvertToSpecializationVM(hospital);
            }

            return null;
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <returns>Task<System.Int32>.</returns>
        public async Task<int> Delete(int subscriberId, int specializationId, UserType type)
        {
            switch (type)
            {
                case UserType.Clinic:
                case UserType.Doctor:
                    return await clinicAndDoctorDao.DeleteSpecialization(specializationId, subscriberId);
                case UserType.Hospital:
                    return await hospitalDao.DeleteSpecialization(specializationId, subscriberId);
            }

            return 0;
        } 
    }
}
