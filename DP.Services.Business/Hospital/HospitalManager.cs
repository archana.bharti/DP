﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class HospitalManager : BaseManager
    {
        private IHospitalDao hospitalCtx;
        public HospitalManager(IHospitalDao hospitalDao)
        {
            this.hospitalCtx = hospitalDao;
        }

        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <returns>Task<List<Hospital>>.</returns>
        public async Task<List<Hospital>> GetAllAsync()
        {
            return await this.hospitalCtx.GetAllAsync();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<Hospital>.</returns>
        public async Task<Hospital> GetByIdAsync(int id)
        {
            return await this.hospitalCtx.GetByIdAsync(id);
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="propertiesToUpdate">The properties to update.</param>
        /// <returns>Task<Hospital>.</returns>
        public async Task<Hospital> UpdateAsync(Hospital model, params string[] propertiesToUpdate)
        {
            return await this.hospitalCtx.UpdateAsync(model, propertiesToUpdate);
        }

        /// <summary>
        /// update rating as an asynchronous operation.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <returns>Task<HospitalRating>.</returns>
        public async Task<HospitalRating> UpdateRatingAsync(HospitalRating rating)
        {
            return await this.hospitalCtx.UpdateRatingAsync(rating);
        }

        public async Task<List<HospitalRating>> GetRatingAsync(string userId)
        {
            return await this.hospitalCtx.GetRatingAsync(userId);
        }
    }
}
