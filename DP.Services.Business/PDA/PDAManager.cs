﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Linq;
using System.Threading.Tasks;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System.Threading;
using DP.Common.Models;
using DP.Common.Constants;
using DP.Common.Enums;
using DP.Common.Utilities;


namespace DP.Services.Business
{
    public class PDAManager : BaseManager
    {
        private IPdaDao pdaDao;
        private IPdaSubscriptionDao pdaSubscriptonDao;
        private IUserDao userDao;
        private IClinicAndDoctorDao doctorDao;
        private ISpecializationClinicDao specializationClinicDao;
        private IFeatureDao featureDao;
        private CommonManager commonManager;

        public PDAManager(IPdaDao pdaDao, IPdaSubscriptionDao pdaSubscriptonDao,
            IUserDao userDao, 
            IClinicAndDoctorDao doctorDao,
            ISpecializationClinicDao specializationClinicDao,
            IFeatureDao featureDao,
            CommonManager commonManager)
        {
            this.pdaDao = pdaDao;
            this.pdaSubscriptonDao = pdaSubscriptonDao;
            this.userDao = userDao;
            this.doctorDao = doctorDao;
            this.specializationClinicDao = specializationClinicDao;
            this.featureDao = featureDao;
            this.commonManager = commonManager;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public async Task<PdaViewModel> Add(PdaViewModel viewModel)
        {
            PersonalAdvice dbModel = ModelFactory.ConvertToPdaDbm(viewModel);
            var subscrptions = await this.pdaSubscriptonDao.GetActiveSubscriptions();
            var spzIds = viewModel.SpecializationIds.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt32(x)).ToList<int>();
            subscrptions = subscrptions.Where(x => spzIds.Contains(x.SpecializationId)).ToList();

            var randomSubscription = subscrptions.ElementAt(new Random().Next(0, subscrptions.Count()));
            ApplicationUser doctor = await this.userDao.GetBySubcriberId(randomSubscription.DoctorOrClinicId, new List<string>() { "DOCTOR", "CLINIC" } );
            ApplicationUser user = await userDao.GetByIdAsync(viewModel.UserId);          

            dbModel.DoctorId = randomSubscription.DoctorOrClinicId;
            dbModel.SpecializationId = randomSubscription.Specialization.SpecializationId;
            dbModel.DoctorEmail = doctor.Email;
            dbModel.UserEmail = user.Email;
            dbModel = await this.pdaDao.Add(dbModel);

            //2. Send a mail to the doctor and to the user in two different templates
            MailStatus doctorMailStatus = await Task.Run(()=> EmailManager.SendPDA(doctor.Email, EmailTemplates.Template_PdaDoctor, user.UserName, "login"));
            MailStatus userEmailStatus = await Task.Run(() => EmailManager.SendPDA(user.Email, EmailTemplates.Template_PdaUser));

            dbModel.DoctorEmailStatusValue = (int)doctorMailStatus;
            dbModel.UserEmailStatusValue = (int)userEmailStatus;

            await this.pdaDao.UpdateAsync(dbModel, new string[] { "DoctorEmailStatusValue", "UserEmailStatusValue" });

            await SMSManager.Instance.SendPdaDoctorAsync(doctor.PhoneNumber, doctor.DisplayName);
            dbModel = await this.pdaDao.GetByIdAsync(dbModel.Id);
            return ModelFactory.ConvertToPdaVm(dbModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriberId"></param>
        /// <returns></returns>
        public async Task<List<PdaViewModel>> Requests(int subscriberId)
        {
            var list = await this.pdaDao.GetByDoctorIdAsync(subscriberId);
            list = list.OrderByDescending(x => x.SubmittedDate).ToList();
            return list.Select(x => ModelFactory.ConvertToPdaVm(x)).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<PdaViewModel>> Requests(string userId)
        {
            var list = await this.pdaDao.GetByUserIdAsync(userId);
            list = list.OrderByDescending(x => x.SubmittedDate).ToList();
            return list.Select(x => ModelFactory.ConvertToPdaVm(x)).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public async Task<PdaViewModel> Reply(PdaViewModel viewModel)
        {
            var dbModel = await this.pdaDao.GetByIdAsync(viewModel.Id.Value);
            ClinicAndDoctor doctor = await this.doctorDao.GetByIdAsync(dbModel.DoctorId);
            ApplicationUser user = await userDao.GetByIdAsync(viewModel.UserId);


            dbModel.Answer = viewModel.Answer + @"<br/><br/>" +doctor.Name+" is replying your questions based on the information (limited) you have provided. It's always good to get an in-person patient consultation";
            dbModel.RepliedDate = DateTime.Now;
            await this.pdaDao.UpdateAsync(dbModel, new string[] { "Answer", "RepliedDate" });            
            var appUrl = GlobalSettings.APP_URL;
            MailStatus userEmailStatus = await Task.Run(() =>EmailManager.SendPDA(user.Email, EmailTemplates.Template_PdaAnswer, doctor.Name, appUrl));
            dbModel = await this.pdaDao.GetByIdAsync(viewModel.Id.Value);
            return ModelFactory.ConvertToPdaVm(dbModel);
        }

        /// <summary>
        /// Gets speclializations of doctors who status is active and PDA enabled
        /// </summary>
        /// <returns></returns>
        public async Task<List<SpecializationClinic>> GetSpecializations()
        {
            var subscrptions = await this.pdaSubscriptonDao.GetActiveSubscriptions();
            var specializations = subscrptions.Select(x => x.Specialization).ToList();
            return specializations;
        }

        public List<FeaturedEntity> Subscribers()
        {
            var clinics = commonManager.GetFeaturedEntities((int)UserType.Doctor);
            var doctors = commonManager.GetFeaturedEntities((int)UserType.Clinic);           
            var list = new List<FeaturedEntity>();
            if (doctors != null) { list.AddRange(doctors); }
            if (clinics != null) { list.AddRange(clinics); }           
            return list;
        }
    }
}
