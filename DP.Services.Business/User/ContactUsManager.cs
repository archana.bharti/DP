﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DP.Common.Enums;
using DP.Common.Utilities;
using DP.Common.Constants;

namespace DP.Services.Business
{
    public class ContactUsManager
    {
        IContactUsDao dao;
        public ContactUsManager(IContactUsDao dao)
        {
            this.dao = dao;
        }

        public async Task<List<ContactUs>> GetAll()
        {
            return await this.dao.All;
        }

        public async Task<ContactUs> Add(ContactUs model)
        {
            //var hasContacted = await this.dao.GetByMobile(model.Mobile);
            
            //if (hasContacted == null)
           // {
                var reason = Convert.ToInt32(model.Reason);
                var rn = (ContactCategory)reason;
                model.Reason = EnumHelper<ContactCategory>.GetDescription(rn);

                await this.dao.Add(model);
                SendEmail(model, rn);
                return model;
            //}

           

           // return model;
        }

        private string GetEmailByReason(ContactCategory reason)
        {
            switch (reason)
            {
                case ContactCategory.GeneralQuery: return GlobalSettings.GetEmailByCategory(GlobalSettings.EMAILCAT_QUERIES);
                case ContactCategory.InfoNotCorrect: return GlobalSettings.GetEmailByCategory(GlobalSettings.EMAILCAT_INFORMATION_NOT_CORRECT); ;
                case ContactCategory.OfferRenewal: return GlobalSettings.GetEmailByCategory(GlobalSettings.EMAILCAT_RENEWAL_OFFER); ;
                case ContactCategory.PlaceOffer: return GlobalSettings.GetEmailByCategory(GlobalSettings.EMAILCAT_PLACE_OFFER); ;
                case ContactCategory.ToBeFeatured: return GlobalSettings.GetEmailByCategory(GlobalSettings.EMAILCAT_FEATURED); ;
                case ContactCategory.ToBeListed: return GlobalSettings.GetEmailByCategory(GlobalSettings.EMAILCAT_LISTED); ;
                case ContactCategory.Careers: return GlobalSettings.GetEmailByCategory(GlobalSettings.EMAILCAT_CAREERS); ;
            }
            return GlobalSettings.ADMIN_EMAIL;
        }

        private void SendEmail(ContactUs model, ContactCategory reason)
        {
            var targetEmail = GetEmailByReason(reason);
            Task.Run(() => { EmailManager.SendContactEmailToUser(model.Email, new string[] { model.Name }); });
            Task.Run(() => { EmailManager.SendContactEmailToAdmin(
                targetEmail,
                new string[] {
                    model.Name,
                    model.Email,
                    model.Mobile,
                    model.Reason,
                    model.Message
                }); });
        }

        public async Task<ContactUs> Update(ContactUs model)
        {
            model = await this.dao.GetById(model.Id);
            model.Status = (int)ContactUsStatus.Answered;
            return await this.dao.Update(model, new string[] { "Status" });
        }

        public async Task<ContactUs> GetByMobile(string mobile)
        {
            return await this.dao.GetByMobile(mobile);
        }
    }
}
