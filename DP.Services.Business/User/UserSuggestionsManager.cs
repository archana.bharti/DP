﻿using System.Collections.Generic;
using System.Linq;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Data.UnitOfWork;
using DP.Services.Data;
using DP.Services.Data.Models;

namespace DP.Services.Business
{
    public sealed class UserSuggestionsManager : BaseManager
    {
        private ICommonDao commonDao;
        private DPContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commonDao"></param>
        public UserSuggestionsManager(ICommonDao commonDao)
        {
            this.commonDao = commonDao;
            this.context = this.commonDao.GetDpContext();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public object GetSuggestions(UserFinderViewModel viewModel)
        {
            var results = new List<UserFinderViewModel>();

            switch (viewModel.UserType)
            {
                case UserType.ClinicAndDoctor:
                    viewModel.UserType = UserType.Clinic;
                    var r1 = GetClinicsOrDoctors(viewModel).ToList();
                    viewModel.UserType = UserType.Doctor;
                    var r2 = GetClinicsOrDoctors(viewModel).ToList();
                    results = r1.Concat(r2).ToList();
                    break;
                case UserType.Clinic:
                case UserType.Doctor:
                    results = GetClinicsOrDoctors(viewModel).ToList();
                    break;
                case UserType.Hospital:
                    results = GetHospitals(viewModel).ToList();
                    break;
                case UserType.College:
                    results = GetColleges(viewModel).ToList();
                    break;
                case UserType.Insurance:
                    results = GetInsurances(viewModel).ToList();
                    break;
            }

            return new
            {
                Results = results,
                Count = results.Count
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private IEnumerable<UserFinderViewModel> GetClinicsOrDoctors(UserFinderViewModel viewModel)
        {
            var type = EnumHelper<UserType>.GetDescription(viewModel.UserType);
            var query = this.context.ClinicsAndDoctors.Where(c => c.Type.ToUpper() == type.ToUpper() &&
                                                             c.Name.ToUpper().Contains(viewModel.SearchTerm.ToUpper()));
            if (viewModel.SubType.HasValue && viewModel.SubType.Value != 0)
            {
                query = query.Where(x => x.Category == viewModel.SubType.Value);
            }

            if (viewModel.ExcludeSubscribers)
            {
                var subscribers = GetSubscribers(viewModel.UserType);
                query =  query.Where(x => !subscribers.Contains(x.Id));
            }

            return query.ToList().Select(c => ModelFactory.ConvertToUserSuggestionVM(c));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private IEnumerable<UserFinderViewModel> GetHospitals(UserFinderViewModel viewModel)
        {
            var type = EnumHelper<UserType>.GetDescription(viewModel.UserType);
            var query = this.context.Hospitals.Where(c => c.Name.ToUpper().Contains(viewModel.SearchTerm.ToUpper()));

            if (viewModel.ExcludeSubscribers)
            {
                var subscribers = GetSubscribers(UserType.Hospital);
                query = query.Where(x => !subscribers.Contains(x.Id));
            }

            return query.ToList().Select(c => ModelFactory.ConvertToUserSuggestionVM(c));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private IEnumerable<UserFinderViewModel> GetColleges(UserFinderViewModel viewModel)
        {
            var type = EnumHelper<UserType>.GetDescription(UserType.College);
            var query = this.context.Colleges.Where(c => c.Name.ToUpper().Contains(viewModel.SearchTerm.ToUpper()));
            if (viewModel.SubType.HasValue && viewModel.SubType.Value != 0)
            {
                query = query.Where(x => x.DepartmentType != null && x.DepartmentType == viewModel.SubType.Value);
            }

            if (viewModel.ExcludeSubscribers)
            {
                var subscribers = GetSubscribers(UserType.College);
                query = query.Where(x => !subscribers.Contains(x.Id));
            }

            return query.ToList().Select(c => ModelFactory.ConvertToUserSuggestionVM(c));
        }

        private IEnumerable<UserFinderViewModel> GetInsurances(UserFinderViewModel viewModel)
        {
            var type = EnumHelper<UserType>.GetDescription(UserType.Insurance);
            var query = this.context.Insurances.Where(c => 
            c.ProviderName.ToUpper().Contains(viewModel.SearchTerm.ToUpper()) ||
            c.PlanType.ToUpper().Contains(viewModel.SearchTerm.ToUpper()) ||
            c.PlanName.ToUpper().Contains(viewModel.SearchTerm.ToUpper())
            );

            if (viewModel.ExcludeSubscribers)
            {
                var subscribers = GetSubscribers(UserType.Insurance);
                query = query.Where(x => !subscribers.Contains(x.Id));
            }

            return query.ToList().Select(c => ModelFactory.ConvertToUserSuggestionVM(c));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private List<int> GetSubscribers(UserType type)
        {
            var userType = EnumHelper<UserType>.GetDescription(type);

            return this.context.Users.Where(u => u.UserType.ToUpper() == userType.ToUpper() && u.SubscriberId.HasValue)
                                     .Select(u => u.SubscriberId.Value).ToList();
        }
    }
}
