﻿using System.Linq;
using DP.Services.Data.Models;
using System.Threading.Tasks;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using System.Data;
using DP.Common.Utilities;
using DP.Common.Enums;
using DP.Services.Security;
using Microsoft.AspNet.Identity;
using System;

namespace DP.Services.Business
{
    public class UserProfileManager : BaseManager
    {
        private ClinicManager clinicMgr;
        private CollegeManager collegeMgr;
        private HospitalManager hospitalMgr;
        private IUserDao userDao;
        private SecurityManager securityMgr;
        private UserManager userManager;
        private InsuranceManager insuranceManager;
        public UserProfileManager(IUserDao userDao, 
            SecurityManager securityMgr, 
            HospitalManager hospitalMgr, 
            ClinicManager clinicMgr, 
            CollegeManager collegeMgr, 
            InsuranceManager insuranceManager,
            UserManager userManager)
        {
            this.userDao = userDao;
            this.hospitalMgr = hospitalMgr;
            this.clinicMgr = clinicMgr;
            this.collegeMgr = collegeMgr;
            this.securityMgr = securityMgr;
            this.userManager = userManager;
            this.insuranceManager = insuranceManager;
        }

       
        public async Task<object> Update(SubscriberViewModel viewModel)
        {
            UserType type = (UserType)(viewModel.SubscriberType);
            var user = await userDao.GetByIdAsync(viewModel.UserId);
            viewModel.SubscriberId = user.SubscriberId;
            string[] propertiesToUpdate = new string[] {
                "Name",
                "Phone_No1",
                "Phone_No2",
                "Phone_No3",
                "Phone_No4",
                "Display_Phone",
                "FullAddress",
                "Website",
                "VideoUrl"};

            switch (type)
            {
                case UserType.Clinic:
                case UserType.Doctor:

                    await this.clinicMgr.UpdateAsync(new ClinicAndDoctor() {
                        Id = viewModel.SubscriberId.Value,
                        Name = viewModel.Name,
                        Phone_No1 =  viewModel.Phone_1,
                        Phone_No2 = viewModel.Phone_2,
                        Phone_No3 = viewModel.Phone_3,
                        Phone_No4 =viewModel.Phone_4,
                        Display_Phone = viewModel.DisplayPhone,
                        FullAddress = viewModel.FullAddress,
                        Website = viewModel.Website,
                        VideoUrl = viewModel.VideoUrl
                    }, propertiesToUpdate);
                    break;
                case UserType.Hospital:
                    //var h = this.hospitalMgr.GetByIdAsync(viewModel.SubscriberId.Value);
                    await this.hospitalMgr.UpdateAsync(new Hospital()
                    {
                        Id = viewModel.SubscriberId.Value,
                        Name = viewModel.Name,
                        Phone_No1 = viewModel.Phone_1,
                        Phone_No2 = viewModel.Phone_2,
                        Phone_No3 = viewModel.Phone_3,
                        Phone_No4 = viewModel.Phone_4,
                        Display_Phone = viewModel.DisplayPhone,
                        FullAddress = viewModel.FullAddress,
                        Website = viewModel.Website,
                        VideoUrl = viewModel.VideoUrl
                    }, propertiesToUpdate);
                    break;
                case UserType.College:
                    await this.collegeMgr.UpdateAsync(new College()
                    {
                        Id = viewModel.SubscriberId.Value,
                        Name = viewModel.Name,
                        Phone_No1 = viewModel.Phone_1,
                        Phone_No2 = viewModel.Phone_2,
                        Phone_No3 = viewModel.Phone_3,
                        Phone_No4 = viewModel.Phone_4,
                        Display_Phone = viewModel.DisplayPhone,
                        FullAddress = viewModel.FullAddress,
                        Website = viewModel.Website,
                        VideoUrl = viewModel.VideoUrl
                    }, propertiesToUpdate);
                    break;
                case UserType.Insurance:
                    var insurance = await this.insuranceManager.GetByIdAsync(viewModel.SubscriberId.Value);
                    insurance.ProviderName = viewModel.Name;
                    insurance.Phone_No1 = viewModel.Phone_1;
                    insurance.Phone_No2 = viewModel.Phone_2;
                    insurance.Phone_No3 = viewModel.Phone_3;
                    insurance.Phone_No4 = viewModel.Phone_4;
                    insurance.VideoUrl = viewModel.VideoUrl;
                    insurance.Website = viewModel.Website;
                    insurance.Display_Phone = viewModel.DisplayPhone;
                    await this.insuranceManager.UpdateAsync(insurance);
                    break;
                case UserType.User:
                       await securityMgr.UpdateUserAsync(new ApplicationUser() { Id = viewModel.UserId,
                        DisplayName = viewModel.DisplayName,
                        PhoneNumber =  viewModel.Phone_1}, new string[] { "PhoneNumber", "DisplayName" });
                    return true;//await this.userManager.GetSubscriberAsync(viewModel.SubscriberId.Value, type);
            }

            var result = await securityMgr.UpdateUserAsync(new ApplicationUser() {
                Id = viewModel.UserId,
                DisplayName = viewModel.DisplayName,
                PhoneNumber = viewModel.Phone_1 }, new string[] { "DisplayName", "PhoneNumber" });
            if (result.Succeeded)
            {
                return await this.userManager.GetSubscriberAsync(viewModel.SubscriberId.Value, type);
            }

            throw new Exception(string.Join(",", result.Errors));
        }


        public async Task<UserRatingAddOrUpdateViewModel> GetReviews(string userId)
        {
            ApplicationUser user = await userDao.GetByIdAsync(userId);
            var clinicRatings = await clinicMgr.GetRatingAsync(userId);
            var hospitalRatings = await hospitalMgr.GetRatingAsync(userId);
            var collegeRatings = await collegeMgr.GetRatingAsync(userId);
            var insuranceRatings = await insuranceManager.GetRatingAsync(userId);

            //    return new UserRatingAddOrUpdateViewModel()
            //    {
            //        ClinicRatings = clinicRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
            //        HospitalRatings = hospitalRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
            //        CollegeRatings = collegeRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
            //        InsuranceRatings = insuranceRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
            //        UserId = user.Id,
            //        UserName = user.UserName
            //    };
            //}

            
            UserRatingAddOrUpdateViewModel obj= new UserRatingAddOrUpdateViewModel()
            {
                ClinicRatings = clinicRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
                HospitalRatings = hospitalRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
                CollegeRatings = collegeRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
                InsuranceRatings = insuranceRatings.Select(r => ModelFactory.ConvertToRatingViewModel(r)).ToList(),
                UserId = user.Id,
                UserName = user.UserName
            };
            return obj;
        }
        
        public async Task UpdateSummary(int entityId, UserType userType, string summary)
        {
            switch (userType)
            {
                case UserType.Clinic:
                case UserType.Doctor:
                    await this.clinicMgr.UpdateAsync(new ClinicAndDoctor() { Id= entityId, Summary = summary } , new string[] { "Summary" });
                    break;
                case UserType.Hospital:
                    await this.hospitalMgr.UpdateAsync(new Hospital() { Id = entityId, Summary = summary }, new string[] { "Summary" });
                    break;
                case UserType.College:
                    await this.collegeMgr.UpdateAsync(new College() { Id = entityId, Summary = summary }, new string[] { "Summary" });
                    break;
                case UserType.Insurance:
                    await this.insuranceManager.UpdateSummary(new Insurance() { Id = entityId, Summary = summary });
                    break;
            }
        }

        public async Task UpdatePrimaryPhone(int entityId, UserType userType, string phoneNumber)
        {
            switch (userType)
            {
                case UserType.Clinic:
                case UserType.Doctor:
                    await this.clinicMgr.UpdateAsync(new ClinicAndDoctor() { Id = entityId, Phone_No1 = phoneNumber }, new string[] { "Phone_No1" });
                    break;
                case UserType.Hospital:
                    await this.hospitalMgr.UpdateAsync(new Hospital() { Id = entityId, Phone_No1 = phoneNumber }, new string[] { "Phone_No1" });
                    break;
                case UserType.College:
                    await this.collegeMgr.UpdateAsync(new College() { Id = entityId, Phone_No1 = phoneNumber }, new string[] { "Phone_No1" });
                    break;
                case UserType.Insurance:
                    await this.insuranceManager.UpdateAsync(new Insurance() { Id = entityId, Phone_No1 = phoneNumber }, new string[] { "Phone_No1" });
                    break;
            }
        }
    }
}
