﻿using System;
using System.Collections.Generic;
using System.Linq;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Data;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using DP.Common.Constants;

namespace DP.Services.Business
{
    public abstract class BaseProfileManager : BaseManager
    {
        protected DPContext context;
        protected HospitalManager hospitalMgr;
        protected ClinicManager clinicMgr;
        protected CollegeManager collegeMgr;
        protected UserManager userMgr;
        protected FeatureManager featureMgr;
        protected QualificationManager qualificationMgr;
        protected RecommendManager recommendMgr;
        protected ClinicRatingManager clinicRatingManager;
        protected HospitalRatingManager hospitalRatingManager;
        protected CollegeRatingManager collegeRatingManager;
    }
}
