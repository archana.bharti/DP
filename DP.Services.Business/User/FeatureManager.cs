﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System;


namespace DP.Services.Business
{
    public class FeatureManager : BaseManager
    {
        private IFeatureDao featureDao;

        public FeatureManager(IFeatureDao featureDao)
        {
            this.featureDao = featureDao;
        }

        public async Task<Feature> CreateAsync(Feature feature)
        {
            return await this.featureDao.CreateAsync(feature);
        }

        public async Task<Feature> Get(Feature feature)
        {
            return await featureDao.GetAsync(feature);
        }

        public async Task<Feature> Get(int id)
        {
            return await featureDao.GetAsync(id);
        }

        public async Task<Feature> Update(Feature feature)
        {
            return await featureDao.UpdateAsync(feature);
        }

        public bool IsFeaturedUser(int id, int type)
        {
            return featureDao.IsFeaturedUser(id, type);
        }
    }
}