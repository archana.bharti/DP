﻿using DP.Common;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class QualificationManager
    {
        private IQualificationDao qualificationDao;
        public QualificationManager(IQualificationDao qualificationDao)
        {
            this.qualificationDao = qualificationDao;
        }
        
        public async Task<List<QualificationLong>> GetLongQualificationsByDoctorId(int doctorId)
        {
            return await this.qualificationDao.GetLongQualificationsByDoctorId(doctorId);
        }


        public async Task<List<QualificationShort>> GetShortQualificationsByDoctorId(int doctorId)
        {
            return await this.qualificationDao.GetShortQualificationsByDoctorId(doctorId);
        }


        public async Task<List<QualificationLong>> GetAllLongQualifications()
        {
            return await this.qualificationDao.GetAllLongQualifications();
        }

        public async Task<List<QualificationShort>> GetAllShortQualifications()
        {
            return await this.qualificationDao.GetAllShortQualifications();
        }

        public async Task<QualificationLong> AddAsync(QualificationLong qualif)
        {
            return await this.qualificationDao.AddLongQualificationAsync(qualif);
        }

        public async Task<QualificationShort> AddAsync(QualificationShort qualif)
        {
            return await this.qualificationDao.AddShortQualificationAsync(qualif);
        }

        public async Task DeleteAsync(QualificationLong qualif)
        {
            await this.qualificationDao.DeleteLongQualificationAsync(qualif);
        }

        public async Task DeleteAsync(QualificationShort qualif)
        {
           await this.qualificationDao.DeleteShortQualificationAsync(qualif);
        }

        public async Task AddAsync(QualificationViewModel vm)
        {
            await this.qualificationDao.AddOrUpdate( vm.DoctorQualification);
        }

        public async Task RemoveAsync(QualificationViewModel vm)
        {
            await this.qualificationDao.Remove(vm.DoctorQualification);
        }
    }
}
