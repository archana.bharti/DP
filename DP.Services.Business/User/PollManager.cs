﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public sealed class PollManager
    {
        public IPollDao pollDao;
        public ICommonDao commonDao;
        public PollManager(IPollDao pollDao, ICommonDao commonDao)
        {
            this.pollDao = pollDao;
            this.commonDao = commonDao;
        }

        public async Task<List<PollMasterView>> GetAllPollMaster(bool isActive)
        {
            var query = @"  select pm.*, 
                              (select count(p.Yes) as TotalYesCount
                              from dp_poll p  where p.PollMasterId = pm.Id ) as TotalYesCount,
                              (select count(p.Yes) as YesCount
                              from dp_poll p  where p.PollMasterId = pm.Id and p.yes =1) as YesCount,
                               (select count(p.no) as TotalNoCount
                              from dp_poll p  where p.PollMasterId = pm.Id ) as TotalNoCount,
                              (select count(p.no) as NoCount
                              from dp_poll p  where p.PollMasterId = pm.Id and p.no =1) as NoCount
                            from dp_poll_master pm ";
            if (isActive)
            {
                query = query + " where pm.active = 1";
            }
            var data  = await commonDao.ExecuteQueryAsync<PollMasterView>(query);
            return data;
        }

        public async Task Add(Poll poll)
        {
            var exitingPoll = await this.pollDao.GetPollsByPollMasterIdAndIpAsync(poll.PollMasterId, poll.IpAddress);
            if(exitingPoll == null)
            {
                await this.pollDao.AddPoll(poll);
            }
        }
    }
}
