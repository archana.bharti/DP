﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using DP.Services.Security;
using DP.Common.Constants;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using DP.Common.Models;
using DP.Common.Enums;
using System.Security.Policy;
using DP.Common.Utilities;
using System.Configuration;
using DP.Common;
using Microsoft.AspNet.Identity;

namespace DP.Services.Business
{
    public class UserManager : BaseManager
    {
        private SecurityManager securityMgr;
        private IUserDao userCtx;
        private FeatureManager featureManager;
        private ClinicManager clinicManager;
        private CollegeManager collegeManger;
        private HospitalManager hospitalManager;
        private QualificationManager qualifManager;
        private SpecializationManager spzManager;
        private RecommendManager recomManager;
        private ClinicRatingManager clinicRatingManager;
        private HospitalRatingManager hospitalRatingManager;
        private CollegeRatingManager collegeRatingManager;
        private InsuranceManager insuranceManager;

        public UserManager(ICommonDao commonDao, IUserDao userDao, 
            SecurityManager securityMgr, 
            FeatureManager featureManager, 
            ClinicManager clinicManager,
            CollegeManager collegeManager,
            HospitalManager hospitalManager,
            QualificationManager qualifManager,
            SpecializationManager spzManager,
            RecommendManager recomManager,
            ClinicRatingManager clinicRatingManager,
            HospitalRatingManager hospitalRatingManager,
            CollegeRatingManager collegeRatingManager,
            InsuranceManager insuranceManager)
        {
            this.securityMgr = securityMgr;
            this.userCtx = userDao;
            this.featureManager = featureManager;
            this.clinicManager = clinicManager;
            this.collegeManger = collegeManager;
            this.hospitalManager = hospitalManager;
            this.qualifManager = qualifManager;
            this.clinicRatingManager = clinicRatingManager;
            this.hospitalRatingManager = hospitalRatingManager;
            this.collegeRatingManager = collegeRatingManager;
            this.recomManager = recomManager;
            this.spzManager = spzManager;
            this.insuranceManager = insuranceManager;
        }

        public async Task<List<ApplicationUser>> GetExternalUsers(int skip, int take)
        {
            return await this.userCtx.Get().Where(x => x.SubscriberId == null && x.UserType.ToUpper() == "USER")
                .OrderByDescending(x => x.CreatedOn)
                .Skip(skip).Take(take)
               .Include(x => x.City)
               .Include(x => x.Location)
               .Include(x => x.State)
               .ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationUser"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<ApplicationUser> CreateAsync(ApplicationUser newUser, string password)
        {
            ApplicationUser user = null;
                user = await this.userCtx.GetByEmailAsync(newUser.Email);
                if (user != null)
                {
                    throw new Exception("A registration with this email already exists. Please choose a different email");
                }

                // a check if the entity user is trying to register with a different email id
                var externalUserType = EnumHelper<UserType>.GetDescription(UserType.User);
                if (externalUserType.ToUpper() != newUser.UserType)
                {
                    user = await GetBySubscriptionAsync(newUser);
                    if (user != null)
                        throw new Exception("This user is already under subscription. Please choose a different user");
                }

                if (externalUserType.ToUpper() != newUser.UserType)
                {
                    newUser.EmailConfirmed = true;
                }

                user = await this.securityMgr.CreateUserAsync(newUser, password);
                user = await GetById(user.Id, false);

            //send activation link email only to external users
            if (externalUserType.ToUpper() == newUser.UserType)
            {
                await SendActivationLink(user, await this.securityMgr.GenerateEmailConfirmationTokenAsync(user.Id));
            }
            //send under approval process email to entity users
            else
            {
                //update entity phone number
                await UpdatePrimaryPhone(user.SubscriberId.Value, EnumHelper<UserType>.GetValueFromDescription(user.UserType), user.PhoneNumber);
                var template = EmailManager.GetTemplate(EmailTemplates.Template_EmailVerified_L2Verification);
                EmailManager.ReplaceTemplateParameters(ref template, new string[] { user.Email });
                await Task.Run(()=>EmailManager.SendVerificationPending(user.Email, user.DisplayName));
            }

            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<ApplicationUser> GetByEmail(string email)
        {
            ApplicationUser user = await this.securityMgr.GetUserByEmailAsync(email);
            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="useIdentityContext"></param>
        /// <returns></returns>
        public async Task<ApplicationUser> GetById(string id, bool useIdentityContext = true)
        {
            ApplicationUser user = null;
            if (useIdentityContext)
                user = await this.securityMgr.GetUserAsync(id);
            else
                user = await this.userCtx.GetByIdAsync(id);
            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<ApplicationUser>> GetAllAsync()
        {
            return await this.userCtx.GetAllAsync();
        }

        /// <summary>
        /// Activate by code
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Tuple<ApplicationUser, string>> Activate(string code)
        {
            var data = SecurityHelper.Decrypt(code).Split(':');
            var token = data[1];
            var userId = data[0];          
            string template = string.Empty;

            ApplicationUser user = await this.userCtx.GetByIdAsync(userId);

            if (user == null)
            {
                throw new Exception("Invalid user id");
            }

            IdentityResult result = await this.securityMgr.ConfirmEmailAsync(user.Id, token);
            UserType userType;
            if (result.Succeeded)
            {
                userType = EnumHelper<UserType>.GetValueFromDescription(user.UserType);
                if (userType == UserType.User)
                {
                    user.IsVerified = true;
                    await this.userCtx.UpdateAsync(user, new string[] { "IsVerified" });
                }
            }
            else
            {
                throw new Exception("Invalid user id");
            }

            if (userType == UserType.User)
            {
                template = EmailManager.GetTemplate(EmailTemplates.Template_ActivationSuccess);
                EmailManager.ReplaceTemplateParameters(ref template, GlobalSettings.APP_URL);
                await SMSManager.Instance.SendActivationSuccessAsync(user.PhoneNumber, user.DisplayName);
            }
            return new Tuple<ApplicationUser,string>(user, template);
        }

        //public async Task Restart(int subscriberId, int type)
        //{
        //    ApplicationUser appUser = await this.userCtx.GetBySubscriptionAsync(subscriberId, EnumHelper<UserType>.GetDescription((UserType)type));
        //    appUser.IsTerminated = true;
        //    await this.userCtx.UpdateAsync(appUser, new string[] { "IsTerminated" });

        //    Feature feature = await this.featureManager.Get(new Feature() { EntityId = subscriberId, EntityType = type });
        //    if (feature != null)
        //    {
        //        feature.StatusValue = (int)UserStatus.Active;
        //        await this.featureManager.Update(feature);
        //    }
        //}

        public async Task Restart(string userId)
        {
            var appUser = await this.userCtx.GetByIdAsync(userId);
            appUser.IsTerminated = false;
            await this.userCtx.UpdateAsync(appUser, new string[] { "IsTerminated" });

            //if (appUser.SubscriberId.HasValue)
            //{
            //    Feature feature = await this.featureManager.Get(new Feature() { EntityId = appUser.SubscriberId.Value, EntityType = (int)EnumHelper<UserType>.GetValueFromDescription(appUser.UserType) });
            //    if (feature != null)
            //    {
            //        feature.StatusValue = (int)UserStatus.Active;
            //        await this.featureManager.Update(feature);
            //    }
            //}
        }

        public async Task Deactivate(int subscriberId, int type)
        {
            ApplicationUser appUser = await this.userCtx.GetBySubscriptionAsync(subscriberId, EnumHelper<UserType>.GetDescription((UserType)type));
            appUser.IsVerified = false;
            await this.userCtx.UpdateAsync(appUser, new string[] { "IsVerified" });

            Feature feature = await this.featureManager.Get(new Feature() { EntityId = subscriberId, EntityType = type });
            if (feature != null)
            {
                feature.StatusValue = (int)UserStatus.Deactive;
                await this.featureManager.Update(feature);
            }
        }

        public async Task Terminate(string userId)
        {
            var appUser = await this.userCtx.GetByIdAsync(userId);
            appUser.IsTerminated = true;
            await this.userCtx.UpdateAsync(appUser, new string[] { "IsTerminated" });

            if (appUser.SubscriberId.HasValue)
            {
                Feature feature = await this.featureManager.Get(new Feature() { EntityId = appUser.SubscriberId.Value, EntityType = (int)EnumHelper<UserType>.GetValueFromDescription(appUser.UserType) });
                if (feature != null)
                {
                    feature.StatusValue = (int)UserStatus.Deactive;
                    feature.EnableBasic = false;
                    feature.EnableFeatured = false;
                    feature.EnableTextual = false;
                    await this.featureManager.Update(feature);
                }
            }
        }

        public async Task<bool> ChangePassword(string userId, string password)
        {
            ApplicationUser user = await this.userCtx.GetByIdAsync(userId);
            if (user == null)
            {
                throw new Exception("User not found");
            }
            user.PasswordHash = this.securityMgr._userManager.PasswordHasher.HashPassword(password);
            var result = await this.securityMgr._userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                throw new Exception("Unable to process change in password. Please try after some time.");
            }
            return true;
        }

        //public async Task<bool> ResetPassword(string email)
        //{
        //    ApplicationUser user = await this.userCtx.GetByEmailAsync(email);
        //    if (user == null)
        //    {
        //        throw new Exception("User not found");
        //    }
        //    user.PasswordHash = this.securityMgr._userManager.PasswordHasher.HashPassword(password);
        //    var result = await this.securityMgr._userManager.UpdateAsync(user);
        //    if (!result.Succeeded)
        //    {
        //        throw new Exception("Unable to process change in password. Please try after some time.");
        //    }
        //    return true;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsUserActivated(string id)
        {
            return userCtx.IsUserActivated(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenKey"></param>
        public void SetUserToken(string tokenKey)
        {
            this.securityMgr.SetUserToken(tokenKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        private async Task SendActivationLink(ApplicationUser user, string code)
        {
            var queryString = string.Format("?code={0}", SecurityHelper.Encrypt((user.Id + ":" + code).ToString()));
            string activationLink = GlobalSettings.APP_URL + "/api/auth/Activate" + queryString;
            string appUrl = GlobalSettings.APP_URL;
            await Task.Run(()=>EmailManager.SendActivationLink(user.Email, activationLink, user.DisplayName, appUrl));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<ApplicationUser> GetBySubscriptionAsync(ApplicationUser user)
        {
            return await this.userCtx.GetBySubscriptionAsync(user.SubscriberId.Value, user.UserType);
        }

        public async Task<SubscriberViewModel> GetSubscriberAsync(int id, UserType userType, bool featured = false)
        {
            SubscriberViewModel vm = null;
            object entity = null;
            int reviewsCount = 0;
            object topRated = null;
            List<QualificationShort> shortQualifs = null;
            List<QualificationLong> longQualifs = null;
            List<SpecializationViewModel> spzs = null;
            CacheManager cacheManager = new CacheManager();
            Dictionary<string, object> fromCache = cacheManager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object>;
            var rootPath = Convert.ToString(fromCache[GlobalSettings.S3_ROOT_PATH]);
            switch (userType)
            {
                case UserType.Clinic:
                case UserType.Doctor:
                    entity = await this.clinicManager.GetByIdAsync(id);
                    ClinicAndDoctor cd = entity as ClinicAndDoctor;
                    longQualifs = await this.qualifManager.GetLongQualificationsByDoctorId(cd.Id);
                    shortQualifs = await this.qualifManager.GetShortQualificationsByDoctorId(cd.Id);
                    spzs = await this.spzManager.Get(cd.Id, EnumHelper<UserType>.GetValueFromDescription(cd.Type));
                    reviewsCount = await this.clinicRatingManager.GetRatingsCount(cd.Id);
                    topRated = await this.clinicRatingManager.GetTopRated(cd.Id);
                    break;
                case UserType.Hospital:
                    entity = await this.hospitalManager.GetByIdAsync(id);
                    reviewsCount = await this.hospitalRatingManager.GetRatingsCount(id);
                    topRated = await this.hospitalRatingManager.GetTopRated(id);
                    break;
                case UserType.College:
                    entity = await this.collegeManger.GetByIdAsync(id);
                    reviewsCount = await this.collegeRatingManager.GetRatingsCount(id);
                    topRated = await this.collegeRatingManager.GetTopRated(id);
                    //if((entity as College).Location == null)
                    {
                        var clg = entity as College;
                        var clgLoc = await this.collegeManger.GetCollegeLocation(clg.Id);
                        clg.Location = new Location();
                        clg.State = new State();
                        clg.State = clgLoc.State;
                        clg.Location.Name = "";
                        clg.Location.City = new City();
                        clg.Location.City.Name = clgLoc.Name;
                        clg.Location.City.Id = clgLoc.Id;
                        clg.Location.CityId = clgLoc.Id;
                    }
                    break;
                case UserType.Insurance:
                    Insurance insurance = await this.insuranceManager.GetByIdAsync(id);
                    vm = new SubscriberViewModel();
                    vm.SubscriberId = insurance.Id;
                    vm.SubscriberType =(int)UserType.Insurance;
                    vm.Summary = insurance.Summary;

                    ApplicationUser appUser = await this.GetBySubscriptionAsync(new ApplicationUser() { SubscriberId = vm.SubscriberId.Value, UserType = vm.SubscriberTypeText });
                    Feature inFeatured = await this.featureManager.Get(new Feature() { EntityId = vm.SubscriberId.Value, EntityType = vm.SubscriberType });
                    vm.ReviewsCount = insurance.RatingCount;
                    vm.TopRated = await this.insuranceManager.GetTopRated(id);
                    if (appUser != null)
                    {
                        vm.UserId = appUser.Id;
                        vm.DisplayName = appUser.DisplayName;
                        vm.Email = appUser.Email;
                        vm.IsFeatured = true;
                        vm.IsTerminated = appUser.IsTerminated;
                    }
                    vm.DisplayPhone = insurance.Display_Phone;
                    vm.ImageUrl = insurance.ImageUrl;
                    vm.Name = insurance.ProviderName;
                    vm.Phone_1 = insurance.Phone_No1;
                    vm.Phone_2 = insurance.Phone_No2;
                    vm.Phone_3 = insurance.Phone_No3;
                    vm.Phone_4 = insurance.Phone_No4;
                    vm.Website = insurance.Website;
                    vm.VideoUrl = insurance.VideoUrl;
                    vm.RecommendationsCount = await this.recomManager.Count(Convert.ToInt32(vm.SubscriberId), (int)userType);
                    vm.ImageUrl = (string.IsNullOrEmpty(vm.ImageUrl)) ? rootPath + vm.SubscriberTypeText.ToLower() + "-default-300x300.png" : vm.ImageUrl;
                    vm.ImageUrl2 = (string.IsNullOrEmpty(vm.ImageUrl2)) ? rootPath + vm.SubscriberTypeText.ToLower() + "-default-300x300.png" : vm.ImageUrl2;
                    vm.ImageUrl3 = (string.IsNullOrEmpty(vm.ImageUrl3)) ? rootPath + vm.SubscriberTypeText.ToLower() + "-default-300x300.png" : vm.ImageUrl3;
                    if (inFeatured != null)
                    {
                        vm.IsFeatured = true;
                        vm.Plan = new SubscriberPlanViewModel();
                        vm.Plan.EnableBasic = inFeatured.EnableBasic;
                        vm.Plan.EnableTextual = inFeatured.EnableTextual;
                        vm.Plan.EnableFeatured = inFeatured.EnableFeatured;
                        vm.Plan.EndDate = inFeatured.EndDate;
                        vm.Plan.StartDate = inFeatured.StartDate;
                        vm.Plan.Id = inFeatured.Id;
                        vm.IsFeatured = true;
                        vm.Plan.IsActive = (inFeatured.Status.Id == (int)UserStatus.Active);
                    }
                    return vm;
            }

            SearchEntityBase dbModel = entity as SearchEntityBase;
            vm = ModelFactory.ConvertToSubscriberViewModel(dbModel);
            vm.LongQualifications = longQualifs;
            vm.ShortQualifications = shortQualifs;
            vm.ReviewsCount = reviewsCount;
            vm.TopRated = topRated;
           
            vm.ImageUrl = (string.IsNullOrEmpty(vm.ImageUrl)) ? rootPath + vm.SubscriberTypeText.ToLower() + "-default-300x300.png" : vm.ImageUrl;
            vm.ImageUrl2 = (string.IsNullOrEmpty(vm.ImageUrl2)) ? rootPath + vm.SubscriberTypeText.ToLower() + "-default-300x300.png" : vm.ImageUrl2;
            vm.ImageUrl3 = (string.IsNullOrEmpty(vm.ImageUrl3)) ? rootPath + vm.SubscriberTypeText.ToLower() + "-default-300x300.png" : vm.ImageUrl3;

            Feature feature = await this.featureManager.Get(new Feature() { EntityId = vm.SubscriberId.Value, EntityType = vm.SubscriberType });
            if (feature != null)
            {
                vm.IsFeatured = true;
                ApplicationUser appUser = await this.GetBySubscriptionAsync(new ApplicationUser() { SubscriberId = vm.SubscriberId.Value, UserType = vm.SubscriberTypeText });
                vm = ModelFactory.ExtendSubscriberViewModel(vm, appUser, feature);
                if (userType != UserType.User && vm.IsFeatured)
                    vm.RecommendationsCount = await this.recomManager.Count(Convert.ToInt32(vm.SubscriberId), (int)userType);
            }
            return vm;
        }

        public async Task<SubscriberViewModel> GetSubscriberAsync(string email)
        {
            ApplicationUser user = await this.GetByEmail(email);
            SubscriberViewModel vm = ModelFactory.ConvertToSubscriberViewModel(user);
            vm.SubscriberType = (int)EnumHelper<UserType>.GetValueFromDescription(user.UserType);
            if(vm.SubscriberType == (int)UserType.User)
            {
                vm.DisplayPhone = "1";
            }
            return vm;
        }

        private async Task<bool> IsEmailConfirmedAsync(string userId)
        {
            return await securityMgr.IsEmailConfirmedAsync(userId);
        }
        

        public async Task<bool> ForgotPassword(string userEmail)
        {
            //if (user == null || !(await IsEmailConfirmedAsync(user.Id)))
            //{
            //    throw new Exception("UserId/Email does not exist.");
            //}


            //var queryString = string.Format("?code={0}", SecurityHelper.Encrypt((user.Id + ":" + passwordResetToken).ToString()));
            //string resetLink = GlobalSettings.APP_URL + "/api/auth/ResetPassword" + queryString;
            //EmailManager.SendResetPasswordLink(user.Email, resetLink, user.DisplayName, GlobalSettings.APP_URL);

            //return true;

            var user = await GetByEmail(userEmail);
            if (user == null || !(await IsEmailConfirmedAsync(user.Id)))
            {
                throw new Exception("UserId/Email does not exist.");
            }
            var passwordResetToken = await securityMgr.GeneratePasswordResetTokenAsync(user.Id);
            var randomPassword = RandomTokenGenerator.Generate(8, 12);
            var result = await securityMgr.ResetPassword(user.Id, passwordResetToken, randomPassword);
            var appUrl = GlobalSettings.APP_URL;
            if (result.Succeeded)
            {
                await Task.Run(() => { EmailManager.SendForgotPassword(user.Email, user.DisplayName, randomPassword, appUrl); });
                return true;
            }
            throw new Exception("Unable to process your request. Kindly try after some time.");
        }

        public async Task<bool> ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                var user = await GetByEmail(model.UserName);
                if (user == null || !(await IsEmailConfirmedAsync(user.Id)))
                {
                    throw new Exception("Invalid user details");
                }
                return await securityMgr.ChangePassword(user.Id, model.OldPassword, model.NewPassword);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateImageUrl(int subscriberId, int type, string url, string pictureNo)
        {
            ApplicationUser appUser = await this.userCtx.GetBySubscriptionAsync(subscriberId, EnumHelper<UserType>.GetDescription((UserType)type));
            appUser.PictureUrl = url;
            await this.userCtx.UpdateAsync(appUser, new string[] { "PictureUrl" });

            var propertyPic = "Profile_Pic_" + pictureNo;

            switch ((UserType)type) {
                case UserType.Clinic:
                case UserType.Doctor:
                    var doc = await this.clinicManager.GetByIdAsync(subscriberId);
                    if(pictureNo == "1") { doc.Profile_Pic_1 = url; }
                    if (pictureNo == "2") { doc.Profile_Pic_2 = url; }
                    if (pictureNo == "3") { doc.Profile_Pic_3 = url; }
                    await this.clinicManager.UpdateAsync(doc, new string[] { propertyPic });
                    break;
                case UserType.Hospital:
                    var hos = await this.hospitalManager.GetByIdAsync(subscriberId);
                    if (pictureNo == "1") { hos.Profile_Pic_1 = url; }
                    if (pictureNo == "2") { hos.Profile_Pic_2 = url; }
                    if (pictureNo == "3") { hos.Profile_Pic_3 = url; }
                    await this.hospitalManager.UpdateAsync(hos, new string[] { propertyPic });
                    break;
                case UserType.College:
                    var col = await this.collegeManger.GetByIdAsync(subscriberId);
                    if (pictureNo == "1") { col.Profile_Pic_1 = url; }
                    if (pictureNo == "2") { col.Profile_Pic_2 = url; }
                    if (pictureNo == "3") { col.Profile_Pic_3 = url; }
                    await this.collegeManger.UpdateAsync(col, new string[] { propertyPic });
                    break;
                case UserType.Insurance:
                    var ins = await this.insuranceManager.GetByIdAsync(subscriberId);
                    ins.ImageUrl = url;
                    await this.insuranceManager.UpdateAsync(ins, new string[] { "ImageUrl" });
                    break;
            }
        }

        public async Task UpdatePrimaryPhone(int entityId, UserType userType, string phoneNumber)
        {
            switch (userType)
            {
                case UserType.Clinic:
                case UserType.Doctor:
                    await this.clinicManager.UpdateAsync(new ClinicAndDoctor() { Id = entityId, Phone_No1 = phoneNumber, Display_Phone = "1" }, new string[] { "Phone_No1", "Display_Phone" });
                    break;
                case UserType.Hospital:
                    await this.hospitalManager.UpdateAsync(new Hospital() { Id = entityId, Phone_No1 = phoneNumber, Display_Phone = "1" }, new string[] { "Phone_No1", "Display_Phone" });
                    break;
                case UserType.College:
                    await this.collegeManger.UpdateAsync(new College() { Id = entityId, Phone_No1 = phoneNumber, Display_Phone = "1" }, new string[] { "Phone_No1", "Display_Phone" });
                    break;
                case UserType.Insurance:
                    await this.insuranceManager.UpdateAsync(new Insurance() { Id = entityId, Phone_No1 = phoneNumber, Display_Phone = "1" }, new string[] { "Phone_No1", "Display_Phone" });
                    break;
            }
        }
    }
}