﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Common.Constants;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;

namespace DP.Services.Business
{
    public sealed class CollegeSearchManager : BaseSearchManager
    {
        private ICommonDao commonDao;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commonDao"></param>
        /// <param name="locationDao"></param>
        public CollegeSearchManager(ICommonDao commonDao, ILocationDao locationDao) : base(commonDao, locationDao)
        {
            this.commonDao = commonDao;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        protected override List<SearchResultViewModel> GetSearchResults(SearchRequestViewModel viewModel, out int total)
        {
            List<SearchResultViewModel> searchResults = null;
            var sorts = string.Join(",", new string[] {
                " ReviewCount " + ((viewModel.ByReview == 1) ? " desc " : " asc "),
                " RecommendedCount " + ((viewModel.ByRecommended == 1) ? " desc " : " asc ")
            });

            var countQuery = CountQuery;
            var baseQuery = GetCollegeQuery;

            //byFeatured
            baseQuery = (viewModel.ByFeatured) ?
                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedInQuery(UserType.College)) :
                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedNotInQuery(UserType.College));

            //ipaddress
            baseQuery = baseQuery.Replace(IP, "'" + viewModel.Ip + "'");

            //department

            baseQuery = (viewModel.DepartmentId.HasValue) ?
                        baseQuery.Replace("[DEPARTMENT_CONDITION]", " c.DepartmentType = [DEPARTMENTTYPE] and ")
                                 .Replace(DEPARTMENTTYPE, ((int)viewModel.DepartmentId).ToString()) :
                        baseQuery.Replace("[DEPARTMENT_CONDITION]", "");
            //baseQuery.Replace(DEPARTMENTTYPE, ((int)viewModel.DepartmentId).ToString());

            //sorts
            baseQuery = baseQuery.Replace(SORTS, sorts);

            //conditions

            //collegeId
            if (viewModel.CollegeId.HasValue)
                conditions.Add(" c.Id =" + ((int)viewModel.CollegeId).ToString());

            //cityId condition as locationId
            if (viewModel.CityId.HasValue)
                conditions.Add(" c.locationId =" + viewModel.CityId.Value.ToString());

            if (viewModel.StateId.HasValue)
                conditions.Add(" c.stateId =" + viewModel.StateId.Value.ToString());

            baseQuery = conditions.Count > 0 ?
                baseQuery.Replace(CONDITIONS, "and " + string.Join(" and", conditions.ToArray())) :
                baseQuery.Replace(CONDITIONS, "");

            total = commonDao.ExecuteQuery(string.Format(countQuery, baseQuery).Replace(TAKE_SKIP, ""));

            if (total > 0)
            {
                baseQuery = (viewModel.ByFeatured == true) ?
                            baseQuery.Replace(TAKE_SKIP, "") :
                            baseQuery.Replace(TAKE_SKIP, "limit " + viewModel.Take + " offset " + viewModel.Skip + "");
                searchResults = commonDao.ExecuteQuery<SearchResultViewModel>(baseQuery);
            }
            //else if ((searchResults == null || searchResults.Count == 0) && viewModel.ByFeatured && viewModel.Distance <= GetRadius(viewModel, Distance.Max))
            //{
            //    viewModel.Distance = viewModel.Distance + GetRadius(viewModel, Distance.Slide);
            //    searchResults = GetSearchResults(viewModel, out total);
            //}

            if (searchResults != null)
            {
                searchResults.ForEach(x =>
                {
                    x.IsFeatured = viewModel.ByFeatured;
                    x.Radius = viewModel.Distance;
                    x.DepartmentId = viewModel.DepartmentId;
                });
            }
            return searchResults;
        }
    }
}
