﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using DP.Common.Models;
//using DP.Common.Utilities;
//using DP.Services.Data;
//using DP.Services.Data.Models;
//using DP.Services.Data.UnitOfWork;
//using DP.Common.Constants;
//using DP.Common.Enums;

//namespace DP.Services.Business
//{
//    public abstract class BaseSearchManager : BaseManager
//    {
//        private DistanceConfig configDoctorOrClinic;
//        private DistanceConfig configHospital;
//        private ILocationDao locationDao;
//        private DPContext context;
//        protected List<string> conditions;
//        protected const string FEATURED_COMPARATOR = "[FEATURED_COMPARATOR]";
//        protected const string IP = "[IP]";
//        protected const string SORTS = "[SORTS]";
//        protected const string SPZ_CONDITION = "[SPZ_CONDITION]";
//        protected const string CONDITIONS = "[CONDITIONS]";
//        protected const string TAKE_SKIP = "[TAKE_SKIP]";
//        protected const string CATEGORY = "[CATEGORY]";
//        protected const string DEPARTMENTTYPE = "[DEPARTMENTTYPE]";

//        public BaseSearchManager()
//        {
            
//        }

//        public BaseSearchManager(ICommonDao commonDao, ILocationDao locationDao)
//        {
//            this.context = commonDao.GetDpContext();
//            this.locationDao = locationDao;
//            this.conditions = new List<string>();
//            configDoctorOrClinic = new DistanceConfig(ConfigSection.DoctorsOrClinic);
//            configHospital = new DistanceConfig(ConfigSection.Hospital);
//        }

//        public virtual SearchResponseViewModel Search(SearchRequestViewModel viewModel)
//        {
//            List<SearchResultViewModel> searchResults = new List<SearchResultViewModel>();

//            if(viewModel.Distance == 0)
//            {
//                if(viewModel.SearchType == SearchType.DoctorOrClinic)
//                {
//                    viewModel.Distance = GetDoctorOrClinicRadius(viewModel, Distance.Default);
//                }
//                else
//                {
//                    viewModel.Distance = GetHospitalRadius(viewModel, Distance.Default);

//                }
//            }

//            int totalCount = 0;
//            searchResults = GetSearchResults(viewModel, out totalCount);
//            SearchResponseViewModel response = new SearchResponseViewModel();
//            response.Rows = searchResults;
//            response.Total = totalCount;
//            return response;
//        }

//        protected abstract List<SearchResultViewModel> GetSearchResults(SearchRequestViewModel viewModel, out int total);

//        protected string GetDistancesQuery(params object[] args)
//        {
//            return string.Format(@"(select id from 
//                        (SELECT Id, ( 6371 * acos( cos( radians({0}) ) 
//                                           * cos( radians( temp1.Latitude ) ) 
//                                           * cos( radians( temp1.longitude ) 
//                                           - radians({1}) ) 
//                                           + sin( radians({0}) ) 
//                                           * sin( radians( temp1.Latitude ) ) ) ) 
//                      AS distance FROM dp_location temp1 HAVING distance <= {2} ) as t )", args[0], args[1], args[2]);
//        }

//        protected string GetClinicOrDoctorQuery
//        {
//           get { return @"select 
//                                UID,EntityId,RecommendedCount,ReviewCount, LongQualifications, ShortQualifications,
//                                (select case when count(ipaddress) > 0 then 1 else 0 end as isRecommended from dp_recommend where EntityId = p1.EntityId and ipaddress = [IP]) 
//                                as isRecommended, 
//                                'DoctorOrClinic' as `Type`,`Type` as SubType,`Name`, ImageUrl,
//                                LocationId,LocationName,Latitude,Longitude,CityName,StateName,
//                                SpecializationIds, SpecializationNames
//                                from  (
//
//		                                select distinct 
//				                                cd.Id as EntityId,
//                                                cd.Type,aspusers.Id as UID, 
//				                                cd.Name,				
//                                                cd.Profile_Pic_1 as ImageUrl,
//				                                loc.Latitude,
//                                                loc.Longitude,
//				                                loc.Id as LocationId,
//                                                loc.Name as LocationName,
//				                                city.Name as CityName,
//                                                state.Name as StateName,                
//                                                (select count(ClinicOrDoctorId) from dp_clinic_rating where ClinicOrDoctorId = cd.id) as ReviewCount,
//                                                (select count(EntityId) from dp_recommend recom where recom.EntityId = cd.id) as RecommendedCount,
//                                                qualif.LongQualifications, qualif.ShortQualifications
//
//                                                from dp_clinic_doctor cd
//
//				                                inner join dp_location loc
//				                                on loc.Id = cd.LocationId
//
//				                                inner join dp_city city
//				                                on city.Id = cd.CityId
//
//				                                inner join dp_state state
//				                                on state.id = cd.StateId
//
//				                                left outer join aspnetusers aspusers
//				                                on aspusers.SubscriberId = cd.Id
//                
//                                                left outer join dp_recommend recom
//				                                on recom.EntityId = cd.id				                                
//                                                
//                                               left outer join (select qd.doctorId, 
//																		group_concat(qfl.longEndDegree) as LongQualifications, 
//																		group_concat(qfs.shortEndDegree) as ShortQualifications from dp_qualification_doctor qd
//																left outer join dp_qualification_long qfl
//																on qfl.Id = qd.LongQualificationId
//																left outer join dp_qualification_short qfs
//																on qfs.Id = qd.ShortQualificationId
//																group by qd.doctorId) qualif
//				                                on qualif.doctorId = cd.id		                        
//                        
//				                                where 
//				                                loc.Longitude is not null and
//				                                loc.Latitude is not null and
//                                                cd.Category = [CATEGORY] and
//				                                cd.Id [FEATURED_COMPARATOR]
//				                                [CONDITIONS]
//
//                                         ) p1
//                                        left outer join (
//										    select distinct group_concat(spz.Id) as SpecializationIds, group_concat(spz.category) as SpecializationNames, spzc.clinicId
//										    from dp_specialization_clinic spzc inner join dp_specialization spz on (spz.Id = spzc.SpecializationId and spz.category is not null)
//										    group by spzc.clinicId                                              
//									    )p2 on (p2.ClinicId =  p1.EntityId)
//                                         [SPZ_CONDITION]   
//                                         order by [SORTS] [TAKE_SKIP]"; }
//        }

//        protected string GetHospitalQuery
//        {
//            get { return @"select 
//                                UID,EntityId,RecommendedCount,ReviewCount, NULL as LongQualifications, NULL as ShortQualifications,
//                                (select case when count(ipaddress) > 0 then 1 else 0 end as isRecommended from dp_recommend where EntityId = p1.EntityId and ipaddress = [IP]) 
//                                as isRecommended, 'Hospital' as `Type`,`Type` as SubType,`Name`, 
//                                LocationId,LocationName,Latitude,Longitude,CityName,StateName,ImageUrl,
//                                SpecializationIds, SpecializationNames
//                                from  (
//
//		                                select distinct 
//				                                h.Id as EntityId,h.Type,aspusers.Id as UID, 
//				                                h.Name,		
//                                                h.Profile_Pic_1 as ImageUrl,		
//				                                loc.Latitude,loc.Longitude,
//				                                loc.Id as LocationId,loc.Name as LocationName,
//				                                city.Name as CityName,state.Name as StateName,                
//                                                (select count(HospitalId) from dp_hospital_rating where HospitalId = h.id) as ReviewCount,
//                                                (select count(EntityId) from dp_recommend recom where recom.EntityId = h.id) as RecommendedCount
//                                                from dp_hospital h
//
//				                                inner join dp_location loc
//				                                on loc.Id = h.LocationId
//
//				                                inner join dp_city city
//				                                on city.Id = h.CityId
//
//				                                inner join dp_state state
//				                                on state.id = h.StateId
//
//				                                left outer join aspnetusers aspusers
//				                                on aspusers.SubscriberId = h.Id
//                
//                                                left outer join dp_recommend recom
//				                                on recom.EntityId = h.id				                                
//
//				                                where 
//				                                loc.Longitude is not null and
//				                                loc.Latitude is not null and
//				                                h.Id [FEATURED_COMPARATOR]
//				                                [CONDITIONS]
//
//                                         ) p1
//                                        inner join (
//										    select distinct group_concat(spz.Id) as SpecializationIds, group_concat(spz.category) as SpecializationNames, spzh.HospitalId
//										    from dp_specialization_hospital spzh inner join dp_specialization spz on (spz.Id = spzh.SpecializationId and spz.category is not null)
//										    group by spzh.HospitalId                                              
//									    )p2 on (p2.HospitalId =  p1.EntityId)
//                                         [SPZ_CONDITION]   
//                                         order by [SORTS] [TAKE_SKIP]"; }
//        }

//        protected string GetCollegeQuery
//        {
//            get { return @"select 
//                                UID,
//                                EntityId,
//                                `Name`, 
//                                RecommendedCount,
//                                ReviewCount, 
//                                LongQualifications, 
//                                NULL as ShortQualifications,
//	                            (select case when count(ipaddress) > 0 then 1 else 0 end as isRecommended from dp_recommend where EntityId = p1.EntityId and ipaddress = [IP]) 
//	                            as isRecommended,
//	                            'College' as `Type`,
//                                'college' as SubType,
//	                             LocationId,
//                                 CityName as LocationName,
//                                 0 as Latitude,
//                                 0 as Longitude,
//                                 CityName,
//                                 StateName
//                                from  (
//
//		                                select distinct 
//					                            c.Id as EntityId,
//                                                c.DepartmentType,
//                                                aspusers.Id as UID, 
//					                            c.Name,		
//                                                Affiliated as LongQualifications, 
//					                            loc.Id as LocationId, -- as city
//                                                loc.Name as CityName,
//                                                state.Name as StateName,   
//                    
//					                            (select count(CollegeId) from dp_college_rating where CollegeId = c.id) as ReviewCount,
//					                            (select count(EntityId) from dp_recommend recom where recom.EntityId = c.id) as RecommendedCount
//					
//					                            from dp_college c
//
//					                            inner join dp_college_location loc
//					                            on loc.Id = c.LocationId && loc.stateId = c.StateId
//
//					                            inner join dp_state state
//					                            on state.id = c.StateId
//
//					                           -- inner join dp_college_department dep
//					                           -- on dep.Id = c.DepartmentType
//
//					                            left outer join aspnetusers aspusers
//					                            on aspusers.SubscriberId = c.Id && aspusers.UserType = 'COLLEGE'
//
//					                            left outer join dp_recommend recom
//					                            on recom.EntityId = c.id				                                
//
//				                                where c.locationId is not null and
//                                                [DEPARTMENT_CONDITION]
//				                                c.Id [FEATURED_COMPARATOR]
//				                                [CONDITIONS]
//
//                                         ) p1
//                                         order by [SORTS] [TAKE_SKIP]"; }
//        }

//        protected string CountQuery
//        {
//            get { return "select count(entityId) from ( {0} ) d"; }
//        }

//        protected string GetFeaturedInQuery(UserType userType)
//        {
//            var q = string.Empty;
//            switch (userType)
//            {
//                case UserType.ClinicAndDoctor:
//                case UserType.Doctor:
//                case UserType.Clinic:
//                    q = @"in (select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2) and (enableFeatured = 1 or enableTextual = 1) and StatusValue = 4 )";
//                    break;
//                case UserType.Hospital:
//                    q =  @"in (select entityId from dp_feature WHERE (lower(EntityType) = 3) and (enableFeatured = 1 or enableTextual = 1) and StatusValue = 4 )";
//                    break;
//                case UserType.College:
//                    q=  @"in (select entityId from dp_feature WHERE (lower(EntityType) = 4) and (enableFeatured = 1 or enableTextual = 1) and StatusValue = 4 )";
//                    break;
//            }

//            return q;
//        }

//        //protected string GetFeaturedNotInQuery(UserType userType)
//        //{
//        //    var q = string.Empty;
//        //    switch (userType)
//        //    {
//        //        case UserType.ClinicAndDoctor:
//        //        case UserType.Doctor:
//        //        case UserType.Clinic:
//        //            q = @" in ( select id from dp_clinic_doctor where id not in 
//        //                         (select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2)
//        //                         union
//        //                         select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2) and StatusValue = 5
//        //                         union
//        //                         select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2) and StatusValue = 4 and EnableBasic = 1 and EnableTextual = 0
//        //                         union
//        //                         select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2) and StatusValue = 5 and EnableBasic = 0 and EnableTextual = 0
//        //                      )) ";
//        //            break;
//        //        case UserType.Hospital:
//        //            q = @" in ( select id from dp_hospital where id not in 
//        //                         (select entityId from dp_feature WHERE lower(EntityType) = 3
//        //                         union
//        //                         select entityId from dp_feature WHERE lower(EntityType) = 3 and StatusValue = 5
//        //                         union
//        //                         select entityId from dp_feature WHERE lower(EntityType) = 3 and StatusValue = 4 and EnableBasic = 1 and EnableTextual = 0
//        //                         union
//        //                         select entityId from dp_feature WHERE lower(EntityType) = 3 and StatusValue = 5 and EnableBasic = 0 and EnableTextual = 0
//        //                        )) ";
//        //            break;
//        //        case UserType.College:
//        //            q = @" in ( select id from dp_college where id not in 
//        //                         (select entityId from dp_feature WHERE lower(EntityType) = 4
//        //                         union
//        //                         select entityId from dp_feature WHERE lower(EntityType) = 4 and StatusValue = 5
//        //                         union
//        //                         select entityId from dp_feature WHERE lower(EntityType) = 4 and StatusValue = 4 and EnableBasic = 1 and EnableTextual = 0
//        //                         union
//        //                         select entityId from dp_feature WHERE lower(EntityType) = 4 and StatusValue = 5 and EnableBasic = 0 and EnableTextual = 0
//        //                      )) ";
//        //            break;
//        //    }

//        //    return q;
//        //    // get { return @"not in (select entityId from dp_feature)"; }
//        //}


//        protected string GetFeaturedNotInQuery(UserType userType)
//        {
//            var q = string.Empty;
//            switch (userType)
//            {
//                case UserType.ClinicAndDoctor:
//                case UserType.Doctor:
//                case UserType.Clinic:
//                    q = @" in ( select id from dp_clinic_doctor where id not in 
//                                (
//                                  select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2) and StatusValue = 4 and (EnableTextual = 1 or EnableFeatured = 1)
//                                  union
//                                  select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2) and StatusValue = 5
//                                )
//                              ) ";
//                    break;
//                case UserType.Hospital:
//                    q = @" in  ( select id from dp_clinic_doctor where id not in 
//                                (
//                                  select entityId from dp_feature WHERE lower(EntityType) = 3 and StatusValue = 4 and (EnableTextual = 1 or EnableFeatured = 1)
//                                  union
//                                  select entityId from dp_feature WHERE lower(EntityType) = 3 and StatusValue = 5
//                                )
//                              ) ";
//                    break;
//                case UserType.College:
//                    q = @" in  ( select id from dp_clinic_doctor where id not in 
//                                (
//                                  select entityId from dp_feature WHERE lower(EntityType) = 4 and StatusValue = 4 and (EnableTextual = 1 or EnableFeatured = 1)
//                                  union
//                                  select entityId from dp_feature WHERE lower(EntityType) = 4 and StatusValue = 5
//                                )
//                              ) ";
//                    break;
//                    //case UserType.Hospital:
//                    //    q = @" in ( select id from dp_hospital where id not in 
//                    //                 (select entityId from dp_feature WHERE lower(EntityType) = 3)
//                    //                 union
//                    //                 select entityId from dp_feature WHERE lower(EntityType) = 3 and StatusValue = 5
//                    //                 union
//                    //                 select entityId from dp_feature WHERE lower(EntityType) = 3 and StatusValue = 4 and EnableBasic =1 and EnableTextual = 0
//                    //                ) ";
//                    //    break;
//                    //case UserType.College:
//                    //    q = @" in ( select id from dp_clinic_doctor where id not in 
//                    //                 (select entityId from dp_feature WHERE lower(EntityType) = 4)
//                    //                 union
//                    //                 select entityId from dp_feature WHERE lower(EntityType) = 4 and StatusValue = 5
//                    //                 union
//                    //                 select entityId from dp_feature WHERE lower(EntityType) = 4 and StatusValue = 4 and EnableBasic =1 and EnableTextual = 0
//                    //              ) ";
//                    //    break;
//            }

//            return q;
//            // get { return @"not in (select entityId from dp_feature)"; }
//        }


//        //protected string GetFeaturedNotInQuery(UserType userType)
//        //{
//        //    var q = string.Empty;
//        //    switch (userType)
//        //    {
//        //        case UserType.ClinicAndDoctor:
//        //        case UserType.Doctor:
//        //        case UserType.Clinic:
//        //            q = @"not in (select entityId from dp_feature WHERE (lower(EntityType) = 1 or lower(EntityType) = 2) and StatusValue = 4 and entityId in(cd.Id) )";
//        //            break;
//        //        case UserType.Hospital:
//        //            q = @"not in (select entityId from dp_feature WHERE (lower(EntityType) = 3) and StatusValue = 4 and entityId in(h.Id) )";
//        //            break;
//        //        case UserType.College:
//        //            q= @"not in (select entityId from dp_feature WHERE (lower(EntityType) = 4) and StatusValue = 4 and entityId in(c.Id) )";
//        //            break;
//        //    }

//        //    return q;
//        //   // get { return @"not in (select entityId from dp_feature)"; }
//        //}

//        protected int GetDoctorOrClinicRadius(SearchRequestViewModel viewModel, string key)
//        {
//            return (viewModel.ByFeatured) ? Convert.ToInt32(configDoctorOrClinic.GetFeatured()[key].Value) :
//                   Convert.ToInt32(configDoctorOrClinic.GetNonFeatured()[key].Value);
//        }

//        protected int GetHospitalRadius(SearchRequestViewModel viewModel, string key)
//        {
//            return (viewModel.ByFeatured) ? Convert.ToInt32(configHospital.GetFeatured()[key].Value) :
//                   Convert.ToInt32(configHospital.GetNonFeatured()[key].Value);
//        }

//        protected List<int> GetLocations(SearchRequestViewModel model)
//        {
//            Location location = null;
//            List<int> locations = null;

//            if (model.Distance != 0 && model.LocationId.HasValue)
//            {
//                location = locationDao.GetByIdAsync(model.LocationId.Value).Result;
//                locations = context.Database.SqlQuery<int>(GetDistancesQuery(location.Latitude, location.Longitude, model.Distance)).ToList<int>();
//            }

//            return locations;
//        }

//        protected List<int> GetFeaturedEntities(int entityType)
//        {
//            return context.Features.Where(f => f.EntityType == entityType)
//                                   .Select(f => f.EntityId).ToList();
//        }
//    }
//}
