﻿//// ***********************************************************************
//// Assembly         : DP.Services.Business
//// Author           : GWE
//// Created          : 10-21-2015
////
//// Last Modified By : GWE
//// Last Modified On : 10-29-2015
//// ***********************************************************************
//// <copyright file="DoctorOrClinicSearchManager.cs" company="">
////     Copyright ©  2015
//// </copyright>
//// <summary></summary>
//// ***********************************************************************
//using DP.Common.Constants;
//using DP.Common.Enums;
//using DP.Common.Models;
//using DP.Common.Utilities;
//using DP.Services.Data.Models;
//using DP.Services.Data.UnitOfWork;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Text;
//using System.Threading.Tasks;

///// <summary>
///// The Business namespace.
///// </summary>
//namespace DP.Services.Business
//{
//    /// <summary>
//    /// Class DoctorOrClinicSearchManager. This class cannot be inherited.
//    /// </summary>
//    public sealed class DoctorOrClinicSearchManager : BaseSearchManager
//    {
//        /// <summary>
//        /// The common DAO
//        /// </summary>
//        private ICommonDao commonDao;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="DoctorOrClinicSearchManager" /> class.
//        /// </summary>
//        /// <param name="commonDao">The common DAO.</param>
//        /// <param name="locationDao">The location DAO.</param>
//        public DoctorOrClinicSearchManager(ICommonDao commonDao, ILocationDao locationDao):base(commonDao, locationDao)
//        {
//            this.commonDao = commonDao;
//        }


//        protected override List<SearchResultViewModel> GetSearchResults(SearchRequestViewModel viewModel, out int total)
//        {
//            List<SearchResultViewModel> searchResults = null;
//            var sorts = string.Join(",", new string[] { 
//                " ReviewCount " + ((viewModel.ByReview == 1) ? " desc " : " asc "),
//                " RecommendedCount " + ((viewModel.ByRecommended == 1) ? " desc " : " asc ")
//            });

//            var countQuery = CountQuery;
//            var baseQuery = GetClinicOrDoctorQuery;

//            //byFeatured
//            baseQuery = (viewModel.ByFeatured) ?
//                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedInQuery(UserType.ClinicAndDoctor)):
//                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedNotInQuery(UserType.ClinicAndDoctor));

//            //ipaddress
//            baseQuery = baseQuery.Replace(IP, "'" + viewModel.Ip + "'");

//            //category
//            baseQuery = baseQuery.Replace(CATEGORY, ((int)viewModel.DoctorOrClinicCategoryType).ToString());

//            //sorts
//            baseQuery = baseQuery.Replace(SORTS, sorts);

//            //Specialization condition
//            var spzCategory = viewModel.SpecializationId.HasValue ? 
//                               this.commonDao.GetDpContext().Specializations.Where(x => x.Id == viewModel.SpecializationId.Value).Single().Category 
//                               : "";
//            baseQuery = viewModel.SpecializationId.HasValue ?
//                 baseQuery.Replace(SPZ_CONDITION, "where SpecializationNames like '%" + spzCategory + "%'") :
//                 baseQuery.Replace(SPZ_CONDITION, "");

//            //conditions

//            //DoctorOrClinicId conditiond
//            if (viewModel.DoctorOrClinicId.HasValue)
//                conditions.Add(" cd.Id =" + ((int)viewModel.DoctorOrClinicId).ToString());

//            //cityId condition
//            if (viewModel.CityId.HasValue)
//                conditions.Add(" cd.cityId =" + viewModel.CityId.Value.ToString());

//            //LocationId condition
//            if (viewModel.LocationId.HasValue)
//            {
//                if (viewModel.Distance != 0)
//                {
//                    var locations = GetLocations(viewModel);
//                    conditions.Add(" cd.locationId in (" + string.Join(",", locations.Select(x => x.ToString()).ToArray<string>()) + " )");
//                }
//            }

//            baseQuery = conditions.Count > 0 ?
//                baseQuery.Replace(CONDITIONS, "and " + string.Join(" and", conditions.ToArray())) :
//                baseQuery.Replace(CONDITIONS, "");
         
//            total = commonDao.ExecuteQuery(string.Format(countQuery, baseQuery).Replace(TAKE_SKIP, ""));

//            if (total > 0)
//            {
//                baseQuery = (viewModel.ByFeatured == true) ?
//                            baseQuery.Replace(TAKE_SKIP, "") :
//                            baseQuery.Replace(TAKE_SKIP, "limit " + viewModel.Take + " offset " + viewModel.Skip + "");
//                searchResults = commonDao.ExecuteQuery<SearchResultViewModel>(baseQuery);
//            }
//            else
//             if (viewModel.LocationId.HasValue)
//            {
//                if ((searchResults == null || searchResults.Count == 0)
//                && viewModel.ByFeatured &&
//                viewModel.Distance <= GetDoctorOrClinicRadius(viewModel, Distance.Max))
//                {
//                    viewModel.Distance = viewModel.Distance + GetDoctorOrClinicRadius(viewModel, Distance.Slide);
//                    searchResults = GetSearchResults(viewModel, out total);
//                }
//            }

//            if (searchResults != null)
//            {
//                searchResults.ForEach(x =>
//                {
//                    x.IsFeatured = viewModel.ByFeatured;
//                    x.Radius = viewModel.Distance;
//                });
//            }
//            return searchResults;
//        }
//    }
//}
