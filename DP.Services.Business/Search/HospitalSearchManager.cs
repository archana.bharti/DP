﻿using DP.Common.Constants;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public sealed class HospitalSearchManager : BaseSearchManager
    {
        private ICommonDao commonDao;
        private ILocationDao locationDao;

        public HospitalSearchManager(ICommonDao commonDao, ILocationDao locationDao) : base(commonDao, locationDao)
        {
            this.commonDao = commonDao;
            this.locationDao = locationDao;
        }

        protected override List<SearchResultViewModel> GetSearchResults(SearchRequestViewModel viewModel, out int total)
        {
            List<SearchResultViewModel> searchResults = null;
            string sorts = string.Empty;
            var countQuery = CountQuery;
            var baseQuery = GetHospitalQuery;

            //has distance selected
            if (viewModel.Distance != 0 && viewModel.LocationId.HasValue)
            {
                sorts = string.Join(",", new string[] {
                " Distance ",
                " ReviewCount " + ((viewModel.ByReview == 1) ? " desc " : " asc "),
                " RecommendedCount " + ((viewModel.ByRecommended == 1) ? " desc " : " asc ")
                    });
                var _location = locationDao.GetByIdAsync(viewModel.LocationId.Value).Result;
                baseQuery = baseQuery.Replace(DISTANCE, " (SELECT  ( 6371 * acos( cos( radians(" + Convert.ToString(_location.Latitude) + ") )  * cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians(" + Convert.ToString(_location.Longitude) + ") ) + sin( radians(" + Convert.ToString(_location.Latitude) + ") ) * sin( radians( Latitude ) ) ) ))");
            }
            else
            {
                baseQuery = baseQuery.Replace(DISTANCE, "0");
                sorts = string.Join(",", new string[] {
                                " ReviewCount " + ((viewModel.ByReview == 1) ? " desc " : " asc "),
                                " RecommendedCount " + ((viewModel.ByRecommended == 1) ? " desc " : " asc ")
                 });
            }


            //byFeatured
            baseQuery = (viewModel.ByFeatured) ?
                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedInQuery(UserType.Hospital)) :
                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedNotInQuery(UserType.Hospital));

            //ipaddress
            baseQuery = baseQuery.Replace(IP, "'" + viewModel.Ip + "'");

            //sorts
            baseQuery = baseQuery.Replace(SORTS, sorts);

            //Specialization condition
            var spzCategory = viewModel.SpecializationId.HasValue ?
                               this.commonDao.GetDpContext().Specializations.Where(x => x.Id == viewModel.SpecializationId.Value).Single().Category
                               : "";
            baseQuery = viewModel.SpecializationId.HasValue ?
                 baseQuery.Replace(SPZ_CONDITION, "where SpecializationNames like '%" + spzCategory + "%'") :
                 baseQuery.Replace(SPZ_CONDITION, "");

            //conditions

            //HospitalId conditiond
            if (viewModel.HospitalId.HasValue)
                conditions.Add(" h.Id =" + ((int)viewModel.HospitalId).ToString());

            //cityId condition
            if (viewModel.CityId.HasValue)
                conditions.Add(" h.cityId =" + viewModel.CityId.Value.ToString());

            //LocationId condition
            if (viewModel.LocationId.HasValue)
            {
                if (viewModel.Distance != 0)
                {
                    var locations = GetLocations(viewModel);
                    conditions.Add(" h.locationId in (" + string.Join(",", locations.Select(x => x.ToString()).ToArray<string>()) + " )");
                }
            }

            baseQuery = conditions.Count > 0 ?
                baseQuery.Replace(CONDITIONS, "and " + string.Join(" and", conditions.ToArray())) :
                baseQuery.Replace(CONDITIONS, "");

            total = commonDao.ExecuteQuery(string.Format(countQuery, baseQuery).Replace(TAKE_SKIP, ""));

            if (total > 0)
            {
                baseQuery = (viewModel.ByFeatured == true) ?
                            baseQuery.Replace(TAKE_SKIP, "") :
                            baseQuery.Replace(TAKE_SKIP, "limit " + viewModel.Take + " offset " + viewModel.Skip + "");
                searchResults = commonDao.ExecuteQuery<SearchResultViewModel>(baseQuery);
            }
            else
            if (viewModel.LocationId.HasValue)
            {
                if ((searchResults == null || searchResults.Count == 0) && viewModel.ByFeatured && viewModel.Distance <= GetHospitalRadius(viewModel, Distance.Max))
                {
                    viewModel.Distance = viewModel.Distance + GetHospitalRadius(viewModel, Distance.Slide);
                    searchResults = GetSearchResults(viewModel, out total);
                }
            }

            if (searchResults != null)
            {
                searchResults.ForEach(x =>
                {
                    x.IsFeatured = viewModel.ByFeatured;
                    x.Radius = viewModel.Distance;
                });
            }
            return searchResults;
        }
    }
}
