﻿using DP.Common.Constants;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class ZoneSearchManager
    {
        ICommonDao commonDao;
        public ZoneSearchManager(ICommonDao commonDao)
        {
            this.commonDao = commonDao;
        }

        public List<ZoneSearchResponseViewModel> Search(ZoneSearchRequestViewModel viewModel, bool forDashboard = false)
        {
            CacheManager cacheManager = new CacheManager();
            string baseQuery = @"select cd.Id as EntityId, cd.Name, cd.type as EntityType, cd.Summary,  loc.Name as LocationName, 
                                cd.Profile_Pic_1 as ImageUrl from dp_clinic_doctor cd 
                                inner join dp_feature feature on (feature.EntityId = cd.Id and (lower(feature.EntityType) =1 or  lower(feature.EntityType) =2) and StatusValue = 4)
                                inner join dp_location loc on loc.id = cd.locationId
                                where locationid in (select id from dp_location where zone in 
                                ( select zone from dp_location [LOCATION_CONDITION] ) [VISIBILITY_CONDITION] )

                                union 

                                select cd.Id as EntityId, cd.Name, cd.type as EntityType, cd.Summary, 
                                loc.Name as LocationName, cd.Profile_Pic_1 as ImageUrl from dp_hospital cd 
                                inner join dp_feature feature on (feature.EntityId = cd.Id and lower(feature.EntityType) =3 and StatusValue = 4)
                                inner join dp_location loc on loc.id = cd.locationId
                                where locationid in (select id from dp_location where zone in (
                                select zone from dp_location
                                [LOCATION_CONDITION]
                                ) [VISIBILITY_CONDITION] )";

            if (forDashboard)
            {
                baseQuery = baseQuery + @" union select cd.Id as EntityId, cd.Name, cd.type as EntityType, cd.Summary,  
                                loc.Name as LocationName, cd.Profile_Pic_1 as ImageUrl from dp_college cd 
                                inner join dp_feature feature on (feature.EntityId = cd.Id and lower(feature.EntityType) =4 and StatusValue = 4)
                                inner join dp_college_location loc on loc.id = cd.locationId
                                where LocationId is not null and locationid in (select id from dp_college_location where zone in (
                                select distinct zone from dp_college_location [LOCATION_CONDITION]) [VISIBILITY_CONDITION] )";
            }

            baseQuery = (viewModel.LocationId.HasValue) ? 
                baseQuery.Replace("[LOCATION_CONDITION]", " where id =" + viewModel.LocationId.Value.ToString()) :
                baseQuery.Replace("[LOCATION_CONDITION]", "");

            baseQuery = baseQuery.Replace("[VISIBILITY_CONDITION]", "and feature.enableFeatured = 1");
            //baseQuery = (viewModel.LocationId.HasValue) ?
            //    baseQuery.Replace("[VISIBILITY_CONDITION]", "and feature.enableFeatured = 1") :
            //    baseQuery.Replace("[VISIBILITY_CONDITION]", "and (feature.enableFeatured = 1 or  feature.enableTextual = 1)") ;

            var results = this.commonDao.ExecuteQuery<ZoneSearchResponseViewModel>(baseQuery);
            Dictionary<string, object> fromCache = cacheManager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object>;

            var rootPath = Convert.ToString(fromCache[GlobalSettings.S3_ROOT_PATH]);
            results.ForEach(x => 
            {
                x.ImageUrl = (string.IsNullOrEmpty(x.ImageUrl)) ? rootPath + x.EntityType.ToLower() + "-default-300x300.png" : x.ImageUrl;
            });

            return results;
        }

        public List<ZoneSearchResponseViewModel> College(ZoneSearchRequestViewModel viewModel)
        {
            CacheManager cacheManager = new CacheManager();
            string baseQuery = @"select cd.Id as EntityId, cd.Name, cd.type as EntityType, cd.Summary,  
                                loc.Name as LocationName, cd.Profile_Pic_1 as ImageUrl from dp_college cd 
                                inner join dp_feature feature on (feature.EntityId = cd.Id and lower(feature.EntityType) =4 and StatusValue = 4)
                                inner join dp_college_location loc on loc.id = cd.locationId
                                where LocationId is not null and locationid in (select id from dp_college_location where zone in (
                                select distinct zone from dp_college_location [LOCATION_CONDITION]) [VISIBILITY_CONDITION] )";

            baseQuery = (viewModel.StateId.HasValue) ?
                baseQuery.Replace("[LOCATION_CONDITION]", " where stateId =" + viewModel.StateId.Value.ToString()) :
                baseQuery.Replace("[LOCATION_CONDITION]", "");

            baseQuery = baseQuery.Replace("[VISIBILITY_CONDITION]", "and feature.enableFeatured = 1");
            //baseQuery = (viewModel.LocationId.HasValue) ?
            //    baseQuery.Replace("[VISIBILITY_CONDITION]", "and feature.enableFeatured = 1") :
            //    baseQuery.Replace("[VISIBILITY_CONDITION]", "and (feature.enableFeatured = 1 or  feature.enableTextual = 1)") ;

            var results = this.commonDao.ExecuteQuery<ZoneSearchResponseViewModel>(baseQuery);
            Dictionary<string, object> fromCache = cacheManager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object>;

            var rootPath = Convert.ToString(fromCache[GlobalSettings.S3_ROOT_PATH]);
            results.ForEach(x =>
            {
                x.ImageUrl = (string.IsNullOrEmpty(x.ImageUrl)) ? rootPath + x.EntityType.ToLower() + "-default-300x300.png" : x.ImageUrl;
            });

            return results;
        }
    }
}
