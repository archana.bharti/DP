﻿//using DP.Common.Constants;
//using DP.Common.Enums;
//using DP.Common.Models;
//using DP.Common.Utilities;
//using DP.Services.Data.Models;
//using DP.Services.Data.UnitOfWork;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace DP.Services.Business
//{
//    public sealed class HospitalSearchManager : BaseSearchManager
//    {
//        private ICommonDao commonDao;

//        public HospitalSearchManager(ICommonDao commonDao, ILocationDao locationDao) : base(commonDao, locationDao)
//        {
//            this.commonDao = commonDao;
//        }

//        protected override List<SearchResultViewModel> GetSearchResults(SearchRequestViewModel viewModel, out int total)
//        {
//            List<SearchResultViewModel> searchResults = null;
//            var sorts = string.Join(",", new string[] {
//                " ReviewCount " + ((viewModel.ByReview == 1) ? " desc " : " asc "),
//                " RecommendedCount " + ((viewModel.ByRecommended == 1) ? " desc " : " asc ")
//            });

//            var countQuery = CountQuery;
//            var baseQuery = GetHospitalQuery;

//            //byFeatured
//            baseQuery = (viewModel.ByFeatured) ?
//                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedInQuery(UserType.Hospital)) :
//                        baseQuery.Replace(FEATURED_COMPARATOR, GetFeaturedNotInQuery(UserType.Hospital));

//            //ipaddress
//            baseQuery = baseQuery.Replace(IP, "'" + viewModel.Ip + "'");

//            //sorts
//            baseQuery = baseQuery.Replace(SORTS, sorts);

//            //Specialization condition
//            var spzCategory = viewModel.SpecializationId.HasValue ?
//                               this.commonDao.GetDpContext().Specializations.Where(x => x.Id == viewModel.SpecializationId.Value).Single().Category
//                               : "";
//            baseQuery = viewModel.SpecializationId.HasValue ?
//                 baseQuery.Replace(SPZ_CONDITION, "where SpecializationNames like '%" + spzCategory + "%'") :
//                 baseQuery.Replace(SPZ_CONDITION, "");

//            //conditions

//            //HospitalId conditiond
//            if (viewModel.HospitalId.HasValue)
//                conditions.Add(" h.Id =" + ((int)viewModel.HospitalId).ToString());

//            //cityId condition
//            if (viewModel.CityId.HasValue)
//                conditions.Add(" h.cityId =" + viewModel.CityId.Value.ToString());

//            //LocationId condition
//            if (viewModel.LocationId.HasValue)
//            {
//                if (viewModel.Distance != 0)
//                {
//                    var locations = GetLocations(viewModel);
//                    conditions.Add(" h.locationId in (" + string.Join(",", locations.Select(x => x.ToString()).ToArray<string>()) + " )");
//                }
//            }

//            baseQuery = conditions.Count > 0 ?
//                baseQuery.Replace(CONDITIONS, "and " + string.Join(" and", conditions.ToArray())) :
//                baseQuery.Replace(CONDITIONS, "");

//            total = commonDao.ExecuteQuery(string.Format(countQuery, baseQuery).Replace(TAKE_SKIP, ""));

//            if (total > 0)
//            {
//                baseQuery = (viewModel.ByFeatured == true) ?
//                            baseQuery.Replace(TAKE_SKIP, "") :
//                            baseQuery.Replace(TAKE_SKIP, "limit " + viewModel.Take + " offset " + viewModel.Skip + "");
//                searchResults = commonDao.ExecuteQuery<SearchResultViewModel>(baseQuery);
//            }
//            else
//            if (viewModel.LocationId.HasValue)
//            {
//                if ((searchResults == null || searchResults.Count == 0) && viewModel.ByFeatured && viewModel.Distance <= GetHospitalRadius(viewModel, Distance.Max))
//                {
//                    viewModel.Distance = viewModel.Distance + GetHospitalRadius(viewModel, Distance.Slide);
//                    searchResults = GetSearchResults(viewModel, out total);
//                }
//            }

//            if (searchResults != null)
//            {
//                searchResults.ForEach(x =>
//                {
//                    x.IsFeatured = viewModel.ByFeatured;
//                    x.Radius = viewModel.Distance;
//                });
//            }
//            return searchResults;
//        }
//    }
//}
