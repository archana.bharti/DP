﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Text;
using DP.Common.Enums;
using MoreLinq;
using DP.Common.Models;
using DP.Services.Data;

namespace DP.Services.Business
{
    public class InsuranceManager
    {
        private ICommonDao commonDao;
        private InsuranceDao insuranceDao;
        private DPContext context;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="commonDao"></param>
        /// <param name="locationDao"></param>
        public InsuranceManager(ICommonDao commonDao, InsuranceDao insuranceDao)
        {
            this.commonDao = commonDao;
            this.insuranceDao = insuranceDao;
            this.context = this.commonDao.GetDpContext();
        }

        public async Task<object> Search(Insurance insurance, string ipAddress, int? take = 10, int? skip = 0, string sort = "Id", string sortOrder = "asc")
        {
            var query = this.insuranceDao.Get;
            query = Parse(query, insurance).Include(x => x.Ratings);

            if (insurance.PlanType.ToLower() != "any")
            {
                query = query.Where(x => x.PlanType.ToUpper() == insurance.PlanType.ToUpper());
            }

            var ctx = commonDao.GetDpContext();
            // var insuranceUsers = await ctx.Users.Where(x => x.UserType.ToUpper() == "INSURANCE" && x.SubscriberId.HasValue).ToListAsync();
      

            var featuredList = await ctx.Features.Where(x => x.EntityType == (int)UserType.Insurance).ToListAsync();
            List<int> featuredSubscriberIds = new List<int>(); //list of entities id that are in feature table 
            if (featuredList.Count > 0)
            {
                featuredSubscriberIds = featuredList.Select(x => x.EntityId).ToList<int>();
            }
            if (insurance.IsFeatured)
            {
                query = query.Where(x => featuredSubscriberIds.Contains(x.Id));
            }
            else
            {
                query = query.Where(x => !featuredSubscriberIds.Contains(x.Id));
            }
            
            var count = query.Count();

            query = sortOrder == "asc" ? query.OrderBy(sort) : query.OrderBy(sort + " descending");
            if (!insurance.IsFeatured)
            {
                query = query.Skip(skip.Value).Take(take.Value);
            }

            var resultList = await query.ToListAsync();

           await FillWithRecommendations(resultList, ipAddress);

            if (insurance.IsFeatured)
            {
                resultList.ForEach(x => x.IsFeatured = true);
            }

            return new { Rows = resultList, Total = count };
        }


        public async Task<object> ConsolateSearch(Insurance insurance,  string Search,string ipAddress, int? take = 10, int? skip = 0, string sort = "Id", string sortOrder = "asc")
        {

            string[] Searchparm = Search.Split(',');
            string PlanType = Searchparm[1];
            string PlanName = Searchparm[0];
            var query = this.insuranceDao.Get;
            query = Parse(query, insurance).Include(x => x.Ratings);

            if (insurance.PlanType.ToLower() != "any")
            {
                query = query.Where(x => x.PlanType.ToUpper() == insurance.PlanType.ToUpper());
            }

            var ctx = commonDao.GetDpContext();
         
            //// Code removed due to stop filterination on bases of featured and non featured///

            //var featuredList = await ctx.Features.Where(x => x.EntityType == (int)UserType.Insurance).ToListAsync();
            //List<int> featuredSubscriberIds = new List<int>(); //list of entities id that are in feature table 
            //if (featuredList.Count > 0)
            //{
            //    featuredSubscriberIds = featuredList.Select(x => x.EntityId).ToList<int>();
            //}
            //if (insurance.IsFeatured)
            //{
            //    query = query.Where(x => featuredSubscriberIds.Contains(x.Id));
            //}
            //else
            //{
            //    query = query.Where(x => !featuredSubscriberIds.Contains(x.Id));
            //}


           
            var count = query.Count();
            query = sortOrder == "asc" ? query.OrderBy(sort) : query.OrderBy(sort + " descending");

            //// Code removed due to stop filterination on bases of featured and non featured///
            //if (!insurance.IsFeatured)
            //{
            //    query = query.Skip(skip.Value).Take(take.Value);
            //}

            var resultList = await query.ToListAsync();

            await FillWithRecommendations(resultList, ipAddress);

            if (insurance.IsFeatured)
            {
                resultList.ForEach(x => x.IsFeatured = true);
            }

          
         
            var finalresultList = resultList.Where(x => x.PlanType.ToLower()==(PlanType.ToLower()) || x.PlanName.ToLower()==(PlanName.ToLower())).ToList();
           
           return new { Rows = finalresultList, Total = count };
        }

        private IQueryable<Insurance> Parse(IQueryable<Insurance> query, Insurance insurance)
        {
            StringBuilder builder = new StringBuilder();

            //Type obj = insurance.GetType();
            //var testproperties = insurance.GetType().GetProperties();

            //var test = testproperties.Where(x => x.PropertyType == typeof(bool) && x.GetValue(insurance).ToString().ToLower()=="true").Select(x=>x.Name);

            var properties = insurance.GetType().GetProperties().Where(x =>
            x.PropertyType == typeof(bool) &&
            x.GetValue(insurance).ToString().ToLower() == "true" && 
            x.Name.ToLower() != "isfeatured");

            var count = properties.Count();
            var index = 0;
            foreach(var prop in properties)
            {
                var name = prop.Name;
                var value = prop.GetValue(insurance).ToString();
                var type = prop.PropertyType;
                if(type == typeof(bool) && value.ToLower() == "true")
                {
                    builder.Append(name + " == true ");
                    index = index + 1;
                    if(index < count)
                    {
                        builder.Append(" && ");
                    }
                }
            }

            return  builder.ToString().Length == 0 ? query :  query.Where(builder.ToString());
        }

        private async Task FillWithRecommendations(List<Insurance> insuranceList, string ipAddress)
        {
            var ctx = commonDao.GetDpContext();
            var insurnaceIds = insuranceList.Select(x => x.Id).ToList();
            var recommendations = await ctx.Recommends.Where(x =>
                                insurnaceIds.Contains(x.EntityId) && 
                                x.EntityType == (int)UserType.Insurance
                                ).ToListAsync();

            insuranceList.ForEach(x => {
                x.RecommendedCount = recommendations.Where(r => r.EntityId == x.Id).Count();
                x.IsRecommended = recommendations.Where(r => r.EntityId == x.Id && r.IpAddress == ipAddress).Count() > 0;
            });
        }

        public async Task<Insurance> GetByIdAsync(int id)
        {
            return await this.insuranceDao.GetByIdAsync(new Insurance() { Id = id }, true);
        }

        public async Task<object> GetTopRated(int id)
        {
            var result = await this.insuranceDao.GetTopRating(id);
            var notList = new List<string>() { "Insurance", "InsuranceId", "ReviewedDate", "YourExperience", "UserId", "User", "Id" };
            if (result == null) { return null; }

            Dictionary<string, int> dict = new Dictionary<string, int>();
            var properties = result.GetType().GetProperties().Where(
                 x =>
                !x.Name.Contains("Percentage") &&
                !x.Name.Contains("SumOfRating") &&
                !x.Name.Contains("Average") &&
                !notList.Contains(x.Name));
            foreach (var prop in properties)
            {
                dict.Add(prop.Name, int.Parse(prop.GetValue(result).ToString()));
            }

            var topRated = dict.MaxBy(x => x.Value);
            if (topRated.Value == 5)
            {
                return new
                {
                    Parameter = topRated.Key,
                    Value = topRated.Value
                };
            }
            return null;
        }

        public async Task<Insurance> UpdateSummary(Insurance insurnace)
        {
            return await this.insuranceDao.UpdateAsync(insurnace, new string[] { "Summary" });
        }

        public async Task<Insurance> UpdateAsync(Insurance insurnace)
        {
            return await this.insuranceDao.UpdateAsync(insurnace);
        }

        public async Task<Insurance> UpdateAsync(Insurance insurnace, string[] properitesToUpdate)
        {
            return await this.insuranceDao.UpdateAsync(insurnace, properitesToUpdate);
        }

        public async Task<List<InsuranceRating>> GetRatingAsync(string userId)
        {
            return await this.insuranceDao.GetRatingAsync(userId);
        }

        public async Task<List<Insurance>> GetFeatured()
        {
            var query = this.insuranceDao.Get;
            List<Insurance> insuranceList = new List<Insurance>();
            var ctx = commonDao.GetDpContext();
            var featuredList = await ctx.Features.Where(x => 
            x.EntityType == (int)UserType.Insurance &&
            x.EnableFeatured == true).ToListAsync();

            if(featuredList.Count > 0)
            {
                var featuredIds = featuredList.Select(x => x.EntityId).ToList<int>();
                insuranceList = query.Where(x => featuredIds.Contains(x.Id)).ToList<Insurance>();
            }

            return insuranceList;
        }


        private IEnumerable<InsuranceTypeFinderViewModel> GetInsurancePlanName(InsuranceTypeFinderViewModel viewModel)
        {
          
            var query = this.context.Insurances.Where(x => x.PlanName.ToLower().Contains(viewModel.searchTerm.ToLower()));
          
             
            return query.ToList().Select(c => ModelFactory.ConvertToPlanNameSuggestionVM(c));
        }

        public object GetSuggestions(InsuranceTypeFinderViewModel viewModel)
        {
            var results = new List<InsuranceTypeFinderViewModel>();

            results = GetInsurancePlanName(viewModel).ToList();
           
            return new
            {
                Results = results,
                Count = results.Count
            };
        }
    }
}
