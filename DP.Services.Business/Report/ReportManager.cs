﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DP.Common.Models;
using DP.Services.Data;
using DP.Services.Data.UnitOfWork;
using DP.Common;
using DP.Common.Utilities;
using DP.Common.Constants;

namespace DP.Services.Business
{
    public class ReportManager : BaseManager
    {
        ICommonDao commonDao;
        public ReportManager(ICommonDao commonDao)
        {
            this.commonDao = commonDao;
        }

        public object GetFeaturedStats(FeaturedReportRequestViewModel vm)
        {
            var clinics = this.commonDao.GetClinicsFeaturedReport(vm.Months.ToList(), vm.Year);
            var doctors = this.commonDao.GetDoctorsFeaturedReport(vm.Months.ToList(), vm.Year);
            var colleges = this.commonDao.GetCollegesFeaturedReport(vm.Months.ToList(), vm.Year);
            var hospitals = this.commonDao.GetHospitalsFeaturedReport(vm.Months.ToList(), vm.Year);
            var insurances = this.commonDao.GetInsurancesFeaturedReport(vm.Months.ToList(), vm.Year);

            CacheManager cacheManager = new CacheManager();
            Dictionary<string, object> fromCache = cacheManager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object>;

            var rootPath = Convert.ToString(fromCache[GlobalSettings.S3_ROOT_PATH]);
            clinics.ForEach(x =>{
                x.Subscribers.ForEach(y => y.ImageUrl = (string.IsNullOrEmpty(y.ImageUrl)) ? rootPath + y.EntityTypeText.ToLower() + "-default-300x300.png" : y.ImageUrl);
            });
            doctors.ForEach(x => {
                x.Subscribers.ForEach(y => y.ImageUrl = (string.IsNullOrEmpty(y.ImageUrl)) ? rootPath + y.EntityTypeText.ToLower() + "-default-300x300.png" : y.ImageUrl);
            });
            colleges.ForEach(x => {
                x.Subscribers.ForEach(y => y.ImageUrl = (string.IsNullOrEmpty(y.ImageUrl)) ? rootPath + y.EntityTypeText.ToLower() + "-default-300x300.png" : y.ImageUrl);
            });
            hospitals.ForEach(x => {
                x.Subscribers.ForEach(y => y.ImageUrl = (string.IsNullOrEmpty(y.ImageUrl)) ? rootPath + y.EntityTypeText.ToLower() + "-default-300x300.png" : y.ImageUrl);
            });
            insurances.ForEach(x => {
                x.Subscribers.ForEach(y => y.ImageUrl = (string.IsNullOrEmpty(y.ImageUrl)) ? rootPath + y.EntityTypeText.ToLower() + "-default-300x300.png" : y.ImageUrl);
            });

            return new
            {
                Clinics = clinics,
                Doctors = doctors,
                Colleges = colleges,
                Hospitals = hospitals,
                Insurances = insurances
            };
        }
    }
}
