﻿using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class ClinicManager : BaseManager
    {
        private IClinicAndDoctorDao clinicCtx;
        public ClinicManager(IClinicAndDoctorDao clinicDao)
        {
            this.clinicCtx = clinicDao;
        }

        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <returns>Task<List<ClinicAndDoctor>>.</returns>
        public async Task<List<ClinicAndDoctor>> GetAllAsync()
        {
            return await this.clinicCtx.GetAllAsync();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<ClinicAndDoctor>.</returns>
        public async Task<ClinicAndDoctor> GetByIdAsync(int id)
        {
            return await this.clinicCtx.GetByIdAsync(id);
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="propertiesToUpdate">The properties to update.</param>
        /// <returns>Task<ClinicAndDoctor>.</returns>
        public async Task<ClinicAndDoctor> UpdateAsync(ClinicAndDoctor model, params string[] propertiesToUpdate)
        {
            return await this.clinicCtx.UpdateAsync(model, propertiesToUpdate);
        }

        /// <summary>
        /// update rating as an asynchronous operation.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <returns>Task<ClinicRating>.</returns>
        public async Task<ClinicRating> UpdateRatingAsync(ClinicRating rating)
        {
            return await this.clinicCtx.UpdateRatingAsync(rating);
        }

        public async Task<List<ClinicRating>> GetRatingAsync(string userId)
        {
            return await this.clinicCtx.GetRatingAsync(userId);
        }
    }
}
