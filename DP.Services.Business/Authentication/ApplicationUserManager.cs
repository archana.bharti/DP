﻿using DP.Services.Data;
using DP.Services.Data.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business.Identity
{
    public class ApplicationUserManager:UserManager<ApplicationUser>
    {
        public static IDataProtectionProvider DataProtectionProvider { get; set; }

        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
            #region Commented
            this.UserValidator = new UserValidator<ApplicationUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            this.UserLockoutEnabledByDefault = true;
            //for the next one year
            this.DefaultAccountLockoutTimeSpan = TimeSpan.FromDays(365);
            this.MaxFailedAccessAttemptsBeforeLockout = 10;
            #endregion
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<DPContext>()));


            //due to unity injection, the below is not setting up the token provider properly
            //var dataProtectionProvider = DataProtectionProvider;
            //if (dataProtectionProvider != null)
            //{
            //    manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("EmailConfirmation"))
            //    {
            //        //Code for email confirmation and reset password life time
            //        TokenLifespan = TimeSpan.FromHours(6)
            //    };
            //}
            //var dataProtectorProvider = DataProtectionProvider;
            //var dataProtector = dataProtectorProvider.Create("DoctorPursuit");
            //manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser,string>(dataProtector)
            //{
            //    TokenLifespan = TimeSpan.FromHours(24),
            //};

            return manager;
        }

       
    }
}
