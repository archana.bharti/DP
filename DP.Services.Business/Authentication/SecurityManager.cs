﻿using DP.Common.Enums;
using DP.Common.Constants;
using DP.Services.Business.Identity;
using DP.Services.Data.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using DP.Services.Business;

namespace DP.Services.Security
{
    public class SecurityManager : BaseManager
    {
        public ApplicationRoleManager _roleManager;
        public ApplicationUserManager _userManager;
        public SecurityManager(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        public async Task<Tuple<SignInStatus, ApplicationUser>> SignIn(string email, string password, bool lockOut = false)
        {
            //by name
            ApplicationUser user = await this._userManager.FindByNameAsync(email);
            if (user == null)
            {
                //by email
                user = await this._userManager.FindByEmailAsync(email);
            }
            if (user == null)
            {
                return new Tuple<SignInStatus, ApplicationUser>(SignInStatus.Failure, user);
            }

            if (!user.EmailConfirmed)
            {
                return new Tuple<SignInStatus, ApplicationUser>(SignInStatus.Inactive, user);
            }
            if (user.UserType != Roles.Administrator && user.UserType != Roles.User && !user.IsVerified)
            {
                return new Tuple<SignInStatus, ApplicationUser>(SignInStatus.RequireVerification, user);
            }

            if (await _userManager.IsLockedOutAsync(user.Id))
            {
                return new Tuple<SignInStatus, ApplicationUser>(SignInStatus.LockedOut, user);
            }
            if (await _userManager.CheckPasswordAsync(user, password))
            {
                return new Tuple<SignInStatus, ApplicationUser>(SignInStatus.Success, user);
                //return await SignInOrTwoFactor(user, false);
            }
            if (lockOut)
            {
                // If lockout is requested, increment access failed count which might lock out the user
                await _userManager.AccessFailedAsync(user.Id);
                if (await _userManager.IsLockedOutAsync(user.Id))
                {
                    return new Tuple<SignInStatus, ApplicationUser>(SignInStatus.LockedOut, user);;
                }
            }
            return new Tuple<SignInStatus, ApplicationUser>(SignInStatus.Failure, user); 
        }

        public async Task<ApplicationUser> GetUserByEmailAsync(string email)
        {
            try
            {
                return await this._userManager.FindByEmailAsync(email);
            }
            catch
            {
                throw new Exception("GET USER BY EMAIL FAILED");
            }
        }

        public async Task<ApplicationUser> GetUserAsync(string id)
        {
            try
            {
                return await this._userManager.FindByIdAsync(id);
            }
            catch
            {
                throw new Exception("GET USER BY ID FAILED");
            }
        }

        public async Task<IList<string>> GetRolesAsync(string userId)
        {
            return await this._userManager.GetRolesAsync(userId);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(string userId, string token)
        {
            SetUserToken();
            return await this._userManager.ConfirmEmailAsync(userId,token);
        }

        public async Task<ApplicationUser> CreateUserAsync(ApplicationUser appUser, string password)
        {
            try
            {
                IdentityResult addUserResult = await this._userManager.CreateAsync(appUser, password);

                if (!addUserResult.Succeeded)
                {
                    throw new Exception(string.Format("Unable to create user. {0}", addUserResult));
                }

                return await this.GetUserByEmailAsync(appUser.Email);
            }
            catch
            {
                throw new Exception("CREATE USER FAILED");
            }
         
        }

        public async Task AssingRole(string userKey, string roleId)
        {
            var role = this._roleManager.FindById(roleId);
            IdentityResult addRoleResult = await this._userManager.AddToRoleAsync(userKey, role.Name);
            if (!addRoleResult.Succeeded)
            {
                throw new Exception(string.Format("Unable to assign role to the user. {0}", addRoleResult));
            }
        }

        public List<IdentityRole> GetAllRoles()
        {
            return this._roleManager.Roles.ToList();
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(string userId)
        {
            SetUserToken();
            return await this._userManager.GenerateEmailConfirmationTokenAsync(userId);
        }

        public void SetUserToken(string tokenKey = null)
        {
            //var provider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("DoctorsPursuit");

            //this._userManager.UserTokenProvider = new Microsoft.AspNet.Identity.Owin.DataProtectorTokenProvider<ApplicationUser>(provider.Create(tokenKey))
            //{
            //    //Code for email confirmation and reset password life time
            //    TokenLifespan = TimeSpan.FromHours(6)
            //};

            if (_userManager.UserTokenProvider == null)
            {
                var dataProtectorProvider = ApplicationUserManager.DataProtectionProvider;
                var dataProtector = dataProtectorProvider.Create("DoctorPursuit");
               // var dataProtector = dataProtectorProvider.Create("devilabc_doctorpursuit");
                _userManager.UserTokenProvider = new Microsoft.AspNet.Identity.Owin.DataProtectorTokenProvider<ApplicationUser, string>(dataProtector)
                {
                    TokenLifespan = TimeSpan.FromHours(24),
                };
            }
        }

        public async Task<IdentityResult> UpdateUserAsync(ApplicationUser user, params string[] propertiesToUpdate)
        {
            try
            {
                var record = await _userManager.FindByIdAsync(user.Id);

                if (record != null)
                {
                    foreach (var prop in propertiesToUpdate)
                    {
                        PropertyInfo pi = typeof(ApplicationUser).GetProperty(prop);
                        var val = pi.GetValue(user);
                        pi.SetValue(record, val);
                    }
                }
                return await _userManager.UpdateAsync(record);
            }
            catch
            {
                throw new Exception("UPDATE USER FAILED");
            }
            
        }

        public async Task<bool> IsEmailConfirmedAsync(string userId)
        {
            return await _userManager.IsEmailConfirmedAsync(userId);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(string userId)
        {
            SetUserToken();
            return await _userManager.GeneratePasswordResetTokenAsync(userId);
        }

        public async Task<IdentityResult> ResetPassword(string userId, string code, string password)
        {
            try
            {
                SetUserToken();
                return await _userManager.ResetPasswordAsync(userId, code, password);
            }
            catch
            {
                throw new Exception("RESET USER PASSWORD FAILED");
            }
          
        }

        public async Task<bool> ChangePassword(string userId, string oldPassword, string newPassword)
        {
            try
            {

                var result = await _userManager.ChangePasswordAsync(userId, oldPassword, newPassword);

                if (result.Succeeded)
                    return true;

                throw new Exception(result.Errors.First());
            }
            catch
            {
                throw new Exception("CHANGE USER PASSWORD FAILED");
            }
        }
    }
}
