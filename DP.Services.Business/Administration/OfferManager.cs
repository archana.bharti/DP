﻿using System.Collections.Generic;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using DP.Services.Data;
using System.Threading.Tasks;
using DP.Common.Utilities;
using DP.Common.Enums;
using System;
using System.Linq;
using System.Collections;
using DP.Services.Data.Models;
using DP.Common.Constants;

namespace DP.Services.Business.Administration
{
    public class OfferManager
    {
        IOfferDao offerDao;
        CommonManager commonManager;
        ICommonDao commonDao;
        UserManager userManager;
        public OfferManager(IOfferDao offerDao, CommonManager commonManager, ICommonDao commonDao, UserManager userManager)
        {
            this.offerDao = offerDao;
            this.commonManager = commonManager;
            this.commonDao = commonDao;
            this.userManager = userManager;
        }

        public List<FeaturedEntity> Subscribers()
        {
            var clinics = commonManager.GetFeaturedEntities((int)UserType.Doctor);
            var doctors = commonManager.GetFeaturedEntities((int)UserType.Clinic);
            var hospitals = commonManager.GetFeaturedEntities((int)UserType.Hospital);
            var colleges = commonManager.GetFeaturedEntities((int)UserType.College);
            var list = new List<FeaturedEntity>();
            if (doctors != null) { list.AddRange(doctors); }
            if (clinics != null) { list.AddRange(clinics); }
            if (hospitals != null) { list.AddRange(hospitals); }
            if (colleges != null) { list.AddRange(colleges); }
            return list;
        }

        public OfferMaster Add(OfferMaster offerMaster)
        {
            return this.offerDao.Add(offerMaster);
        }

        public async Task<OfferTransaction> Claim(OfferTransaction offerTransaction)
        {
            var query = this.offerDao.OfferTransactions();
            var appUser = this.commonDao.GetDpContext().Users.Where(x => x.Id == offerTransaction.UserId).SingleOrDefault();
            if (appUser == null) { throw new Exception("Not a valid user"); }

            offerTransaction.UserEmail = appUser.Email;
            offerTransaction.UserPhone = appUser.PhoneNumber;

            var offer = this.offerDao.GetById(new OfferMaster() { Id = offerTransaction.OfferMasterId });

            if(offerTransaction.SubscriberId != 0)
            {
                if(offer.ProviderId == offerTransaction.SubscriberId
                    && offerTransaction.SubscriberType != (int)UserType.User
                    && offer.ProviderType == offerTransaction.SubscriberType)
                {
                    throw new Exception("Offer is not allowed to claim by its owner");
                }
            }

            if (offer.OfferStatusValue == (int)OfferStatus.Deactive)
            {
                throw new Exception("This is offer is not valid/active any more. Please claim a different offer");
            }

            var isUserAlreadyClaimed = query.Where(x =>
                          x.OfferMasterId == offerTransaction.OfferMasterId &&
                          x.UserEmail == offerTransaction.UserEmail).Count() >= 1;

            if (isUserAlreadyClaimed)
            {
                throw new Exception("You have recently claimed this offer. Please choose a different offer to claim");
            }

            var count = query.Where(x => x.OfferMasterId == offerTransaction.OfferMasterId).Count();

            if (offer.Quantity == count)
            {
                // throw new Exception("You have reached maximum number of claims on this offer. Please claim a different offer.");
                throw new Exception("The maximum number of claims on this offer. Please choose a different offer to claim.");
            }
            offerTransaction.TotalRedeemCount = count + 1;
            offerTransaction.Token = RandomTokenGenerator.Generate(6, 12, false);
            offerTransaction.DateTime = DateTime.Now;
            offerTransaction = this.offerDao.Add(offerTransaction);

            await SendOfferEmail(offer, offerTransaction);

            return offerTransaction;
        }

        private async Task SendOfferEmail(OfferMaster offer, OfferTransaction offerTrans)
        {
            var endUserName = offerTrans.UserEmail.Substring(0, offerTrans.UserEmail.IndexOf('@'));
            var endUser = await this.userManager.GetByEmail(offerTrans.UserEmail);
            var adminEmail = GlobalSettings.ADMIN_EMAIL;
          

            var offerProviderDetails = this.commonManager.GetOfferProviderDetails(offer.ProviderId, (UserType)offer.ProviderType);
            await Task.Run(() => EmailManager.SendOfferClaim(offerTrans.UserEmail, new string[] { endUserName, offerTrans.Token }));
            await Task.Run(() => EmailManager.SendOfferIntimation(offerProviderDetails.Email,
                new string[] { offerProviderDetails.Name, endUserName, offerTrans.Token, offerTrans.DateTime.ToLongDateString() }));
            await Task.Run(() => EmailManager.SendOfferDetailsToAdmin(
                adminEmail,
                new string[] {
                    offerProviderDetails.Email,
                    offerProviderDetails.DisplayName,
                    offerTrans.UserEmail,
                    endUser.PhoneNumber,
                    offer.Id.ToString(),
                    offerTrans.Id.ToString(),
                    offerTrans.Token,
                    offerTrans.DateTime.ToLongDateString()
                }
                ));


            await SMSManager.Instance.SendOfferClaimToEntityUserAsync(offerProviderDetails.Phone_1, offerProviderDetails.DisplayName, offerTrans.Token);
            await SMSManager.Instance.SendOfferClaimToExternalUserAsync(endUser.PhoneNumber, endUser.DisplayName, offerTrans.Token, offerProviderDetails.Name);
        }

        public List<OfferMaster> Get(int? status = null)
        {
            List<OfferMaster> results = new List<OfferMaster>();

            var doctors = this.GetDoctorOffers(status);
            var clinics = this.GetClinicOffers(status);
            var hospitals = this.GetHospitalOffers(status);
            var colleges = this.GetCollegeOffers(status);
            var insurances = this.GetInsuranceOffers(status);

            if (doctors != null) { results.AddRange(doctors); }
            if (clinics != null) { results.AddRange(clinics); }
            if (hospitals != null) { results.AddRange(hospitals); }
            if (colleges != null) { results.AddRange(colleges); }
            if (insurances != null) { results.AddRange(insurances); }
            return results.OrderByDescending(x=>x.StartDateTime).ToList();
        }

        public List<OfferMaster> GetDoctorOffers(int? status = null)
        {
            var query = this.offerDao.Offers();
            var ctx = this.commonDao.GetDpContext();
            query = query.Where(x => x.ProviderType == (int)UserType.Doctor);
            var doctorOffers = (status.HasValue) ? query.Where(x => x.OfferStatusValue == status.Value).ToList() : query.ToList();

            var doctors = (from off in doctorOffers
                           join doc in ctx.ClinicsAndDoctors.Where(x => x.Type.ToLower() == "doctor")
                           on off.ProviderId equals doc.Id
                           select off.Set<OfferMaster>(x =>
                           {
                               x.ProviderName = doc.Name; x.ProviderTypeText = "doctor";
                               x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                               x.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == off.Id).Count();
                           })).ToList();

            return doctors;
        }

        public List<OfferMaster> GetClinicOffers(int? status = null)
        {
            var query = this.offerDao.Offers();
            var ctx = this.commonDao.GetDpContext();
            query = query.Where(x => x.ProviderType == (int)UserType.Clinic);
            var clinicOffers = (status.HasValue) ? query.Where(x => x.OfferStatusValue == status.Value).ToList() : query.ToList();

            var clinics = (from off in clinicOffers
                           join doc in ctx.ClinicsAndDoctors.Where(x => x.Type.ToLower() == "clinic")
                           on off.ProviderId equals doc.Id
                           select off.Set<OfferMaster>(x =>
                           {
                               x.ProviderName = doc.Name; x.ProviderTypeText = "clinic";
                               x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                               x.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == off.Id).Count();
                           })).ToList();

            return clinics;
        }

        public List<OfferMaster> GetHospitalOffers(int? status = null)
        {
            var query = this.offerDao.Offers();
            var ctx = this.commonDao.GetDpContext();
            query = query.Where(x => x.ProviderType == (int)UserType.Hospital);
            var hospitalOffers = (status.HasValue) ? query.Where(x => x.OfferStatusValue == status.Value).ToList() : query.ToList();

            var hospitals = (from off in hospitalOffers
                             join doc in ctx.Hospitals.Where(x => x.Type.ToLower() == "hospital")
                           on off.ProviderId equals doc.Id
                             select off.Set<OfferMaster>(x =>
                             {
                                 x.ProviderName = doc.Name; x.ProviderTypeText = "hospital";
                                 x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                 x.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == off.Id).Count();
                             })).ToList();

            return hospitals;
        }

        public List<OfferMaster> GetCollegeOffers(int? status = null)
        {
            var query = this.offerDao.Offers();
            var ctx = this.commonDao.GetDpContext();
            query = query.Where(x => x.ProviderType == (int)UserType.College);
            var collegeOffers = (status.HasValue) ? query.Where(x => x.OfferStatusValue == status.Value).ToList() : query.ToList();

            var colleges = (from off in collegeOffers
                            join doc in ctx.Hospitals.Where(x => x.Type.ToLower() == "college")
                             on off.ProviderId equals doc.Id
                            select off.Set<OfferMaster>(x =>
                            {
                                x.ProviderName = doc.Name; x.ProviderTypeText = "college";
                                x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                x.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == off.Id).Count();
                            })).ToList();

            return colleges;
        }

        public List<OfferMaster> GetInsuranceOffers(int? status = null)
        {
            var query = this.offerDao.Offers();
            var ctx = this.commonDao.GetDpContext();
            query = query.Where(x => x.ProviderType == (int)UserType.Insurance);
            var insuranceOffers = (status.HasValue) ? query.Where(x => x.OfferStatusValue == status.Value).ToList() : query.ToList();

            var insurances = (from off in insuranceOffers
                              join ins in ctx.Insurances
                           on off.ProviderId equals ins.Id
                              select off.Set<OfferMaster>(x =>
                              {
                                  x.ProviderName = ins.Name; x.ProviderTypeText = "insurance";
                                  x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                  x.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == off.Id).Count();
                              })).ToList();

            return insurances;
        }

        public List<OfferMaster> Get(int subscriberId, int type, bool includeTransactions = false)
        {
            var query = this.offerDao.Offers().Where(x => x.ProviderId == subscriberId);
            var ctx = this.commonDao.GetDpContext();
            var userType = (UserType)type;
            List<OfferMaster> results = new List<OfferMaster>();

            switch (userType)
            {
                case UserType.Clinic:
                    {
                        List<OfferMaster> clinicOffers = query.Where(x => x.ProviderType == (int)UserType.Clinic).ToList();
                        var clinics = (from off in clinicOffers
                                       join doc in ctx.ClinicsAndDoctors.Where(x => x.Type.ToLower() == "clinic")
                                       on off.ProviderId equals doc.Id
                                       let offTrans = (includeTransactions == false) ? null : ctx.OfferTransactions.Where(x => x.OfferMasterId == off.Id).ToList()
                                       //where off.OfferStatusValue == (int)OfferStatus.Active
                                       select off.Set<OfferMaster>(x =>
                                       {
                                           x.ProviderName = doc.Name; x.ProviderTypeText = "Clinic";
                                           x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                           if (includeTransactions)
                                           {
                                               x.OfferTransactions = offTrans;
                                           }
                                       })).ToList();
                        if (clinics != null) { results.AddRange(clinics); }
                    }
                    break;
                case UserType.Doctor:
                    {
                        var doctorOffers = query.Where(x => x.ProviderType == (int)UserType.Doctor).ToList();
                        var doctors = (from off in doctorOffers
                                       join doc in ctx.ClinicsAndDoctors.Where(x => x.Type.ToLower() == "doctor")
                                       on off.ProviderId equals doc.Id
                                       //where off.OfferStatusValue == (int)OfferStatus.Active
                                       let offTrans = (includeTransactions == false) ? null : ctx.OfferTransactions.Where(x => x.OfferMasterId == off.Id).ToList()
                                       select off.Set<OfferMaster>(x =>
                                       {
                                           x.ProviderName = doc.Name; x.ProviderTypeText = "Doctor";
                                           x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                           if (includeTransactions)
                                           {
                                               x.OfferTransactions = offTrans;
                                           }
                                       })).ToList();
                        if (doctors != null) { results.AddRange(doctors); }
                    }
                    break;
                case UserType.Hospital:
                    {
                        var hospitalOffers = query.Where(x => x.ProviderType == (int)UserType.Hospital).ToList();
                        var hospitals = (from off in hospitalOffers
                                         join doc in ctx.Hospitals.Where(x => x.Type.ToLower() == "hospital")
                                          on off.ProviderId equals doc.Id
                                         //where off.OfferStatusValue == (int)OfferStatus.Active
                                         let offTrans = (includeTransactions == false) ? null : ctx.OfferTransactions.Where(x => x.OfferMasterId == off.Id).ToList()
                                         select off.Set<OfferMaster>(x =>
                                         {
                                             x.ProviderName = doc.Name; x.ProviderTypeText = "Hospital";
                                             x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                             if (includeTransactions)
                                             {
                                                 x.OfferTransactions = offTrans;
                                             }
                                         })).ToList();
                        if (hospitals != null) { results.AddRange(hospitals); }
                    }
                    break;
                case UserType.College:
                    {
                        var collegeOffers = query.Where(x => x.ProviderType == (int)UserType.College).ToList();
                        var colleges = (from off in collegeOffers
                                        join doc in ctx.Hospitals.Where(x => x.Type.ToLower() == "college")
                                         on off.ProviderId equals doc.Id
                                        //where off.OfferStatusValue == (int)OfferStatus.Active
                                        let offTrans = (includeTransactions == false) ? null : ctx.OfferTransactions.Where(x => x.OfferMasterId == off.Id).ToList()
                                        select off.Set<OfferMaster>(x =>
                                        {
                                            x.ProviderName = doc.Name; x.ProviderTypeText = "College";
                                            x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                            if (includeTransactions)
                                            {
                                                x.OfferTransactions = offTrans;
                                            }
                                        })).ToList();
                        if (colleges != null) { results.AddRange(colleges); }
                    }
                    break;
                case UserType.Insurance:
                    {
                        var insuranceOffers = query.Where(x => x.ProviderType == (int)UserType.Insurance).ToList();
                        var insurances = (from off in insuranceOffers
                                          join ins in ctx.Insurances
                                         on off.ProviderId equals ins.Id
                                          //where off.OfferStatusValue == (int)OfferStatus.Active
                                          let offTrans = (includeTransactions == false) ? null : ctx.OfferTransactions.Where(x => x.OfferMasterId == off.Id).ToList()
                                          select off.Set<OfferMaster>(x =>
                                        {
                                            x.ProviderName = ins.Name; x.ProviderTypeText = "Insurance";
                                            x.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)off.OfferStatusValue);
                                            if (includeTransactions)
                                            {
                                                x.OfferTransactions = offTrans;
                                            }
                                        })).ToList();
                        if (insurances != null) { results.AddRange(insurances); }
                    }
                    break;
            }

            return results;
        }

        public OfferMaster GetById(int id)
        {
            var offer = this.offerDao.GetById(new OfferMaster() { Id = id });
            var ctx = this.commonDao.GetDpContext();
            var userType = (UserType)offer.ProviderType;
            List<OfferMaster> results = new List<OfferMaster>();

            switch (userType)
            {
                case UserType.Clinic:
                    {
                        var clinic = ctx.ClinicsAndDoctors.Where(x => x.Id == offer.ProviderId).SingleOrDefault();
                        if (clinic != null)
                        {
                            offer.ProviderName = clinic.Name;
                            offer.ProviderTypeText = "Clinic";
                            offer.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)offer.OfferStatusValue);
                            offer.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == id).Count();
                        }
                    }
                    break;
                case UserType.Doctor:
                    {
                        var doctor = ctx.ClinicsAndDoctors.Where(x => x.Id == offer.ProviderId).SingleOrDefault();
                        if (doctor != null)
                        {
                            offer.ProviderName = doctor.Name;
                            offer.ProviderTypeText = "Doctor";
                            offer.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)offer.OfferStatusValue);
                            offer.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == id).Count();
                        }
                    }
                    break;
                case UserType.Hospital:
                    {
                        var hos = ctx.Hospitals.Where(x => x.Id == offer.ProviderId).SingleOrDefault();
                        if (hos != null)
                        {
                            offer.ProviderName = hos.Name;
                            offer.ProviderTypeText = "Hospital";
                            offer.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)offer.OfferStatusValue);
                            offer.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == id).Count();
                        }
                    }
                    break;
                case UserType.College:
                    {
                        var col = ctx.Colleges.Where(x => x.Id == offer.ProviderId).SingleOrDefault();
                        if (col != null)
                        {
                            offer.ProviderName = col.Name;
                            offer.ProviderTypeText = "College";
                            offer.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)offer.OfferStatusValue);
                            offer.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == id).Count();
                        }
                        break;
                    }
                case UserType.Insurance:
                    {
                        var ins = ctx.Insurances.Where(x => x.Id == offer.ProviderId).SingleOrDefault();
                        if (ins != null)
                        {
                            offer.ProviderName = ins.Name;
                            offer.ProviderTypeText = "Insurance";
                            offer.OfferStatusText = EnumHelper<OfferStatus>.GetDescription((OfferStatus)offer.OfferStatusValue);
                            offer.TransactionCount = ctx.OfferTransactions.Where(z => z.OfferMasterId == id).Count();
                        }
                    }
                    break;
            }
            return offer;
        }

        public OfferMaster Update(OfferMaster offerMaster)
        {
            return this.offerDao.Update(offerMaster);
        }

    }
}
