﻿using System.Collections.Generic;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using DP.Services.Data;
using System.Threading.Tasks;
using DP.Common.Utilities;
using DP.Common.Enums;
using System;
using System.Linq;
using System.Collections;
using System.Data.Linq;
using System.Data.Entity;
using DP.Services.Data.Models;

namespace DP.Services.Business.Administration
{
    public sealed class PdaSubscriptionManager
    {
        ICommonDao commonDao;
        IPdaSubscriptionDao pdaSubscriptionDao;
        public PdaSubscriptionManager(ICommonDao commonDao, IPdaSubscriptionDao pdaSubscriptionDao)
        {
            this.commonDao = commonDao;
            this.pdaSubscriptionDao = pdaSubscriptionDao;
        }

        public bool HasSubscriptionWithSpecializationActive(int doctorOrClinicId, int specializationId)
        {
            var query = this.pdaSubscriptionDao.Get();
            query = query.Where(x => x.DoctorOrClinicId == doctorOrClinicId &&
                                   x.SpecializationId == specializationId &&
                                   //TODO: Date fall check
                                   x.StatusValue == (int)PdaSubscriptionStatus.Active);
            return (query.SingleOrDefault() != null);
        }

        public async Task<PdaSubscription> Add(PdaSubscription model)
        {
            var result = await this.pdaSubscriptionDao.Add(model);
            return result;
        }

        public async Task<List<PdaSubscription>> Get(int doctorOrClinicId)
        {
            return await this.pdaSubscriptionDao.GetByDoctorOrClinicIdAsync(doctorOrClinicId);
        }

        public async Task<List<PdaSubscription>> Get()
        {
            return await this.pdaSubscriptionDao.Get()
                .Include(x => x.Status)
                .Include(x => x.DoctorOrClinic)
                .Include(x => x.Specialization)
                .Include(x => x.Specialization.Specialization)
                .OrderByDescending(x => x.StartDate).ToListAsync();
        }

        public List<SpecializationClinic> GetSpecializations(int doctorOrClinicId)
        {
            return this.commonDao.GetDpContext()
                .SpecializationClinics
                .Where(x => x.ClinicId == doctorOrClinicId)
                .Include(x => x.Specialization).ToList();
        }

        public async Task Deactivate(int id)
        {
            var model = await this.pdaSubscriptionDao.GetByIdAsync(id);
            model.StatusValue = (int)PdaSubscriptionStatus.Deactive;
            await this.pdaSubscriptionDao.UpdateAsync(model);
        }
    }
}
