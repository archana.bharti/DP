﻿using System.Collections.Generic;
using DP.Common.Models;
using Admin = DP.Common.Models.Administration;
using DP.Services.Data.UnitOfWork;
using DP.Services.Data;
using System.Threading.Tasks;
using DP.Common.Utilities;
using DP.Common.Enums;
using System;
using System.Linq;
using System.Collections;
using DP.Services.Data.Models;
using DP.Common.Constants;

namespace DP.Services.Business.Administration
{
    public class SubscriberManager
    {
        ICommonDao commonDao;
        IFeatureDao featureDao;
        IUserDao userDao;
        UserManager userManager;
        public SubscriberManager(ICommonDao commonDao, IFeatureDao featureDao, IUserDao userDao, UserManager userManager)
        {
            this.commonDao = commonDao;
            this.featureDao = featureDao;
            this.userDao = userDao;
        }

        public List<Admin.SubscriberViewModel> GetClinicOrDoctorSubscribers()
        {
            return this.commonDao.GetClinicOrDoctorSubscribers();
        }

        public List<Admin.SubscriberViewModel> GetHospitalSubscribers()
        {
            return this.commonDao.GetHospitalSubscribers();
        }

        public List<Admin.SubscriberViewModel> GetCollegeSubscribers()
        {
            return this.commonDao.GetCollegeSubscribers();
        }

        public List<Admin.SubscriberViewModel> GetInsuranceSubscribers()
        {
            return this.commonDao.GetInsuranceSubscribers();
        }

        public Admin.SubscriberViewModel Details(string userId, UserType type)
        {
            switch (type)
            {
                case UserType.Clinic:
                    return this.commonDao.GetClinicSubscriber(userId);
                case UserType.Doctor:
                    return this.commonDao.GetDoctorSubscriber(userId);
                case UserType.Hospital:
                    return this.commonDao.GetHospitalSubscriber(userId);
                case UserType.College:
                    return this.commonDao.GetCollegeSubscriber(userId);
                case UserType.Insurance:
                    return this.commonDao.GetInsuranceSubscriber(userId);
            }

            return null;
        }

        public async Task<Feature> SubscribeToPlan(SubscriberPlanViewModel viewModel)
        {
            try
            {
                var dbModel = ModelFactory.ConvertToFeatureDbm(viewModel);
                var savedFeature = await this.featureDao.GetAsync(dbModel);
                if (savedFeature == null)
                {
                    dbModel.StatusValue = (int)UserStatus.Active;
                    dbModel = await featureDao.CreateAsync(dbModel);

                    //set IsVerified
                    ApplicationUser appUser = await userDao.GetBySubscriptionAsync(viewModel.EntityId, EnumHelper<UserType>.GetDescription((UserType)viewModel.EntityType));
                    appUser.IsVerified = true;
                    await userDao.UpdateAsync(appUser, new string[] { "IsVerified" });
                    string appURL = GlobalSettings.CLIENT_URL;
                    await Task.Run(()=>EmailManager.SendApprovalSuccess(appUser.Email, appURL));
                    await SMSManager.Instance.SendUnderVerificationSuccessAsync(appUser.PhoneNumber, appUser.DisplayName);
                    return dbModel;
                }
                else
                {
                    //TODO: Need to improve this code by reducing 2-3 lines
                    savedFeature.EndDate = dbModel.EndDate;
                    savedFeature.StartDate = dbModel.StartDate;
                    savedFeature.EnableBasic = dbModel.EnableBasic;
                    savedFeature.EnableFeatured = dbModel.EnableFeatured;
                    savedFeature.EnableTextual = dbModel.EnableTextual;
                    savedFeature.StatusValue = 4;
                    await featureDao.UpdateAsync(savedFeature);
                    return savedFeature;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dictionary<string, List<Feature>> Subscribers()
        {
            return featureDao.Features()
                             .ToList()
                             .GroupBy(f => f.EntityType)
                             .OrderBy(f => f.Key)
                             .ToDictionary(f => EnumHelper<UserType>.GetDescription((UserType)f.Key), f => f.ToList());
        }
    }
}
