﻿using System.Collections.Generic;
using DP.Common.Models;
using DP.Services.Data.UnitOfWork;
using DP.Services.Data;
using System.Threading.Tasks;
using DP.Common.Utilities;
using DP.Common.Enums;
using System;
using System.Linq;
using System.Collections;

namespace DP.Services.Business.Administration
{  
    public class DatabaseManager
    {
        ICommonDao commonDao;
        public DatabaseManager(ICommonDao commonDao)
        {
            this.commonDao = commonDao;
        }

        public List<DatabaseTableViewModel> GetTableNames()
        {
            return this.commonDao.GetTableNames();
        }

        public object GetTableRows(string dbTable)
        {
            var context = this.commonDao.GetDpContext();
            string query = string.Format("select * from  {0}", dbTable);
            string entity = this.commonDao.GetTableNames().Where(x => x.TableName == dbTable).Single().EntityName;
            Type entityInstanceType = Type.GetType("DP.Services.Data.Models." + entity +", DP.Models");

            var results = context.GetDataTable(query).ToList(entityInstanceType);
             
            return Format(results, entityInstanceType, dbTable);
        }       

        private object Format(IList data, Type type, string table)
        {
            var props = type.GetProperties();
            List<GridColumn> Columns = new List<GridColumn>();
            foreach (var prop in props)
            {
                Columns.Add(new GridColumn() { Field = Char.ToLowerInvariant(prop.Name[0]) + prop.Name.Substring(1), Title = prop.Name });
            }

            return new
            {
                Columns = Columns,
                Data = data,
                Table = table
            };
        }
    }
}
