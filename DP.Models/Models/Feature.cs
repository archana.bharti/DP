﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_feature")]
    public class Feature : ModelBase
    {
        public int EntityId { get; set; }
        public int EntityType { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        [DefaultValue(0)]
        public int StatusValue { get; set; }
        [ForeignKey("StatusValue")]
        public virtual EnumStatus Status { get; set; }

        [DefaultValue(0)]
        public bool EnableBasic { get; set; }
        [DefaultValue(0)]
        public bool EnableTextual { get; set; }
        [DefaultValue(0)]
        public bool EnableFeatured { get; set; }

        [NotMapped]
        public string Formatted_StartDate { get; set; }
        [NotMapped]
        public string Formatted_EndDate { get; set; }
        [NotMapped]
        public int EntityTypeText { get; set; }

        public override string ToString()
        {
            return this.Id.ToString() + "-" + this.EntityId.ToString() + "-" + this.EntityType; 
        }
    }
}
