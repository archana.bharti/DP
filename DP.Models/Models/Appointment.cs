﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP.Services.Data.Models
{
    [Table("dp_appointment")]
    public class Appointment : ModelBase
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Message { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int EntityId { get; set; }
        public int EntityType { get; set; }
        public string Token { get; set; }
        public int StatusValue { get; set; }

        [NotMapped]
        public string EntityName { get; set; }
    }
}
