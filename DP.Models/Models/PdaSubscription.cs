﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_pda_subscription")]
    public class PdaSubscription : ModelBase
    {
        public int DoctorOrClinicId { get; set; }
        [ForeignKey("DoctorOrClinicId")]
        public ClinicAndDoctor DoctorOrClinic { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [DefaultValue(4)]
        public int StatusValue { get; set; }
        [ForeignKey("StatusValue")]
        public virtual EnumStatus Status { get; set; }
        public int SpecializationId { get; set; }
        [ForeignKey("SpecializationId")]
        public SpecializationClinic Specialization { get; set; }

        public override string ToString()
        {
            return this.DoctorOrClinicId.ToString()+"-" + this.DoctorOrClinic.Name;
        }
    }
}
