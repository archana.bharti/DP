﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_insurance")]
    public class Insurance : ModelBase
    {
        [MaxLength(512)]
        public string ProviderName { get; set; }
        [MaxLength(512)]
        public string Summary { get; set; }
        [MaxLength(512)]
        public string PlanName { get; set; }
        [MaxLength(64)]
        public string PlanType { get; set; }
        [MaxLength(512)]
        public string ImageUrl { get; set; }
        [MaxLength(512)]
        public string Address { get; set; }
        [MaxLength(32)]
        public string Phone_No1 { get; set; }
        [MaxLength(32)]
        public string Phone_No2 { get; set; }
        [MaxLength(32)]
        public string Phone_No3 { get; set; }
        [MaxLength(32)]
        public string Phone_No4 { get; set; }

        //visbility and primary contact of the phone numbers
        [MaxLength(8)]
        [DefaultValue(1)]
        public string Display_Phone { get; set; }

        [DefaultValue(0)]
        public bool CashlessFacility { get; set; }
        [DefaultValue(0)]
        public bool MaternityBenefits { get; set; }
        [DefaultValue(0)]
        public bool AmbulanceCharges { get; set; }
        [DefaultValue(0)]
        public bool DayCareProcedures { get; set; }
        [DefaultValue(0)]
        public bool HealthCheckup { get; set; }
        [DefaultValue(0)]
        public bool TaxBenefits { get; set; }
        [DefaultValue(0)]
        public bool HospitalizationExpenses { get; set; }
        [DefaultValue(0)]
        public bool FreelookPeriod { get; set; }
        [DefaultValue(0)]
        public bool AlternativeTreatment { get; set; }
        [DefaultValue(0)]
        public bool CumulativeBonus { get; set; }
        [DefaultValue(0)]
        public bool OrganDonorExpenses { get; set; }
        [DefaultValue(0)]
        public bool PrePostHospitalizationCharges { get; set; }
        [DefaultValue(0)]
        public bool Portability { get; set; }
        [MaxLength(512)]
        public string Website { get; set; }
        [MaxLength(512)]
        public string VideoUrl { get; set; }

        public IList<InsuranceRating> Ratings { get; set; }

        [NotMapped]
        public int RatingCount { get
            {
                return (this.Ratings != null) ? Ratings.Count() : 0;
            }
        }

        [NotMapped]
        public int RecommendedCount
        {
            get; set;
        }

        [NotMapped]
        public bool IsFeatured { get; set;}

        [NotMapped]
        public bool IsRecommended { get; set; }

        [NotMapped]  //ALIAS to maintain consistency
        public string Name {
            //set
            //{
            //    this.ProviderName = value;
            //}

            get {
                return 
                    string.Format("{0}, {1}",this.PlanName, this.PlanType);
            }
        }

        [NotMapped]  
        public int LocationId = 0;
        [NotMapped]  
        public int StateId = 0;
        [NotMapped] 
        public int CityId = 0;

        [NotMapped]
        public List<string> DisplayContactList
        {
            get
            {
                var list = this.Display_Phone == null ? null : this.Display_Phone.Split(new char[','], StringSplitOptions.RemoveEmptyEntries).ToList<string>();
                return list;
            }
        }
    }
}
