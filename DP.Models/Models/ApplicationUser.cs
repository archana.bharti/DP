﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string UserType { get; set; }
      
        [MaxLength(256)]
        public string PictureUrl { get; set; }     
         
        public DateTime? LastSignInOn { get; set; }

        public DateTime CreatedOn { get; set; }
        
        public int? SubscriberId { get; set; }

        public int? StateId { get; set; }
        [ForeignKey("StateId")]
        public virtual State State { get; set; }

        public int? CityId { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        public int? LocationId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }
        
        public string DisplayName { get; set; }

        public bool IsVerified { get; set; }

        public bool IsTerminated { get; set; }

        [NotMapped]
        public string CreatedOn2
        {
            get
            {
                return this.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
    }
}
