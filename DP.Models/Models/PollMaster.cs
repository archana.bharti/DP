﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP.Services.Data.Models
{
    [Table("dp_poll_master")]
    public class PollMaster : ModelBase
    {
        [MaxLength(512)]
        public string Question { get; set; }
        [DefaultValue(1)]
        public bool Active { get; set; }
        private DateTime _lastUpdatedOn;
        public DateTime LastUpdatedOn
        {
            get
            {
                return _lastUpdatedOn;
            }
            set
            {
                _lastUpdatedOn = DateTime.Now;
            }
        }
        private string _yesText;
        public string YesText
        {
            get
            {
                return _yesText;
            }
            set
            {
                var tmp = value;
                if (string.IsNullOrEmpty(tmp) || string.IsNullOrWhiteSpace(tmp)) _yesText = "Yes";
                else _yesText = tmp;
            }
        }
        private string _noText;
        public string NoText
        {
            get
            {
                return _noText;
            }
            set
            {
                var tmp = value;
                if (string.IsNullOrEmpty(tmp) || string.IsNullOrWhiteSpace(tmp)) _noText = "No";
                else _noText = tmp;
            }
        }
    }

    public class PollMasterView
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public bool Active { get; set; }
        private DateTime _lastUpdatedOn;
        public DateTime LastUpdatedOn
        {
            get
            {
                return _lastUpdatedOn;
            }
            set
            {
                _lastUpdatedOn = DateTime.Now;
            }
        }
        private string _yesText;
        public string YesText
        {
            get
            {
                return _yesText;
            }
            set
            {
                var tmp = value;
                if (string.IsNullOrEmpty(tmp) || string.IsNullOrWhiteSpace(tmp)) _yesText = "Yes";
                else _yesText = tmp;
            }
        }
        private string _noText;
        public string NoText
        {
            get
            {
                return _noText;
            }
            set
            {
                var tmp = value;
                if (string.IsNullOrEmpty(tmp) || string.IsNullOrWhiteSpace(tmp)) _noText = "No";
                else _noText = tmp;
            }
        }
        
        public int YesCount { get; set; }
        public int TotalYesCount { get; set; }
        public float YesPercentage
        {
            get
            {
                if (TotalYesCount == 0) return 0f;
                return
                   (float)Math.Round(((float)this.YesCount / (float)TotalYesCount) * 100f, 2); 
            }
        }
        public int NoCount { get; set; }
        public int TotalNoCount { get; set; }
        public float NoPercentage
        {
            get
            {
                if (TotalNoCount == 0) return 0f;
                return
                  (float)Math.Round(((float)this.NoCount / (float)TotalNoCount) * 100f, 2);
            }
        }
    }
}
