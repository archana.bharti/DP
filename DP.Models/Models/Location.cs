﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_location")]
    public class Location : ModelBase
    {
        [MaxLength(256)]
        public string Name { get; set; }

        public int CityId { get; set; }
        [ForeignKey("CityId")]
        public City City { get; set; }

        public int Pincode { get; set; }
        public float? Longitude { get; set; }
        public float? Latitude { get; set; }
        public Int16? Zone { get; set; }
    }
}
