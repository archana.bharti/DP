﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_std_code")]
    public class STDCode : ModelBase
    {
        [MaxLength(25)]
        public string State { get; set; }
        [MaxLength(256)]
        public string Capital { get; set; }
        public int Code { get; set; }
    }
}
