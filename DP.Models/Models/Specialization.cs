﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_specialization")]
    public class Specialization : ModelBase
    {
        [MaxLength(256)]
        public string Category { get; set; }
        [MaxLength(256)]
        public string Group { get; set; }
        public int Specific { get; set; }
    }
}
