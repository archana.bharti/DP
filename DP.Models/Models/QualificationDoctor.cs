﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_qualification_doctor")]
    public class QualificationDoctor : ModelBase
    {
        public int DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        public ClinicAndDoctor Doctor { get; set; }

        public int? ShortQualificationId { get; set; }
        [ForeignKey("ShortQualificationId")]
        public QualificationShort ShortQualification { get; set; }

        public int? LongQualificationId { get; set; }
        [ForeignKey("LongQualificationId")]
        public QualificationLong LongQualification { get; set; }
    }
}
