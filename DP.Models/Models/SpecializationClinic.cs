﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_specialization_clinic")]
    public class SpecializationClinic : ModelBase
    {
        public int ClinicId { get; set; }
        [ForeignKey("ClinicId")]
        public ClinicAndDoctor Clinic { get; set; }

        public int SpecializationId { get; set; }
        [ForeignKey("SpecializationId")]
        public Specialization Specialization { get; set; }
    }
}
