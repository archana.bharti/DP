﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace DP.Services.Data.Models
{
    [Table("dp_offer_master")]
    public class OfferMaster : ModelBase
    {
        public int ProviderId { get; set; }
        public int ProviderType { get; set; }
        [MaxLength(64)]
        public string ProviderEmail { get; set; }

       [AllowHtml]
        public string Content { get; set; }
        public float CurrentPrice { get; set; }
        public float DiscountedPercentage { get; set; }
        public float ActualPrice { get; set; }
        public byte[] ContentImage { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }

        public int OfferStatusValue { get; set; }
        [ForeignKey("OfferStatusValue")]
        public EnumStatus OfferStatus { get; set; }
        public int Quantity { get; set; }

        #region NotMapped
        [NotMapped]
        public string ProviderName { get; set; }
        [NotMapped]
        public string ProviderTypeText { get; set; }
        [NotMapped]
        public string OfferStatusText { get; set; }
        [NotMapped]
        public int TransactionCount { get; set; }

        [NotMapped]
        public List<OfferTransaction> OfferTransactions { get; set; }
        #endregion

        public override string ToString() {
            return this.Id.ToString() + "-" + this.ProviderId.ToString();
        }
    }
}
