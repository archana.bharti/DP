﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DP.Services.Data.Models
{
    [Table("dp_recommend")]
    public class Recommend : ModelBase
    {
        [MaxLength(32)]
        public string IpAddress { get; set; }
        public int EntityType { get; set; }
        public int EntityId { get; set; }
        [NotMapped]
        public bool Remove { get; set; }
    }
}
