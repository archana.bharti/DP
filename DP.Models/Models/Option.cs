﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP.Services.Data.Models
{
    [Table("dp_options")]
    public class Option
    {
        [Key]
        public string Key { get; set; }
        public string Value { get; set; }
        public int? Context { get; set; }
    }
}
