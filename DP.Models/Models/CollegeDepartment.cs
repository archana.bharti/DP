﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_college_department")]
    public class CollegeDepartment : ModelBase
    {
        [MaxLength(256)]
        public string Name { get; set; }
        public int Category { get; set; }
    }
}
