﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_state")]
    public class State : ModelBase
    {
        public string Name { get; set; }
        public string ShortCode { get; set; }
    }
}
