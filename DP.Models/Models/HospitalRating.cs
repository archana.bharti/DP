﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_hospital_rating")]
    public class HospitalRating : ModelBase
    {
        [MaxLength(128)]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public int HospitalId{ get; set; }
        [ForeignKey("HospitalId")]
        public Hospital Hospital { get; set; }

        public int Infrastructure { get; set; }
        public float InfrastructureSumOfRating { get; set; }
        public float InfrastructureAverage { get; set; }
        public float InfrastructurePercentage { get; set; }
        
        public int SafetyCleanliness { get; set; }
        public float SafetyCleanlinessSumOfRating { get; set; }
        public float SafetyCleanlinessAverage { get; set; }
        public float SafetyCleanlinessPercentage { get; set; }

        public int CourteousStaff { get; set; }
        public float CourteousStaffSumOfRating { get; set; }
        public float CourteousStaffAverage { get; set; }
        public float CourteousStaffPercentage { get; set; }

        public int CafeFood { get; set; }
        public float CafeFoodSumOfRating { get; set; }
        public float CafeFoodAverage { get; set; }
        public float CafeFoodPercentage { get; set; }

        public int QualityOfCare { get; set; }
        public float QualityOfCareSumOfRating { get; set; }
        public float QualityOfCareAverage { get; set; }
        public float QualityOfCarePercentage { get; set; }
        
        public int VistingHours { get; set; }
        public float VistingHoursSumOfRating { get; set; }
        public float VistingHoursAverage { get; set; }
        public float VistingHoursPercentage { get; set; }

        public int OvernightStay { get; set; }
        public float OvernightStaySumOfRating { get; set; }
        public float OvernightStayAverage { get; set; }
        public float OvernightStayPercentage { get; set; }

        public int EmergencyCare { get; set; }
        public float EmergencyCareSumOfRating { get; set; }
        public float EmergencyCareAverage { get; set; }
        public float EmergencyCarePercentage { get; set; }

        public int OverallSatisfaction { get; set; }
        public float OverallSatisfactionSumOfRating { get; set; }
        public float OverallSatisfactionAverage { get; set; }
        public float OverallSatisfactionPercentage { get; set; }

        public DateTime ReviewedDate { get; set; }
        public string LastVisited { get; set; }
    }
}
