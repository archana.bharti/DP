﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_college")]
    public class College : SearchEntityBase
    {
        public int? DepartmentType { get; set; }

       // [ForeignKey("DepartmentType")]
       // public CollegeDepartment Department { get; set; }

        [MaxLength(256)]
        public string Affiliated { get; set; }
        [MaxLength(256)]
        public string Approved { get; set; }

        [MaxLength(256)]
        public string PlacementSummary { get; set; }

        [MaxLength(256)]
        public string Established { get; set; }

        [MaxLength(150)]
        public string Management { get; set; }
    }
}