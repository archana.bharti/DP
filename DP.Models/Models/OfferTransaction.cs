﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_offer_transaction")]
    public class OfferTransaction : ModelBase
    {
        public int OfferMasterId { get; set; }
        [ForeignKey("OfferMasterId")]
        public OfferMaster OfferMaster { get; set; }
        public string UserEmail { get; set; }
        public string UserPhone { get; set; }
        public int TotalRedeemCount { get; set; }
        public string Token { get; set; }
        public DateTime DateTime { get; set; }
        public string IpAddress { get; set; }

        [NotMapped]
        public string UserId { get; set; }
        [NotMapped]
        public int SubscriberId { get; set; }
        [NotMapped]
        public int SubscriberType { get; set; }
    }
}
