﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_qualification_long")]
    public class QualificationLong : ModelBase
    {
        [MaxLength(256)]
        public string LongEndDegree { get; set; }
    }
}
