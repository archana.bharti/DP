﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_insurance_rating")]
    public class InsuranceRating : ModelBase
    {
        public int InsuranceId { get; set; }
        [ForeignKey("InsuranceId")]
        public Insurance Insurance { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public int CustomerService { get; set; }
        public float CustomerServiceSumOfRating { get; set; }
        public float CustomerServiceAverage { get; set; }
        public float CustomerServicePercentage { get; set; }

        public int InNetworkFacilities { get; set; }
        public float InNetworkFacilitiesSumOfRating { get; set; }
        public float InNetworkFacilitiesAverage { get; set; }
        public float InNetworkFacilitiesPercentage { get; set; }

        public int EaseOfClaimFilling { get; set; }
        public float EaseOfClaimFillingSumOfRating { get; set; }
        public float EaseOfClaimFillingAverage { get; set; }
        public float EaseOfClaimFillingPercentage { get; set; }

        public int MeetsPromisedBenefits { get; set; }
        public float MeetsPromisedBenefitsSumOfRating { get; set; }
        public float MeetsPromisedBenefitsAverage { get; set; }
        public float MeetsPromisedBenefitsPercentage { get; set; }

        public int ValueForMoney { get; set; }
        public float ValueForMoneySumOfRating { get; set; }
        public float ValueForMoneyAverage { get; set; }
        public float ValueForMoneyPercentage { get; set; }

        public int ClaimSettlement { get; set; }
        public float ClaimSettlementSumOfRating { get; set; }
        public float ClaimSettlementAverage { get; set; }
        public float ClaimSettlementPercentage { get; set; }

        public int OverallSatisfaction { get; set; }
        public float OverallSatisfactionSumOfRating { get; set; }
        public float OverallSatisfactionAverage { get; set; }
        public float OverallSatisfactionPercentage { get; set; }

        public DateTime ReviewedDate { get; set; }
    }
}
