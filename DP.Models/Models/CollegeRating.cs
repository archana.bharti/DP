﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_college_rating")]
    public class CollegeRating : ModelBase
    {
        [MaxLength(128)]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public int CollegeId { get; set; }
        [ForeignKey("CollegeId")]
        public College College { get; set; }

        public int QualityOfEducation { get; set; }
        public float QualityOfEducationSumOfRating { get; set; }
        public float QualityOfEducationAverage { get; set; }
        public float QualityOfEducationPercentage { get; set; }

        public int Location { get; set; }
        public float LocationSumOfRating { get; set; }
        public float LocationAverage { get; set; }
        public float LocationPercentage { get; set; }
        
        public int Library { get; set; }
        public float LibrarySumOfRating { get; set; }
        public float LibraryAverage { get; set; }
        public float LibraryPercentage { get; set; }
        
        public int Campus { get; set; }
        public float CampusSumOfRating { get; set; }
        public float CampusAverage { get; set; }
        public float CampusPercentage { get; set; }

        public int Food { get; set; }
        public float FoodSumOfRating { get; set; }
        public float FoodAverage { get; set; }
        public float FoodPercentage { get; set; }
        
        public int SocialActivities { get; set; }
        public float SocialActivitiesSumOfRating { get; set; }
        public float SocialActivitiesAverage { get; set; }
        public float SocialActivitiesPercentage { get; set; }
        
        public int IntenetConnectivity { get; set; }
        public float IntenetConnectivitySumOfRating { get; set; }
        public float IntenetConnectivityAverage { get; set; }
        public float IntenetConnectivityPercentage { get; set; }
        
        public int OverallSatisfaction { get; set; }
        public float OverallSatisfactionSumOfRating { get; set; }
        public float OverallSatisfactionAverage { get; set; }
        public float OverallSatisfactionPercentage { get; set; }

        public DateTime ReviewedDate { get; set; }
    }
}
