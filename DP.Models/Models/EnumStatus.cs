﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DP.Services.Data.Models
{
    [Table("dp_enum_status")]
    public class EnumStatus : ModelBase
    {
        public string Text { get; set; }
        public string Description { get; set; }
    }
}
