﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_college_location")]
    public class CollegeLocation : ModelBase
    {
        [MaxLength(64)]
        public string Name { get; set; }
        public int StateId { get; set; }
        public int Zone { get; set; }

        [NotMapped]
        public State State { get; set; }
    }
}
