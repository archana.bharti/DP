﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DP.Services.Data.Models
{
    [Table("dp_endorsement")]
    public class Endorsement : ModelBase
    {
        public int EndorseeId { get; set; }
        public int EndorseeType { get; set; }
        public int EndorserId { get; set; }
        public int EndorserType { get; set; }
        public int SpecializationId { get; set; }
        //[ForeignKey("SpecializationId")]
        //public Specialization Specialization { get; set; }
    }
}
