﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_personaladvice")]
    public class PersonalAdvice : ModelBase
    {
        [MaxLength(512)]
        public string Subject { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }

        public int SpecializationId { get; set; }
        [ForeignKey("SpecializationId")]
        public Specialization Specialization { get; set; }

        public int? CityId { get; set; }
        [ForeignKey("CityId")]
        public City City { get; set; }

        [MaxLength(512)]
        public string UserEmail { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public int UserEmailStatusValue { get; set; }
        [ForeignKey("UserEmailStatusValue")]
        public EnumStatus UserEmailStatus { get; set; }

        public DateTime SubmittedDate { get; set; }
        public DateTime? RepliedDate { get; set; }

        public int DoctorId { get; set; }
        [ForeignKey("DoctorId")]
        public ClinicAndDoctor Doctor { get; set; }

        [MaxLength(512)]
        public string DoctorEmail { get; set; }
        public int DoctorEmailStatusValue { get; set; }
        [ForeignKey("DoctorEmailStatusValue")]
        public EnumStatus DoctorEmailStatus { get; set; }
    }
}
