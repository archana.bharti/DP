﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DP.Services.Data.Models
{
    [Table("dp_bookmarks")]
    public class Bookmark : ModelBase
    {
        public int EntityId { get; set; }
        public int EntityType { get; set; }
        public int BookmarkEntityId { get; set; }
        public int BookmarkEntityType { get; set; }

        [NotMapped]
        public string BookmarkEntityName { get; set; }
        [NotMapped]
        public string BookmarkEntityTypeText { get; set; }
        [NotMapped]
        public string BookmarkImageUrl { get; set; }
    }
}
