﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_qualification_short")]
    public class QualificationShort : ModelBase
    {
        [MaxLength(256)]
        public string ShortEndDegree { get; set; }
    }
}
