﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    public abstract class SearchEntityBase : ModelBase
    {
        [MaxLength(256)]
        public string Type { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        public string Summary { get; set; }

        [MaxLength(256)]
        public string FullAddress { get; set; }

        public int? LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }

        public int? CityId { get; set; }

        [ForeignKey("CityId")]
        public City City { get; set; }

        public int? StateId { get; set; }

        [ForeignKey("StateId")]
        public State State { get; set; }

        [MaxLength(32)]
        public string Phone_No1 { get; set; }
        [MaxLength(32)]
        public string Phone_No2 { get; set; }
        [MaxLength(32)]
        public string Phone_No3 { get; set; }
        [MaxLength(32)]
        public string Phone_No4 { get; set; }

        [MaxLength(256)]
        public string Profile_Pic_1 { get; set; }
        [MaxLength(256)]
        public string Profile_Pic_2 { get; set; }
        [MaxLength(256)]
        public string Profile_Pic_3 { get; set; }

        [MaxLength(8)]
        [DefaultValue(1)]
        public string Display_Phone { get; set; }

        [MaxLength(256)]
        public string Website { get; set; }

        [MaxLength(512)]
        public string VideoUrl { get; set; }

        [NotMapped]
        public List<string> DisplayContactList
        {
            get
            {
                var list = this.Display_Phone == null ? null : this.Display_Phone.Split(new char[','], StringSplitOptions.RemoveEmptyEntries).ToList<string>();
                return list;
            }
        }
    }
}
