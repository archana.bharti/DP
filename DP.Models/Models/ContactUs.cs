﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_contactus")]
    public class ContactUs : ModelBase
    {
        [MaxLength(128)]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Email { get; set; }
        [MaxLength(16)]
        public string Mobile { get; set; }
        [MaxLength(200)]
        public string Message { get; set; }
        [MaxLength(32)]
        public string Reason { get; set; }
       // public Int16 Category { get; set; }
        public int _status;
        public int Status
        {
            set
            {
                if (value == 0) _status = 1;
                else _status = value;
            }
            get
            {
                return _status;
            }
        }
    }

    public enum ContactUsStatus
    {
        Pending = 1,
        Answered =2
    }
}
