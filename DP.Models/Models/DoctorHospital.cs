﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_doctor_hospital")]
    public class DoctorHospital : ModelBase
    {
        public int DoctorId { get; set; }
        public int HospitalId { get; set; }
    }
}
