﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DP.Services.Data.Models
{
    [Table("dp_poll")]
    public class Poll : ModelBase
    {
        public int PollMasterId { get; set; }
        [ForeignKey("PollMasterId")]
        public PollMaster PollMaster { get; set; }
        [MaxLength(32)]
        public string IpAddress { get; set; }
        public bool Yes { get; set; }
        public bool No { get; set; }
    }
}
