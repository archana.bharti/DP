﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_clinic_doctor")]
    public class ClinicAndDoctor : SearchEntityBase
    {
        public int Category { get; set; }
        public ICollection<SpecializationClinic> Specialiazations { get; set; }
    }
}
