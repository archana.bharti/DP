﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.Models
{
    [Table("dp_clinic_rating")]
    public class ClinicRating : ModelBase
    {
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public int ClinicOrDoctorId { get; set; }
        [ForeignKey("ClinicOrDoctorId")]
        public ClinicAndDoctor ClinicOrDoctor { get; set; }

        public int Cleanliness { get; set; }
        public float CleanlinessSumOfRating { get; set; }
        public float CleanlinessAverage { get; set; }
        public float CleanlinessPercentage { get; set; }

        public int FeesCharged { get; set; }
        public float FeesChargedSumOfRating { get; set; }
        public float FeesChargedAverage { get; set; }
        public float FeesChargedPercentage { get; set; }

        public int WaitingHours { get; set; }
        public float WaitingHoursSumOfRating { get; set; }
        public float WaitingHoursAverage { get; set; }
        public float WaitingHoursPercentage { get; set; }

        public int Listen { get; set; }
        public float ListenSumOfRating { get; set; }
        public float ListenAverage { get; set; }
        public float ListenPercentage { get; set; }
        
        public int Comfort { get; set; }
        public float ComfortSumOfRating { get; set; }
        public float ComfortAverage { get; set; }
        public float ComfortPercentage { get; set; }

        public int FriendlyStaff { get; set; }
        public float FriendlyStaffSumOfRating { get; set; }
        public float FriendlyStaffAverage { get; set; }
        public float FriendlyStaffPercentage { get; set; }

        public int CustomerSatisfaction { get; set; }
        public float CustomerSatisfactionSumOfRating { get; set; }
        public float CustomerSatisfactionAverage { get; set; }
        public float CustomerSatisfactionPercentage { get; set; }

        public int QualityOfService { get; set; }
        public float QualityOfServiceSumOfRating { get; set; }
        public float QualityOfServiceAverage { get; set; }
        public float QualityOfServicePercentage { get; set; }

        public int OnCall { get; set; }
        public float OnCallSumOfRating { get; set; }
        public float OnCallAverage { get; set; }
        public float OnCallPercentage { get; set; }

        public int LatestTechnology { get; set; }
        public float LatestTechnologySumOfRating { get; set; }
        public float LatestTechnologyAverage { get; set; }
        public float LatestTechnologyPercentage { get; set; }

        public string YourExperience { get; set; }

        [MaxLength(256)]
        public string Type { get; set; }

        public DateTime ReviewedDate { get; set; }

        public string LastVisited { get; set; }
    }
}
