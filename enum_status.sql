SET FOREIGN_KEY_CHECKS=0;

DELETE FROM `dp_enum_status`;
ALTER TABLE `dp_enum_status` AUTO_INCREMENT = 1;

LOCK TABLES `dp_enum_status` WRITE;
/*!40000 ALTER TABLE `dp_enum_status` DISABLE KEYS */;
INSERT INTO `dp_enum_status` (`Id`,`Text`, `Description`) VALUES (1,'Sent', 'Mail Sent');
INSERT INTO `dp_enum_status` (`Id`,`Text`, `Description`) VALUES (2,'Failed', 'Mail Failed');
INSERT INTO `dp_enum_status` (`Id`,`Text`, `Description`) VALUES (3,'Pending', 'Pending');
INSERT INTO `dp_enum_status` (`Id`,`Text`, `Description`) VALUES (4,'Active', 'Active');
INSERT INTO `dp_enum_status` (`Id`,`Text`, `Description`) VALUES (5,'Deactive', 'Deactive');
/*!40000 ALTER TABLE `dp_enum_status` ENABLE KEYS */;
UNLOCK TABLES;

SET FOREIGN_KEY_CHECKS=1;