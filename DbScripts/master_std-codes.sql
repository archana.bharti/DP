/*
-- Query: SELECT * FROM dp_old.dp_std_code
LIMIT 0, 1000

-- Date: 2015-09-11 14:53
*/
SET FOREIGN_KEY_CHECKS=0;
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (1,'Tripura','Agartala',91380);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (2,'Mizoram','Aizawl',91389);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (3,'Mizoram','Aizawl',913838);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (4,'Karnataka','Bangalore',9180);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (5,'Mizoram','Aizawl',913838);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (6,'Madhya Pradesh','Bangalore',9180);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (7,'Orissa','Bhubaneshwar',91674);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (8,'Chandigarh','Chandigarh',91172);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (9,'Tamil Nadu','Chennai',9144);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (10,'Daman and Diu','Daman',91260);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (11,'Uttarakhand','Dehradun',91135);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (12,'Delhi','New Delhi',9111);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (13,'Gujarat','Gandhinagar',9179);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (14,'Sikkim','Gangtok',913592);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (15,'Assam','Guwahati',91361);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (16,'Andhra Pradesh','Hyderabad  East',918415);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (17,'Andhra Pradesh','Hyderabad  Local',9140);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (18,'Andhra Pradesh','Hyderabad  West',918413);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (19,'Manipur','Imphal',91385);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (20,'Arunachal Pradesh','Itanagar',91360);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (21,'Rajasthan','Jaipur',91141);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (22,'Lakshwadeep','Kavaratti',914896);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (23,'Nagaland','Kohima',91370);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (24,'West Bengal','Kolkata',9133);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (25,'Uttar Pradesh','Lucknow',91522);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (26,'Maharahstra','Mumbai',9122);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (27,'Goa','Panaji',91832);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (28,'Bihar','Patna',91612);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (29,'Puducherry','Puducherry',91413);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (30,'Andaman & Nicobar Islands','Port Blair  Andaman & Nicobar Islands',913192);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (31,'Chhattisgarh','Raipur',91771);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (32,'Jharkhand','Ranchi',91651);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (33,'Meghalaya','Shillong',91364);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (34,'Himachal Pradesh','Shimla',91177);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (35,'Silvassa  Dadra','Silvassa  Dadra & Nagar Haveli',912638);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (36,'Jammu & Kashmir','Srinagar',91194);
INSERT INTO `dp_std_code` (`id`,`state`,`capital`,`code`) VALUES (37,'Kerala','Thiruvananthapuram',91471);
SET FOREIGN_KEY_CHECKS=1;
