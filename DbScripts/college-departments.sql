/*
-- Query: SELECT * FROM doctorpursuit.dp_college_department
LIMIT 0, 10000

-- Date: 2016-04-25 07:04
*/
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (1,'Anatomy',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (2,'Microbiology',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (3,'Biochemistry',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (4,'Physiology',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (5,'Pathology',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (6,'Pharmacology',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (7,'General medicine',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (8,'General surgery',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (9,'Orthopedics',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (10,'Obstetrics and gynecology',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (11,'Ophthalmology',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (12,'Dermatology',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (13,'Psychiatry',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (14,'Community medicine',1);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (15,'Oral and maxilofacial surgery ',2);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (16,'Community dentistry',2);
INSERT INTO `dp_college_department` (`Id`,`Name`,`Category`) VALUES (17,'Orthodontics',2);
