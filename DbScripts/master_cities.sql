/*
-- Query: SELECT * FROM doctorpursuit.dp_city
LIMIT 0, 1000

-- Date: 2015-09-11 17:34
*/
SET FOREIGN_KEY_CHECKS=0;

DELETE FROM `dp_city`;
ALTER TABLE `dp_city` AUTO_INCREMENT = 1;

LOCK TABLES `dp_city` WRITE;
/*!40000 ALTER TABLE `dp_specialization_clinic` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS=0;
INSERT INTO `dp_city` (`Id`,`Name`,`StateId`) VALUES (1,'Hyderabad',1);
INSERT INTO `dp_city` (`Id`,`Name`,`StateId`) VALUES (2,'Bengaluru',12);
/*!40000 ALTER TABLE `dp_city` ENABLE KEYS */;
UNLOCK TABLES;

SET FOREIGN_KEY_CHECKS=1;
