/*
-- Query: SELECT * FROM doctorpursuit.aspnetroles
LIMIT 0, 1000

-- Date: 2015-09-12 01:16
*/
INSERT INTO `aspnetroles` (`Id`,`Name`) VALUES ('79787cdf-cd06-456c-904c-3772c9a086f6','ADMIN');
INSERT INTO `aspnetroles` (`Id`,`Name`) VALUES ('054e0377-2fb5-48ef-a5a1-d2f6d9f498b5','CLINIC');
INSERT INTO `aspnetroles` (`Id`,`Name`) VALUES ('8ee02543-6f52-4524-aebb-fd731f671c5a','COLLEGE');
INSERT INTO `aspnetroles` (`Id`,`Name`) VALUES ('dd1245ee-ebd4-4a42-ba96-b33654edec2d','DOCTOR');
INSERT INTO `aspnetroles` (`Id`,`Name`) VALUES ('7a7b499c-14fc-4c56-846f-3d1b44e76ad8','HOSPITAL');
INSERT INTO `aspnetroles` (`Id`,`Name`) VALUES ('9e1dfad8-dd28-491b-ad5d-37968e6cf854','USER');
INSERT INTO `aspnetroles` (`Id`,`Name`) VALUES ('26c9ce0b-b9ce-41e0-aeb5-0e8a802327e6','INSURANCE');


