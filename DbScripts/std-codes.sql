/*
-- Query: SELECT * FROM doctorpursuit.dp_std_code
LIMIT 0, 10000

-- Date: 2016-04-23 13:45
*/
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (27,'Bangalore (Karnataka)',NULL,80);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (28,'Bhopal (Madhya Pradesh)',NULL,755);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (29,'Bhubaneshwar (Orissa)',NULL,674);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (30,'Chandigarh',NULL,172);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (31,'Daman (Daman & Diu)',NULL,260);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (32,'Dehradun (Uttarakhand)','',135);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (33,'Delhi',NULL,11);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (34,'Gandhinagar (Gujarat)',NULL,79);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (35,'Gangtok',NULL,3592);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (36,'Guwahati',NULL,361);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (37,'Hyderabad',NULL,8415);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (38,'Hyderabad',NULL,40);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (39,'Imphal',NULL,385);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (40,'Itanagar',NULL,360);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (41,'Jaipu',NULL,141);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (42,'Kavaratti',NULL,4896);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (43,'Kohima',NULL,370);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (44,'Kolkata',NULL,33);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (45,'Lucknow',NULL,522);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (46,'Mumbai',NULL,22);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (47,'Panaji',NULL,832);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (48,'Patna',NULL,612);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (49,'Puducherry',NULL,413);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (50,'Port Blair',NULL,3192);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (51,'Raipur',NULL,771);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (52,'Ranchi',NULL,651);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (53,'Shillong',NULL,364);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (54,'Shimla',NULL,177);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (55,'Silvassa',NULL,2638);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (56,'Srinagar',NULL,194);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (57,'Thiruvananthapuram',NULL,471);
INSERT INTO `dp_std_code` (`Id`,`State`,`Capital`,`Code`) VALUES (58,'Chennai',NULL,44);
