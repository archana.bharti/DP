/*
-- Query: SELECT * FROM dp_old.dp_state
LIMIT 0, 1000

-- Date: 2015-09-11 14:50
*/
SET FOREIGN_KEY_CHECKS=0;

DELETE FROM `dp_state`;
ALTER TABLE `dp_state` AUTO_INCREMENT = 1;

LOCK TABLES `dp_state` WRITE;
/*!40000 ALTER TABLE `dp_state` DISABLE KEYS */;
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (1,'Andhra Pradesh','AP');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (2,'Arunachal Pradesh','AR');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (3,'Assam','AS');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (4,'Bihar','BR');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (5,'Chhattisgarh','CT');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (6,'Goa','GA');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (7,'Gujarat','GJ');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (8,'Haryana','HR');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (9,'Himachal Pradesh','HP');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (10,'Jammu and Kashmir','JK');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (11,'Jharkhand','JH');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (12,'Karnataka','KA');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (13,'Kerala','KL');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (14,'Madhya Pradesh','MP');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (15,'Maharashtra','MH');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (16,'Manipur','MN');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (17,'Meghalaya','ML');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (18,'Mizoram','MZ');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (19,'Nagaland','NL');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (20,'Odisha','OR');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (21,'Punjab','PB');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (22,'Rajasthan','RJ');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (23,'Sikkim','SK');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (24,'Tamil Nadu','TN');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (25,'Telangana','TG');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (26,'Tripura','TR');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (27,'Uttar Pradesh','UP');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (29,'West Bengal','WB');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (30,'Pondicherry','DP');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (31,'Delhi','DD');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (32,'Uttrakhand','UK');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (34,'Uttaranchal','UT');
INSERT INTO `dp_state` (`Id`,`name`,`shortcode`) VALUES (35,'Daman and Diu\r\n','DA');
/*!40000 ALTER TABLE `dp_state` ENABLE KEYS */;
UNLOCK TABLES;

SET FOREIGN_KEY_CHECKS=1;