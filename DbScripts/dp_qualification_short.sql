/*
-- Query: SELECT * FROM dp_snt.dp_qualification_short
LIMIT 0, 5000

-- Date: 2016-03-17 15:58
*/
DELETE FROM `dp_qualification_short`;
ALTER TABLE `dp_qualification_short` AUTO_INCREMENT = 1;
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (5,'Diploma');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (6,'DM');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (7,'DNB');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (8,'DPB');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (9,'FCPS');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (10,'FMT');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (11,'M.Ch');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (12,'M.Sc');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (13,'MBBS');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (14,'MCPS');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (15,'MD');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (16,'MS');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (17,'Ph.D');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (18,'PSM');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (19,'FRCS');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (20,'PGDMLE');
INSERT INTO `dp_qualification_short` (`Id`,`ShortEndDegree`) VALUES (21,'MDS');
