﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWE.SmsProvider
{
    public interface ISmsProvider
    {
        string Send(string receipent, string text);
        string Send(string template, string[] values, string receipent);
    }
}
