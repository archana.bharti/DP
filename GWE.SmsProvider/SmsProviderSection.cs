﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWE.SmsProvider
{
    public class SmsProviderConfig
    {
        static SmsProviderSection section = null;
        static SmsProviderConfig()
        {
            section = System.Configuration.ConfigurationManager.GetSection("smsProvider") as SmsProviderSection;
        }

        public static Tuple<string, string> Get(string providerType)
        {
            if (section.Provider.Name.ToLower() == providerType.ToLower())
            {
              return new Tuple<string, string>(section.Provider.UserName, section.Provider.Hash);
            }
            throw new Exception("Invalid SmsProvider. Please check the configuration");
        }
    }

    internal class SmsProviderSection : ConfigurationSection
    {
        [ConfigurationProperty("provider")]
        public SmsProviderElement Provider
        {
            get
            {
                return (SmsProviderElement)this["provider"];
            }
            set
            { this["provider"] = value; }
        }

    }

    internal class SmsProviderElement : ConfigurationElement
    {
        public SmsProviderElement() { }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
        }

        [ConfigurationProperty("username", IsRequired = true)]
        public string UserName
        {
            get { return (string)this["username"]; }
        }

        [ConfigurationProperty("hash", IsRequired = true)]
        public string Hash
        {
            get { return (string)this["hash"]; }
        }
    }
}
