﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace GWE.SmsProvider
{
    public class TextLocalSmsTemplateData
    {
        public string status { get; set; }
        public List<TextLocalSmsTemplate> templates { get; set; }
    }

    public class TextLocalSmsTemplate
    {
        public string title { get; set; }
        public string body { get; set; }
    }

    public abstract class TextLocalSmsTempalteNames
    {
        public const string PDA_QUESTION_DOCTOR = "PDA-QUESTION-DOCTOR:user";
        public const string APPOINTMENT_EXTERNAL_USER = "APPOINTMENT-EXTERNAL-USER:appo,des,dial";
        public const string APPOINTMENT_ENTITY_USER = "APPOINTMENT-ENTITY-USER:user,phone,date,token";
        public const string OFFER_CLIAM_EXTERNAL_USER = "OFFER-CLAIM-EXTERNAL-USER:user,token,provider";
        public const string OFFER_CLIAM_ENTITY_USER = "OFFER-CLAIM-ENTITY-USER:user,token";
        public const string USER_ACTIVATION_SUCCESS_VERIFICATION = "USER-ACTIVATION-SUCCESS-VERIFICATION:user";
        public const string USER_ACTIVATION_SUCCESS_DIRECT = "USER-ACTIVATION-SUCCESS-DIRECT:userName";
    }

    public class TextLocalSmsTemplateManager
    {
        TextLocalSmsTemplateCacheManager cacheManager;
        TextLocalSmsTemplateData templateData;
        public TextLocalSmsTemplateManager()
        {
            cacheManager = new TextLocalSmsTemplateCacheManager();
            GetTemplates();
        }

        private void GetTemplates()
        {
            if (cacheManager.Count == 0)
            {
                Tuple<string, string> config = SmsProviderConfig.Get(SmsProviderType.TextLocal);
                System.Net.ServicePointManager.Expect100Continue = false;
                using (var wb = new WebClient())
                {
                    byte[] response = wb.UploadValues("http://api.textlocal.in/get_templates/", new NameValueCollection()
                {
                {"username" , config.Item1},
                {"hash" , config.Item2}
                });

                    string result = System.Text.Encoding.UTF8.GetString(response);
                    templateData = Newtonsoft.Json.JsonConvert.DeserializeObject<TextLocalSmsTemplateData>(result);

                    if (templateData.templates.Count > 0)
                    {
                        foreach (var tpl in templateData.templates)
                        {
                            cacheManager.Add(tpl.title.ToUpper(), tpl.body);
                        }
                    }
                }
            }
        }

        public string GetTemplate(string template, params string[] parameters)
        {
            var tplSplit = template.Split(':');
            var body = Clean(Convert.ToString(cacheManager.GetItem(tplSplit[0])));
            var tplParameters = tplSplit[1].Split(',');

            if (parameters.Length != tplParameters.Length) { throw new Exception("SMS Parameters length not matched"); }
            for (var i = 0; i <= parameters.Length - 1; i++)
            {
                string v = parameters[i];
                string p = tplParameters[i];
                body = body.Replace(string.Format("{0}{1}{2}", "%", p, "%"), v).Replace(@"\", ""); //Replace(body, p, v);
            }

            return body;
        }

        private string Clean(string body)
        {
            var str = body;
            var arr = new string[] { "|", "{", "}", "^", "\r", "\\r", "\\n", "\n", "\"",  "inputtype", "text", "maxlength" }; //":",
            Regex re = new Regex(@"\{(.*?)\}");

            foreach (Match m in re.Matches(str))
            {
                str = str.Replace(m.Value, "");
            }
            foreach (var a in arr)
            {
                str = str.Replace(a, "");
            }
            str = str.Replace("%%", "%");
            return str;
        }
    }

    public class TextLocalSmsTemplateCacheManager
    {
        static MemoryCache memCache;
        static readonly object padlock = new object();
        static TextLocalSmsTemplateCacheManager()
        {
            memCache = new MemoryCache("TEXTLOCAL_CACHE");
        }

        public long Count
        {
            get
            {
                return memCache.GetCount();
            }
        }


        public void Add(string key, object value, DateTimeOffset? offset = null)
        {
            lock (padlock)
            {
                memCache.Add(key, value, (offset == null ? DateTimeOffset.MaxValue : offset.Value));
            }
        }

        public virtual void RemoveItem(string key)
        {
            lock (padlock)
            {
                memCache.Remove(key);
            }
        }

        public virtual object GetItem(string key)
        {
            lock (padlock)
            {
                var res = memCache[key];
                return res;
            }
        }

        public virtual void RemoveAll()
        {
            lock (padlock)
            {
                foreach (var obj in memCache)
                {
                    memCache.Remove(obj.Key);
                }
            }
        }
    }
}
