﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GWE.SmsProvider
{
    internal class TextLocalSmsProvider : ISmsProvider
    {
        public TextLocalSmsProvider()
        {
            
        }

        public string Send(string template, string[] values, string receipent)
        {
            TextLocalSmsTemplateManager tplManager = new TextLocalSmsTemplateManager();
            String message = tplManager.GetTemplate(template, values);
            message = HttpUtility.UrlEncode(message);
            Tuple<string, string> config = SmsProviderConfig.Get(SmsProviderType.TextLocal);
            using (var wb = new WebClient())
            {
                byte[] response = wb.UploadValues("http://api.textlocal.in/send/", new NameValueCollection()
                {
                {"username" , config.Item1},
                {"hash" , config.Item2},
                {"numbers" , receipent},
                {"message" , message},
                {"sender" , "DOCPUR"}
                });
                return System.Text.Encoding.UTF8.GetString(response);
            }
        }

        public string Send(string receipent, string text)
        {
            String message = HttpUtility.UrlEncode(text);
            Tuple<string, string> config = SmsProviderConfig.Get(SmsProviderType.TextLocal);
            using (var wb = new WebClient())
            {
                byte[] response = wb.UploadValues("http://api.textlocal.in/send/", new NameValueCollection()
                {
                {"username" , config.Item1},
                {"hash" , config.Item2},
                {"numbers" , receipent},
                {"message" , message},
                {"sender" , "DOCPUR"}
                });
                return System.Text.Encoding.UTF8.GetString(response);
            }
        }
        
    }

    public class TextLocalSmsFactory
    {
        static ISmsProvider provider = null;
        public static ISmsProvider Get
        {
            get
            {
                if(provider == null)
                {
                    provider = new TextLocalSmsProvider();
                }

                return provider;
            }
        }
    }
}
