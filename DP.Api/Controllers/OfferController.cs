﻿using DP.Api.Utilities;
using DP.Common.Constants;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;

using DP.Services.Business;
using DP.Services.Business.Administration;
using DP.Services.Data.Models;
using DP.Services.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
    [AllowAnonymous]
    public class OfferController : BaseApiController
    {
        OfferManager offerManager;
        private Logger _logger;
        public OfferController(OfferManager offerManager)
        {
            this.offerManager = offerManager;
            this._logger = base.GetLogger();
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                return Ok(this.offerManager.Get((int)OfferStatus.Active));
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET OFFER LIST FAILED");
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> MyOffers()
        {
            try
            {
                return Ok(this.offerManager.Get(base.SubscriberId, base.SubscriberType, true));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET MY OFFER LIST FAILED");
            }
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                return Ok(this.offerManager.GetById(id));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET OFFER FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> Claim(OfferTransaction model)
        {
            try
            {
                model.UserId = base.UserId;
                model.SubscriberId = base.SubscriberId;
                model.SubscriberType = base.SubscriberType; 
                return Ok(await this.offerManager.Claim(model));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("OFFER CLIAM FAILED");
            }
        }
    }
}
