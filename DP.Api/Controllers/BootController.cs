﻿using DP.Common.Constants;
using DP.Common.Utilities;
using DP.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DP.Api.Controllers
{
    [AllowAnonymous]
    public class BootController : ApiController
    {
        private CommonManager commonManager;
        public BootController(CommonManager commonManager)
        {
            this.commonManager = commonManager;
        }


        // GET: api/BootController
        public IHttpActionResult Get()
        {
            CacheManager manager = new CacheManager();
            return Ok( manager.GetItem(GlobalSettings.GLOBAL_SETTINGS) as Dictionary<string, object> );
            
        }

        [HttpGet]
        public string Db()
        {
            try
            {
                this.commonManager.GetCities();
                return "Success";
            }
            catch(Exception ex)
            {
                return "Failed:" +ex.Message;
            }
        }
    }
}
