﻿using DP.Api.Utilities;
using DP.Common.Models;

using DP.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DP.Api.Controllers
{
    public class ReportController : BaseApiController
    {
        ReportManager reportManager;
        public ReportController(ReportManager reportManager)
        {
            this.reportManager = reportManager;
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult GetFeaturedReport([FromBody]FeaturedReportRequestViewModel vm)
        {
            try
            {
                return Ok(this.reportManager.GetFeaturedStats(vm));
            }
            catch(Exception ex)
            {
                return BadRequest(ExceptionUtility.Parse(ex));
            }
        }
    }
}
