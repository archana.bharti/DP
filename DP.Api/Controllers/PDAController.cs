﻿using DP.Api.Utilities;
using DP.Common.Constants;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;

using DP.Services.Business;
using DP.Services.Data.Models;
using DP.Services.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{

    [Authorize]
    public class PDAController : BaseApiController
    {
        private PDAManager pdaManager;
        private Logger _logger;
        public PDAController(PDAManager pdaManager)
        {
            this.pdaManager = pdaManager;
            this._logger = base.GetLogger();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Post([FromBody]PdaViewModel viewModel)
        {
            try
            {
                return Ok(await this.pdaManager.Add(viewModel));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("CREATE PDA FAILED");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                string role = base.UserRole;
                List<PdaViewModel> viewModels  = null;
                switch (role)
                {
                    case Roles.Doctor:
                    case Roles.Clinic:
                        viewModels =await this.pdaManager.Requests(base.SubscriberId);
                        break;
                    case Roles.User:
                       viewModels = await this.pdaManager.Requests(base.UserId);
                        break;
                }
                return Ok((viewModels == null) ? new List<PdaViewModel>() : viewModels);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET PDA LIST FAILED");
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> Specializations()
        {
            try
            {  
                List<TextValueViewModel> listTxtVal = new List<TextValueViewModel>();
                foreach(var  item in await this.pdaManager.GetSpecializations())
                {
                    listTxtVal.Add(new TextValueViewModel() { Text = item.Specialization.Category, Value = item.Id });
                }
                //remove duplicates
                Dictionary<string, string> textIdDict = new Dictionary<string, string>();
                listTxtVal.ForEach(x => {
                    if (!textIdDict.ContainsKey(x.Text)) {
                        textIdDict.Add(x.Text, x.Value.ToString());
                    }else
                    {
                        textIdDict[x.Text] = textIdDict[x.Text] + "-" + x.Value.ToString();
                    }
                });
                listTxtVal = new List<TextValueViewModel>();
                textIdDict.ToList().ForEach(x=> {
                    listTxtVal.Add(new TextValueViewModel() { Text = x.Key, Value = x.Value });
                });
                return Ok(listTxtVal);
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET PDA SPECIALIZATIONS FAILED");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Authorize]
        public async Task<IHttpActionResult> Answer(PdaViewModel viewModel)
        {
            try
            {
                if (base.SubscriberType == (int)UserType.Clinic
                    || base.SubscriberType == (int)UserType.Doctor)
                {
                    if (string.IsNullOrEmpty(viewModel.UserId)) { viewModel.UserId = base.UserId; }
                    return Ok(await this.pdaManager.Reply(viewModel));
                }
                else
                {
                    ExceptionUtility.Log("NOT AUTHORIZED TO ANSWER PDA", _logger);
                    return BadRequest("You are not authorized.");
                }
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("PDA ANSWER FAILED");
            }
        }
    }
}
