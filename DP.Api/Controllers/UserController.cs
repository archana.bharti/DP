﻿using DP.Common.Models;
using DP.Services.Business;
using DP.Services.Data.Models;
using DP.Services.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DP.Common.Utilities;
using DP.Common.Enums;
using NLog;
using DP.Api.Utilities;

namespace DP.Api.Controllers
{
    [AllowAnonymous]
    public class UserController : BaseApiController
    {
        private UserManager userManager;
        private SecurityManager securityManager;
        private UserSuggestionsManager userSuggestionsManager;
        private PollManager pollManager;
        private Logger _logger;
        public UserController(UserManager userManager,
            UserSuggestionsManager userSuggestionsManager,
            SecurityManager securityManager,
            PollManager pollManager)
        {
            this.userManager = userManager;
            this.securityManager = securityManager;
            this.userSuggestionsManager = userSuggestionsManager;
            this.pollManager = pollManager;
            this._logger = base.GetLogger();
        }

        public async Task<IHttpActionResult> Post(UserRegistrationViewModel viewModel)
        {
            try
            {
                if(viewModel.RegistrationType=="5")
                {
                    viewModel.Contact = viewModel.UserPhone;
                }
                ApplicationUser appUser = ModelFactory.ConvertUserVmToDbm(viewModel);
                List<IdentityRole> roles = this.securityManager.GetAllRoles();

                //create user               
                appUser = await this.userManager.CreateAsync(appUser, viewModel.Password);

                //assign role
                await this.securityManager.AssingRole(appUser.Id, viewModel.RoleId);

                UserType userType = EnumHelper<UserType>.GetValueFromDescription(appUser.UserType);
                SubscriberViewModel svm = (appUser.SubscriberId == null) ? null : await this.userManager.GetSubscriberAsync(appUser.SubscriberId.Value, userType, true);

                if (userType != UserType.User && userType != UserType.Insurance)
                    svm = ModelFactory.ExtendSubscriberViewModel(svm, appUser);

                return Ok(svm);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("USER REGISTRATION FAILED");
            }
        }

        //GET BY GUID
        //public async Task<IHttpActionResult> GetById(string id)
        //{
        //    try
        //    {
        //        ApplicationUser user = await this.userManager.GetById(id);
        //        if (user == null) { throw new Exception("Identity not found"); }
        //        UserViewModel vm = ModelFactory.ConvertToUserVM(user);
        //        return Ok(vm);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}

        //public async Task<IHttpActionResult> Get()
        //{
        //    try
        //    {
        //        List<IdentityRole> roles = this.securityManager.GetAllRoles();
        //        List<ApplicationUser> users = await this.userManager.GetAllAsync();
        //        List<UserViewModel> vms = users.Select(x => ModelFactory.ConvertToUserVM(x, roles)).ToList();
        //        return Ok(vms);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}

        [HttpGet]
        public async Task<IHttpActionResult> GetByEmail(string email)
        {
            try
            {
                ApplicationUser appUser = await this.userManager.GetByEmail(email);
                SubscriberViewModel vm = await this.userManager.GetSubscriberAsync(appUser.SubscriberId.Value, EnumHelper<UserType>.GetValueFromDescription(appUser.UserType));
                ModelFactory.ExtendSubscriberViewModel(vm, appUser);
                return Ok(vm);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET USER BY EMAIL FAILED");
            }
        }

        [HttpPost]
        public IHttpActionResult UserFinder(UserFinderViewModel viewModel)
        {
            return Ok(this.userSuggestionsManager.GetSuggestions(viewModel));
        }

        [HttpGet]
        public async Task<IHttpActionResult> Polls(string ip= null)
        {
            try
            {
                return Ok(
                    new
                    {
                        list = await this.pollManager.GetAllPollMaster(true),
                        stats = ""
                    });
            }
            catch (Exception ex)
            {

                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET POLSS LIST FAILED");
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddPoll(Poll poll)
        {
            try
            {
                await this.pollManager.Add(poll);
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("ADD POLL FAILED");
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> PollStats()
        {
            try
            {
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET POLL STATS FAILED");
            }
        }
    }
}



