﻿using DP.Api.Utilities;
using DP.Services.Business;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DP.Api.Controllers
{
     [AllowAnonymous]
    public class RecommendController : BaseApiController
    {
         RecommendManager manager;
         public RecommendController(RecommendManager manager)
        {
            this.manager = manager;
        }

        public async Task<IHttpActionResult> Post(Recommend dbModel)
        {
            try
            {
                await this.manager.AddOrRemove(dbModel);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ExceptionUtility.Parse(ex));
            }
        }
    }
}
