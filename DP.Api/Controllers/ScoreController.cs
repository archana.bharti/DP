﻿using DP.Api.Utilities;
using DP.Common.Enums;

using DP.Services.Business;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
   //[Authorize]
    public class ScoreController : BaseApiController
    {
        private ClinicRatingManager clinicRatingMgr;
        private HospitalRatingManager hospitalRatingMgr;
        private UserManager userManager;
        private Logger _logger;
        private CollegeRatingManager collegeRatingMgr;
        private InsuranceRatingManager insuranceRatingMgr;

        public ScoreController(UserManager userManager, 
            ClinicRatingManager clinicRatingMgr, 
            HospitalRatingManager hospitalRatingMgr, 
            CollegeRatingManager collegeRatingMgr,
            InsuranceRatingManager insuranceRatingMgr)
        {
            this.clinicRatingMgr = clinicRatingMgr;
            this.hospitalRatingMgr = hospitalRatingMgr;
            this.collegeRatingMgr = collegeRatingMgr;
            this.insuranceRatingMgr = insuranceRatingMgr;
            this.userManager = userManager;
            this._logger = base.GetLogger();
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(int id, int type)
        {
            try
            {
                object result = null;
                switch ((UserType)type)
                {
                    case UserType.Clinic:
                    case UserType.Doctor:
                        result = await clinicRatingMgr.GetScores(id);
                        break;
                    case UserType.Hospital:
                        result = await hospitalRatingMgr.GetScores(id);
                        break;
                    case UserType.Insurance:
                        result = await insuranceRatingMgr.GetScores(id);
                        break;
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET SCORE FAILED");
            }
        }
    }
}



