﻿using DP.Common.Constants;
using DP.Common.Enums;
using DP.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GWE.SmsProvider;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace DP.Api.Controllers
{
    public class TestController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent(@"<html><body style='text-align: center;padding-top: 12%;font-size: 45px;font-family: sans-serif;background-color: #81BBBA;color: white;'>Welcome to doctors pursuit</body></html>");
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
            return response;
        }

        [HttpGet]
        public IHttpActionResult SendActivationLink(string to)
        {
            EmailManager.SendActivationLink(to, GlobalSettings.APP_URL + "/api/auth/Activate" + Guid.NewGuid().ToString(), to, GlobalSettings.APP_URL);
            return Ok("An email has been sent to " + to);
        }

        [HttpGet]
        public void SendPda(string doctoremail, string useremail)
        {
            //2. Send a mail to the doctor and to the user in two different templates
            MailStatus doctorMailStatus = EmailManager.SendPDA(doctoremail, EmailTemplates.Template_PdaDoctor, useremail, "login");
            MailStatus userEmailStatus = EmailManager.SendPDA(useremail, EmailTemplates.Template_PdaUser);
        }

        [HttpGet]
        public void PdaAnswer(string useremail, string doctorname)
        {
            MailStatus userEmailStatus = EmailManager.SendPDA(useremail, EmailTemplates.Template_PdaAnswer, doctorname, "login");
        }

        [HttpGet]
        public async Task<IHttpActionResult> SendSms()

        {
            //ISmsProvider smsProvider = TextLocalSmsFactory.Get;
           // return Ok(smsProvider.Send(TextLocalSmsTempalteNames.USER_ACTIVATION_SUCCESS_DIRECT, new string[] { "Tarakesh" }, "9886624004"));
          //  await SMSManager.Instance.SendActivationSuccessAsync("9886624004", "Tarakesh");
          //  await SMSManager.Instance.SendAppointmentToExternalUserAsync("9886624004", "26-04-2015 10:30AM", "tara clinic", "9886624004");
          //  await SMSManager.Instance.SendOfferClaimToEntityUserAsync("9886624004", "Tarakesh", "e7ene0");
          //  await SMSManager.Instance.SendOfferClaimToExternalUserAsync("9886624004", "Tarakesh", "e7ene0", "Apollo");
          //  await SMSManager.Instance.SendPdaDoctorAsync("9886624004", "Tarakesh");
         //   await SMSManager.Instance.SendUnderVerificationSuccessAsync("9886624004", "Tarakesh");
            await SMSManager.Instance.SendAppointmentToExternalUserAsync("9886624004", "22-03-2016", "tarakesh", "9886624004");
            await SMSManager.Instance.SendAppointmentToEntityUserAsync("Tarakesh","9145190063", "22-03-2016", "ds454", "9886624004");
            return Ok(true);
        }

        [HttpGet]
        public IHttpActionResult GetAllSMSTemplates()
        {
            using (var wb = new WebClient())
            {
                byte[] response = wb.UploadValues("http://api.textlocal.in/get_templates/", new NameValueCollection()
                {
                {"username" , "admin@doctorpursuit.com"},
                {"hash" , "8309b2335736ed2270481c7eefe2634f4e07ff58"}
                });

                string result = System.Text.Encoding.UTF8.GetString(response);
                return Ok(result);
            }
        }
    }
}
