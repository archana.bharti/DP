﻿using DP.Api.Utilities;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Services.Business;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
    [AllowAnonymous]
    public class InsuranceController : BaseApiController
    {
        InsuranceManager manager;
        UserProfileManager userProfileManager;
        
        private Logger _logger;
        public InsuranceController(InsuranceManager manager, UserProfileManager userProfileManager)
        {
            this.manager = manager;
            this.userProfileManager = userProfileManager;
            this._logger = base.GetLogger();
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                return Ok(await this.manager.GetByIdAsync(id));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET INSURANCE FAILED");
            }
        }

        [HttpPost]
        public async Task<object> Search(InsuranceSearchViewModel viewmodel)
        {
            try
            {
                return await this.manager.Search(viewmodel.insurance, HttpContext.Current.Request.QueryString["ip"], viewmodel.Take, viewmodel.Skip);
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET INSURANCE LIST FAILED");
            }
        }


         [HttpPost]
        public async Task<object> Consolatedsearch(InsuranceSearchViewModel viewmodel)
        {
            try
            {
               
                return await this.manager.ConsolateSearch(viewmodel.insurance, viewmodel.Search, HttpContext.Current.Request.QueryString["ip"], viewmodel.Take, viewmodel.Skip);
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET INSURANCE LIST FAILED");
            }
        }

        
        public async Task<object> GetFeatured()
        {
            try
            {
                return await this.manager.GetFeatured();
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET FEATURED INSURANCE LIST FAILED");
            }
        }

        [Authorize]
        public async Task<IHttpActionResult> Put(SubscriberViewModel viewModel)
        {
            try
            {
                return Ok(await this.userProfileManager.Update(viewModel));
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("UPDATE INSURANCE SUBSCRIBER FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> UpdateSummary(Insurance insurance)
        {
            try
            {
                await this.userProfileManager.UpdateSummary(base.SubscriberId, (UserType)base.SubscriberType, insurance.Summary);
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("UPDATE INSURANCE SUMMARY FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> Post(SubscriberViewModel viewModel)
        {
            try
            {
                return Ok(await this.userProfileManager.Update(viewModel));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("UPDATE INSURANCE SUBSCRIBER FAILED");
            }
        }



        [HttpPost]
        public IHttpActionResult InsuranceTypeFinder(InsuranceTypeFinderViewModel viewModel)
        {
            return Ok(this.manager.GetSuggestions(viewModel));
        }
    }
}
