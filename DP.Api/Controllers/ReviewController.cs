﻿using DP.Api.Utilities;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Services.Business;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using System.Linq.Expressions;
using NLog;

namespace DP.Api.Controllers
{
    [Authorize]
    public class ReviewController : BaseApiController
    {
        private ClinicRatingManager clinicRatingMgr;
        private HospitalRatingManager hospitalRatingMgr;
        private UserManager userManager;
        private CollegeRatingManager collegeRatingMgr;
        private InsuranceRatingManager insuranceRatingMgr;
        private Logger _logger;
        public ReviewController(
            UserManager userManager, 
            ClinicRatingManager clinicRatingMgr, 
            HospitalRatingManager hospitalRatingMgr, 
            CollegeRatingManager collegeRatingMgr,
            InsuranceRatingManager insuranceRatingMgr)
        {
            this.clinicRatingMgr = clinicRatingMgr;
            this.hospitalRatingMgr = hospitalRatingMgr;
            this.collegeRatingMgr = collegeRatingMgr;
            this.userManager = userManager;
            this.insuranceRatingMgr = insuranceRatingMgr;
            this._logger = base.GetLogger();
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> Post([FromBody]RatingAddOrUpdateViewModel viewModel)
        {
            int reviewId = 0;
            if (base.SubscriberType != (int)UserType.User) {
                Ok(new { ReviewId = 0 });
            }
            try
            {
               
                switch (viewModel.UserType)
                {
                    case UserType.Clinic:
                    case UserType.Doctor:
                        reviewId = await this.clinicRatingMgr.AddOrUpdate(viewModel.Clinic);
                        break;
                    case UserType.Hospital:
                        reviewId = await this.hospitalRatingMgr.AddOrUpdate(viewModel.Hospital);
                        break;
                    case UserType.College:
                        reviewId = await this.collegeRatingMgr.AddOrUpdate(viewModel.College);
                        break;
                    case UserType.Insurance:
                        reviewId = await this.insuranceRatingMgr.AddOrUpdate(viewModel.Insurance);
                        break;
                }

                return Ok(new { ReviewId = reviewId });
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("ADD OR UPDATE RATING FAILED");
            }
        }


        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetUserReviews(int id, int type)
        {
            try
            {
                object results = null;
                switch ((UserType)type)
                {
                    case UserType.Clinic:
                    case UserType.Doctor:
                        results = await clinicRatingMgr.Get(id);
                        break;
                    case UserType.Hospital:
                        results = await hospitalRatingMgr.Get(id);
                        break;
                    case UserType.College:
                        results = await collegeRatingMgr.Get(id);
                        break;
                    case UserType.Insurance:
                        results = await insuranceRatingMgr.Get(id);
                        break;
                }

                return Ok(results);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET USER REVIEWS FAILED");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetTopRated(int entityId, UserType entityType)
        {
            try
            {
                switch (entityType)
                {
                    case UserType.Clinic:
                    case UserType.Doctor:
                        return Ok( new { TopRated = await clinicRatingMgr.GetTopRated(entityId) });
                    case UserType.Hospital:
                        return Ok(new { TopRated = await hospitalRatingMgr.GetTopRated(entityId) }); ;
                    case UserType.College:
                        return Ok(new { TopRated = await collegeRatingMgr.GetTopRated(entityId) });
                }

                object dummy = null;
                return Ok(new { TopRated = dummy });
                
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET TOP RATED REVIEW FAILED");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetRatingParameters(int type)
        {
            try
            {
                List<string> properties = new List<string>();
                switch ((UserType)type)
                {
                    case UserType.Clinic:
                    case UserType.Doctor:
                        ClinicRatingViewModel cvm = new ClinicRatingViewModel();
                        cvm.GetType().GetProperties().ToList().ForEach(x => properties.Add(x.Name));
                        break;
                    case UserType.Hospital:
                        HospitalRatingViewModel hvm = new HospitalRatingViewModel();
                        hvm.GetType().GetProperties().ToList().ForEach(x => properties.Add(x.Name));
                        break;
                    case UserType.College:
                        CollegeRatingViewModel covm = new CollegeRatingViewModel();
                        covm.GetType().GetProperties().ToList().ForEach(x => properties.Add(x.Name));
                        break;
                    case UserType.Insurance:
                        InsuranceRatingViewModel ivm = new InsuranceRatingViewModel();
                        ivm.GetType().GetProperties().ToList().ForEach(x => properties.Add(x.Name));
                        break;
                }

                List<string> excludes = new List<string>(){
                "Id",
                "UserId",
                "UserName",
                "SubscriberName",
                "ReviewedDate",
                "ClinicOrDoctorId",
                "HospitalId",
                "InsuranceId",
                "CollegeId",
                "LastVisited"
            };
                return Ok(properties.Where(x => !excludes.Contains(x)));
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET RATING PARAMETERS FAILED");
            }
        }
    }
}



