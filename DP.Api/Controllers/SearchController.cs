﻿using DP.Api.Utilities;
// ***********************************************************************
// Assembly         : DP.Api
// Author           : GWE
// Created          : 10-21-2015
//
// Last Modified By : GWE
// Last Modified On : 10-28-2015
// ***********************************************************************
// <copyright file="SearchController.cs" company="">
//     Copyright ©  2015
// </copyright>
// <summary></summary>
// ***********************************************************************
using DP.Common.Enums;
using DP.Common.Models;

using DP.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;

/// <summary>
/// The Controllers namespace.
/// </summary>
namespace DP.Api.Controllers
{
    /// <summary>
    /// Class SearchController.
    /// </summary>
    public class SearchController : BaseApiController
    {

        private HospitalSearchManager hospitalSearchManager;
        private DoctorOrClinicSearchManager dcSearchManager;
        private CollegeSearchManager collegeSearchManager;
        private ZoneSearchManager zoneSearchManager;
        private Logger _logger;
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchController" /> class.
        /// </summary>
        /// <param name="hsMgr">The hs MGR.</param>
        /// <param name="dcMgr">The dc MGR.</param>
        /// <param name="clgMgr">The CLG MGR.</param>
        public SearchController(HospitalSearchManager hsMgr, DoctorOrClinicSearchManager dcMgr, CollegeSearchManager clgMgr, ZoneSearchManager zoneSearchManager)
        {
            this.hospitalSearchManager = hsMgr;
            this.dcSearchManager = dcMgr;
            this.collegeSearchManager = clgMgr;
            this.zoneSearchManager = zoneSearchManager;
            this._logger = base.GetLogger();
        }

        /// <summary>
        /// Posts the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>IHttpActionResult.</returns>
        public object Post(SearchRequestViewModel viewModel)
        {
            try
            {
                object results = null;
                switch (viewModel.SearchType)
                {
                    case SearchType.DoctorOrClinic:
                        results = dcSearchManager.Search(viewModel);
                        break;
                    case SearchType.Hospital:
                        results = hospitalSearchManager.Search(viewModel);
                        break;
                    case SearchType.College:
                        results = collegeSearchManager.Search(viewModel);
                        break;
                }

                return results;
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET SEARCH USER RESULTS FAILED");
            }
        }

        [HttpPost]
        public object Zone(ZoneSearchRequestViewModel viewModel)
        {
            try
            {
                return Ok(zoneSearchManager.Search(viewModel));

            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET ZONE RESULTS FAILED");
            }
        }


        [HttpPost]
        public object ZoneDashboard(ZoneSearchRequestViewModel viewModel)
        {
            try
            {
                return Ok(zoneSearchManager.Search(viewModel, true));

            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET ZONE RESULTS FAILED");
            }
        }

        [HttpGet]
        public object CollegeFeatured()
        {
            SearchRequestViewModel viewModel = new SearchRequestViewModel();
            //{ order: "asc",limit: 5,offset: 0,SearchType: 3, DepartmentId: 2, ip: window.IP, byFeatured: true};
            viewModel.ByFeatured = true;
            viewModel.SearchType = SearchType.College;
            return collegeSearchManager.Search(viewModel);
        }


        [HttpPost]
        public object College(ZoneSearchRequestViewModel viewModel)
        {
            try
            {
                return Ok(zoneSearchManager.College(viewModel));

            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET COLLEGE ZONE RESULTS FAILED");
            }
        }
    }
}
