﻿using DP.Api.Utilities;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Business;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
    [Authorize]
    public class EndorsementController : BaseApiController
    {
        private EndorsementManager endorsementMgr;
        private Logger _logger;
        public EndorsementController(EndorsementManager endorsementMgr)
        {
            this.endorsementMgr = endorsementMgr;
            this._logger = base.GetLogger();
        }

        [AllowAnonymous]
        public async Task<IHttpActionResult> Get(int id, int type)
        {
            try
            {   
                return Ok(await endorsementMgr.Get(id, type));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET ENDORSETMENT FAILED");
            }
        }

        public async Task<IHttpActionResult> Post([FromBody]EndorsementAddOrUpdateViewModel viewModel)
        {
            try
            {
                var endorserId = base.SubscriberId;
                var endorserType = EnumHelper<UserType>.GetValueFromDescription(base.UserRole.ToUpper()); 
                return Ok(await this.endorsementMgr.AddOrUpdate(viewModel));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("ADD OR UPDATE ENDORSETMENT FAILED");
            }
        }
    }
}



