﻿using DP.Api.Utilities;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Services.Business;
using DP.Services.Data.Models;
using DP.Services.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
   
    public class CommonController : BaseApiController
    {
        private CommonManager commonManager;
        private SecurityManager securityManager;
        private Logger _logger;
        public CommonController(CommonManager commonManager, SecurityManager securityManager)
        {
            this.commonManager = commonManager;
            this.securityManager = securityManager;
            this._logger = base.GetLogger();
        }

        [HttpGet]
        public IHttpActionResult GetCities(int? stateId = null)
        {

            try
            {
                return Ok(ModelFactory.ConvertToTextValuePair(this.commonManager.GetCities(stateId), "Id", "Name"));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET CITIES FAILED");
            }
        }

        [HttpGet]
        public IHttpActionResult GetCollegeCities(int? stateId = null)
        {
            try
            {
                return Ok(ModelFactory.ConvertToTextValuePair(this.commonManager.GetCollegeCities(stateId), "Id", "Name"));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET COLLEGE CITIES FAILED");
            }
           
        }


        [HttpGet]
        public IHttpActionResult GetStates()
        {
            try
            {
                return Ok(ModelFactory.ConvertToTextValuePair(this.commonManager.GetStates(), "Id", "Name"));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET STATES FAILED");
            }
        }

        [HttpGet]
        public IHttpActionResult GetRoles()
        {
            return Ok(ModelFactory.ConvertToTextValuePair(this.securityManager.GetAllRoles(), "Id", "Name"));
        }

        [HttpGet]
        public IHttpActionResult GetLocations(int? cityId = null)
        {
            return Ok(ModelFactory.ConvertToTextValuePair(this.commonManager.GetLocations(cityId), "Id", "Name"));
        }

        [HttpGet]
        public IHttpActionResult GetStdCodes()
        {
            return Ok(ModelFactory.ConvertToTextValuePair(this.commonManager.GetStdCodes(), "Code", "State"));
        }

        [HttpGet]
        public IHttpActionResult GetSpecializations(int? id, int? type)
        {
            return Ok(this.commonManager.GetSpecializations(id, type));
        }

        [HttpGet]
        public IHttpActionResult GetSpecializations()
        {
            //return Ok(ModelFactory.ConvertToTextValuePair(this.commonManager.GetSpecializations(), "Id", "Category"));
            return Ok(this.commonManager.GetSpecializations());
        }

        [HttpGet]
        public IHttpActionResult GetClinicsAndDoctors(DoctorOrClinicType? type =null, DoctorOrClinicCategoryType? category = null)
        {
            return Ok(ModelFactory.ConvertToTextValuePair(this.commonManager.GetClinicsAndDoctors(type, category), "Id", "Name", new string[]{"CityId", "LocationId"}));
        }

        [HttpGet]
        public IHttpActionResult GetFeaturedEntites(int type)
        {
            try
            {
                return Ok(commonManager.GetFeaturedEntities(type));
            }
            catch (Exception ex)
            {
                return BadRequest(ExceptionUtility.Parse(ex));
            }
        }
    }
}
