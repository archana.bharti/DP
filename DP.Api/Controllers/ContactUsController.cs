﻿using DP.Services.Business;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DP.Api.Controllers
{
    public class ContactUsController : ApiController
    {
        ContactUsManager manager;
        public ContactUsController(ContactUsManager manager)
        {
            this.manager = manager;
        }

        public async Task<IHttpActionResult> Post(ContactUs model)
        {
            await this.manager.Add(model);
            return Ok(true);
        }
    }
}
