﻿using DP.Api.Utilities;
using DP.Common.Models;

using DP.Services.Business;
using DP.Services.Data.Models;
using System;
using System.Threading.Tasks;
using System.Web.Http;
namespace DP.Api.Controllers
{
    //[Authorize]
    public class BookmarkController : BaseApiController
    {
        private BookmarkManager bookmarkManager;
        public BookmarkController(BookmarkManager bookmarkManager)
        {
            this.bookmarkManager = bookmarkManager;
        }

        /// <summary>
        /// Posts the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
        public async Task<IHttpActionResult> Post([FromBody]Bookmark bookmark)
        {
            try
            {
                return Ok(await this.bookmarkManager.Add(bookmark));
            }
            catch (Exception ex)
            {
                 return BadRequest(ExceptionUtility.Parse(ex));
            }
        }

        /// <summary>
        /// Gets the specified user identifier.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
        [Authorize]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                return Ok(await bookmarkManager.Get(base.SubscriberId, base.SubscriberType));
            }
            catch (Exception ex)
            {
                return BadRequest(ExceptionUtility.Parse(ex));
            }
        }

        /// <summary>
        /// Deletes the bookmark
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<IHttpActionResult>.</returns>
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                return Ok(await bookmarkManager.Delete(id));
            }
            catch(Exception ex)
            {
                return BadRequest(ExceptionUtility.Parse(ex));
            }
        }
    }
}
