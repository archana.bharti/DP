﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Web.Http.Controllers;
using DP.Common.Utilities;
using NLog;

namespace DP.Api.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public Logger GetLogger()
        {
            return LogManager.GetLogger("DP");
        }

        public BaseApiController()
        {
            
        }

        protected override void Initialize(HttpControllerContext context)
        {   
            base.Initialize(context);
        }

        public async Task<Dictionary<string, object>> ParseEncodedForm(MultipartMemoryStreamProvider memoryStreamProvider)
        {

            try
            {
                var resultSet =  new Dictionary<string,object>();
                string dataAppender = string.Empty;
                foreach (var content in memoryStreamProvider.Contents)
                {

                    if (content.Headers.ContentDisposition != null && String.IsNullOrEmpty(content.Headers.ContentDisposition.FileName)) //Form data and not file data
                    {
                        string formFieldName = UnquoteToken(content.Headers.ContentDisposition.Name);
                        string formFieldValue = await content.ReadAsStringAsync();
                        resultSet.Add(formFieldName, formFieldValue);
                    }
                    else// this is file content
                    {
                        if (content.Headers.ContentType != null) //Only allow white listed mine types
                        {
                            dataAppender = "data:" + content.Headers.ContentType.MediaType + ";base64,";
                            byte[] imageData = await content.ReadAsByteArrayAsync();
                            string fileName = content.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                            string fileType = fileName.Split('.')[1];
                            resultSet.Add("file", imageData);
                            resultSet.Add("fileName", fileName);
                            resultSet.Add("fileType", "." + fileType);
                        }
                    }

                }

                return resultSet;
            }

            catch (Exception e)
            {
                return null;
            }
        }

        private static string UnquoteToken(string token)
        {
            if (String.IsNullOrWhiteSpace(token))
            {
                return token;
            }

            if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1)
            {
                return token.Substring(1, token.Length - 2);
            }

            return token;
        }

        protected string UserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        protected string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        protected string UserRoleId
        {
            get
            {
                var claims = User.Identity as ClaimsIdentity;
                return claims.FindFirst(ClaimTypes.Role).Value;
            }
        }

        protected string UserRole
        {
            get
            {
                var claims = User.Identity as ClaimsIdentity;
                return claims.FindFirst("UserType").Value;
            }
        }

        protected int SubscriberType
        {
            get
            {
                var claims = User.Identity as ClaimsIdentity;
                return (int)EnumHelper<DP.Common.Enums.UserType>.GetValueFromDescription(
                    claims.FindFirst("UserType").Value);
            }
        }

        protected Int32 SubscriberId
        {
            get
            {
                var claims = User.Identity as ClaimsIdentity;
                return Convert.ToInt32(claims.FindFirst("SubscriberId").Value);
            }
        }

        protected string Decode(string encoded)
        {
           Func < string, string> decode = (_encoded) => {
                var decBytes = Convert.FromBase64String(encoded);
                return System.Text.Encoding.UTF8.GetString(decBytes);
            };
            return decode(encoded);
        }
    }
}
