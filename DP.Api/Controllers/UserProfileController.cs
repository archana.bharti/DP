﻿using DP.Api.Utilities;
using DP.Common;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Common.Utilities;
using DP.Services.Business;
using DP.Services.Data.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
    
    public class UserProfileController : BaseApiController
    {
        private UserProfileManager userProfileMgr;
        private EndorsementManager endorsementMgr;
        private UserManager userMgr;
        private QualificationManager qualificationManager;
        private Logger _logger;
        public UserProfileController(UserProfileManager userProfileMgr, EndorsementManager endorsementMgr, UserManager userManager, QualificationManager qualificationManager)
        {
            this.endorsementMgr = endorsementMgr;
            this.userProfileMgr = userProfileMgr;
            this.userMgr = userManager;
            this.qualificationManager = qualificationManager;
            this._logger = base.GetLogger();
        }

       [AllowAnonymous]
        public async Task<IHttpActionResult> Get(string uId)
        {
            try
            {
                var decoded = Decode(uId).Split(':');
                var entityId = decoded[0];
                var entityType = (UserType)Convert.ToInt32(decoded[1]);
                return Ok(await userMgr.GetSubscriberAsync(Convert.ToInt32(entityId), entityType));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET USER PROFILE BY UID FAILED");
            }
        }

        /// <summary>
        /// Puts the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="viewModel">The view model.</param>
        /// <returns>Task&lt;IHttpActionResult&gt;.</returns>
        [Authorize]
        public async Task<IHttpActionResult> Put(string uId, SubscriberViewModel viewModel)
        {
            try
            {
                return Ok(await userProfileMgr.Update(viewModel));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("UPDATE USER PROFILE BY UID FAILED");
            }
        }

        /// <summary>
        /// Reviewses the specified u identifier.
        /// </summary>
        /// <param name="uId">The u identifier.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
        [HttpGet]
        public async Task<IHttpActionResult> MyReviews(string uId)
        {
            try
            {
                return Ok(await userProfileMgr.GetReviews(uId));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET MY REVIEWS FAILED");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Endorsements(int id, int type)
        {
            try
            {
                return Ok(await endorsementMgr.Get(id, type));
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET ENDORSEMENTS FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> UploadImage()
        {
            try {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    return InternalServerError(new HttpResponseException(HttpStatusCode.UnsupportedMediaType));
                }

                var memoryStreamProvider = new MultipartMemoryStreamProvider();

                await Request.Content.ReadAsMultipartAsync(memoryStreamProvider);

                var dataDictionary = await base.ParseEncodedForm(memoryStreamProvider);

                if (dataDictionary != null)
                {
                    AwsS3Manager s3Manager = new AwsS3Manager();
                    var subscriberId = base.SubscriberId;
                    var userType = base.SubscriberType;
                    var pictureNo = dataDictionary["pictureNo"].ToString();

                    string fileName = "sub" + subscriberId.ToString() + "_t" + userType.ToString() +"_img" + pictureNo + Convert.ToString(dataDictionary["fileType"]);
                    var exception = await s3Manager.Upload2Async(dataDictionary["file"] as byte[], fileName, true);
                    if(exception != null)
                    {
                        ExceptionUtility.Log(exception, _logger);
                        return BadRequest("IMAGED UPLOAD FAILED");
                    }

                    string url = s3Manager.GetS3Url(fileName);
                    await this.userMgr.UpdateImageUrl(subscriberId, userType, url, pictureNo);
                    ExceptionUtility.Log("s3 url:" + url, _logger);
                    return Ok(url);
                }
                return Ok("");
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("IMAGED UPLOAD FAILED");
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetUnifiedQualifications()
        {
            return Ok(new
            {
                LongQualifications = await this.qualificationManager.GetAllLongQualifications(),
                ShortQualifications = await this.qualificationManager.GetAllShortQualifications()
            });
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetUnifiedUserQualifications(int userId)
        {
            return Ok(new
            {
                LongQualifications = await this.qualificationManager.GetLongQualificationsByDoctorId(userId),
                ShortQualifications = await this.qualificationManager.GetShortQualificationsByDoctorId(userId)
            });
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> AddLongQualification(QualificationLong qualif)
        {
            try
            {
                return Ok(await this.qualificationManager.AddAsync(qualif));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("ADD LONG QUALIFICATION FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> AddShortQualification(QualificationShort qualif)
        {
            try
            {
                return Ok(await this.qualificationManager.AddAsync(qualif));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("ADD SHORT QUALIFICATION FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> DeleteLongQualification(QualificationLong qualif)
        {
            try
            {
                await this.qualificationManager.DeleteAsync(qualif);
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("DELETE LONG QUALIFICATION FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> DeleteShortQualification(QualificationShort qualif)
        {
            try
            {
                await this.qualificationManager.DeleteAsync(qualif);
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("DELETE SHORT QUALIFICATION FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> AddQualification(QualificationViewModel vm)
        {
            try
            {
                vm.DoctorQualification.DoctorId = base.SubscriberId;
                await this.qualificationManager.AddAsync(vm);
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("ADD QUALIFICATION FAILED");
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> RemoveQualification(QualificationViewModel vm)
        {
            try
            {
                vm.DoctorQualification.DoctorId = base.SubscriberId;
                await this.qualificationManager.RemoveAsync(vm);
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("DELETE QUALIFICATION FAILED");
            }
        }

        [HttpPut]
        [Authorize]
        public async Task<IHttpActionResult> UpdateSummary(string uId, SubscriberViewModel viewModel)
        {
            try
            {
                var decoded = Decode(uId).Split(':');
                var entityId = Convert.ToInt32(decoded[0]);
                var entityType = (UserType)Convert.ToInt32(decoded[1]);
                await this.userProfileMgr.UpdateSummary(entityId, entityType, viewModel.Summary);
                return Ok(viewModel.Summary);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("UPDATE USER SUMMARY FAILED");
            }
        }
    }
}



