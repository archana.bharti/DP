﻿using DP.Api.Utilities;
using DP.Common.Models;
using DP.Services.Business;
using DP.Services.Data.Models;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
    public class AppointmentController : BaseApiController
    {
        private AppointmentManager appointmentMgr;
        private Logger _logger;
        public AppointmentController(AppointmentManager appointmentMgr)
        {
            this.appointmentMgr = appointmentMgr;
            this._logger = base.GetLogger();
        }

        public async Task<IHttpActionResult> Post(Appointment model)
        {
            try
            {              
                return Ok(await appointmentMgr.Add(model));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("CREATE APPOINTMENT FAILED");
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> CheckAvailability()
        {
            try
            {
                var app = await appointmentMgr.CheckAvailability();
                if (app.Item1 == true) return Ok(true);
                if(app.Item1 == false)
                {
                    ExceptionUtility.Log(string.Format("Appointment Response:SDate-{0}, EDate-{1}, WeekOff-{2}, Now-{3}",app.Item2, app.Item3, app.Item4, app.Item5), _logger);
                    return BadRequest(app.Item6.Message);
                }
                return Ok(true);
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("CHECK AVAILABILITY FAILED");
            }
        }
    }
}
