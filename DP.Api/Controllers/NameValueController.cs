﻿using DP.Common.Models;
using DP.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DP.Api.Controllers
{
    public class NameValueController : BaseApiController
    {
        private ClinicManager clinicMgr;
        public NameValueController(ClinicManager clinicMgr)
        {
            this.clinicMgr = clinicMgr;
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                return Ok(ModelFactory.ConvertToClinicVMs(await clinicMgr.GetAllAsync()));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
