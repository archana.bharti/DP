﻿using DP.Common.Enums;
using DP.Common.Models;
using DP.Services.Data.Models;
using DP.Services.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DP.Common.Utilities;
using DP.Services.Business;
using DP.Common.Constants;
using DP.Api.Utilities;
using NLog;

namespace DP.Api.Controllers
{

    public class AuthController : ApiController
    {
        
        private SecurityManager securityManager;
        private UserManager userManager;
        private FeatureManager featureManager;
        private Logger _logger;
        private const string COOKIE_USERNAME = "DOCTORPURSUIT-USERNAME",
                             COOKIE_USERROLE = "DOCTORPURSUIT-USERROLE",
                             COOKIE_USERID = "DOCTORPURSUIT-USERID";

        public AuthController(SecurityManager securityManager, UserManager userManager, FeatureManager featureManager)
        {
            this.securityManager = securityManager;
            this.userManager = userManager;
            this.featureManager = featureManager;
            this._logger = LogManager.GetCurrentClassLogger();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> Login(LoginViewModel viewModel)
        {
            try
            {
                ApplicationUser appUser = await this.userManager.GetByEmail(viewModel.UserName);
                UserType userType;
                Tuple<SignInStatus, ApplicationUser> result = null;

                if (appUser != null)
                {
                    userType = EnumHelper<UserType>.GetValueFromDescription(appUser.UserType);
                    //1. IS EMAIL CONFIRMED
                    if (appUser.EmailConfirmed)
                    {
                        //2. IS OFFLINE VERIFIED
                        if (appUser.IsVerified)
                        {
                            //3 IS NOT TERMINATED
                            if (!appUser.IsTerminated)
                            {

                                if (userType == UserType.User)
                                {
                                    return await SignIn(appUser, viewModel.Password, userType);
                                }
                                else
                                {
                                    //4. CHECK FEATURE PLAN
                                    var feature = await this.featureManager.Get(new Feature() { EntityId = appUser.SubscriberId.Value, EntityType = (int)userType });
                                    if (feature == null)
                                    {
                                        return BadRequest("Your application is currently pending for approval.");
                                    }

                                    if (!feature.EnableBasic)
                                    {
                                        return BadRequest("Your account has been suspended. Kindly contact administrator.");
                                    }

                                    //TODO: what if subscription expires
                                    //var isActive = (feature.StatusValue == (int)UserStatus.Active);
                                    //if(!isActive)
                                    //{
                                    //    return BadRequest("Your application for an account is currently pending approval.");
                                    //}

                                    return await SignIn(appUser, viewModel.Password, userType, feature);
                                }
                            }
                            else
                            {
                                return BadRequest("Your account has been suspended. Kindly contact administrator.");
                            }
                        }
                        else
                        {
                            return BadRequest("Your application is currently pending for approval.");
                        }
                    }
                    else
                    {
                        return BadRequest("Your account is not activated yet. Please follow the link sent to your email address.");
                    }
                }
                else
                {
                    //return BadRequest(EnumHelper<SignInStatus>.GetDescription(result.Item1));
                    return BadRequest("The email/user name and password you entered don't match.");
                }
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("LOGIN FAILED");
            }

        }

        private async Task<IHttpActionResult> SignIn(ApplicationUser user, string password, UserType userType, Feature feature = null)
        {
            Tuple<SignInStatus, ApplicationUser> result = await securityManager.SignIn(user.UserName, password);
            if (result.Item1 == SignInStatus.Success)
            {
                user = result.Item2;
                SubscriberViewModel userVM = (user.SubscriberId == null) ? await this.userManager.GetSubscriberAsync(user.Email) : await this.userManager.GetSubscriberAsync(user.SubscriberId.Value, userType,true);
                if (user.SubscriberId != null)
                {
                    userVM = ModelFactory.ExtendSubscriberViewModel(userVM, user);
                }
                SetOwinContext(userVM);
                return Ok(userVM);
            }
            return BadRequest("The email/user name and password you entered don't match.");
        }
         [AllowAnonymous]
        [HttpGet]
        public async Task<HttpResponseMessage> Activate(string code)
        {
            Tuple<ApplicationUser, string> user = null;
            var response = new HttpResponseMessage();

            try
            {
                user = await this.userManager.Activate(code);
                if (user == null) throw new Exception("User are not found");
                response.Content = new StringContent(user.Item2);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
                return response;

            }
            catch (Exception ex)
            {
                response.Content = new StringContent(ex.Message);
                ExceptionUtility.Log(ex, _logger);
                return response;
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IHttpActionResult> IsLoggedIn()
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    var userId = User.Identity.GetUserId();
                    ApplicationUser user = await this.userManager.GetById(userId);
                    SubscriberViewModel userVM = (user.SubscriberId == null) ? await this.userManager.GetSubscriberAsync(user.Email) : await this.userManager.GetSubscriberAsync(user.SubscriberId.Value, EnumHelper<UserType>.GetValueFromDescription(user.UserType), true);
                    if (user.SubscriberId != null)
                    {
                        userVM = ModelFactory.ExtendSubscriberViewModel(userVM, user);
                    }
                    SetOwinContext(userVM);
                    return Ok(userVM);
                }

                return Unauthorized();
            }
            catch
            {
                return Unauthorized();
            }
        }

        [HttpGet]
        [Authorize]
        public IHttpActionResult Logout()
        {

            if (User.Identity.IsAuthenticated)
            {
                ClearOwinContext();
            }

            return Ok(true);
        }

       
        [HttpGet]
        public async Task<IHttpActionResult> ForgotPassword(string email)
        {
            try {
                return Ok(await this.userManager.ForgotPassword(email));
            }
            catch(Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("FORGET PASSWORD FAILED");
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                model.UserName = this.User.Identity.GetUserName();
                Func<string, string> decode = (encoded) =>
                {
                    var decBytes = Convert.FromBase64String(encoded);
                    return System.Text.Encoding.UTF8.GetString(decBytes);

                };
                model.OldPassword = decode(model.OldPassword);
                model.NewPassword = decode(model.NewPassword);
                return Ok(await this.userManager.ChangePassword(model));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("CHANGE PASSWORD FAILED");
            }
        }

        private void ClearOwinContext()
        {
            HttpContext.Current.Request.Cookies["DP-COOKIE"].Expires = DateTime.Now.AddYears(-10);

            Request.GetOwinContext().Authentication.SignOut(Microsoft.Owin.Security.Cookies.CookieAuthenticationDefaults.AuthenticationType);

            Request.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);

            Request.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);

            HttpContext.Current.GetOwinContext().Authentication.SignOut(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);

            HttpContext.Current.GetOwinContext().Authentication.SignOut(Microsoft.Owin.Security.Cookies.CookieAuthenticationDefaults.AuthenticationType);
        }

        private void SetCookies(SubscriberViewModel response)
        {
            var httpContext = HttpContext.Current;
            httpContext.Response.Cookies.Clear();
            httpContext.Response.Cookies.Add(new HttpCookie(COOKIE_USERID) { Value = response.UserId, Path = "/", Expires = DateTime.Now.AddYears(10), });
            httpContext.Response.Cookies.Add(new HttpCookie(COOKIE_USERNAME) { Value = response.Name, Path = "/", Expires = DateTime.Now.AddYears(10), });
            //httpContext.Response.Cookies.Add(new HttpCookie(COOKIE_USERROLE) { Value = response.Role, Path = "/", Expires = DateTime.Now.AddYears(10), });
        }

        private void SetOwinContext(SubscriberViewModel response) //, string role
        {

            var claims = new List<Claim>() {
                                             new Claim(ClaimTypes.Name, response.Email),
                                           //  new Claim(ClaimTypes.Role, response.Role),
                                             new Claim(ClaimTypes.NameIdentifier, response.UserId),
                                             new Claim("UserType", EnumHelper<DP.Common.Enums.UserType>.GetDescription((UserType) response.SubscriberType)),
                                             new Claim("SubscriberId", response.SubscriberId.HasValue ? response.SubscriberId.ToString() : "0")
                                           };

            var id = new ClaimsIdentity(claims,
                                        DefaultAuthenticationTypes.ApplicationCookie);

            Request.GetOwinContext().Authentication.SignIn(id);

        }
    }
}
