﻿using DP.Api.Utilities;
using DP.Common.Enums;
using DP.Common.Models;
using DP.Services.Business;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;

namespace DP.Api.Controllers
{
    //[Authorize]
    public class SpecializationController : BaseApiController
    {
        private SpecializationManager specializationMgr;
        private Logger _logger;
        public SpecializationController(SpecializationManager specializationMgr)
        {
            this.specializationMgr = specializationMgr;
            this._logger = base.GetLogger();
        }

        public async Task<IHttpActionResult> Get()
        {
            try
            {
                return Ok(await this.specializationMgr.Get());
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET SPECIALIZATION LIST FAILED");
            }
        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">Type of the user.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
        [HttpGet]
        public async Task<IHttpActionResult> Get(int id, int type)
        {
            try
            {
                return Ok(await this.specializationMgr.Get(id, (UserType)type));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("GET SPECIALIZATION FAILED");
            }
        }

        /// <summary>
        /// Posts the specified view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="type">The type.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
         [Authorize]
        public async Task<IHttpActionResult> Post(SpecializationViewModel viewModel, int type)
        {
            try
            {
                viewModel.SubscriberId = viewModel.SubscriberId == 0 ? base.SubscriberId : viewModel.SubscriberId;
                return Ok(await this.specializationMgr.Add(viewModel, (UserType)type));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("ADD SPECIALIZATION FAILED");
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <returns>Task<IHttpActionResult>.</returns>
        [Authorize]
        public async Task<IHttpActionResult> Delete(int id, int type)
        {
            try
            {
                return Ok(await this.specializationMgr.Delete(base.SubscriberId, id, (UserType)type));
            }
            catch (Exception ex)
            {
                ExceptionUtility.Log(ex, _logger);
                return BadRequest("DELETE SPECIALIZATION FAILED");
            }
        }
    }
}



