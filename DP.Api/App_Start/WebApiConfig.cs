﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Routing;
using System.Net.Http.Formatting;
using System.Web.Http.Controllers;
using Microsoft.AspNet.Identity;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.ModelBinding.Binders;
using System.Web.Http.ModelBinding;
using DP.Api.Utilities;

namespace DP.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // Web API routes
            config.EnableCors();
            config.MapHttpAttributeRoutes();

            // Web API configuration and services. Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(DefaultAuthenticationTypes.ApplicationCookie));

            //Services
            config.Filters.Add(new LoggingFilterAttribute());

            //API Custom routes
            config.Routes.IgnoreRoute("Html", "{whatever}.html/{*pathInfo}");
            config.Routes.MapHttpRoute("DefaultApiWithId", "api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
            config.Routes.MapHttpRoute("DefaultApiWithAction", "api/{controller}/{action}");
            config.Routes.MapHttpRoute("DefaultApiWithActionWithValue", "api/{controller}/{action}/{value}");
            config.Routes.MapHttpRoute("DefaultApiGet", "api/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("DefaultApiPost", "api/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("DefaultApiPut", "api/{controller}", new { action = "Put" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("DefaultApiDelete", "api/{controller}", new { action = "Delete" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Delete) });
            //config.Routes.MapHttpRoute(name: "NotFound", routeTemplate: "{*path}",  defaults: new { controller = "Error", action = "NotFound" });
            //Json Configuration
            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.Remove(formatters.XmlFormatter);
            config.Formatters.Insert(0, new DP.Api.Utilities.JsonResponseFormatter());
            var json = config.Formatters.OfType<JsonMediaTypeFormatter>().FirstOrDefault();
            json.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            json.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;
            json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }
    }
}
