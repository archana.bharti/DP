using DP.Services.Business;
using DP.Services.Business.Identity;
using DP.Services.Data;
using DP.Services.Data.Models;
using DP.Services.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using System.Data.Entity;
using System.Web.Http;
using Unity.WebApi;

namespace DP.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            IocBusiness.Register(container);

            //container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());
            //container.RegisterType<IRoleStore<IdentityRole, string>, RoleStore<IdentityRole>>(new HierarchicalLifetimeManager());
            //container.RegisterType<DbContext, DPContext>(new HierarchicalLifetimeManager());
            
            ////DA Layer
            //container.RegisterType<IClinicAndDoctorDao, ClinicAndDoctorDao>(new HierarchicalLifetimeManager());
            //container.RegisterType<IUserDao, UserDao>(new HierarchicalLifetimeManager());
            //container.RegisterType<ICommonDao, CommonDao>(new HierarchicalLifetimeManager());
            //container.RegisterType<ILocationDao, LocationDao>(new HierarchicalLifetimeManager());

            ////BA Layer
            //container.RegisterType<SecurityManager>(new HierarchicalLifetimeManager());
            //container.RegisterType<ClinicManager>(new HierarchicalLifetimeManager());
            //container.RegisterType<UserManager>(new HierarchicalLifetimeManager());
            //container.RegisterType<CommonManager>(new HierarchicalLifetimeManager());
            //container.RegisterType<DoctorOrClinicSearchManager>(new HierarchicalLifetimeManager());
            //container.RegisterType<HospitalSearchManager>(new HierarchicalLifetimeManager());
            //container.RegisterType<CollegeSearchManager>(new HierarchicalLifetimeManager());
        }
    }
}