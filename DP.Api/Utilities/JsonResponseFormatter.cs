﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using System.Net.Http;
using Newtonsoft.Json.Converters;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Dynamic;
using System.Collections;
using Newtonsoft.Json;
using System.Reflection;


namespace DP.Api.Utilities
{
    public class JsonResponseFormatter : JsonMediaTypeFormatter
    {

        private JsonSerializerSettings _jsonSerializerSettings;

        public JsonResponseFormatter()
        {
            _jsonSerializerSettings = CreateDefaultSerializerSettings();
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override bool CanWriteType(Type type)
        {
            return true;
        }


        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream,
            HttpContent content, TransportContext transportContext)
        {
            var isADataGrid = IsDataGrid(value);

            if (isADataGrid)
            {
                return base.WriteToStreamAsync(type, value, writeStream, content, transportContext);
            }

            var eo = new ExpandoObject() as IDictionary<string, Object>;
            var data = new Dictionary<string, object>();
            string primaryMsg = Enum.GetName(typeof(HttpStatusCode), HttpContext.Current.Response.StatusCode);
            string secondaryMsg = string.Empty;

            if (type.Name == "HttpError")
            {
                var dict = value as Dictionary<string, object>;
                data.Add("Code", 500);

                if (dict.ContainsKey("Message"))
                {
                    data.Add("Error1", dict["Message"].ToString());
                }

                if (dict.ContainsKey("ExceptionMessage"))
                {
                    data.Add("Error2", dict["ExceptionMessage"].ToString());
                }
                eo.Add("response", data);

            }
            else
            {
                data.Add("Code", HttpContext.Current.Response.StatusCode);
                data.Add("Message", primaryMsg);
                data.Add("Model", value);
                eo.Add("response", data);
            }



            return base.WriteToStreamAsync(type, eo as object, writeStream, content, transportContext);
        }

        //public override Task WriteToStreamAsync(Type type, object value, Stream writeStream,
        //    HttpContent content, TransportContext transportContext)
        //{

        //    var rootFieldName = GetRootFieldName(type, value);
        //    if (!string.IsNullOrEmpty(rootFieldName) && rootFieldName.Equals("None"))
        //    {
        //        return base.WriteToStreamAsync(type, value, writeStream, content, transportContext);
        //    }
        //    else
        //    {
        //        var obj = new ExpandoObject() as IDictionary<string, Object>;
        //        //Wrap all responses into the response root.
        //        obj["response"] = value;
        //        return base.WriteToStreamAsync(type, obj as object, writeStream, content, transportContext);

        //    }

        //    //var tempObj = value.ToDynamic();
        //    //tempObj.AuthKey = HttpContext.Current.Response.Cookies[".AspNet.ApplicationCookie"].Value;


        //}

        private bool IsDataGrid(object value)
        {
            var propName = "IsJqueryDataGrid";
            bool isFound = false;
            if (value != null)
            {
                if (value.GetType().BaseType != null)
                {
                    var result = value.GetType().BaseType.GetMember(propName).SingleOrDefault();
                    if (result != null)
                    {
                        isFound = true;
                        return isFound;
                    }
                }
            }

            return isFound;
        }

        private string GetRootFieldName(Type type, dynamic value = null)
        {
            //get element type if array
            if (value != null && (value is IEnumerable || type.IsArray))
                type = Enumerable.FirstOrDefault(value).GetType();

            var attrs = type.CustomAttributes.Where(x => x.AttributeType == typeof(JsonObjectAttribute)).ToList();

            if (attrs.Any())
            {
                var titles = attrs.First().NamedArguments.Where(arg => arg.MemberName == "Title")
                    .Select(arg => arg.TypedValue.Value.ToString()).ToList();
                if (titles.Any()) return titles.First();
            }

            return type.Name;
        }

    }
}