﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Tracing;
using NLog;

namespace DP.Api.Utilities
{
    public static class ExceptionUtility
    {
        public static void Log(Exception ex, Logger logger)
        {
            var str = Parse(ex);
            logger.Error(str);
        }

        public static void Log(string message, Logger logger)
        {
            logger.Error(message);
        }

        public static string Parse(Exception ex)
        {
            LogError(ex);
            var errors = new List<string>();
            if (!string.IsNullOrEmpty(ex.Message))
            {
                errors.Add(ex.Message);
            }

            if (ex.InnerException != null)
            {
                if (!string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    errors.Add(ex.InnerException.Message);
                }

                if (ex.InnerException.InnerException != null)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.InnerException.Message))
                    {
                        errors.Add(ex.InnerException.InnerException.Message);
                    }
                }
            }

            errors.Add(ex.StackTrace);
            return string.Join("\n", errors.ToArray<string>());
        }

        private static void LogError(Exception ex)
        {
            //Log4NetTraceWriter.s_log.Error(ex.Message, ex);
        }
    }
}