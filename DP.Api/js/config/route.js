 define([
    "apiConfig",
    "constantsConfig",
    "app",
    "modules/login/controller",
    "modules/register/controller",
    "modules/dashboard/controller",
    "modules/search/controller",
    "modules/user/controller",
    "modules/pda/controller",
    "modules/studentPursuit/controller",
    "modules/healthyWay/controller",
    "modules/featured/controller",
    "modules/insurance/controller",
    "modules/offers/controller"
], function (
    ApiConfig,
    Constants,
    Application,
    LoginController,
    RegisterController,
    DashboardController,
    SearchController,
    UserController,
    PDAController,
    StudentPursuitController,
    HealthyWayController,
    FeaturedController,
    InsuranceController,
    OffersController
) {

    "use strict";

    var router = Backbone.Router.extend({

        initialize: function () {

            this.setupAjax();
            window.app = new Application(this);
            Backbone.history.start({ root: '/', silent: true, pushState: true });
            app.trigger("start");
        },

        setupAjax: function () {
            var self = this;
            $.ajaxSetup({
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                timeout: Constants.AjaxSettings.timeout,
                retry: Constants.AjaxSettings.retry,
                statusCode: {
                    401: function () {
                        app.router.redirect(window.location.pathname.substring(1));
                    }
                    ,
                    400: function (a, b, c) {
                        app.trigger("response:error", a.responseJSON);
                    },

                    403: function (a, b, c) {
                        app.trigger("response:error", a.responseJSON);
                    },

                    500: function (a, b, c) {
                        app.trigger("response:error", (a.responseJSON || a.responseText));
                    },
                    //0: function (a, b, c) {
                    //    app.trigger("response:error", "Unable to reach server. Check your internet connection or try after sometime");
                    //}
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if (jqXHR.status == 404) {
                    //    self.login();
                    //} else {
                    //    app.trigger("response:error", "Error: " + textStatus + ": " + errorThrown);
                    //}
                }
            });

            $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                options.beforeSend = function (xhr, obj) {
                    $("#loading-control").show();
                };
                options.complete = function () {
                    $("#loading-control").hide();
                };
            });
        },


        // ------------------------------------------------------------------------------------------
        // Backbone route handling on page refresh
        // ------------------------------------------------------------------------------------------
        redirect: function (route) {
            route = route === "" ? "home" : route;
            var url = window.location.pathname;
            if (url != '/' && url.substring(1) === route)
                Backbone.history.loadUrl(route)
            else
                app.router.navigate(route, { trigger: true });
        },

        // ------------------------------------------------------------------------------------------
        // Routes
        // ------------------------------------------------------------------------------------------

        routes: {
            "(/)": "default",
            "login": "login",
            "register": "register",
            "contactus": "contactus",

            "account":"account",
            "account/bookmarks": "bookmarks",
            "profile/:type/:uid(/:any)": "profile",

            "home": "home",
            "search/results": "searchResults",

            "pda": "pda",

            "studentpursuit": "studentPursuit",
            "healthyway": "healthyWay",
            "featured": "featured",
            "insurance": "insurance",
            "offers": "offers",
            "offers/user": "userOffers",
            "offer/claim/:id": "offerClaim",

            "settings": "settings",
            "themes": "themes"
        },

        // ------------------------------------------------------------------------------------------
        // Route Handlers
        // ------------------------------------------------------------------------------------------
        
        login: function() {
            app.executeController("Login",LoginController, "login", null);
        },

        register: function() {
            app.executeController("Register", RegisterController, "register", null);
        },

        //TODO: this needs to be changed
        account : function(){
            app.executeController("User", UserController, "profile", { account: true });
        },
        
        default: function (params) {
            //app.shell();
        },

        shell: function (callback) {
            app.shell();
        },

        home: function () {
            $("#loading-control").hide();
            app.executeController("Dashboard", DashboardController, "dashboard", null);
        },

        contactus : function(){
            app.executeController("Dashboard", DashboardController, "contactus", null);
        },

        profile: function (type, id, any) {
            if (app.sessionManager.get("USER") != undefined) {
                if (app.sessionManager.get("USER").subscriberId == id) {
                    app.router.redirect("account");
                    return;
                }
            }
            app.executeController("User", UserController, "profile", { type: type, id: id, account: false });
            ga('set', 'page', '/' + Backbone.history.fragment);
        },

        bookmarks: function (type, id) {
            app.executeController("User", UserController, "bookmarks", { type: type, id: id });
        },

        editProfile: function(type, id){
            app.executeController("User", UserController, "editProfile", { type: type, id: id });
        },

        pda: function(){
            app.executeController("PDA", PDAController, "pda", null);
        },

        studentPursuit: function(){
            app.executeController("Student", StudentPursuitController, "studentPursuit", null);
        },

        healthyWay: function(){
            app.executeController("Healthy", HealthyWayController, "healthyWay", null);
        },

        featured: function(){
            app.executeController("Featured", FeaturedController, "featured", null);
        },

        insurance: function(){
            app.executeController("Insurance", InsuranceController, "insurance", null);
        },

        offers: function(){
            app.executeController("Offer", OffersController, "offers", null);
        },

        offerClaim  : function(id){
            app.executeController("Offer", OffersController, "claim", { id: id });
        },

        userOffers : function(){
            app.executeController("Offer", OffersController, "myOffers");
        },

        /***************************************************
              SEARCH RESULTS
        ****************************************************/

        searchResults: function () {
            app.executeController("Search", SearchController, "searchResults", null);
        }
    });

    return router;

});
