﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity;
using DP.Services.Business.Identity;

[assembly: OwinStartup(typeof(DP.Api.Startup))]
namespace DP.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var httpConfig = new HttpConfiguration();
            //Use cookie based authentication
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                // LoginPath = new PathString("/Auth/Login"),
                CookieName = "DP-COOKIE",
                SlidingExpiration = false
            });

            app.UseWebApi(httpConfig);
            app.UseCors(CorsOptions.AllowAll);
            ApplicationUserManager.DataProtectionProvider = app.GetDataProtectionProvider();
            DP.Services.Business.IocBusiness.SetAppBuilder(app);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
        }

    }
}
