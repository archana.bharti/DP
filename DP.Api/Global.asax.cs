﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using DP.Common.Utilities;
using DP.Common.Constants;
using System.Web.Http.ExceptionHandling;
using DP.Api.Utilities;
using System.Web.Http.Tracing;

namespace DP.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents();   
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            //Services
            //GlobalConfiguration.Configuration.Services.Add(typeof(IExceptionLogger), new GlobalExceptionLogger());
            //GlobalConfiguration.Configuration.Services.Replace(typeof(ITraceWriter), new Log4NetTraceWriter());
            SetCache();
        }

        private void SetCache()
        {
            CacheManager manager = new CacheManager();
            manager.RemoveAll();

            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add(GlobalSettings.S3_ROOT_PATH, System.Configuration.ConfigurationManager.AppSettings[GlobalSettings.S3_ROOT_PATH].ToString());
            dict.Add(GlobalSettings.GRID_PAGE_SIZE, System.Configuration.ConfigurationManager.AppSettings[GlobalSettings.GRID_PAGE_SIZE].ToString());

            manager.Add(GlobalSettings.GLOBAL_SETTINGS, dict);
        }

        public void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            WebApiApplication mvcApplication = sender as WebApiApplication;
            HttpRequest request = null;
            if (mvcApplication != null) request = mvcApplication.Request;
        }
    }
}
