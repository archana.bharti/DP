﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data
{
    public static class EFExtensions
    {
        public static void Update<T>(this DPContext context, T entityObject, params string[] properties) where T : class
        {
            //context.Set<T>().Attach(entityObject);
            //context.Set<T>().AsNoTracking();

            //    var entry = context.Entry(entityObject);

            //    foreach (string name in properties)
            //        entry.Property(name).IsModified = true;

            //    await context.SaveChangesAsync();

            if (context.GetEntityKey(entityObject) != null && context.IsAttached(context.GetEntityKey(entityObject)))
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)context).ObjectContext.Detach(entityObject);
            }

            using (var newCtxt = new DPContext())
            {
                newCtxt.Set<T>().Attach(entityObject);
                newCtxt.Set<T>().AsNoTracking();

                var entry = newCtxt.Entry(entityObject);

                foreach (string name in properties)
                    entry.Property(name).IsModified = true;

                newCtxt.SaveChanges();
            }
        }

        public static async Task UpdateAsync<T>(this DPContext context, T entityObject, params string[] properties) where T : class
        {
            //context.Set<T>().Attach(entityObject);
            //context.Set<T>().AsNoTracking();

            //    var entry = context.Entry(entityObject);

            //    foreach (string name in properties)
            //        entry.Property(name).IsModified = true;

            //    await context.SaveChangesAsync();

            if (context.GetEntityKey(entityObject) != null && context.IsAttached(context.GetEntityKey(entityObject)))
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)context).ObjectContext.Detach(entityObject);
            }

            using (var newCtxt = new DPContext())
            {
                newCtxt.Set<T>().Attach(entityObject);
                newCtxt.Set<T>().AsNoTracking();

                var entry = newCtxt.Entry(entityObject);

                foreach (string name in properties)
                    entry.Property(name).IsModified = true;

                await newCtxt.SaveChangesAsync();
            }

        }

        internal static bool IsAttached(this DPContext context, EntityKey key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            var manager = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)context).ObjectContext.ObjectStateManager;
            ObjectStateEntry entry;
            if (manager.TryGetObjectStateEntry(key, out entry))
            {
                return (entry.State != System.Data.Entity.EntityState.Detached);
            }
            return false;
        }

        public static EntityKey GetEntityKey<T>(this DPContext context, T entity)  where T : class
        {
            var oc = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)context).ObjectContext;
            ObjectStateEntry ose;
            if (null != entity && oc.ObjectStateManager
                                    .TryGetObjectStateEntry(entity, out ose))
            {
                return ose.EntityKey;
            }
            return null;
        }


        public static IEnumerable<T> ExceptBy<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other, Func<T, TKey> getKey)
        {
            return from item in items
                   join otherItem in other on getKey(item)
                   equals getKey(otherItem) into tempItems
                   from temp in tempItems.DefaultIfEmpty()
                   where ReferenceEquals(null, temp) || temp.Equals(default(T))
                   select item;

        }

        public static List<T> ExceptBy<T, TKey>(this List<T> items, List<T> other, Func<T, TKey> getKey)
        {
            return (from item in items
                    join otherItem in other on getKey(item)
                    equals getKey(otherItem) into tempItems
                    from temp in tempItems.DefaultIfEmpty()
                    where ReferenceEquals(null, temp) || temp.Equals(default(T))
                    select item).ToList();

        }

        public static Type GetEnumeratedType<T>(this IEnumerable<T> _)
        {
            return typeof(T);
        }

        /*Converts DataTable To List*/
        public static List<TSource> ToList<TSource>(this DataTable dataTable) where TSource : new()
        {
            var dataList = new List<TSource>();

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            var objFieldNames = (from PropertyInfo aProp in typeof(TSource).GetProperties(flags)
                                 select new
                                 {
                                     Name = aProp.Name,
                                     Type = Nullable.GetUnderlyingType(aProp.PropertyType) ??
                         aProp.PropertyType
                                 }).ToList();
            var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
                                     select new
                                     {
                                         Name = aHeader.ColumnName,
                                         Type = aHeader.DataType
                                     }).ToList();
            var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

            foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
            {
                var aTSource = new TSource();
                foreach (var aField in commonFields)
                {
                    PropertyInfo propertyInfos = aTSource.GetType().GetProperty(aField.Name);
                    var value = (dataRow[aField.Name] == DBNull.Value) ?
                    null : dataRow[aField.Name]; //if database field is nullable
                    propertyInfos.SetValue(aTSource, value, null);
                }
                dataList.Add(aTSource);
            }
            return dataList;
        }

        /*Converts DataTable To List*/
        public static IList ToList(this DataTable dataTable, Type type)
        {
            Type listType = typeof(List<>).MakeGenericType(new[] { type });
            var dataList = (IList)Activator.CreateInstance(listType);

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            var objFieldNames = (from PropertyInfo aProp in type.GetProperties(flags)
                                 select new
                                 {
                                     Name = aProp.Name,
                                     Type = Nullable.GetUnderlyingType(aProp.PropertyType) ??
                         aProp.PropertyType
                                 }).ToList();
            var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
                                     select new
                                     {
                                         Name = aHeader.ColumnName,
                                         Type = aHeader.DataType
                                     }).ToList();
            var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

            foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
            {
                var aTSource = Activator.CreateInstance(type);
                foreach (var aField in commonFields)
                {
                    PropertyInfo propertyInfos = aTSource.GetType().GetProperty(aField.Name);
                    var value = (dataRow[aField.Name] == DBNull.Value) ?
                    null : dataRow[aField.Name]; //if database field is nullable
                    propertyInfos.SetValue(aTSource, value, null);
                }
                dataList.Add(aTSource);
            }
            return dataList;
        }

        public static TSource Set<TSource>(this TSource input, Action<TSource> updater)
        {
            updater(input);
            return input;
        }
    }
}
