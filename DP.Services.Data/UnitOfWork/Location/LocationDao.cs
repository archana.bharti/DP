﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public class LocationDao : ILocationDao, IDisposable
    {
        private DPContext context;
        public LocationDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<Location> Get()
        {
            return this.context.Locations;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<Location>> GetAllAsync()
        {
            return await this.context.Locations.ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Location> GetByIdAsync(int id)
        {
            var query = this.context.Locations.Where(x => x.Id == id);
            return await query.SingleOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <returns></returns>
        public async Task<Location> GetByCoordinates(float longitude, float latitude)
        {
            var query = this.context.Locations.Where(x => x.Latitude == latitude && x.Longitude == longitude);
            return await query.SingleOrDefaultAsync();
        }
    }
}
