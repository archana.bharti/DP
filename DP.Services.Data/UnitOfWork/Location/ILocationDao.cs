﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface ILocationDao
    {
        IQueryable<Location> Get();
        Task<List<Location>> GetAllAsync();
        Task<Location> GetByIdAsync(int id);
        Task<Location> GetByCoordinates(float longitude, float latitude);
    }
}
