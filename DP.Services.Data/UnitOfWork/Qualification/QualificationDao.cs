﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;


namespace DP.Services.Data.UnitOfWork
{
    public sealed class QualificationDao : IQualificationDao, IDisposable
    {
        private DPContext context;
        public QualificationDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public IQueryable<QualificationLong> LongQualifications { get { return this.context.QualificationLongs; } }
        public IQueryable<QualificationShort> ShortQualifications { get { return this.context.QualificationShorts; } }

        public async Task<List<QualificationLong>> GetAllLongQualifications() {  return await this.context.QualificationLongs.ToListAsync();  }
        public async Task<List<QualificationShort>> GetAllShortQualifications() {  return await this.context.QualificationShorts.ToListAsync();  }


        public async Task<List<QualificationLong>> GetLongQualificationsByDoctorId(int id)
        {
            var data = await this.context.QualificationDoctors
                .Where(x => x.DoctorId == id && x.LongQualificationId.HasValue)
                .Include(x => x.LongQualification )
                .Select(x=>x.LongQualification).ToListAsync();
            if (data == null || data.Count == 0) { return new List<QualificationLong>(); }
            return data;
        }

        public async Task<List<QualificationShort>> GetShortQualificationsByDoctorId(int id)
        {
            var data =  await this.context.QualificationDoctors
               .Where(x => x.DoctorId == id && x.ShortQualificationId.HasValue)
               .Include(x => x.ShortQualification)
               .Select(x => x.ShortQualification).ToListAsync();
            if(data == null || data.Count == 0) { return new List<QualificationShort>(); }
            return data;
        }

        public async Task<QualificationDoctor> GetDoctorById(int doctorId)
        {
            return await this.context.QualificationDoctors
               .Where(x => x.DoctorId == doctorId).SingleOrDefaultAsync();
        }

        public async Task<QualificationLong> AddLongQualificationAsync(QualificationLong qualification)
        {
            var hasWithSameName = this.context.QualificationLongs.Where(x => x.LongEndDegree.ToUpper() == qualification.LongEndDegree.ToUpper()).SingleOrDefault();
            if(hasWithSameName != null) { throw new Exception("A qualification with the same name already exists. Please try with a different name."); }
            this.context.QualificationLongs.Add(qualification);
            await this.context.SaveChangesAsync();
            return qualification;
        }

        public async Task DeleteLongQualificationAsync(QualificationLong qualification)
        {
            var _q = await this.context.QualificationLongs.Where(x => x.Id == qualification.Id).SingleAsync();
            this.context.QualificationLongs.Remove(_q);
            await this.context.SaveChangesAsync();
        }

        public async Task<QualificationShort> AddShortQualificationAsync(QualificationShort qualification)
        {
            var hasWithSameName = this.context.QualificationShorts.Where(x => x.ShortEndDegree.ToUpper() == qualification.ShortEndDegree.ToUpper()).SingleOrDefault();
            if (hasWithSameName != null) { throw new Exception("A qualification with the same name already exists. Please try with a different name."); }
            this.context.QualificationShorts.Add(qualification);
            await this.context.SaveChangesAsync();
            return qualification;
        }

        public async Task DeleteShortQualificationAsync(QualificationShort qualification)
        {
            var _q = await this.context.QualificationShorts.Where(x => x.Id == qualification.Id).SingleAsync();
            this.context.QualificationShorts.Remove(_q);
            await this.context.SaveChangesAsync();
        }

        public async Task<QualificationDoctor> AddOrUpdate(QualificationDoctor qualif)
        {
            QualificationDoctor qualifAddOrUpdate = null;
            if (qualif.LongQualificationId.HasValue)
            {
                qualifAddOrUpdate = GetRowByLongQualificationNull(qualif.DoctorId);
                if(qualifAddOrUpdate == null)
                {
                    qualifAddOrUpdate = AddDoctorQualification(qualif);
                }
                else
                {
                    qualifAddOrUpdate.LongQualificationId = qualif.LongQualificationId;
                    await this.context.SaveChangesAsync();
                }
            }

            if (qualif.ShortQualificationId.HasValue)
            {
                qualifAddOrUpdate = GetRowByShortQualificationNull(qualif.DoctorId);
                if (qualifAddOrUpdate == null)
                {
                    qualifAddOrUpdate = AddDoctorQualification(qualif);
                }
                else
                {
                    qualifAddOrUpdate.ShortQualificationId = qualif.ShortQualificationId;
                    await this.context.SaveChangesAsync();
                }
            }

            return qualifAddOrUpdate;
        }

        public async Task<QualificationDoctor> Remove(QualificationDoctor qualif)
        {
            QualificationDoctor qualifAddOrUpdate = null;
            if (qualif.LongQualificationId.HasValue)
            {
                qualifAddOrUpdate = GetRowByLongQualification(qualif.DoctorId, qualif.LongQualificationId.Value);
                if (qualifAddOrUpdate != null)
                {
                    qualifAddOrUpdate.LongQualificationId = null;
                    await this.context.SaveChangesAsync();
                }
            }

            if (qualif.ShortQualificationId.HasValue)
            {
                qualifAddOrUpdate = GetRowByShortQualification(qualif.DoctorId, qualif.ShortQualificationId.Value);
                if (qualifAddOrUpdate != null)
                {
                    qualifAddOrUpdate.ShortQualificationId = null;// qualif.ShortQualificationId;
                    await this.context.SaveChangesAsync();
                }
            }

            return qualifAddOrUpdate;
        }

        private bool DoctorHasLongQualification(int doctorId, int longQualificationId)
        {
           return this.context.QualificationDoctors.Where(x => 
           x.DoctorId == doctorId && 
           x.LongQualificationId.HasValue && 
           x.LongQualificationId.Value == longQualificationId).Count() == 1;
        }
        
        private bool DoctorHasShortQualification(int doctorId, int shortQualificationId)
        {
            return this.context.QualificationDoctors.Where(x =>
            x.DoctorId == doctorId &&
            x.ShortQualificationId.HasValue &&
            x.ShortQualificationId.Value == shortQualificationId).Count() == 1;
        }

        private QualificationDoctor GetRowByLongQualificationNull(int doctorId)
        {
            var row = this.context.QualificationDoctors.Where(x => x.DoctorId == doctorId &&
            x.LongQualificationId == null).FirstOrDefault();
            return row;
        }

        private QualificationDoctor GetRowByLongQualification(int doctorId, int qualifId)
        {
            var row = this.context.QualificationDoctors.Where(x => x.DoctorId == doctorId &&
            x.LongQualificationId == qualifId).Single();
            return row;
        }

        private QualificationDoctor GetRowByShortQualificationNull(int doctorId)
        {
            var row = this.context.QualificationDoctors.Where(x => x.DoctorId == doctorId &&
            x.ShortQualificationId == null).FirstOrDefault();
            return row;
        }

        private QualificationDoctor GetRowByShortQualification(int doctorId, int qualifId)
        {
            var row = this.context.QualificationDoctors.Where(x => x.DoctorId == doctorId &&
            x.ShortQualificationId == qualifId).Single();
            return row;
        }

        private bool IsLongQualificationExists(string name, out QualificationLong qualification)
        {
            qualification = null;
            qualification = this.context.QualificationLongs.Where(x => x.LongEndDegree.ToUpper() == name.ToUpper()).SingleOrDefault();
            return qualification != null;
        }

        private bool IsShortQualificationExists(string name, out QualificationShort qualification)
        {
            qualification = null;
            qualification = this.context.QualificationShorts.Where(x => x.ShortEndDegree.ToUpper() == name.ToUpper()).SingleOrDefault();
            return qualification != null;
        }

        private QualificationDoctor AddDoctorQualification(QualificationDoctor doctor)
        {
            this.context.QualificationDoctors.Add(doctor);
            this.context.SaveChanges();
            return doctor;
        }

        private async Task<QualificationLong> AddLongQualification(QualificationLong qualification)
        {
            this.context.QualificationLongs.Add(qualification);
            await this.context.SaveChangesAsync();
            return qualification;
        }

        private async Task<QualificationShort> AddShortQualification(QualificationShort qualification)
        {
            this.context.QualificationShorts.Add(qualification);
            await this.context.SaveChangesAsync();
            return qualification;
        }
    }
}
