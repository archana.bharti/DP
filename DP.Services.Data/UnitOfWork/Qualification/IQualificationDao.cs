﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IQualificationDao
    {
        IQueryable<QualificationLong> LongQualifications { get; }
        IQueryable<QualificationShort> ShortQualifications { get; }

        Task<List<QualificationLong>> GetAllLongQualifications();
        Task<List<QualificationShort>> GetAllShortQualifications();

        Task<List<QualificationLong>> GetLongQualificationsByDoctorId(int id);
        Task<List<QualificationShort>> GetShortQualificationsByDoctorId(int id);

        Task<QualificationLong> AddLongQualificationAsync(QualificationLong qualification);
        Task<QualificationShort> AddShortQualificationAsync(QualificationShort qualification);

        Task DeleteLongQualificationAsync(QualificationLong qualification);
        Task DeleteShortQualificationAsync(QualificationShort qualification);

        Task<QualificationDoctor> AddOrUpdate(QualificationDoctor doctorQualification);
        Task<QualificationDoctor> Remove(QualificationDoctor doctorQualification);
        // Task<QualificationLong> RemoveDoctorLongQualification(QualificationDoctor doctor);
        // Task<QualificationShort> RemoveDoctorShortQualification(QualificationDoctor doctor);
    }
}
