﻿//using DP.Services.Data.Models;
//using DP.Common.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using DP.Common.Utilities;
//using DP.Common.Enums;
//using System.Data.Entity.Infrastructure;
//using System.Data.Entity.Core.Metadata.Edm;
//using System.Threading.Tasks;
//using System.Data.Entity;
//using Admin = DP.Common.Models.Administration;

//namespace DP.Services.Data.UnitOfWork
//{
//    public class CommonDao : ICommonDao, IDisposable
//    {
//        private DPContext context;
//        public CommonDao(DPContext context)
//        {
//            this.context = context;
//        }

//        public void Dispose()
//        {
//            if (this.context != null)
//            {
//                this.context.Dispose();
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public IQueryable<State> States()
//        {
//            return this.context.States;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public List<State> GetStates()
//        {
//            return this.context.States.ToList();
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public IQueryable<City> Cities()
//        {
//            return this.context.Cities;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public List<City> GetCities()
//        {
//            return this.context.Cities.ToList();
//        }


//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public IQueryable<Location> Locations()
//        {
//            return this.context.Locations;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public List<Location> GetLocations()
//        {
//            return this.context.Locations.ToList();
//        }


//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public IQueryable<Specialization> Specializations()
//        {
//            return this.context.Specializations;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public List<Specialization> GetSpecializations()
//        {
//            return this.context.Specializations.ToList();
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public IQueryable<ClinicAndDoctor> ClinicsAndDoctors()
//        {
//            return this.context.ClinicsAndDoctors;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public List<ClinicAndDoctor> GetClinicsAndDoctors()
//        {
//            return this.context.ClinicsAndDoctors.ToList();
//        }

//        public IQueryable<Hospital> Hospitals()
//        {
//            return this.context.Hospitals;
//        }

//        public IQueryable<College> Colleges()
//        {
//            return this.context.Colleges;
//        }

//        public List<Feature> GetFeatureEntities(int type)
//        {
//            return this.context.Features.Where(f => f.EntityType == type && f.StatusValue == (int)UserStatus.Active).ToList();
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public DPContext GetDpContext()
//        {
//            return this.context;
//        }

//        public List<T> ExecuteQuery<T>(string query)
//        {
//            return this.context.Database.SqlQuery<T>(query).ToList<T>();
//        }

//        public async Task<List<T>> ExecuteQueryAsync<T>(string query)
//        {
//            return await this.context.Database.SqlQuery<T>(query).ToListAsync();
//        }

//        public T ExecuteQuerySingle<T>(string query)
//        {
//            return this.context.Database.SqlQuery<T>(query).SingleOrDefault<T>();
//        }

//        public int ExecuteQuery(string query)
//        {
//            return this.context.Database.SqlQuery<int>(query).First<int>();
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="months"></param>
//        /// <param name="years"></param>
//        /// <param name="userType"></param>
//        /// <returns>1.Month 2.Count 3.SubsriberIds</returns>
//        public List<FeaturedReportViewModel> GetClinicsFeaturedReport(List<int> months, int year)
//        {
//            var query = @"select concat(monthname(STR_TO_DATE(Month, '%m')),'-',Count,'-',SubscriberIds) as data from (
//                            SELECT 
//                            group_concat(asp.SubscriberId) as SubscriberIds, 
//                            Month(asp.CreatedOn) as Month, 
//                            Year(asp.createdOn) as Year, 
//                            Count(asp.Id) as Count FROM aspnetusers asp
//                            inner join dp_feature fea on fea.EntityId = asp.SubscriberId and fea.EntityType =1 and fea.StatusValue = 4
//                            where asp.SubscriberId is not null
//                            and Month(asp.CreatedOn) in ({0})
//                            and Year(asp.CreatedOn) = {1}
//                            and upper(asp.UserType) = 'CLINIC'
//                            GROUP BY MONTH(asp.CreatedOn)
//                            ) p1";
//            query = string.Format(query, string.Join(",", months.Select(x => x.ToString()).ToArray()), year);
//            var datum = this.ExecuteQuery<string>(query);

//            string[] temp = null;
//            string month = string.Empty;
//            int count = 0;
//            List<int> subscriberIds = new List<int>();
//            List<FeaturedReportViewModel> reportList = new List<FeaturedReportViewModel>();
//            List<EntityViewModel> entityList;
//            foreach (var data in datum)
//            {
//                temp = data.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
//                month = temp[0];
//                count = Convert.ToInt32(temp[1]);
//                entityList = new List<EntityViewModel>();
//                foreach (var sub in temp[2].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
//                {
//                    subscriberIds.Add(Convert.ToInt32(sub));
//                    entityList.Add(new EntityViewModel() { EntityId = Convert.ToInt32(sub) });
//                }
//                reportList.Add(new FeaturedReportViewModel()
//                {
//                    Count = count,
//                    Month = month,
//                    Year = year,
//                    Subscribers = entityList
//                });
//            }

//            if (subscriberIds.Count > 0)
//            {
//                var clinicsOrDoctors = this.context.ClinicsAndDoctors.Where(x => subscriberIds.Contains(x.Id)).ToList();
//                reportList.ForEach(x =>
//                {
//                    var ss = x.Subscribers;
//                    ss.ForEach(y =>
//                    {
//                        var clicOrDoc = clinicsOrDoctors.Where(z => z.Id == y.EntityId).Single();
//                        y.EntityType = (int)EnumHelper<UserType>.GetValueFromDescription(clicOrDoc.Type);
//                        y.EntityTypeText = clicOrDoc.Type;
//                        y.ImageUrl = clicOrDoc.Profile_Pic_1;
//                        y.EntityName = clicOrDoc.Name;
//                    });
//                });
//            }
//            return reportList;

//        }

//        public List<FeaturedReportViewModel> GetDoctorsFeaturedReport(List<int> months, int year)
//        {
//            var query = @"select concat(monthname(STR_TO_DATE(Month, '%m')),'-',Count,'-',SubscriberIds) as data from (
//                            SELECT 
//                            group_concat(asp.SubscriberId) as SubscriberIds, 
//                            Month(asp.CreatedOn) as Month, 
//                            Year(asp.createdOn) as Year, 
//                            Count(asp.Id) as Count FROM aspnetusers asp
//                            inner join dp_feature fea on fea.EntityId = asp.SubscriberId and fea.EntityType =2 and fea.StatusValue = 4
//                            where asp.SubscriberId is not null
//                            and Month(asp.CreatedOn) in ({0})
//                            and Year(asp.CreatedOn) = {1}
//                            and upper(asp.UserType) = 'DOCTOR'
//                            GROUP BY MONTH(asp.CreatedOn)
//                            ) p1";
//            query = string.Format(query, string.Join(",", months.Select(x => x.ToString()).ToArray()), year);
//            var datum = this.ExecuteQuery<string>(query);

//            string[] temp = null;
//            string month = string.Empty;
//            int count = 0;
//            List<int> subscriberIds = new List<int>();
//            List<FeaturedReportViewModel> reportList = new List<FeaturedReportViewModel>();
//            List<EntityViewModel> entityList;
//            foreach (var data in datum)
//            {
//                temp = data.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
//                month = temp[0];
//                count = Convert.ToInt32(temp[1]);
//                entityList = new List<EntityViewModel>();
//                foreach (var sub in temp[2].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
//                {
//                    subscriberIds.Add(Convert.ToInt32(sub));
//                    entityList.Add(new EntityViewModel() { EntityId = Convert.ToInt32(sub) });
//                }
//                reportList.Add(new FeaturedReportViewModel()
//                {
//                    Count = count,
//                    Month = month,
//                    Year = year,
//                    Subscribers = entityList
//                });
//            }
//            if (subscriberIds.Count > 0)
//            {
//                var clinicsOrDoctors = this.context.ClinicsAndDoctors.Where(x => subscriberIds.Contains(x.Id)).ToList();
//                reportList.ForEach(x =>
//                {
//                    var ss = x.Subscribers;
//                    ss.ForEach(y =>
//                    {
//                        var clicOrDoc = clinicsOrDoctors.Where(z => z.Id == y.EntityId).Single();
//                        y.EntityType = (int)EnumHelper<UserType>.GetValueFromDescription(clicOrDoc.Type);
//                        y.EntityTypeText = clicOrDoc.Type;
//                        y.ImageUrl = clicOrDoc.Profile_Pic_1;
//                        y.EntityName = clicOrDoc.Name;
//                    });
//                });
//            }
//            return reportList;
//        }

//        public List<FeaturedReportViewModel> GetHospitalsFeaturedReport(List<int> months, int year)
//        {
//            var query = @"select concat(monthname(STR_TO_DATE(Month, '%m')),'-',Count,'-',SubscriberIds) as data from (
//                            SELECT 
//                            group_concat(asp.SubscriberId) as SubscriberIds, 
//                            Month(asp.CreatedOn) as Month, 
//                            Year(asp.createdOn) as Year, 
//                            Count(asp.Id) as Count FROM aspnetusers asp
//                            inner join dp_feature fea on fea.EntityId = asp.SubscriberId and fea.EntityType =3 and fea.StatusValue = 4
//                            where asp.SubscriberId is not null
//                            and Month(asp.CreatedOn) in ({0})
//                            and Year(asp.CreatedOn) = {1}
//                            and upper(asp.UserType) = 'HOSPITAL'
//                            GROUP BY MONTH(asp.CreatedOn)
//                            ) p1";
//            query = string.Format(query, string.Join(",", months.Select(x => x.ToString()).ToArray()), year);
//            var datum = this.ExecuteQuery<string>(query);

//            string[] temp = null;
//            string month = string.Empty;
//            int count = 0;
//            List<int> subscriberIds = new List<int>();
//            List<FeaturedReportViewModel> reportList = new List<FeaturedReportViewModel>();
//            List<EntityViewModel> entityList;
//            foreach (var data in datum)
//            {
//                temp = data.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
//                month = temp[0];
//                count = Convert.ToInt32(temp[1]);
//                entityList = new List<EntityViewModel>();
//                foreach (var sub in temp[2].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
//                {
//                    subscriberIds.Add(Convert.ToInt32(sub));
//                    entityList.Add(new EntityViewModel() { EntityId = Convert.ToInt32(sub) });
//                }
//                reportList.Add(new FeaturedReportViewModel()
//                {
//                    Count = count,
//                    Month = month,
//                    Year = year,
//                    Subscribers = entityList
//                });
//            }
//            if (subscriberIds.Count > 0)
//            {
//                var clinicsOrDoctors = this.context.Hospitals.Where(x => subscriberIds.Contains(x.Id)).ToList();
//                reportList.ForEach(x =>
//                {
//                    var ss = x.Subscribers;
//                    ss.ForEach(y =>
//                    {
//                        var clicOrDoc = clinicsOrDoctors.Where(z => z.Id == y.EntityId).Single();
//                        y.EntityType = (int)EnumHelper<UserType>.GetValueFromDescription(clicOrDoc.Type);
//                        y.EntityTypeText = clicOrDoc.Type;
//                        y.ImageUrl = clicOrDoc.Profile_Pic_1;
//                        y.EntityName = clicOrDoc.Name;
//                    });
//                });
//            }
//            return reportList;
//        }

//        public List<FeaturedReportViewModel> GetCollegesFeaturedReport(List<int> months, int year)
//        {
//            var query = @"select concat(monthname(STR_TO_DATE(Month, '%m')),'-',Count,'-',SubscriberIds) as data from (
//                            SELECT 
//                            group_concat(asp.SubscriberId) as SubscriberIds, 
//                            Month(asp.CreatedOn) as Month, 
//                            Year(asp.createdOn) as Year, 
//                            Count(asp.Id) as Count FROM aspnetusers asp
//                            inner join dp_feature fea on fea.EntityId = asp.SubscriberId and fea.EntityType = 4 and fea.StatusValue = 4
//                            where asp.SubscriberId is not null
//                            and Month(asp.CreatedOn) in ({0})
//                            and Year(asp.CreatedOn) = {1}
//                            and upper(asp.UserType) = 'COLLEGE'
//                            GROUP BY MONTH(asp.CreatedOn)
//                            ) p1";
//            query = string.Format(query, string.Join(",", months.Select(x => x.ToString()).ToArray()), year);
//            var datum = this.ExecuteQuery<string>(query);

//            string[] temp = null;
//            string month = string.Empty;
//            int count = 0;
//            List<int> subscriberIds = new List<int>();
//            List<FeaturedReportViewModel> reportList = new List<FeaturedReportViewModel>();
//            List<EntityViewModel> entityList;
//            foreach (var data in datum)
//            {
//                temp = data.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
//                month = temp[0];
//                count = Convert.ToInt32(temp[1]);
//                entityList = new List<EntityViewModel>();
//                foreach (var sub in temp[2].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
//                {
//                    subscriberIds.Add(Convert.ToInt32(sub));
//                    entityList.Add(new EntityViewModel() { EntityId = Convert.ToInt32(sub) });
//                }
//                reportList.Add(new FeaturedReportViewModel()
//                {
//                    Count = count,
//                    Month = month,
//                    Year = year,
//                    Subscribers = entityList
//                });
//            }

//            if(subscriberIds.Count > 0)
//            {
//                var clinicsOrDoctors = this.context.Colleges.Where(x => subscriberIds.Contains(x.Id)).ToList();
//                reportList.ForEach(x =>
//                {
//                    var ss = x.Subscribers;
//                    ss.ForEach(y =>
//                    {
//                        var clicOrDoc = clinicsOrDoctors.Where(z => z.Id == y.EntityId).Single();
//                        y.EntityType = (int)EnumHelper<UserType>.GetValueFromDescription(clicOrDoc.Type);
//                        y.EntityTypeText = clicOrDoc.Type;
//                        y.ImageUrl = clicOrDoc.Profile_Pic_1;
//                        y.EntityName = clicOrDoc.Name;
//                    });
//                });
//            }
           
//            return reportList;
//        }

//        public List<FeaturedReportViewModel> GetInsurancesFeaturedReport(List<int> months, int year)
//        {
//            var query = @"select concat(monthname(STR_TO_DATE(Month, '%m')),'-',Count,'-',SubscriberIds) as data from (
//                            SELECT 
//                            group_concat(asp.SubscriberId) as SubscriberIds, 
//                            Month(asp.CreatedOn) as Month, 
//                            Year(asp.createdOn) as Year, 
//                            Count(asp.Id) as Count FROM aspnetusers asp
//                            inner join dp_feature fea on fea.EntityId = asp.SubscriberId and fea.EntityType = 6 and fea.StatusValue = 4
//                            where asp.SubscriberId is not null
//                            and Month(asp.CreatedOn) in ({0})
//                            and Year(asp.CreatedOn) = {1}
//                            and upper(asp.UserType) = 'INSURANCE'
//                            GROUP BY MONTH(asp.CreatedOn)
//                            ) p1";
//            query = string.Format(query, string.Join(",", months.Select(x => x.ToString()).ToArray()), year);
//            var datum = this.ExecuteQuery<string>(query);

//            string[] temp = null;
//            string month = string.Empty;
//            int count = 0;
//            List<int> subscriberIds = new List<int>();
//            List<FeaturedReportViewModel> reportList = new List<FeaturedReportViewModel>();
//            List<EntityViewModel> entityList;
//            foreach (var data in datum)
//            {
//                temp = data.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
//                month = temp[0];
//                count = Convert.ToInt32(temp[1]);
//                entityList = new List<EntityViewModel>();
//                foreach (var sub in temp[2].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
//                {
//                    subscriberIds.Add(Convert.ToInt32(sub));
//                    entityList.Add(new EntityViewModel() { EntityId = Convert.ToInt32(sub) });
//                }
//                reportList.Add(new FeaturedReportViewModel()
//                {
//                    Count = count,
//                    Month = month,
//                    Year = year,
//                    Subscribers = entityList
//                });
//            }

//            if (subscriberIds.Count > 0)
//            {
//                var clinicsOrDoctors = this.context.Insurances.Where(x => subscriberIds.Contains(x.Id)).ToList();
//                reportList.ForEach(x =>
//                {
//                    var ss = x.Subscribers;
//                    ss.ForEach(y =>
//                    {
//                        var clicOrDoc = clinicsOrDoctors.Where(z => z.Id == y.EntityId).Single();
//                        y.EntityType = 6;
//                        y.EntityTypeText = "INSURANCE";
//                        y.ImageUrl = clicOrDoc.ImageUrl;
//                        y.EntityName = clicOrDoc.Name;
//                    });
//                });
//            }

//            return reportList;
//        }

//        public Dictionary<string, string> Options()
//        {
//            return context.Options.ToDictionary(x => x.Key, x => x.Value);
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public List<DatabaseTableViewModel> GetTableNames()
//        {
//            var metadata = ((IObjectContextAdapter)this.context).ObjectContext.MetadataWorkspace;
//            List<DatabaseTableViewModel> tablesList = new List<DatabaseTableViewModel>();
//            var tables = metadata.GetItemCollection(DataSpace.SSpace)
//                .GetItems<EntityContainer>()
//                .Single()
//                .BaseEntitySets
//                .OfType<EntitySet>()
//                .Where(s => !s.MetadataProperties.Contains("Type") || s.MetadataProperties["Type"]
//                .ToString() == "Tables");

//            foreach (var table in tables)
//            {
//                var tableName = table
//                                .MetadataProperties
//                                .Contains("Table") && table.MetadataProperties["Table"].Value != null
//                                      ? table.MetadataProperties["Table"].Value.ToString()
//                                      : table.Name;

//                tablesList.Add(new DatabaseTableViewModel() { EntityName = table.Name, TableName = tableName });
//            }

//            return tablesList;
//        }

//        private string clinicSubscriberQuery = @"select 
//                            asp.Id as UserId,
//                            asp.UserName as Email,
//                            asp.IsTerminated,
//                            dc.Id as SubscriberId,
//                            dc.`Name` as `Name`,
//                            dc.Profile_Pic_1 as ImageUrl,
//                            asp.userType as `Type`,
//                            loc.Name as Location,
//                            group_concat(spz.Category) as Specializations,
//                            ifnull(rec.RecommendationCount,0) as RecommendationCount,
//                            ifnull(rat.RatingCount,0) as RatingCount,
//                          
//                            ftr.StartDate,
//                            ftr.EndDate,
//                            ifnull(ftr.EnableBasic,0) as EnableBasic,
//							ifnull(ftr.EnableTextual,0) as EnableTextual,
//							ifnull(ftr.EnableFeatured,0) as EnableFeatured,
//                            ifnull(ftr.StatusValue,0) as Active
//
//                            from aspnetusers asp
//                            inner join dp_clinic_doctor dc 
//                            on dc.id = asp.SubscriberId
//
//                            left outer join dp_specialization_clinic spc on
//                            spc.ClinicId = dc.Id
//
//                            left outer join dp_specialization spz on
//                            spz.id = spc.SpecializationId
//
//                            inner join dp_location loc
//                            on loc.Id = dc.LocationId
//
//                            left outer join dp_feature ftr
//                            on ftr.EntityId = dc.Id and ftr.EntityType = 1
//
//
//                            left outer join
//                            (select EntityId, EntityType, Count(EntityId) as RecommendationCount from dp_recommend group by EntityId, EntityType) rec
//                            on rec.EntityId = dc.Id and rec.EntityType = 1
//
//                            left outer join
//                            (select ClinicOrDoctorId, Count(ClinicOrDoctorId) as RatingCount from dp_clinic_rating group by ClinicOrDoctorId) rat
//                            on rat.ClinicOrDoctorId = dc.Id
//
//                            where asp.userType = 'CLINIC'  [CONDITIONS]
//                            group by asp.Id, asp.UserName, dc.Id,asp.userType, loc.Name, dc.Profile_Pic_1,
//                            ftr.StartDate,
//                            ftr.EndDate,
//                            ftr.StatusValue,
//                            ftr.EnableBasic,
//                            ftr.EnableFeatured,
//                            ftr.EnableTextual,
//                            rec.EntityId";

//        private string doctorSubscriberQuery = @"select 
//                            asp.Id as UserId,
//                            asp.UserName as Email,
//                            asp.IsTerminated,
//                            dc.Id as SubscriberId,
//                            dc.`Name` as `Name`,
//                            dc.Profile_Pic_1 as ImageUrl,
//                            asp.userType as `Type`,
//                            loc.Name as Location,
//                            group_concat(spz.Category) as Specializations,
//                            ifnull(rec.RecommendationCount,0) as RecommendationCount,
//                            ifnull(rat.RatingCount,0) as RatingCount,
//                           
//                            ftr.StartDate,
//                            ftr.EndDate,
//                            ifnull(ftr.EnableBasic,0) as EnableBasic,
//							ifnull(ftr.EnableTextual,0) as EnableTextual,
//							ifnull(ftr.EnableFeatured,0) as EnableFeatured,
//                            ifnull(ftr.StatusValue,0) as Active
//
//                            from aspnetusers asp
//                            inner join dp_clinic_doctor dc 
//                            on dc.id = asp.SubscriberId
//
//                            left outer join dp_specialization_clinic spc on
//                            spc.ClinicId = dc.Id
//
//                            left outer join dp_specialization spz on
//                            spz.id = spc.SpecializationId
//
//                            inner join dp_location loc
//                            on loc.Id = dc.LocationId
//
//                            left outer join dp_feature ftr
//                            on ftr.EntityId = dc.Id and ftr.EntityType = 2
//
//
//                            left outer join
//                            (select EntityId, EntityType, Count(EntityId) as RecommendationCount from dp_recommend group by EntityId, EntityType) rec
//                            on rec.EntityId = dc.Id and rec.EntityType = 2
//
//                            left outer join
//                            (select ClinicOrDoctorId, Count(ClinicOrDoctorId) as RatingCount from dp_clinic_rating group by ClinicOrDoctorId) rat
//                            on rat.ClinicOrDoctorId = dc.Id
//
//                            where asp.userType = 'DOCTOR' [CONDITIONS]
//                            group by asp.Id, asp.UserName, dc.Id,asp.userType, loc.Name, dc.Profile_Pic_1,
//                            ftr.StartDate,
//                            ftr.EndDate,
//                            ftr.StatusValue,
//                            ftr.EnableBasic,
//                            ftr.EnableFeatured,
//                            ftr.EnableTextual,
//                            rec.EntityId";

//        private string hosptialSubscriberQuery = @"select 
//                            asp.Id as UserId,
//                            asp.UserName as Email,
//                            asp.IsTerminated,
//                            hosp.Id as SubscriberId,
//                            hosp.`Name` as `Name`,
//                            hosp.Profile_Pic_1 as ImageUrl,
//                            asp.userType as `Type`,
//                            loc.Name as Location,
//                            group_concat(spz.Category) as Specializations,
//                            ifnull(rec.RecommendationCount,0) as RecommendationCount,
//                            ifnull(rat.RatingCount,0) as RatingCount,
//                           
//                            ftr.StartDate,
//                            ftr.EndDate,
//                            ifnull(ftr.EnableBasic,0) as EnableBasic,
//							ifnull(ftr.EnableTextual,0) as EnableTextual,
//							ifnull(ftr.EnableFeatured,0) as EnableFeatured,
//                            ifnull(ftr.StatusValue,0) as Active
//
//                            from aspnetusers asp
//                            inner join dp_hospital hosp 
//                            on hosp.id = asp.SubscriberId
//
//                            left outer join dp_specialization_hospital spc on
//                            spc.HospitalId = hosp.Id
//
//                            left outer join dp_specialization spz on
//                            spz.id = spc.SpecializationId
//
//                            inner join dp_location loc
//                            on loc.Id = hosp.LocationId
//
//                            left outer join dp_feature ftr
//                            on ftr.EntityId = hosp.Id and ftr.EntityType = 3
//
//
//                            left outer join
//                            (select EntityId, EntityType, Count(EntityId) as RecommendationCount from dp_recommend group by EntityId, EntityType) rec
//                            on rec.EntityId = hosp.Id and rec.EntityType = 3
//
//                            left outer join
//                            (select HospitalId, Count(HospitalId) as RatingCount from dp_hospital_rating group by HospitalId) rat
//                            on rat.HospitalId = hosp.Id
//
//                            where asp.userType = 'HOSPITAL' [CONDITIONS]
//                            group by asp.Id, asp.UserName, hosp.Id,asp.userType, loc.Name, hosp.Profile_Pic_1,
//                            ftr.StartDate,
//                            ftr.EndDate,
//                            ftr.StatusValue,
//                            ftr.EnableBasic,
//                            ftr.EnableFeatured,
//                            ftr.EnableTextual,
//                            rec.EntityId";

//        private string collegeSubscriberQuery = @"select 
//	                                                asp.Id as UserId,
//	                                                asp.UserName as Email,
//	                                                asp.IsTerminated,
//	                                                colg.Id as SubscriberId,
//	                                                colg.`Name` as `Name`,
//	                                                colg.Profile_Pic_1 as ImageUrl,
//	                                                asp.userType as `Type`,
//	                                                loc.Name as Location,
//	                                                NULL as Specializations,
//	                                                ifnull(rec.RecommendationCount,0) as RecommendationCount,
//	                                                ifnull(rat.RatingCount,0) as RatingCount,
//   
//	                                                ftr.StartDate,
//	                                                ftr.EndDate,
//	                                                ifnull(ftr.EnableBasic,0) as EnableBasic,
//	                                                ifnull(ftr.EnableTextual,0) as EnableTextual,
//	                                                1 as EnableFeatured,
//	                                                ifnull(ftr.StatusValue,0) as Active
//
//	                                                from aspnetusers asp
//	                                                inner join dp_college colg 
//	                                                on colg.id = asp.SubscriberId
//
//	                                                inner join dp_college_location loc
//	                                                on loc.Id = colg.cityId
//
//	                                                left outer join dp_feature ftr
//	                                                on ftr.EntityId = colg.Id and ftr.EntityType = 4
//
//	                                                left outer join
//	                                                (select EntityId, EntityType, Count(EntityId) as RecommendationCount from dp_recommend group by EntityId, EntityType) rec
//	                                                on rec.EntityId = colg.Id and rec.EntityType = 4
//
//	                                                left outer join
//	                                                (select collegeId, Count(collegeId) as RatingCount from dp_college_rating group by CollegeId) rat
//	                                                on rat.collegeId = colg.Id
//
//	                                                where asp.userType = 'COLLEGE' [CONDITIONS]
//	                                                group by asp.Id, asp.UserName, colg.Id,asp.userType, loc.Name, colg.Profile_Pic_1,
//	                                                ftr.StartDate,
//	                                                ftr.EndDate,
//	                                                ftr.StatusValue,
//	                                                ftr.EnableBasic,
//	                                                ftr.EnableFeatured,
//	                                                ftr.EnableTextual,
//	                                                rec.EntityId";

//        private string insuranceSubscriberQuery = @"select 
//		                                            asp.Id as UserId,
//		                                            asp.UserName as Email,
//		                                            asp.IsTerminated,
//		                                            ins.Id as SubscriberId,
//		                                            concat(ins.`PlanName`, ':', ins.providerName, ':', planType)  as `Name`,
//		                                            ins.ImageUrl as ImageUrl,
//		                                            asp.userType as `Type`,
//		                                            NULL as Location,
//		                                            NULL as Specializations,
//		                                            ifnull(rec.RecommendationCount,0) as RecommendationCount,
//		                                            ifnull(rat.RatingCount,0) as RatingCount,
//
//		                                            ftr.StartDate,
//		                                            ftr.EndDate,
//		                                            ifnull(ftr.EnableBasic,0) as EnableBasic,
//		                                            ifnull(ftr.EnableTextual,0) as EnableTextual,
//		                                            ifnull(ftr.EnableFeatured,0) as EnableFeatured,
//		                                            ifnull(ftr.StatusValue,0) as Active
//
//		                                            from aspnetusers asp
//		                                            inner join dp_insurance ins 
//		                                            on ins.id = asp.SubscriberId	                                                
//
//		                                            left outer join dp_feature ftr
//		                                            on ftr.EntityId = ins.Id and ftr.EntityType = 6
//
//		                                            left outer join
//		                                            (select EntityId, EntityType, Count(EntityId) as RecommendationCount from dp_recommend group by EntityId, EntityType) rec
//		                                            on rec.EntityId = ins.Id and rec.EntityType = 6
//
//		                                            left outer join
//		                                            (select insuranceId, Count(insuranceId) as RatingCount from dp_insurance_rating group by insuranceId) rat
//		                                            on rat.insuranceId = ins.Id
//
//		                                            where asp.userType = 'INSURANCE'   [CONDITIONS]
//		                                            group by asp.Id, 
//		                                            asp.UserName, 
//		                                            ins.Id,asp.userType, 
//		                                            ins.ImageUrl,
//		                                            ftr.StartDate,
//		                                            ftr.EndDate,
//		                                            ftr.StatusValue,
//		                                            ftr.EnableBasic,
//		                                            ftr.EnableFeatured,
//		                                            ftr.EnableTextual,
//		                                            rec.EntityId";

//        public List<Admin.SubscriberViewModel> GetClinicOrDoctorSubscribers()
//        {
//            var query = clinicSubscriberQuery + " union " + doctorSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "");
//            return this.ExecuteQuery<Admin.SubscriberViewModel>(query);
//        }

//        public Admin.SubscriberViewModel GetClinicSubscriber(string userId)
//        {
//            var query = clinicSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "and asp.id ='" + userId + "'");
//            return this.ExecuteQuerySingle<Admin.SubscriberViewModel>(query);
//        }

//        public Admin.SubscriberViewModel GetDoctorSubscriber(string userId)
//        {
//            var query = doctorSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "and asp.id ='" + userId + "'");
//            return this.ExecuteQuerySingle<Admin.SubscriberViewModel>(query);
//        }

//        public List<Admin.SubscriberViewModel> GetHospitalSubscribers()
//        {
//            var query = hosptialSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "");
//            return this.ExecuteQuery<Admin.SubscriberViewModel>(query);
//        }

//        public Admin.SubscriberViewModel GetHospitalSubscriber(string userId)
//        {
//            var query = hosptialSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "and asp.id ='" + userId + "'");
//            return this.ExecuteQuerySingle<Admin.SubscriberViewModel>(query);
//        }

//        public List<Admin.SubscriberViewModel> GetCollegeSubscribers()
//        {
//            var query = collegeSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "");
//            return this.ExecuteQuery<Admin.SubscriberViewModel>(query);
//        }

//        public Admin.SubscriberViewModel GetCollegeSubscriber(string userId)
//        {
//            var query = collegeSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "and asp.id ='" + userId + "'");
//            return this.ExecuteQuerySingle<Admin.SubscriberViewModel>(query);
//        }

//        public List<Admin.SubscriberViewModel> GetInsuranceSubscribers()
//        {
//            var query = insuranceSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "");
//            return this.ExecuteQuery<Admin.SubscriberViewModel>(query);
//        }

//        public Admin.SubscriberViewModel GetInsuranceSubscriber(string userId)
//        {
//            var query = insuranceSubscriberQuery;
//            query = query.Replace("[CONDITIONS]", "and asp.id ='" + userId + "'");
//            return this.ExecuteQuerySingle<Admin.SubscriberViewModel>(query);
//        }

//        public Admin.SubscriberViewModel GetUserSubscriber(string userId) { throw new NotImplementedException(); }
//    }
//}
