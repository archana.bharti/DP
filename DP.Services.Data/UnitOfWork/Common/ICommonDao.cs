﻿using DP.Common.Enums;
using DP.Common.Models;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin = DP.Common.Models.Administration;

namespace DP.Services.Data.UnitOfWork
{
    public interface ICommonDao
    {
        IQueryable<State> States();
        List<State> GetStates();
        IQueryable<City> Cities();
        List<City> GetCities();
        IQueryable<Location> Locations();
        List<Location> GetLocations();
        IQueryable<Specialization> Specializations();
        List<Specialization> GetSpecializations();
        DPContext GetDpContext();
        List<ClinicAndDoctor> GetClinicsAndDoctors();
        IQueryable<Hospital> Hospitals();
        IQueryable<College> Colleges();
        IQueryable<ClinicAndDoctor> ClinicsAndDoctors();
        List<Feature> GetFeatureEntities(int type);
        List<T> ExecuteQuery<T>(string query);
        Task<List<T>> ExecuteQueryAsync<T>(string query);
        int ExecuteQuery(string query);
        List<FeaturedReportViewModel> GetClinicsFeaturedReport(List<int> months, int year);
        List<FeaturedReportViewModel> GetDoctorsFeaturedReport(List<int> months, int year);
        List<FeaturedReportViewModel> GetHospitalsFeaturedReport(List<int> months, int year);
        List<FeaturedReportViewModel> GetCollegesFeaturedReport(List<int> months, int year);
        List<FeaturedReportViewModel> GetInsurancesFeaturedReport(List<int> months, int year);
        Dictionary<string, string> Options();
        List<DatabaseTableViewModel> GetTableNames();

        List<Admin.SubscriberViewModel> GetClinicOrDoctorSubscribers();
        List<Admin.SubscriberViewModel> GetHospitalSubscribers();
        List<Admin.SubscriberViewModel> GetCollegeSubscribers();
        List<Admin.SubscriberViewModel> GetInsuranceSubscribers();
        //List<Admin.SubscriberViewModel> GetUserSubscribers();

        Admin.SubscriberViewModel GetUserSubscriber(string userId);
        Admin.SubscriberViewModel GetClinicSubscriber(string userId);
        Admin.SubscriberViewModel GetDoctorSubscriber(string userId);
        Admin.SubscriberViewModel GetHospitalSubscriber(string userId);
        Admin.SubscriberViewModel GetCollegeSubscriber(string userId);
        Admin.SubscriberViewModel GetInsuranceSubscriber(string userId);
    }
}
