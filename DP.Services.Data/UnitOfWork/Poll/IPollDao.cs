﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IPollDao
    {
        IQueryable<PollMaster> GetPollMaster { get; }
        IQueryable<Poll> GetPoll { get; }
        Task<List<PollMaster>> GetPollMasterAllAsync();
        Task<List<Poll>> GetPollAllAsync();
        Task<PollMaster> GetPollMasterByIdAsync(int id);
        Task<Poll> GetPollByIdAsync(int id);
        Task<List<Poll>> GetPollsByPollMasterIdAsync(int pollMasterId);
        Task<Poll> GetPollsByPollMasterIdAndIpAsync(int pollMasterId, string ip);
        Task<PollMaster> AddPollMaster(PollMaster model);
        Task<PollMaster> UpdatePollMaster(PollMaster model);
        Task<Poll> AddPoll(Poll model);
    }
}
