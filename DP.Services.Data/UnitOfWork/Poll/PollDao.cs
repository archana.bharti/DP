﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DP.Common.Enums;

namespace DP.Services.Data.UnitOfWork
{
    public class PollDao : IPollDao, IDisposable
    {
        private DPContext context;
        public PollDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public IQueryable<PollMaster> GetPollMaster { get { return this.context.PollMasters; } }
        public IQueryable<Poll> GetPoll { get { return this.context.Polls; } }
        public async Task<List<PollMaster>> GetPollMasterAllAsync() { return await this.context.PollMasters.ToListAsync(); } 
        public async Task<List<Poll>> GetPollAllAsync() { return await this.context.Polls.ToListAsync(); }
        public async Task<PollMaster> GetPollMasterByIdAsync(int id) { return await this.context.PollMasters.Where(x => x.Id == id).SingleOrDefaultAsync(); }
        public async Task<Poll> GetPollByIdAsync(int id) { return await this.context.Polls.Where(x => x.Id == id).SingleOrDefaultAsync(); }
        public async Task<List<Poll>> GetPollsByPollMasterIdAsync(int pollMasterId) { return await this.context.Polls.Where(x => x.PollMasterId == pollMasterId).ToListAsync(); }
        public async Task<Poll> GetPollsByPollMasterIdAndIpAsync(int pollMasterId, string ip) { return await this.context.Polls.Where(x => x.PollMasterId == pollMasterId && x.IpAddress == ip).SingleOrDefaultAsync(); }
        public async Task<PollMaster> AddPollMaster(PollMaster model) { this.context.PollMasters.Add(model);  await this.context.SaveChangesAsync(); return model; }
        public async Task<Poll> AddPoll(Poll model) { this.context.Polls.Add(model); await this.context.SaveChangesAsync(); return model; }
        public async Task<PollMaster> UpdatePollMaster(PollMaster model)
        {
            context.PollMasters.Attach(model);
            context.Entry(model).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return model;
        }
    }
}
