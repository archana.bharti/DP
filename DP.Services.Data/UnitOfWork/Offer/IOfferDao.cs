﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IOfferDao
    {
        IQueryable<OfferMaster> Offers();
        IQueryable<OfferTransaction> OfferTransactions();
        List<OfferMaster> GetOffers();
        List<OfferTransaction> GetOfferTransactions();
        OfferMaster GetById(OfferMaster offerMaster);
        OfferTransaction GetById(OfferTransaction offerTransaction);
        OfferMaster Add(OfferMaster offerMaster);
        OfferTransaction Add(OfferTransaction offerTransaction);
        OfferMaster Update(OfferMaster offerMaster);
        OfferTransaction Update(OfferTransaction offerTransaction);
    }
}
