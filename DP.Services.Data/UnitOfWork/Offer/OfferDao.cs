﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data;

namespace DP.Services.Data.UnitOfWork
{
    public sealed class OfferDao: IOfferDao, IDisposable
    {
        private DPContext context;
        public OfferDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<OfferMaster> Offers()
        {
            return this.context.OfferMasters;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<OfferTransaction> OfferTransactions()
        {
            return this.context.OfferTransactions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<OfferMaster> GetOffers()
        {
            return this.context.OfferMasters
                .Include(x=>x.OfferStatus)
                .ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<OfferTransaction> GetOfferTransactions()
        {
            return this.context.OfferTransactions
                .ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerMaster"></param>
        /// <returns></returns>
        public OfferMaster GetById(OfferMaster offerMaster)
        {
            return this.context.OfferMasters.Where(x => x.Id == offerMaster.Id)
              .Include(x => x.OfferStatus).SingleOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerTransaction"></param>
        /// <returns></returns>
        public OfferTransaction GetById(OfferTransaction offerTransaction)
        {
            return this.context.OfferTransactions.Where(x => x.Id == offerTransaction.Id).SingleOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerMaster"></param>
        /// <returns></returns>
        public OfferMaster Add(OfferMaster offerMaster)
        {
            this.context.OfferMasters.Add(offerMaster);
            this.context.SaveChanges();
            return offerMaster;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerTransaction"></param>
        /// <returns></returns>
        public OfferTransaction Add(OfferTransaction offerTransaction)
        {
            this.context.OfferTransactions.Add(offerTransaction);
            this.context.SaveChanges();
            return offerTransaction;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerMaster"></param>
        /// <returns></returns>
        public OfferMaster Update(OfferMaster offerMaster)
        {
            this.context.Entry(offerMaster).State = EntityState.Modified;
            this.context.SaveChanges();
            return offerMaster;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerTransaction"></param>
        /// <returns></returns>
        public OfferTransaction Update(OfferTransaction offerTransaction)
        {
            this.context.Entry(offerTransaction).State = EntityState.Modified;
            this.context.SaveChanges();
            return offerTransaction;
        }
    }
}
