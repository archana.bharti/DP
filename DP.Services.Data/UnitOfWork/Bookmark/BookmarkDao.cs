﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class BookmarkDao : IBookmarkDao, IDisposable
    {
        private DPContext context;
        public BookmarkDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// Bookmarkses this instance.
        /// </summary>
        /// <returns>IQueryable<Bookmark></returns>
        public IQueryable<Bookmark> Bookmarks()
        {
            return context.Bookmarks;
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns>IQueryable<Bookmark>.</returns>
        public IQueryable<Bookmark> Get()
        {
            return context.Bookmarks;
        }

        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <returns>Task<List<Bookmark>>.</returns>
        /// B
        public async Task<List<Bookmark>> GetAllAsync()
        {
            return await context.Bookmarks.ToListAsync<Bookmark>();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<Bookmark>.</returns>
        public async Task<Bookmark> GetByIdAsync(int id)
        {
            return await context.Bookmarks
                .Where(x => x.Id == id).SingleOrDefaultAsync();
        }

        /// <summary>
        /// get by user identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<List<Bookmark>>.</returns>
        public async Task<List<Bookmark>> GetByUserAsync(Bookmark bookmark)
        {
            return await context.Bookmarks
                .Where(x => x.EntityId == bookmark.EntityId && x.EntityType == x.EntityType).ToListAsync<Bookmark>();
        }

        /// <summary>
        /// Adds the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task<System.Int32>.</returns>
        public async Task<int> Add(Bookmark model)
        {
            if (model != null)
            {
                context.Bookmarks.Add(model);
                return await context.SaveChangesAsync();
            }

            return 0;
        }

        /// <summary>
        /// Deletes the bookmark
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<System.Int32>.</returns>
        public async Task<int> Delete(int id)
        {
            var model = await GetByIdAsync(id);

            if (model != null)
            {
                context.Entry(model).State = EntityState.Deleted;
                return await context.SaveChangesAsync();
            }

            return 0;
        }
    }
}
