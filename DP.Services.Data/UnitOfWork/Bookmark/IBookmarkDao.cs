﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IBookmarkDao
    {
        IQueryable<Bookmark> Bookmarks();
        IQueryable<Bookmark> Get();
        Task<List<Bookmark>> GetAllAsync();
        Task<Bookmark> GetByIdAsync(int id);
        Task<List<Bookmark>> GetByUserAsync(Bookmark bookmark);
        Task<int> Add(Bookmark model);
        Task<int> Delete(int id);
    }
}
