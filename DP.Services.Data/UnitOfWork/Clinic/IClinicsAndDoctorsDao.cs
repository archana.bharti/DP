﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IClinicAndDoctorDao
    {
        IQueryable<ClinicAndDoctor> Get();
        IQueryable<ClinicRating> GetRating();
        Task<List<ClinicAndDoctor>> GetAllAsync();
        Task<ClinicAndDoctor> GetByIdAsync(int id);
        Task<ClinicRating> Add(ClinicRating model);
        Task<ClinicAndDoctor> UpdateAsync(ClinicAndDoctor model, params string[] propertiesToUpdate);
        Task<List<ClinicRating>> GetRatingAsync(string userId);
        Task<List<ClinicRating>> GetRatingAsync(int id);
        Task<ClinicRating> GetRatingAsync(int id, string uId);
        Task<ClinicRating> UpdateRatingAsync(ClinicRating rating);
        Task<ClinicRating> GetRatingScores(int id);
        Task<int> GetRatingCount(string userId);
        Task<List<SpecializationClinic>> GetSpecializations(int id);
        Task<SpecializationClinic> GetSpecializationClinicByIdAsync(int id);
        Task<SpecializationClinic> AddSpecialization(SpecializationClinic model);
        Task<int> DeleteSpecialization(int id, int subscriberId);
    }
}
