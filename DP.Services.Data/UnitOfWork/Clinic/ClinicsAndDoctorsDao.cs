﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class ClinicAndDoctorDao : IClinicAndDoctorDao, IDisposable
    {
        private DPContext context;
        public ClinicAndDoctorDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns>IQueryable<ClinicAndDoctor>.</returns>
        public IQueryable<ClinicAndDoctor> Get()
        {
            return context.ClinicsAndDoctors;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<ClinicRating> GetRating()
        {
            return context.ClinicRatings;
        }

        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <returns>Task<List<ClinicAndDoctor>>.</returns>
        public async Task<List<ClinicAndDoctor>> GetAllAsync()
        {
            return await context.ClinicsAndDoctors
                .Include(x => x.City)
                .Include(x => x.State)
                .Include(x => x.Location)
                .ToListAsync<ClinicAndDoctor>();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<ClinicAndDoctor>.</returns>
        public async Task<ClinicAndDoctor> GetByIdAsync(int id)
        {
            return await context.ClinicsAndDoctors
                .Where(x => x.Id == id)
                .Include(x => x.City)
                .Include(x => x.State)
                .Include(x => x.Location)
                .SingleOrDefaultAsync(); 
        }

        /// <summary>
        /// get rating as an asynchronous operation.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Task<List<ClinicRating>>.</returns>
        public async Task<List<ClinicRating>> GetRatingAsync(string userId)
        {
                return await context.ClinicRatings.Where(x => x.UserId == userId)
                                                  .Include(x => x.ClinicOrDoctor).ToListAsync();
        }

        /// <summary>
        /// get rating as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<List<ClinicRating>>.</returns>
        public async Task<List<ClinicRating>> GetRatingAsync(int id)
        {
            return await context.ClinicRatings.Where(x => x.ClinicOrDoctorId == id)
                                                .Include(x => x.ClinicOrDoctor).Include(x=>x.User).ToListAsync();
        }

        public async Task<ClinicRating> GetRatingAsync(int id, string uId)
        {
            return await context.ClinicRatings.FirstOrDefaultAsync(x => x.ClinicOrDoctorId == id && x.UserId == uId);
        }

        /// <summary>
        /// Gets the rating scores.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<ClinicRating>.</returns>
        public async Task<ClinicRating> GetRatingScores(int id)
        {
            return await context.ClinicRatings.Where(x => x.ClinicOrDoctorId == id)
                                           .OrderByDescending(o => o.ReviewedDate)
                                           .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Adds the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task<ClinicRating>.</returns>
        public async Task<ClinicRating> Add(ClinicRating model)
        {
            if (model != null)
            {
                var rating = await context.ClinicRatings.FirstOrDefaultAsync(r => r.ClinicOrDoctorId == model.ClinicOrDoctorId && r.UserId == model.UserId);
                if(rating != null)
                {
                    context.Entry(rating).State = EntityState.Deleted;
                }
                context.ClinicRatings.Add(model);
                await context.SaveChangesAsync();
            }

            return model;
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="propertiesToUpdate">The properties to update.</param>
        /// <returns>Task<ClinicAndDoctor>.</returns>
        public async Task<ClinicAndDoctor> UpdateAsync(ClinicAndDoctor model, params string[] propertiesToUpdate)
        {
            await context.UpdateAsync<ClinicAndDoctor>(model, propertiesToUpdate);
            return await GetByIdAsync(model.Id);
        }

        /// <summary>
        /// update rating as an asynchronous operation.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <returns>Task<ClinicRating>.</returns>
        public async Task<ClinicRating> UpdateRatingAsync(ClinicRating rating)
        {
            ClinicRating existing = await context.ClinicRatings.Where(x => x.UserId == rating.UserId && x.ClinicOrDoctorId == rating.ClinicOrDoctorId).SingleOrDefaultAsync();

            if (existing != null)
            {
                context.ClinicRatings.Attach(rating);
                context.Entry(existing).State = EntityState.Modified;

                await context.SaveChangesAsync();
            }
            return existing;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<int> GetRatingCount(string userId)
        {
            return await this.context.ClinicRatings.Where(x => x.UserId == userId).CountAsync();
        }

        /// <summary>
        /// Gets the specializations.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<List<SpecializationClinic>>.</returns>
        public async Task<List<SpecializationClinic>> GetSpecializations(int id)
        {
            return await context.SpecializationClinics.Where(x => x.ClinicId == id)
                                                      .Include(x => x.Specialization).ToListAsync();
        }

        /// <summary>
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<SpecializationClinic>.</returns>
        public async Task<SpecializationClinic> GetSpecializationClinicByIdAsync(int id)
        {
            return await context.SpecializationClinics.Where(x => x.Id == id)
                                                      .Include(x => x.Specialization).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Adds the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task<SpecializationClinic>.</returns>
        public async Task<SpecializationClinic> AddSpecialization(SpecializationClinic model)
        {
            if (model != null)
            {
                context.SpecializationClinics.Add(model);
                await context.SaveChangesAsync();
            }

            return await context
                .SpecializationClinics.Include(x => x.Specialization)
                .Where(x => x.Id == model.Id)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<System.Int32>.</returns>
        public async Task<int> DeleteSpecialization(int id, int subscriberId)
        {
            var model = await context
                .SpecializationClinics
                .Where(x => x.SpecializationId == id && x.ClinicId == subscriberId)
                .SingleOrDefaultAsync();

            if (model != null)
            {
                context.SpecializationClinics.Remove(model);
                return await context.SaveChangesAsync();
            }

            return 0;
        }
    }
}