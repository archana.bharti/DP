﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IEndorsementDao
    {
        Task<int> Add(Endorsement model);
        Task<int> Update(Endorsement model);
        Task<int> AddOrUpdate(List<Endorsement> model);
        Task<Endorsement> Get(int endorseeId, int endorseeType, int endorserId, int endorserType, int specialization);
        Task<List<Endorsement>> Get(int endorseeId, int endorseeType);
        string GetClinicOrDoctorQuery { get; }
        string GetHospitalQuery { get; }
        string GetCollegeQuery { get; }
    }
}
