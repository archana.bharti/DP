﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class EndorsementDao : IEndorsementDao, IDisposable
    {
        private DPContext context;

        public EndorsementDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public async Task<int> Add(Endorsement endorsement)
        {
            var model = await this.Get(endorsement.EndorseeId, endorsement.EndorseeType, endorsement.EndorserId, endorsement.EndorserType, endorsement.SpecializationId);
            if (model == null)
            {
                if (endorsement != null)
                {
                    context.Endorsements.Add(endorsement);
                    await context.SaveChangesAsync();
                    return endorsement.Id;
                }
                return 0;
            }
            else
            {
                return model.Id;
            }
        }

        public async Task<int> Update(Endorsement model)
        {
            //var endorsement = await context.Endorsements.Where(e => e.Id == model.Id).SingleOrDefaultAsync();

            //if (endorsement != null)
            //{
            //    endorsement.SpecializationIds = model.SpecializationIds;
            //    context.Entry(endorsement).State = EntityState.Modified;
            //    await context.SaveChangesAsync();
            //}
            return model.Id;
        }

        public async Task<Endorsement> Get(int endorseeId, int endorseeType, int endorserId, int endorserType, int specializationId)
        {
            return await context.Endorsements.Where(e => e.EndorseeId == endorseeId && e.EndorseeType == endorseeType &&
                                                          e.EndorserId == endorserId && e.EndorserType == endorserType &&
                                                          e.SpecializationId == specializationId).SingleOrDefaultAsync();
        }

        public async Task<List<Endorsement>> Get(int endorseeId, int endorseeType)
        {
            return await context.Endorsements.Where(e => e.EndorseeId == endorseeId && e.EndorseeType == endorseeType).ToListAsync();
        }

        public async Task<int> AddOrUpdate(List<Endorsement> endorsements)
        {
            if (endorsements != null && endorsements.Count > 0)
            {
                var record = endorsements.FirstOrDefault();
                var dbExistingEndorsements = await Get(record.EndorseeId, record.EndorseeType);

                var addedEndorsements = endorsements.Where(e => !dbExistingEndorsements.Any(x => x.SpecializationId == e.SpecializationId &&
                                                                        x.EndorserId == e.EndorserId &&
                                                                        x.EndorserType == e.EndorserType)).ToList();

                var deletedEndorsements = dbExistingEndorsements.Where(e => !endorsements.Any(x => x.SpecializationId == e.SpecializationId &&
                                                                        x.EndorserId == e.EndorserId &&
                                                                        x.EndorserType == e.EndorserType)).ToList();

                context.Endorsements.RemoveRange(deletedEndorsements);
                context.Endorsements.AddRange(addedEndorsements);

                await context.SaveChangesAsync();

                return 1;
            }

            return 0;
        }

        public string GetClinicOrDoctorQuery
        { /*Change ifnull(cd.Profile_Pic_1,'default') to
             * (case when cd.Profile_Pic_1 is null  then 'default' else case when cd.Profile_Pic_1 ='' then 'default' else cd.Profile_Pic_1 end end) 
             * on 17-01-17 by Vikram */
            get
            {
                return @"select *, 'ClinicOrDoctor' as Type from (
                           select  
                               endor.SpecializationId, 
                               count(endor.SpecializationId) as Count,
                               spz.Category as SpecializationName,
                               group_concat(concat(cd.Name,':',(case when cd.Profile_Pic_1 is null  then 'default' else case when cd.Profile_Pic_1 ='' then 'default' else cd.Profile_Pic_1 end end),':',endor.EndorserId, ':',endor.EndorserType )) as Users
                               from dp_endorsement endor
                               inner join dp_specialization spz
                               on spz.id = endor.SpecializationId
                               left outer join dp_clinic_doctor cd
                               on (cd.id = endor.EndorserId and (endor.EndorseeType = 1 or endor.EndorseeType = 2))
                           where 
                           endor.EndorseeId = [ID] and endor.EndorseeType = [TYPE]
                           group by  SpecializationId, SpecializationName
                           ) P";
            }
        }

        public string GetHospitalQuery
        {
            /*Change ifnull(cd.Profile_Pic_1,'default') to
             * (case when cd.Profile_Pic_1 is null  then 'default' else case when cd.Profile_Pic_1 ='' then 'default' else cd.Profile_Pic_1 end end) 
             * on 17-01-17 by Vikram */
            get
            {
                return @"select SpecializationId, SpecializationName, Count(SpecializationId) as Count, group_concat(Users) as Users, 'Hospital' as Type from (
                    select *, 'Hospital' as Type from (
                         select  
                             endor.SpecializationId, 
                             count(endor.SpecializationId) as Count,
                             spz.Category as SpecializationName,
                             group_concat(concat(cd.Name,':', (case when cd.Profile_Pic_1 is null  then 'default' else case when cd.Profile_Pic_1 ='' then 'default' else cd.Profile_Pic_1 end end),':',endor.EndorserId, ':',endor.EndorserType )) as Users
                             from dp_endorsement endor
                             inner join dp_specialization spz
                             on spz.id = endor.SpecializationId
                             inner join dp_clinic_doctor cd
                             on (cd.id = endor.EndorserId and (endor.EndorserType = 1 or endor.EndorserType = 2))
                         where 
                         endor.EndorseeId = [ID] and endor.EndorseeType = [TYPE]
                         group by  SpecializationId, SpecializationName
                         ) p1
                         
                         union
                         
                         select *, 'Hospital' as Type from ( 
                         select  
                             endor.SpecializationId, 
                             count(endor.SpecializationId) as Count,
                             spz.Category as SpecializationName,
                             group_concat(concat(cd.Name,':', (case when cd.Profile_Pic_1 is null  then 'default' else case when cd.Profile_Pic_1 ='' then 'default' else cd.Profile_Pic_1 end end),':',endor.EndorserId, ':',endor.EndorserType )) as Users
                             from dp_endorsement endor
                             inner join dp_specialization spz
                             on spz.id = endor.SpecializationId
                             inner join dp_hospital cd
                             on (cd.id = endor.EndorserId and endor.EndorserType = 3)
                         where 
                         endor.EndorseeId = [ID] and endor.EndorseeType = [TYPE]
                         group by  SpecializationId, SpecializationName
                          ) p1 ) p2 group by SpecializationId"; 
            }
        }

        public string GetCollegeQuery
        {
            /*Change ifnull(cd.Profile_Pic_1,'default') to
             * (case when cd.Profile_Pic_1 is null  then 'default' else case when cd.Profile_Pic_1 ='' then 'default' else cd.Profile_Pic_1 end end) 
             * on 17-01-17 by Vikram */
            get
            {
                return @"select SpecializationId, SpecializationName, Count(SpecializationId) as Count, group_concat(Users) as Users, 'College' as Type from (
                        select *, 'College' as Type from (
                         select  
                             endor.SpecializationId, 
                             count(endor.SpecializationId) as Count,
                             spz.Name as SpecializationName,
                             group_concat(concat(cd.Name,':', (case when cd.Profile_Pic_1 is null  then 'default' else case when cd.Profile_Pic_1 ='' then 'default' else cd.Profile_Pic_1 end end),':',endor.EndorserId, ':',endor.EndorserType )) as Users
                             from dp_endorsement endor
                             inner join dp_college_department spz
                             on spz.id = endor.SpecializationId
                             inner join dp_clinic_doctor cd
                             on (cd.id = endor.EndorserId and (endor.EndorserType = 1 or endor.EndorserType = 2))
                         where 
                         endor.EndorseeId = [ID] and endor.EndorseeType = [TYPE]
                         group by  SpecializationId, SpecializationName
                         ) p1
                         
                         union
                         
                         select *, 'College' as Type from ( 
                         select  
                             endor.SpecializationId, 
                             count(endor.SpecializationId) as Count,
                             spz.Name as SpecializationName,
                             group_concat(concat(cd.Name,':', ifnull(cd.Profile_Pic_1,'default'),':',endor.EndorserId, ':',endor.EndorserType )) as Users
                             from dp_endorsement endor
                             inner join dp_college_department spz
                             on spz.id = endor.SpecializationId
                             inner join dp_hospital cd
                             on (cd.id = endor.EndorserId and endor.EndorserType = 4)
                         where 
                         endor.EndorseeId = [ID] and endor.EndorseeType = [TYPE]
                         group by  SpecializationId, SpecializationName
                          ) p1 ) p2 group by SpecializationId";
            }
        }
    }
}