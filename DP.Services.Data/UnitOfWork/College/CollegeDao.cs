﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class CollegeDao : ICollegeDao, IDisposable
    {
        private DPContext context;
        public CollegeDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns>IQueryable<College>.</returns>
        public IQueryable<College> Get()
        {
            return context.Colleges;
        }

        public IQueryable<CollegeRating> GetRating()
        {
            return context.CollegeRatings;
        }

        public async Task<List<College>> GetAllAsync()
        {
            return await context.Colleges
                .Include(x => x.City)
                .Include(x => x.State)
                .Include(x => x.Location)
                .ToListAsync<College>();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<College>.</returns>
        public async Task<College> GetByIdAsync(int id)
        {
            return await context.Colleges
                .Where(x => x.Id == id)
                .Include(x => x.City)
                .Include(x => x.State)
                .Include(x => x.Location)
                .SingleOrDefaultAsync(); ;
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="propertiesToUpdate">The properties to update.</param>
        /// <returns>Task<College>.</returns>
        public async Task<College> UpdateAsync(College model, params string[] propertiesToUpdate)
        {
            context.Update<College>(model, propertiesToUpdate);
            await context.SaveChangesAsync();
            return await GetByIdAsync(model.Id);
        }

        public async Task<CollegeLocation> GetCollegeLocation(int collegeId)
        {
            var college = await this.context.Colleges.Where(x => x.Id == collegeId).SingleAsync();
            var collegeLocation = await this.context.CollegeLocations.Where(x => x.Id == college.LocationId).SingleAsync();
            var state = await this.context.States.Where(x => x.Id == collegeLocation.StateId).SingleAsync();
            collegeLocation.State = state;
            return collegeLocation;
        }
        

        /// <summary>
        /// get rating as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<List<CollegeRating>>.</returns>
        public async Task<List<CollegeRating>> GetRatingAsync(int id)
        {
            return await context.CollegeRatings.Where(x => x.CollegeId == id)
                                               .Include(x => x.User).Include(x => x.College).ToListAsync();
        }

        /// <summary>
        /// get rating as an asynchronous operation.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Task<List<CollegeRating>>.</returns>
        public async Task<List<CollegeRating>> GetRatingAsync(string userId)
        {
            return await context.CollegeRatings.Where(x => x.UserId == userId)
                                                  .Include(x => x.College).ToListAsync();
        }
        public async Task<CollegeRating> GetRatingAsync(int id, string uId)
        {
            return await context.CollegeRatings.FirstOrDefaultAsync(x => x.CollegeId == id && x.UserId == uId);
        }

        /// <summary>
        /// update rating as an asynchronous operation.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <returns>Task<CollegeRating>.</returns>
        public async Task<CollegeRating> UpdateRatingAsync(CollegeRating rating)
        {
            CollegeRating existing = await context.CollegeRatings.Where(x => x.UserId == rating.UserId && x.CollegeId == rating.CollegeId).SingleOrDefaultAsync();

            if (existing != null)
            {
                context.CollegeRatings.Attach(rating);
                context.Entry(existing).State = EntityState.Modified;

                await context.SaveChangesAsync();
            }

            return existing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<int> GetRatingCount(string userId)
        {
            return await this.context.CollegeRatings.Where(x => x.UserId == userId).CountAsync();
        }

        /// <summary>
        /// Adds the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task<CollegeRating>.</returns>
        public async Task<CollegeRating> Add(CollegeRating model)
        {
            if (model != null)
            {
                var rating = await context.CollegeRatings.FirstOrDefaultAsync(r => r.CollegeId == model.CollegeId && r.UserId == model.UserId);
                if (rating != null)
                {
                    context.Entry(rating).State = EntityState.Deleted;
                }
                context.CollegeRatings.Add(model);
                await context.SaveChangesAsync();
            }

            return model;
        }
    }
}
