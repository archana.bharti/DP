﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface ICollegeDao
    {
        IQueryable<College> Get();
        IQueryable<CollegeRating> GetRating();
        Task<List<College>> GetAllAsync();
        Task<College> GetByIdAsync(int id);
        Task<CollegeRating> Add(CollegeRating model);
        Task<College> UpdateAsync(College model, params string[] propertiesToUpdate);
        Task<List<CollegeRating>> GetRatingAsync(int id);
        Task<List<CollegeRating>> GetRatingAsync(string userId);
        Task<CollegeRating> GetRatingAsync(int id, string uId);
        Task<CollegeRating> UpdateRatingAsync(CollegeRating rating);
        Task<int> GetRatingCount(string userId);
        Task<CollegeLocation> GetCollegeLocation(int collegeId);
    }
}
