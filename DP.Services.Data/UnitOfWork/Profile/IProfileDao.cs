﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IProfileDao
    {
        Task<ApplicationUser> GetByIdAsync(string id);
    }
}
