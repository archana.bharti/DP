﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public class ProfileDao : IProfileDao, IDisposable
    {
        private DPContext context;
        public ProfileDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public async Task<ApplicationUser> GetByIdAsync(string id)
        {
            return await this.context.Users.Where(x => x.Id == id).SingleOrDefaultAsync();
        }
    }
}
