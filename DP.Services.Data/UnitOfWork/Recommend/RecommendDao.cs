﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    internal sealed class RecommendDao : IRecommendDao
    {
        private DPContext context;
        public RecommendDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public async Task<int> AddOrRemove(Recommend recommend)
        {
            if (recommend.Remove)
            {
                var item = await this.Get(recommend);
                if (item != null)
                {
                    this.context.Recommends.Remove(item);
                    await this.context.SaveChangesAsync();
                }
                return 0;
            }
            this.context.Recommends.Add(recommend);
            await this.context.SaveChangesAsync();
            return recommend.Id;
        }

        public async Task<List<Recommend>> Get(string ipAddress)
        {
            return await this.context.Recommends.Where(x => x.IpAddress == ipAddress).ToListAsync();
        }

        public async Task<List<Recommend>> Get(int entityId, int type)
        {
            return await this.context.Recommends.Where(x => x.EntityId == entityId && x.EntityType == type).ToListAsync();
        }

        public async Task<int> Count(int entityId, int type)
        {
            return await this.context.Recommends.Where(x => x.EntityId == entityId && x.EntityType == type).CountAsync();
        }

        public async Task<Recommend> Get(Recommend recommend)
        {
            return await this.context.Recommends.Where(x => 
                x.IpAddress == recommend.IpAddress && 
                x.EntityId == recommend.EntityId && 
                x.EntityType == recommend.EntityType).SingleOrDefaultAsync();
        }
    }
}
