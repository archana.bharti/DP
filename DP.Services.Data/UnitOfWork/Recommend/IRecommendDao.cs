﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IRecommendDao
    {
        Task<int> AddOrRemove(Recommend recommend);
        Task<List<Recommend>> Get(string ipAddress);
        Task<List<Recommend>> Get(int entityId, int type);
        Task<int> Count(int entityId, int type);
        Task<Recommend> Get(Recommend recommed);
    }
}
