﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IUserDao
    {
        IQueryable<ApplicationUser> Get();
        Task<List<ApplicationUser>> GetAllAsync();
        Task<ApplicationUser> GetByIdAsync(string id);
        Task<ApplicationUser> GetBySubcriberId(int subscriberId);
        Task<ApplicationUser> GetBySubcriberId(int subscriberId, List<string> userTypes);
        Task<ApplicationUser> GetByEmailAsync(string email);
        Task<ApplicationUser> ConfirmEmail(ApplicationUser user);
        Task<List<ApplicationUser>> GetAllSubscribers(string userType);
        Task<ApplicationUser> GetBySubscriptionAsync(int? subscriberId, string userType);
        Task<ApplicationUser> UpdateAsync(ApplicationUser user, params string[] propertiesToUpdate);
        bool IsUserActivated(string id);
    }
}
