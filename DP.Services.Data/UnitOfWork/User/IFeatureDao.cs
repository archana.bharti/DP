﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IFeatureDao
    {
        IQueryable<Feature> Features();
        Task<List<Feature>> GetFeaturesAsync();
        Task<Feature> CreateAsync(Feature feature);
        Task<Feature> UpdateAsync(Feature feature);
        Task<Feature> GetAsync(Feature feature);
        Task<Feature> GetAsync(int id);
        bool IsFeaturedUser(int id, int type);
    }
}
