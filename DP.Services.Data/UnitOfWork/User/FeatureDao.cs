﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class FeatureDao : IFeatureDao, IDisposable
    {
        private DPContext context;
        public FeatureDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<List<Feature>> GetFeaturesAsync()
        {
            return await this.context.Features
                .Include(x=>x.Status)
                .ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="feature"></param>
        /// <returns></returns>
        public async Task<Feature> CreateAsync(Feature feature)
        {
            this.context.Features.Add(feature);
            await this.context.SaveChangesAsync();
            return feature;
        }

        public async Task<Feature> UpdateAsync(Feature feature)
        {  
            this.context.Entry<Feature>(feature).State = EntityState.Modified;
            await this.context.SaveChangesAsync();
            return feature;
        }

        /// <summary>
        /// Get featured user by entityId/subscriberId and entityType/subscriberType
        /// </summary>
        /// <param name="feature"></param>
        /// <returns></returns>
        public async Task<Feature> GetAsync(Feature feature)
        {
            return await this.context.Features
                .Where(f => f.EntityId == feature.EntityId && f.EntityType == feature.EntityType)
                .Include(x=>x.Status)
                .SingleOrDefaultAsync();
        }

        public async Task<Feature> GetAsync(int id)
        {
            return await this.context.Features
                .Where(f => f.Id == id)
                .Include(x => x.Status)
                .SingleOrDefaultAsync();
        }

        public bool IsFeaturedUser(int id, int type)
        {
            return this.context.Features.Where(f => f.EntityId == id && f.EntityType == type).Count() == 1;
        }

        public IQueryable<Feature> Features()
        {
            return this.context.Features.AsQueryable();
        }
    }
}
