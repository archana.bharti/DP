﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Linq;
    
namespace DP.Services.Data.UnitOfWork
{
    public class UserDao : IUserDao, IDisposable
    {
        private DPContext context;
        public UserDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns>IQueryable&lt;ApplicationUser&gt;.</returns>
        public IQueryable<ApplicationUser> Get()
        {
            return this.context.Users;
        }

        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <returns>Task&lt;List&lt;ApplicationUser&gt;&gt;.</returns>
        public async Task<List<ApplicationUser>> GetAllAsync()
        {
            return await this.context.Users.ToListAsync();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;ApplicationUser&gt;.</returns>
        public async Task<ApplicationUser> GetByIdAsync(string id)
        {
            return await this.context.Users.Where(x => x.Id == id).SingleOrDefaultAsync();
        }

        public async Task<ApplicationUser> GetBySubcriberId(int subscriberId)
        {
            return await this.context.Users.Where(x => x.SubscriberId == (int?)subscriberId).SingleOrDefaultAsync();
        }

        public async Task<ApplicationUser> GetBySubcriberId(int subscriberId, List<string> userTypes)
        {
            var query = this.context.Users.Where(x =>
            x.SubscriberId != null &&
            x.SubscriberId == subscriberId &&
            userTypes.Contains(x.UserType));
            return await query.SingleOrDefaultAsync();
        }

        /// <summary>
        /// get by subscription as an asynchronous operation.
        /// </summary>
        /// <param name="subscriberId">The subscriber identifier.</param>
        /// <param name="userType">Type of the user.</param>
        /// <returns>Task&lt;ApplicationUser&gt;.</returns>
        public async Task<ApplicationUser> GetBySubscriptionAsync(int? subscriberId, string userType)
        {
            if (!subscriberId.HasValue)
                return null;

            return await this.context.Users.Where(x => x.SubscriberId.Value == subscriberId.Value &&
                                                       x.UserType.ToUpper() == userType.ToUpper())
                                           .SingleOrDefaultAsync();
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="subscriberId"></param>
       /// <param name="userType"></param>
       /// <returns></returns>
        public async Task<List<ApplicationUser>> GetAllSubscribers(string userType = null)
        {
            var query = this.context.Users.Where(x => x.SubscriberId != null && x.SubscriberId.HasValue);
            if (userType != null)
                query = query.Where(x => x.UserType == userType);
            return await query.ToListAsync <ApplicationUser>();
        }

        /// <summary>
        /// get by email as an asynchronous operation.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Task&lt;ApplicationUser&gt;.</returns>
        public async Task<ApplicationUser> GetByEmailAsync(string email)
        {
            try
            {
                return await this.context.Users.Where(x => x.Email == email).SingleOrDefaultAsync();
            }
         catch(Exception ex)
            {
                
            }
            return await this.context.Users.Where(x => x.Email == email).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Confirms the email.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>Task&lt;ApplicationUser&gt;.</returns>
        public async Task<ApplicationUser> ConfirmEmail(ApplicationUser user)
        {
            context.Entry(user).State = EntityState.Modified;

            await this.context.SaveChangesAsync();

            return user;
        }

        /// <summary>
        /// Determines whether [is user activated] [the specified identifier].
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns><c>true</c> if [is user activated] [the specified identifier]; otherwise, <c>false</c>.</returns>
        public bool IsUserActivated(string id)
        {
            ApplicationUser user = this.context.Users.Where(x => x.Id == id).SingleOrDefault();

            if (user != null)
            {
                return user.EmailConfirmed;
            }

            return false;
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="propertiesToUpdate">The properties to update.</param>
        /// <returns>Task&lt;ApplicationUser&gt;.</returns>
        public async Task<ApplicationUser> UpdateAsync(ApplicationUser user, params string[] propertiesToUpdate)
        {
            await context.UpdateAsync<ApplicationUser>(user, propertiesToUpdate);
            await context.SaveChangesAsync();
            return await GetByIdAsync(user.Id);
        }
    }
}
