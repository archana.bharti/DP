﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class HospitalDao : IHospitalDao, IDisposable
    {
        private DPContext context;
        public HospitalDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns>IQueryable<Hospital>.</returns>
        public IQueryable<Hospital> Get()
        {
            return context.Hospitals;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<HospitalRating> GetRating()
        {
            return context.HospitalRatings;
        }

        /// <summary>
        /// get all as an asynchronous operation.
        /// </summary>
        /// <returns>Task<List<Hospital>>.</returns>
        public async Task<List<Hospital>> GetAllAsync()
        {
            return await context.Hospitals
                .Include(x => x.City)
                .Include(x => x.State)
                .Include(x => x.Location)
                .ToListAsync<Hospital>();
        }

        /// <summary>
        /// get by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<Hospital>.</returns>
        public async Task<Hospital> GetByIdAsync(int id)
        {
            return await context.Hospitals
                .Where(x => x.Id == id)
                .Include(x => x.City)
                .Include(x => x.State)
                .Include(x => x.Location)
                .SingleOrDefaultAsync(); ;
        }

        /// <summary>
        /// update as an asynchronous operation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="propertiesToUpdate">The properties to update.</param>
        /// <returns>Task<Hospital>.</returns>
        public async Task<Hospital> UpdateAsync(Hospital model, params string[] propertiesToUpdate)
        {
            context.Update<Hospital>(model, propertiesToUpdate);
            await context.SaveChangesAsync();
            return await GetByIdAsync(model.Id);
        }

        /// <summary>
        /// get rating as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<List<HospitalRating>>.</returns>
        public async Task<List<HospitalRating>> GetRatingAsync(int id)
        {
            return await context.HospitalRatings.Where(x => x.HospitalId == id)
                                                .Include(x => x.User).Include(x => x.Hospital).ToListAsync();
        }

        /// <summary>
        /// get rating as an asynchronous operation.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>Task<List<HospitalRating>>.</returns>
        public async Task<List<HospitalRating>> GetRatingAsync(string userId)
        {
            return await context.HospitalRatings.Where(x => x.UserId == userId)
                                                   .Include(x => x.Hospital).ToListAsync();
        }

        public async Task<HospitalRating> GetRatingAsync(int id, string uId)
        {
            return await context.HospitalRatings.FirstOrDefaultAsync(x => x.HospitalId == id && x.UserId == uId);
        }

        /// <summary>
        /// update rating as an asynchronous operation.
        /// </summary>
        /// <param name="rating">The rating.</param>
        /// <returns>Task<HospitalRating>.</returns>
        public async Task<HospitalRating> UpdateRatingAsync(HospitalRating rating)
        {
            HospitalRating existing = await context.HospitalRatings.Where(x => x.UserId == rating.UserId && x.HospitalId == rating.HospitalId).SingleOrDefaultAsync();

            if (existing != null)
            {
                context.HospitalRatings.Attach(rating);
                context.Entry(existing).State = EntityState.Modified;

                await context.SaveChangesAsync();
            }

            return existing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<int> GetRatingCount(string userId)
        {
            return await this.context.HospitalRatings.Where(x => x.UserId == userId).CountAsync();
        }

        /// <summary>
        /// Adds the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task<HospitalRating>.</returns>
        public async Task<HospitalRating> Add(HospitalRating model)
        {
            if (model != null)
            {
                var rating = await context.HospitalRatings.FirstOrDefaultAsync(r => r.HospitalId == model.HospitalId && r.UserId == model.UserId);
                if (rating != null)
                {
                    context.Entry(rating).State = EntityState.Deleted;
                }
                context.HospitalRatings.Add(model);
                await context.SaveChangesAsync();
            }

            return model;
        }

        /// <summary>
        /// Gets the rating scores.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<HospitalRating>.</returns>
        public async Task<HospitalRating> GetRatingScores(int id)
        {
            return await context.HospitalRatings.Where(x => x.HospitalId == id)
                                       .OrderByDescending(o => o.ReviewedDate)
                                       .FirstOrDefaultAsync();
        }

        /// <summary>
        /// get specialization hospital by identifier as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<SpecializationHospital>.</returns>
        public async Task<SpecializationHospital> GetSpecializationHospitalByIdAsync(int id)
        {
            return await context.SpecializationHospitals.Where(x => x.Id == id)
                                                      .Include(x => x.Specialization).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Gets the specializations.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<List<SpecializationHospital>>.</returns>
        public async Task<List<SpecializationHospital>> GetSpecializations(int id)
        {
            return await context.SpecializationHospitals.Where(x => x.HospitalId == id)
                                                        .Include(x => x.Specialization).ToListAsync();
        }

        /// <summary>
        /// Adds the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task<SpecializationHospital>.</returns>
        public async Task<SpecializationHospital> AddSpecialization(SpecializationHospital model)
        {
            if (model != null)
            {
                context.SpecializationHospitals.Add(model);
                await context.SaveChangesAsync();
            }

            return await GetSpecializationHospitalByIdAsync(model.Id);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task<System.Int32>.</returns>
        public async Task<int> DeleteSpecialization(int id, int subscriberId)
        {
            var model = await context
                .SpecializationHospitals
                .Where(x => x.SpecializationId == id && x.HospitalId == subscriberId)
                .SingleOrDefaultAsync();

            if (model != null)
            {
                context.SpecializationHospitals.Remove(model);
                return await context.SaveChangesAsync();
            }

            return 0;
        }
    }
}
