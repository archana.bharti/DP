﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IHospitalDao
    {
        IQueryable<Hospital> Get();
        IQueryable<HospitalRating> GetRating();
        Task<List<Hospital>> GetAllAsync();
        Task<Hospital> GetByIdAsync(int id);
        Task<HospitalRating> Add(HospitalRating model);
        Task<Hospital> UpdateAsync(Hospital model, params string[] propertiesToUpdate);
        Task<List<HospitalRating>> GetRatingAsync(string userId);
        Task<List<HospitalRating>> GetRatingAsync(int id);
        Task<HospitalRating> GetRatingAsync(int id, string uId);
        Task<HospitalRating> UpdateRatingAsync(HospitalRating rating);
        Task<HospitalRating> GetRatingScores(int id);
        Task<int> GetRatingCount(string userId);
        Task<List<SpecializationHospital>> GetSpecializations(int id);
        Task<SpecializationHospital> GetSpecializationHospitalByIdAsync(int id);
        Task<SpecializationHospital> AddSpecialization(SpecializationHospital model);
        Task<int> DeleteSpecialization(int id, int subscriberId);
    }
}
