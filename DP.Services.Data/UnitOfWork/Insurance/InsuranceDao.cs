﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DP.Common.Enums;

namespace DP.Services.Data.UnitOfWork
{
    public sealed class InsuranceDao : IInsuranceDao
    {
        private DPContext context;
        public InsuranceDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public IQueryable<Insurance> Get
        {
            get
            {
                return this.context.Insurances;
            }
        }

        public async Task<List<Insurance>> GetAll(bool includeRatings = false)
        {
            var query = this.context.Insurances;
            if (includeRatings)
            {
                query.Include(x => x.Ratings);
            }
            return await query.ToListAsync();
        }

        public async Task<Insurance> GetByIdAsync(Insurance insurance, bool includeRatings = false)
        {
            var query = this.context.Insurances.Where(x => x.Id == insurance.Id);
            if (includeRatings)
            {
                query.Include(x => x.Ratings);
            }
            return await query.SingleOrDefaultAsync();
        }

        public async Task<Insurance> UpdateAsync(Insurance insurance)
        {
            this.context.Entry(insurance).State = EntityState.Modified;
            await this.context.SaveChangesAsync();
            return insurance;
        }

        public async Task<Insurance> UpdateAsync(Insurance insurance, params string[] propertiesToUpdate)
        {
            context.Update<Insurance>(insurance, propertiesToUpdate);
            await context.SaveChangesAsync();
            return await GetByIdAsync(insurance);
        }

        public async Task<List<InsuranceRating>> GetAllRatings(int insuranceId)
        {
            return await this.context.InsurnaceRatings.Where(x => x.InsuranceId == insuranceId).ToListAsync();
        }

        public async Task<int> GetRecommendationsCount(int insuranceId)
        {
            return await this.context.Recommends.Where(x => x.EntityId == insuranceId && x.EntityType == (int)UserType.Insurance).CountAsync();
        }

        public async Task<InsuranceRating> GetTopRating(int insuranceId)
        {
            return await this.context.InsurnaceRatings.Where(x=>x.InsuranceId == insuranceId).OrderByDescending(x => x.Id).FirstOrDefaultAsync();
        }

        /* RATING */
        public async Task<InsuranceRating> GetRatingAsync(int id, string uId)
        {
            return await context.InsurnaceRatings.FirstOrDefaultAsync(x => x.InsuranceId == id && x.UserId == uId);
        }

        public async Task<List<InsuranceRating>> GetRatingAsync(string userId)
        {
            return await context.InsurnaceRatings.Where(x => x.UserId == userId)
                                                  .Include(x => x.Insurance).ToListAsync();
        }

        public async Task<List<InsuranceRating>> GetRatingAsync(int id)
        {
            return await context.InsurnaceRatings.Where(x => x.InsuranceId == id)
                                               .Include(x => x.User).Include(x => x.Insurance).ToListAsync();
        }

        public async Task<InsuranceRating> GetRatingScores(int id)
        {
            return await context.InsurnaceRatings.Where(x => x.InsuranceId == id)
                                       .OrderByDescending(o => o.ReviewedDate)
                                       .FirstOrDefaultAsync();
        }

        public async Task<InsuranceRating> AddRating(InsuranceRating model)
        {
            if (model != null)
            {
                var rating = await context.ClinicRatings.FirstOrDefaultAsync(r => r.ClinicOrDoctorId == model.InsuranceId && r.UserId == model.UserId);
                if (rating != null)
                {
                    context.Entry(rating).State = EntityState.Deleted;
                }
                context.InsurnaceRatings.Add(model);
                await context.SaveChangesAsync();
            }

            return model;
        }

        public async Task<InsuranceRating> UpdateRatingAsync(InsuranceRating rating)
        {
            InsuranceRating existing = await context.InsurnaceRatings.Where(x => x.UserId == rating.UserId && x.InsuranceId == rating.InsuranceId).SingleOrDefaultAsync();

            if (existing != null)
            {
                context.InsurnaceRatings.Attach(rating);
                context.Entry(existing).State = EntityState.Modified;

                await context.SaveChangesAsync();
            }
            return existing;
        }

    }
}
