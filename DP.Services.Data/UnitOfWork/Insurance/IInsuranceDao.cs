﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IInsuranceDao
    {
        IQueryable<Insurance> Get { get; }
        Task<List<Insurance>> GetAll(bool includeRatings);
        Task<Insurance> GetByIdAsync(Insurance insurance, bool includeRatings);
        Task<Insurance> UpdateAsync(Insurance insurance);
        Task<Insurance> UpdateAsync(Insurance insurance, params string[] propertiesToUpdate);
        Task<List<InsuranceRating>> GetAllRatings(int insuranceId);
        Task<int> GetRecommendationsCount(int insuranceId);
        Task<InsuranceRating> GetTopRating(int insuranceId);
        Task<InsuranceRating> GetRatingScores(int id);
        Task<InsuranceRating> AddRating(InsuranceRating model);
        Task<InsuranceRating> UpdateRatingAsync(InsuranceRating rating);
        Task<List<InsuranceRating>> GetRatingAsync(int id);
        Task<List<InsuranceRating>> GetRatingAsync(string userId);
        Task<InsuranceRating> GetRatingAsync(int id, string uId);
    }
}
