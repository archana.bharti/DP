﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public sealed class ContactUsDao : IContactUsDao
    {
        private DPContext context;
        public ContactUsDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public IQueryable<ContactUs> Query { get { return this.context.Contacts; } }
        public Task<List<ContactUs>> All { get { return this.context.Contacts.ToListAsync(); } }
        public async Task<ContactUs> GetById(int id) { return await this.context.Contacts.Where(x => x.Id == id).SingleOrDefaultAsync(); }
        public async Task<ContactUs> GetByMobile(string mobile) { return await this.context.Contacts.Where(x => x.Mobile == mobile).SingleOrDefaultAsync(); }

        public async Task<ContactUs> Update(ContactUs model, params string[] propertiesToUpdate)
        {
            context.Update<ContactUs>(model, propertiesToUpdate);
            await context.SaveChangesAsync();
            return model;
        }

        public async Task<ContactUs> Add(ContactUs model)
        {
            context.Contacts.Add(model);
            await context.SaveChangesAsync();
            return model;
        }
    }
}
