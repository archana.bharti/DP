﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DP.Services.Data.Models;
namespace DP.Services.Data.UnitOfWork
{
    public interface IContactUsDao
    {
        IQueryable<ContactUs> Query { get; }
        Task<List<ContactUs>> All { get; }
        Task<ContactUs> GetById(int id);
        Task<ContactUs> GetByMobile(string mobile);
        Task<ContactUs> Add(ContactUs model);
        Task<ContactUs> Update(ContactUs model, params string[] propertiesToUpdate);
    }
}
