﻿using DP.Common.Enums;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IOptionDao
    {
        Task<List<Option>> Get();
        Task<List<Option>> Get(OptionsContextType optionContextType);
        Task<Option> Get(string key);
        Task<List<Option>> Get(List<string> keys);
    }
}
