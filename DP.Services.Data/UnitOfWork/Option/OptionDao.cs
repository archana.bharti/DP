﻿using DP.Common.Enums;
using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Linq;

namespace DP.Services.Data.UnitOfWork
{
    public sealed class OptionDao : IOptionDao, IDisposable
    {
        private DPContext context;
        public OptionDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public IQueryable<Option> Options()
        {
            return this.context.Options;
        }

        public async Task<List<Option>> Get()
        {
            return await this.context.Options.ToListAsync<Option>();
        }

        public async Task<List<Option>> Get(OptionsContextType optionContextType)
        {
            return await context.Options.Where(o => o.Context.HasValue && o.Context.Value == (int)optionContextType).ToListAsync<Option>();
        }

        public async Task<Option> Get(string key)
        {
            return await context.Options.Where(x => x.Key.ToUpper() == key.ToUpper()).SingleOrDefaultAsync();
        }

        public async Task<List<Option>> Get(List<string> keys)
        {
            keys.ForEach(x => x.ToUpper());
            return await context.Options.Where(x => keys.Contains(x.Key)).ToListAsync<Option>();
        }
    }
}
