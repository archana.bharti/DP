﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IPdaSubscriptionDao
    {
        IQueryable<PdaSubscription> Get();
        Task<List<PdaSubscription>> GetAllAsync();
        Task<PdaSubscription> GetByIdAsync(int id);
        Task<List<PdaSubscription>> GetByDoctorOrClinicIdAsync(int id);
        Task<PdaSubscription> Add(PdaSubscription model);
        Task<PdaSubscription> UpdateAsync(PdaSubscription model);
        Task<PdaSubscription> UpdateAsync(PdaSubscription model, params string[] propertiesToUpdate);
        Task<List<PdaSubscription>> GetActiveSubscriptions(int doctorOrClinicId);
        Task<List<PdaSubscription>> GetActiveSubscriptions();
    }
}
