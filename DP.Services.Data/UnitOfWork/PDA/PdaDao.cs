﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DP.Common.Enums;

namespace DP.Services.Data.UnitOfWork
{
    public class PdaDao : IPdaDao, IDisposable
    {
        private DPContext context;
        public PdaDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<PersonalAdvice> Get()
        {
            return context.PersonalAdvices;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<PersonalAdvice>> GetAllAsync()
        {
            return await context.PersonalAdvices
                .Include(x => x.City)
                .Include(x => x.Doctor)
                .Include(x => x.Specialization)
                .Include(x => x.User)
                .Include(x => x.DoctorEmailStatus)
                .Include(x => x.UserEmailStatus)
                .ToListAsync<PersonalAdvice>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<PersonalAdvice> GetByIdAsync(int id)
        {
            return await context.PersonalAdvices
                .Where(x => x.Id == id)
                .Include(x => x.City)
                .Include(x => x.Doctor)
                .Include(x => x.Specialization)
                .Include(x => x.User)
                .Include(x => x.DoctorEmailStatus)
                .Include(x => x.UserEmailStatus)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<List<PersonalAdvice>> GetByUserIdAsync(string id)
        {
            return await context.PersonalAdvices
                .Where(x => x.UserId == id)
                .Include(x => x.City)
                .Include(x => x.Doctor)
                .Include(x => x.Specialization)
                .Include(x => x.User)
                .Include(x => x.DoctorEmailStatus)
                .Include(x => x.UserEmailStatus)
                .ToListAsync<PersonalAdvice>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<List<PersonalAdvice>> GetByDoctorIdAsync(int id)
        {
            return await context.PersonalAdvices
                .Where(x => x.DoctorId == id)
                .Include(x => x.City)
                .Include(x => x.Doctor)
                .Include(x => x.Specialization)
                .Include(x => x.User)
                .Include(x => x.DoctorEmailStatus)
                .Include(x => x.UserEmailStatus)
                .ToListAsync<PersonalAdvice>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="propertiesToUpdate"></param>
        /// <returns></returns>
        public async Task<PersonalAdvice> UpdateAsync(PersonalAdvice model, params string[] propertiesToUpdate)
        {
            context.Update<PersonalAdvice>(model, propertiesToUpdate);
            await context.SaveChangesAsync();
            return model;
        }

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PersonalAdvice> Add(PersonalAdvice model)
        {
            if (model != null)
            {
                context.PersonalAdvices.Add(model);
                await context.SaveChangesAsync();
            }

            return model; //await GetByIdAsync(model.Id);
        }
    }
}
