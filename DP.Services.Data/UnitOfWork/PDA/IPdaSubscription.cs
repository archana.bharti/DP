﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IPdaSubscription
    {
        IQueryable<PdaSubscription> Get();
        Task<List<PdaSubscription>> GetAllAsync();
        Task<PdaSubscription> GetByIdAsync(int id);
        Task<PdaSubscription> Add(PdaSubscription model);
        Task<PdaSubscription> UpdateAsync(PdaSubscription model);
        Task<PdaSubscription> UpdateAsync(PdaSubscription model, params string[] propertiesToUpdate);
    }
}
