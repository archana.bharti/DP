﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IPdaDao
    {
        IQueryable<PersonalAdvice> Get();
        Task<List<PersonalAdvice>> GetAllAsync();
        Task<PersonalAdvice> GetByIdAsync(int id);
        Task<List<PersonalAdvice>> GetByUserIdAsync(string id);
        Task<List<PersonalAdvice>> GetByDoctorIdAsync(int id);
        Task<PersonalAdvice> Add(PersonalAdvice model);
        Task<PersonalAdvice> UpdateAsync(PersonalAdvice model, params string[] propertiesToUpdate);
    }
}
