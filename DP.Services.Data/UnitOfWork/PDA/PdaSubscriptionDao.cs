﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using DP.Common.Enums;

namespace DP.Services.Data.UnitOfWork
{
    public sealed class PdaSubscriptionDao : IPdaSubscriptionDao
    {
        private DPContext context;
        public PdaSubscriptionDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public IQueryable<PdaSubscription> Get()
        {
            return this.context.PdaSubscriptions;
        }

        public async Task<List<PdaSubscription>> GetAllAsync()
        {
            return await this.context.PdaSubscriptions.ToListAsync();
        }

        public async Task<PdaSubscription> GetByIdAsync(int id)
        {
            return await this.context.PdaSubscriptions.Where(x=>x.Id == id)
                .Include(x => x.Status)
                .Include(x => x.DoctorOrClinic)
                .Include(x=>x.Specialization)
                .Include(x => x.Specialization.Specialization)
                .SingleOrDefaultAsync();
        }

        public async Task<List<PdaSubscription>> GetByDoctorOrClinicIdAsync(int doctorOrClinicId)
        {
            return await this.context.PdaSubscriptions.Where(x => x.DoctorOrClinicId  == doctorOrClinicId)
                .Include(x=>x.Status)
                .Include(x=>x.DoctorOrClinic)
                .Include(x => x.Specialization)
                .Include(x => x.Specialization.Specialization)
                .ToListAsync();
        }

        public async Task<PdaSubscription> Add(PdaSubscription model)
        {
            this.context.PdaSubscriptions.Add(model);
            await this.context.SaveChangesAsync();
            return model;
        }

        public async Task<PdaSubscription> UpdateAsync(PdaSubscription model)
        {
            this.context.Entry(model).State = EntityState.Modified;
            await this.context.SaveChangesAsync();
            return model;
        }

        public async Task<PdaSubscription> UpdateAsync(PdaSubscription model, params string[] propertiesToUpdate)
        {
            context.Update<PdaSubscription>(model, propertiesToUpdate);
            await context.SaveChangesAsync();
            return await GetByIdAsync(model.Id);
        }

        public async Task<List<PdaSubscription>> GetActiveSubscriptions(int doctorOrClinicId)
        {
            return await this.context
                .PdaSubscriptions
                .Where(x => x.DoctorOrClinicId == doctorOrClinicId)
                .Where(x=>x.StatusValue == (int)PdaSubscriptionStatus.Active)
                .Include(x => x.Status)
                .Include(x => x.DoctorOrClinic)
                .Include(x => x.Specialization)
                .Include(x => x.Specialization.Specialization)
                .ToListAsync();
        }

        public async Task<List<PdaSubscription>> GetActiveSubscriptions()
        {
            return await this.context
                .PdaSubscriptions               
                .Where(x => x.StatusValue == (int)PdaSubscriptionStatus.Active)
                .Include(x => x.Status)
                .Include(x => x.DoctorOrClinic)
                .Include(x => x.Specialization)
                .Include(x => x.Specialization.Specialization)
                .ToListAsync();
        }
    }
}
