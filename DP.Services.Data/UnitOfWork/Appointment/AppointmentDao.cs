﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class AppointmentDao : IAppointmentDao, IDisposable
    {
        private DPContext context;
        public AppointmentDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        public async Task<int> Add(Appointment model)
        {
            try
            {
                if (model != null)
                {
                    context.Appointments.Add(model);
                    return await context.SaveChangesAsync();
                }
            }
            catch(Exception ex)
            {
                throw new Exception("APPOINTMENT ADD DATA FAILURE");
            }

            return 0;
        }

        public IQueryable<Appointment> Appointments()
        {
            try
            {
                return context.Appointments;
            }
            catch (Exception ex)
            {
                throw new Exception("GET APPOINTMENTS DATA FAILURE");
            }
        }
    }
}
