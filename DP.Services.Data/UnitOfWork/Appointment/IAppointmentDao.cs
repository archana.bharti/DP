﻿using DP.Services.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface IAppointmentDao
    {
        IQueryable<Appointment> Appointments();
        Task<int> Add(Appointment model);
    }
}
