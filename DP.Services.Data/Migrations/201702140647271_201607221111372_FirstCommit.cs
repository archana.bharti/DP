namespace DP.Services.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _201607221111372_FirstCommit : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.dp_offer_master", "Content", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.dp_offer_master", "Content", c => c.String(maxLength: 512, storeType: "nvarchar"));
        }
    }
}
