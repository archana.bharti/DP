namespace DP.Services.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstCommit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dp_appointment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        Mobile = c.String(unicode: false),
                        Message = c.String(unicode: false),
                        StartTime = c.DateTime(nullable: false, precision: 0),
                        EndTime = c.DateTime(nullable: false, precision: 0),
                        EntityId = c.Int(nullable: false),
                        EntityType = c.Int(nullable: false),
                        Token = c.String(unicode: false),
                        StatusValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_bookmarks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EntityId = c.Int(nullable: false),
                        EntityType = c.Int(nullable: false),
                        BookmarkEntityId = c.Int(nullable: false),
                        BookmarkEntityType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_city",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        StateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_state", t => t.StateId, cascadeDelete: true)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.dp_state",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        ShortCode = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_clinic_rating",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128, storeType: "nvarchar"),
                        ClinicOrDoctorId = c.Int(nullable: false),
                        Cleanliness = c.Int(nullable: false),
                        CleanlinessSumOfRating = c.Single(nullable: false),
                        CleanlinessAverage = c.Single(nullable: false),
                        CleanlinessPercentage = c.Single(nullable: false),
                        FeesCharged = c.Int(nullable: false),
                        FeesChargedSumOfRating = c.Single(nullable: false),
                        FeesChargedAverage = c.Single(nullable: false),
                        FeesChargedPercentage = c.Single(nullable: false),
                        WaitingHours = c.Int(nullable: false),
                        WaitingHoursSumOfRating = c.Single(nullable: false),
                        WaitingHoursAverage = c.Single(nullable: false),
                        WaitingHoursPercentage = c.Single(nullable: false),
                        Listen = c.Int(nullable: false),
                        ListenSumOfRating = c.Single(nullable: false),
                        ListenAverage = c.Single(nullable: false),
                        ListenPercentage = c.Single(nullable: false),
                        Comfort = c.Int(nullable: false),
                        ComfortSumOfRating = c.Single(nullable: false),
                        ComfortAverage = c.Single(nullable: false),
                        ComfortPercentage = c.Single(nullable: false),
                        FriendlyStaff = c.Int(nullable: false),
                        FriendlyStaffSumOfRating = c.Single(nullable: false),
                        FriendlyStaffAverage = c.Single(nullable: false),
                        FriendlyStaffPercentage = c.Single(nullable: false),
                        CustomerSatisfaction = c.Int(nullable: false),
                        CustomerSatisfactionSumOfRating = c.Single(nullable: false),
                        CustomerSatisfactionAverage = c.Single(nullable: false),
                        CustomerSatisfactionPercentage = c.Single(nullable: false),
                        QualityOfService = c.Int(nullable: false),
                        QualityOfServiceSumOfRating = c.Single(nullable: false),
                        QualityOfServiceAverage = c.Single(nullable: false),
                        QualityOfServicePercentage = c.Single(nullable: false),
                        OnCall = c.Int(nullable: false),
                        OnCallSumOfRating = c.Single(nullable: false),
                        OnCallAverage = c.Single(nullable: false),
                        OnCallPercentage = c.Single(nullable: false),
                        LatestTechnology = c.Int(nullable: false),
                        LatestTechnologySumOfRating = c.Single(nullable: false),
                        LatestTechnologyAverage = c.Single(nullable: false),
                        LatestTechnologyPercentage = c.Single(nullable: false),
                        YourExperience = c.String(unicode: false),
                        Type = c.String(maxLength: 256, storeType: "nvarchar"),
                        ReviewedDate = c.DateTime(nullable: false, precision: 0),
                        LastVisited = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_clinic_doctor", t => t.ClinicOrDoctorId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ClinicOrDoctorId);
            
            CreateTable(
                "dbo.dp_clinic_doctor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category = c.Int(nullable: false),
                        Type = c.String(maxLength: 256, storeType: "nvarchar"),
                        Name = c.String(maxLength: 256, storeType: "nvarchar"),
                        Summary = c.String(unicode: false),
                        FullAddress = c.String(maxLength: 256, storeType: "nvarchar"),
                        LocationId = c.Int(),
                        CityId = c.Int(),
                        StateId = c.Int(),
                        Phone_No1 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No2 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No3 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No4 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Profile_Pic_1 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Profile_Pic_2 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Profile_Pic_3 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Display_Phone = c.String(maxLength: 8, storeType: "nvarchar"),
                        Website = c.String(maxLength: 256, storeType: "nvarchar"),
                        VideoUrl = c.String(maxLength: 512, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_city", t => t.CityId)
                .ForeignKey("dbo.dp_location", t => t.LocationId)
                .ForeignKey("dbo.dp_state", t => t.StateId)
                .Index(t => t.LocationId)
                .Index(t => t.CityId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.dp_location",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256, storeType: "nvarchar"),
                        CityId = c.Int(nullable: false),
                        Pincode = c.Int(nullable: false),
                        Longitude = c.Single(),
                        Latitude = c.Single(),
                        Zone = c.Short(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_city", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.dp_specialization_clinic",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClinicId = c.Int(nullable: false),
                        SpecializationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_clinic_doctor", t => t.ClinicId, cascadeDelete: true)
                .ForeignKey("dbo.dp_specialization", t => t.SpecializationId, cascadeDelete: true)
                .Index(t => t.ClinicId)
                .Index(t => t.SpecializationId);
            
            CreateTable(
                "dbo.dp_specialization",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category = c.String(maxLength: 256, storeType: "nvarchar"),
                        Group = c.String(maxLength: 256, storeType: "nvarchar"),
                        Specific = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        UserType = c.String(unicode: false),
                        PictureUrl = c.String(maxLength: 256, storeType: "nvarchar"),
                        LastSignInOn = c.DateTime(precision: 0),
                        CreatedOn = c.DateTime(nullable: false, precision: 0),
                        SubscriberId = c.Int(),
                        StateId = c.Int(),
                        CityId = c.Int(),
                        LocationId = c.Int(),
                        DisplayName = c.String(unicode: false),
                        IsVerified = c.Boolean(nullable: false),
                        IsTerminated = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256, storeType: "nvarchar"),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(unicode: false),
                        SecurityStamp = c.String(unicode: false),
                        PhoneNumber = c.String(unicode: false),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(precision: 0),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_city", t => t.CityId)
                .ForeignKey("dbo.dp_location", t => t.LocationId)
                .ForeignKey("dbo.dp_state", t => t.StateId)
                .Index(t => t.StateId)
                .Index(t => t.CityId)
                .Index(t => t.LocationId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        ClaimType = c.String(unicode: false),
                        ClaimValue = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        ProviderKey = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        RoleId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.dp_college_department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256, storeType: "nvarchar"),
                        Category = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_college_location",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 64, storeType: "nvarchar"),
                        StateId = c.Int(nullable: false),
                        Zone = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_college_rating",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128, storeType: "nvarchar"),
                        CollegeId = c.Int(nullable: false),
                        QualityOfEducation = c.Int(nullable: false),
                        QualityOfEducationSumOfRating = c.Single(nullable: false),
                        QualityOfEducationAverage = c.Single(nullable: false),
                        QualityOfEducationPercentage = c.Single(nullable: false),
                        Location = c.Int(nullable: false),
                        LocationSumOfRating = c.Single(nullable: false),
                        LocationAverage = c.Single(nullable: false),
                        LocationPercentage = c.Single(nullable: false),
                        Library = c.Int(nullable: false),
                        LibrarySumOfRating = c.Single(nullable: false),
                        LibraryAverage = c.Single(nullable: false),
                        LibraryPercentage = c.Single(nullable: false),
                        Campus = c.Int(nullable: false),
                        CampusSumOfRating = c.Single(nullable: false),
                        CampusAverage = c.Single(nullable: false),
                        CampusPercentage = c.Single(nullable: false),
                        Food = c.Int(nullable: false),
                        FoodSumOfRating = c.Single(nullable: false),
                        FoodAverage = c.Single(nullable: false),
                        FoodPercentage = c.Single(nullable: false),
                        SocialActivities = c.Int(nullable: false),
                        SocialActivitiesSumOfRating = c.Single(nullable: false),
                        SocialActivitiesAverage = c.Single(nullable: false),
                        SocialActivitiesPercentage = c.Single(nullable: false),
                        IntenetConnectivity = c.Int(nullable: false),
                        IntenetConnectivitySumOfRating = c.Single(nullable: false),
                        IntenetConnectivityAverage = c.Single(nullable: false),
                        IntenetConnectivityPercentage = c.Single(nullable: false),
                        OverallSatisfaction = c.Int(nullable: false),
                        OverallSatisfactionSumOfRating = c.Single(nullable: false),
                        OverallSatisfactionAverage = c.Single(nullable: false),
                        OverallSatisfactionPercentage = c.Single(nullable: false),
                        ReviewedDate = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_college", t => t.CollegeId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CollegeId);
            
            CreateTable(
                "dbo.dp_college",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentType = c.Int(),
                        Affiliated = c.String(maxLength: 256, storeType: "nvarchar"),
                        Approved = c.String(maxLength: 256, storeType: "nvarchar"),
                        PlacementSummary = c.String(maxLength: 256, storeType: "nvarchar"),
                        Established = c.String(maxLength: 256, storeType: "nvarchar"),
                        Management = c.String(maxLength: 150, storeType: "nvarchar"),
                        Type = c.String(maxLength: 256, storeType: "nvarchar"),
                        Name = c.String(maxLength: 256, storeType: "nvarchar"),
                        Summary = c.String(unicode: false),
                        FullAddress = c.String(maxLength: 256, storeType: "nvarchar"),
                        LocationId = c.Int(),
                        CityId = c.Int(),
                        StateId = c.Int(),
                        Phone_No1 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No2 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No3 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No4 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Profile_Pic_1 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Profile_Pic_2 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Profile_Pic_3 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Display_Phone = c.String(maxLength: 8, storeType: "nvarchar"),
                        Website = c.String(maxLength: 256, storeType: "nvarchar"),
                        VideoUrl = c.String(maxLength: 512, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_city", t => t.CityId)
                .ForeignKey("dbo.dp_location", t => t.LocationId)
                .ForeignKey("dbo.dp_state", t => t.StateId)
                .Index(t => t.LocationId)
                .Index(t => t.CityId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.dp_contactus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 128, storeType: "nvarchar"),
                        Email = c.String(maxLength: 128, storeType: "nvarchar"),
                        Mobile = c.String(maxLength: 16, storeType: "nvarchar"),
                        Message = c.String(maxLength: 200, storeType: "nvarchar"),
                        Reason = c.String(maxLength: 32, storeType: "nvarchar"),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_doctor_hospital",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.Int(nullable: false),
                        HospitalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_endorsement",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EndorseeId = c.Int(nullable: false),
                        EndorseeType = c.Int(nullable: false),
                        EndorserId = c.Int(nullable: false),
                        EndorserType = c.Int(nullable: false),
                        SpecializationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_enum_status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(unicode: false),
                        Description = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_feature",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EntityId = c.Int(nullable: false),
                        EntityType = c.Int(nullable: false),
                        StartDate = c.DateTime(precision: 0),
                        EndDate = c.DateTime(precision: 0),
                        StatusValue = c.Int(nullable: false),
                        EnableBasic = c.Boolean(nullable: false),
                        EnableTextual = c.Boolean(nullable: false),
                        EnableFeatured = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_enum_status", t => t.StatusValue, cascadeDelete: true)
                .Index(t => t.StatusValue);
            
            CreateTable(
                "dbo.dp_hospital_city",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CityId = c.Int(nullable: false),
                        StateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_city", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.dp_state", t => t.StateId, cascadeDelete: true)
                .Index(t => t.CityId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.dp_hospital_rating",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128, storeType: "nvarchar"),
                        HospitalId = c.Int(nullable: false),
                        Infrastructure = c.Int(nullable: false),
                        InfrastructureSumOfRating = c.Single(nullable: false),
                        InfrastructureAverage = c.Single(nullable: false),
                        InfrastructurePercentage = c.Single(nullable: false),
                        SafetyCleanliness = c.Int(nullable: false),
                        SafetyCleanlinessSumOfRating = c.Single(nullable: false),
                        SafetyCleanlinessAverage = c.Single(nullable: false),
                        SafetyCleanlinessPercentage = c.Single(nullable: false),
                        CourteousStaff = c.Int(nullable: false),
                        CourteousStaffSumOfRating = c.Single(nullable: false),
                        CourteousStaffAverage = c.Single(nullable: false),
                        CourteousStaffPercentage = c.Single(nullable: false),
                        CafeFood = c.Int(nullable: false),
                        CafeFoodSumOfRating = c.Single(nullable: false),
                        CafeFoodAverage = c.Single(nullable: false),
                        CafeFoodPercentage = c.Single(nullable: false),
                        QualityOfCare = c.Int(nullable: false),
                        QualityOfCareSumOfRating = c.Single(nullable: false),
                        QualityOfCareAverage = c.Single(nullable: false),
                        QualityOfCarePercentage = c.Single(nullable: false),
                        VistingHours = c.Int(nullable: false),
                        VistingHoursSumOfRating = c.Single(nullable: false),
                        VistingHoursAverage = c.Single(nullable: false),
                        VistingHoursPercentage = c.Single(nullable: false),
                        OvernightStay = c.Int(nullable: false),
                        OvernightStaySumOfRating = c.Single(nullable: false),
                        OvernightStayAverage = c.Single(nullable: false),
                        OvernightStayPercentage = c.Single(nullable: false),
                        EmergencyCare = c.Int(nullable: false),
                        EmergencyCareSumOfRating = c.Single(nullable: false),
                        EmergencyCareAverage = c.Single(nullable: false),
                        EmergencyCarePercentage = c.Single(nullable: false),
                        OverallSatisfaction = c.Int(nullable: false),
                        OverallSatisfactionSumOfRating = c.Single(nullable: false),
                        OverallSatisfactionAverage = c.Single(nullable: false),
                        OverallSatisfactionPercentage = c.Single(nullable: false),
                        ReviewedDate = c.DateTime(nullable: false, precision: 0),
                        LastVisited = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_hospital", t => t.HospitalId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.HospitalId);
            
            CreateTable(
                "dbo.dp_hospital",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(maxLength: 256, storeType: "nvarchar"),
                        Name = c.String(maxLength: 256, storeType: "nvarchar"),
                        Summary = c.String(unicode: false),
                        FullAddress = c.String(maxLength: 256, storeType: "nvarchar"),
                        LocationId = c.Int(),
                        CityId = c.Int(),
                        StateId = c.Int(),
                        Phone_No1 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No2 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No3 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No4 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Profile_Pic_1 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Profile_Pic_2 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Profile_Pic_3 = c.String(maxLength: 256, storeType: "nvarchar"),
                        Display_Phone = c.String(maxLength: 8, storeType: "nvarchar"),
                        Website = c.String(maxLength: 256, storeType: "nvarchar"),
                        VideoUrl = c.String(maxLength: 512, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_city", t => t.CityId)
                .ForeignKey("dbo.dp_location", t => t.LocationId)
                .ForeignKey("dbo.dp_state", t => t.StateId)
                .Index(t => t.LocationId)
                .Index(t => t.CityId)
                .Index(t => t.StateId);
            
            CreateTable(
                "dbo.dp_insurance",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProviderName = c.String(maxLength: 512, storeType: "nvarchar"),
                        Summary = c.String(maxLength: 512, storeType: "nvarchar"),
                        PlanName = c.String(maxLength: 512, storeType: "nvarchar"),
                        PlanType = c.String(maxLength: 64, storeType: "nvarchar"),
                        ImageUrl = c.String(maxLength: 512, storeType: "nvarchar"),
                        Address = c.String(maxLength: 512, storeType: "nvarchar"),
                        Phone_No1 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No2 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No3 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Phone_No4 = c.String(maxLength: 32, storeType: "nvarchar"),
                        Display_Phone = c.String(maxLength: 8, storeType: "nvarchar"),
                        CashlessFacility = c.Boolean(nullable: false),
                        MaternityBenefits = c.Boolean(nullable: false),
                        AmbulanceCharges = c.Boolean(nullable: false),
                        DayCareProcedures = c.Boolean(nullable: false),
                        HealthCheckup = c.Boolean(nullable: false),
                        TaxBenefits = c.Boolean(nullable: false),
                        HospitalizationExpenses = c.Boolean(nullable: false),
                        FreelookPeriod = c.Boolean(nullable: false),
                        AlternativeTreatment = c.Boolean(nullable: false),
                        CumulativeBonus = c.Boolean(nullable: false),
                        OrganDonorExpenses = c.Boolean(nullable: false),
                        PrePostHospitalizationCharges = c.Boolean(nullable: false),
                        Portability = c.Boolean(nullable: false),
                        Website = c.String(maxLength: 512, storeType: "nvarchar"),
                        VideoUrl = c.String(maxLength: 512, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_insurance_rating",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InsuranceId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128, storeType: "nvarchar"),
                        CustomerService = c.Int(nullable: false),
                        CustomerServiceSumOfRating = c.Single(nullable: false),
                        CustomerServiceAverage = c.Single(nullable: false),
                        CustomerServicePercentage = c.Single(nullable: false),
                        InNetworkFacilities = c.Int(nullable: false),
                        InNetworkFacilitiesSumOfRating = c.Single(nullable: false),
                        InNetworkFacilitiesAverage = c.Single(nullable: false),
                        InNetworkFacilitiesPercentage = c.Single(nullable: false),
                        EaseOfClaimFilling = c.Int(nullable: false),
                        EaseOfClaimFillingSumOfRating = c.Single(nullable: false),
                        EaseOfClaimFillingAverage = c.Single(nullable: false),
                        EaseOfClaimFillingPercentage = c.Single(nullable: false),
                        MeetsPromisedBenefits = c.Int(nullable: false),
                        MeetsPromisedBenefitsSumOfRating = c.Single(nullable: false),
                        MeetsPromisedBenefitsAverage = c.Single(nullable: false),
                        MeetsPromisedBenefitsPercentage = c.Single(nullable: false),
                        ValueForMoney = c.Int(nullable: false),
                        ValueForMoneySumOfRating = c.Single(nullable: false),
                        ValueForMoneyAverage = c.Single(nullable: false),
                        ValueForMoneyPercentage = c.Single(nullable: false),
                        ClaimSettlement = c.Int(nullable: false),
                        ClaimSettlementSumOfRating = c.Single(nullable: false),
                        ClaimSettlementAverage = c.Single(nullable: false),
                        ClaimSettlementPercentage = c.Single(nullable: false),
                        OverallSatisfaction = c.Int(nullable: false),
                        OverallSatisfactionSumOfRating = c.Single(nullable: false),
                        OverallSatisfactionAverage = c.Single(nullable: false),
                        OverallSatisfactionPercentage = c.Single(nullable: false),
                        ReviewedDate = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_insurance", t => t.InsuranceId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.InsuranceId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.dp_offer_master",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProviderId = c.Int(nullable: false),
                        ProviderType = c.Int(nullable: false),
                        ProviderEmail = c.String(maxLength: 64, storeType: "nvarchar"),
                        Content = c.String(maxLength: 512, storeType: "nvarchar"),
                        CurrentPrice = c.Single(nullable: false),
                        DiscountedPercentage = c.Single(nullable: false),
                        ActualPrice = c.Single(nullable: false),
                        ContentImage = c.Binary(),
                        ImageUrl = c.String(unicode: false),
                        StartDateTime = c.DateTime(precision: 0),
                        EndDateTime = c.DateTime(precision: 0),
                        OfferStatusValue = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_enum_status", t => t.OfferStatusValue, cascadeDelete: true)
                .Index(t => t.OfferStatusValue);
            
            CreateTable(
                "dbo.dp_offer_transaction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OfferMasterId = c.Int(nullable: false),
                        UserEmail = c.String(unicode: false),
                        UserPhone = c.String(unicode: false),
                        TotalRedeemCount = c.Int(nullable: false),
                        Token = c.String(unicode: false),
                        DateTime = c.DateTime(nullable: false, precision: 0),
                        IpAddress = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_offer_master", t => t.OfferMasterId, cascadeDelete: true)
                .Index(t => t.OfferMasterId);
            
            CreateTable(
                "dbo.dp_options",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Value = c.String(unicode: false),
                        Context = c.Int(),
                    })
                .PrimaryKey(t => t.Key);
            
            CreateTable(
                "dbo.dp_pda_subscription",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorOrClinicId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false, precision: 0),
                        EndDate = c.DateTime(nullable: false, precision: 0),
                        StatusValue = c.Int(nullable: false),
                        SpecializationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_clinic_doctor", t => t.DoctorOrClinicId, cascadeDelete: true)
                .ForeignKey("dbo.dp_specialization_clinic", t => t.SpecializationId, cascadeDelete: true)
                .ForeignKey("dbo.dp_enum_status", t => t.StatusValue, cascadeDelete: true)
                .Index(t => t.DoctorOrClinicId)
                .Index(t => t.StatusValue)
                .Index(t => t.SpecializationId);
            
            CreateTable(
                "dbo.dp_personaladvice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(maxLength: 512, storeType: "nvarchar"),
                        Question = c.String(unicode: false),
                        Answer = c.String(unicode: false),
                        SpecializationId = c.Int(nullable: false),
                        CityId = c.Int(),
                        UserEmail = c.String(maxLength: 512, storeType: "nvarchar"),
                        UserId = c.String(maxLength: 128, storeType: "nvarchar"),
                        UserEmailStatusValue = c.Int(nullable: false),
                        SubmittedDate = c.DateTime(nullable: false, precision: 0),
                        RepliedDate = c.DateTime(precision: 0),
                        DoctorId = c.Int(nullable: false),
                        DoctorEmail = c.String(maxLength: 512, storeType: "nvarchar"),
                        DoctorEmailStatusValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_city", t => t.CityId)
                .ForeignKey("dbo.dp_clinic_doctor", t => t.DoctorId, cascadeDelete: true)
                .ForeignKey("dbo.dp_enum_status", t => t.DoctorEmailStatusValue, cascadeDelete: true)
                .ForeignKey("dbo.dp_specialization", t => t.SpecializationId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.dp_enum_status", t => t.UserEmailStatusValue, cascadeDelete: true)
                .Index(t => t.SpecializationId)
                .Index(t => t.CityId)
                .Index(t => t.UserId)
                .Index(t => t.UserEmailStatusValue)
                .Index(t => t.DoctorId)
                .Index(t => t.DoctorEmailStatusValue);
            
            CreateTable(
                "dbo.dp_poll_master",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(maxLength: 512, storeType: "nvarchar"),
                        Active = c.Boolean(nullable: false),
                        LastUpdatedOn = c.DateTime(nullable: false, precision: 0),
                        YesText = c.String(unicode: false),
                        NoText = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_poll",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PollMasterId = c.Int(nullable: false),
                        IpAddress = c.String(maxLength: 32, storeType: "nvarchar"),
                        Yes = c.Boolean(nullable: false),
                        No = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_poll_master", t => t.PollMasterId, cascadeDelete: true)
                .Index(t => t.PollMasterId);
            
            CreateTable(
                "dbo.dp_qualification_doctor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.Int(nullable: false),
                        ShortQualificationId = c.Int(),
                        LongQualificationId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_clinic_doctor", t => t.DoctorId, cascadeDelete: true, name : "dp_qualif_doc_fk_cli_doc")
                .ForeignKey("dbo.dp_qualification_long", t => t.LongQualificationId, name: "dp_qualif_doc_fk_qualif_long")
                .ForeignKey("dbo.dp_qualification_short", t => t.ShortQualificationId, name: "dp_qualif_doc_fk_qualif_short")
                .Index(t => t.DoctorId)
                .Index(t => t.ShortQualificationId)
                .Index(t => t.LongQualificationId);
            
            CreateTable(
                "dbo.dp_qualification_long",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LongEndDegree = c.String(maxLength: 256, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_qualification_short",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShortEndDegree = c.String(maxLength: 256, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.dp_recommend",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IpAddress = c.String(maxLength: 32, storeType: "nvarchar"),
                        EntityType = c.Int(nullable: false),
                        EntityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Name = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.dp_specialization_hospital",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HospitalId = c.Int(nullable: false),
                        SpecializationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.dp_hospital", t => t.HospitalId, cascadeDelete: true, name: "dp_spz_hos_fk_hos")
                .ForeignKey("dbo.dp_specialization", t => t.SpecializationId, cascadeDelete: true, name: "dp_spz_hos_fk_spz")
                .Index(t => t.HospitalId)
                .Index(t => t.SpecializationId);
            
            CreateTable(
                "dbo.dp_std_code",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        State = c.String(maxLength: 25, storeType: "nvarchar"),
                        Capital = c.String(maxLength: 256, storeType: "nvarchar"),
                        Code = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dp_specialization_hospital", "SpecializationId", "dbo.dp_specialization");
            DropForeignKey("dbo.dp_specialization_hospital", "HospitalId", "dbo.dp_hospital");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.dp_qualification_doctor", "ShortQualificationId", "dbo.dp_qualification_short");
            DropForeignKey("dbo.dp_qualification_doctor", "LongQualificationId", "dbo.dp_qualification_long");
            DropForeignKey("dbo.dp_qualification_doctor", "DoctorId", "dbo.dp_clinic_doctor");
            DropForeignKey("dbo.dp_poll", "PollMasterId", "dbo.dp_poll_master");
            DropForeignKey("dbo.dp_personaladvice", "UserEmailStatusValue", "dbo.dp_enum_status");
            DropForeignKey("dbo.dp_personaladvice", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.dp_personaladvice", "SpecializationId", "dbo.dp_specialization");
            DropForeignKey("dbo.dp_personaladvice", "DoctorEmailStatusValue", "dbo.dp_enum_status");
            DropForeignKey("dbo.dp_personaladvice", "DoctorId", "dbo.dp_clinic_doctor");
            DropForeignKey("dbo.dp_personaladvice", "CityId", "dbo.dp_city");
            DropForeignKey("dbo.dp_pda_subscription", "StatusValue", "dbo.dp_enum_status");
            DropForeignKey("dbo.dp_pda_subscription", "SpecializationId", "dbo.dp_specialization_clinic");
            DropForeignKey("dbo.dp_pda_subscription", "DoctorOrClinicId", "dbo.dp_clinic_doctor");
            DropForeignKey("dbo.dp_offer_transaction", "OfferMasterId", "dbo.dp_offer_master");
            DropForeignKey("dbo.dp_offer_master", "OfferStatusValue", "dbo.dp_enum_status");
            DropForeignKey("dbo.dp_insurance_rating", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.dp_insurance_rating", "InsuranceId", "dbo.dp_insurance");
            DropForeignKey("dbo.dp_hospital_rating", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.dp_hospital_rating", "HospitalId", "dbo.dp_hospital");
            DropForeignKey("dbo.dp_hospital", "StateId", "dbo.dp_state");
            DropForeignKey("dbo.dp_hospital", "LocationId", "dbo.dp_location");
            DropForeignKey("dbo.dp_hospital", "CityId", "dbo.dp_city");
            DropForeignKey("dbo.dp_hospital_city", "StateId", "dbo.dp_state");
            DropForeignKey("dbo.dp_hospital_city", "CityId", "dbo.dp_city");
            DropForeignKey("dbo.dp_feature", "StatusValue", "dbo.dp_enum_status");
            DropForeignKey("dbo.dp_college_rating", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.dp_college_rating", "CollegeId", "dbo.dp_college");
            DropForeignKey("dbo.dp_college", "StateId", "dbo.dp_state");
            DropForeignKey("dbo.dp_college", "LocationId", "dbo.dp_location");
            DropForeignKey("dbo.dp_college", "CityId", "dbo.dp_city");
            DropForeignKey("dbo.dp_clinic_rating", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "StateId", "dbo.dp_state");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "LocationId", "dbo.dp_location");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "CityId", "dbo.dp_city");
            DropForeignKey("dbo.dp_clinic_rating", "ClinicOrDoctorId", "dbo.dp_clinic_doctor");
            DropForeignKey("dbo.dp_clinic_doctor", "StateId", "dbo.dp_state");
            DropForeignKey("dbo.dp_specialization_clinic", "SpecializationId", "dbo.dp_specialization");
            DropForeignKey("dbo.dp_specialization_clinic", "ClinicId", "dbo.dp_clinic_doctor");
            DropForeignKey("dbo.dp_clinic_doctor", "LocationId", "dbo.dp_location");
            DropForeignKey("dbo.dp_location", "CityId", "dbo.dp_city");
            DropForeignKey("dbo.dp_clinic_doctor", "CityId", "dbo.dp_city");
            DropForeignKey("dbo.dp_city", "StateId", "dbo.dp_state");
            DropIndex("dbo.dp_specialization_hospital", new[] { "SpecializationId" });
            DropIndex("dbo.dp_specialization_hospital", new[] { "HospitalId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.dp_qualification_doctor", new[] { "LongQualificationId" });
            DropIndex("dbo.dp_qualification_doctor", new[] { "ShortQualificationId" });
            DropIndex("dbo.dp_qualification_doctor", new[] { "DoctorId" });
            DropIndex("dbo.dp_poll", new[] { "PollMasterId" });
            DropIndex("dbo.dp_personaladvice", new[] { "DoctorEmailStatusValue" });
            DropIndex("dbo.dp_personaladvice", new[] { "DoctorId" });
            DropIndex("dbo.dp_personaladvice", new[] { "UserEmailStatusValue" });
            DropIndex("dbo.dp_personaladvice", new[] { "UserId" });
            DropIndex("dbo.dp_personaladvice", new[] { "CityId" });
            DropIndex("dbo.dp_personaladvice", new[] { "SpecializationId" });
            DropIndex("dbo.dp_pda_subscription", new[] { "SpecializationId" });
            DropIndex("dbo.dp_pda_subscription", new[] { "StatusValue" });
            DropIndex("dbo.dp_pda_subscription", new[] { "DoctorOrClinicId" });
            DropIndex("dbo.dp_offer_transaction", new[] { "OfferMasterId" });
            DropIndex("dbo.dp_offer_master", new[] { "OfferStatusValue" });
            DropIndex("dbo.dp_insurance_rating", new[] { "UserId" });
            DropIndex("dbo.dp_insurance_rating", new[] { "InsuranceId" });
            DropIndex("dbo.dp_hospital", new[] { "StateId" });
            DropIndex("dbo.dp_hospital", new[] { "CityId" });
            DropIndex("dbo.dp_hospital", new[] { "LocationId" });
            DropIndex("dbo.dp_hospital_rating", new[] { "HospitalId" });
            DropIndex("dbo.dp_hospital_rating", new[] { "UserId" });
            DropIndex("dbo.dp_hospital_city", new[] { "StateId" });
            DropIndex("dbo.dp_hospital_city", new[] { "CityId" });
            DropIndex("dbo.dp_feature", new[] { "StatusValue" });
            DropIndex("dbo.dp_college", new[] { "StateId" });
            DropIndex("dbo.dp_college", new[] { "CityId" });
            DropIndex("dbo.dp_college", new[] { "LocationId" });
            DropIndex("dbo.dp_college_rating", new[] { "CollegeId" });
            DropIndex("dbo.dp_college_rating", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "LocationId" });
            DropIndex("dbo.AspNetUsers", new[] { "CityId" });
            DropIndex("dbo.AspNetUsers", new[] { "StateId" });
            DropIndex("dbo.dp_specialization_clinic", new[] { "SpecializationId" });
            DropIndex("dbo.dp_specialization_clinic", new[] { "ClinicId" });
            DropIndex("dbo.dp_location", new[] { "CityId" });
            DropIndex("dbo.dp_clinic_doctor", new[] { "StateId" });
            DropIndex("dbo.dp_clinic_doctor", new[] { "CityId" });
            DropIndex("dbo.dp_clinic_doctor", new[] { "LocationId" });
            DropIndex("dbo.dp_clinic_rating", new[] { "ClinicOrDoctorId" });
            DropIndex("dbo.dp_clinic_rating", new[] { "UserId" });
            DropIndex("dbo.dp_city", new[] { "StateId" });
            DropTable("dbo.dp_std_code");
            DropTable("dbo.dp_specialization_hospital");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.dp_recommend");
            DropTable("dbo.dp_qualification_short");
            DropTable("dbo.dp_qualification_long");
            DropTable("dbo.dp_qualification_doctor");
            DropTable("dbo.dp_poll");
            DropTable("dbo.dp_poll_master");
            DropTable("dbo.dp_personaladvice");
            DropTable("dbo.dp_pda_subscription");
            DropTable("dbo.dp_options");
            DropTable("dbo.dp_offer_transaction");
            DropTable("dbo.dp_offer_master");
            DropTable("dbo.dp_insurance_rating");
            DropTable("dbo.dp_insurance");
            DropTable("dbo.dp_hospital");
            DropTable("dbo.dp_hospital_rating");
            DropTable("dbo.dp_hospital_city");
            DropTable("dbo.dp_feature");
            DropTable("dbo.dp_enum_status");
            DropTable("dbo.dp_endorsement");
            DropTable("dbo.dp_doctor_hospital");
            DropTable("dbo.dp_contactus");
            DropTable("dbo.dp_college");
            DropTable("dbo.dp_college_rating");
            DropTable("dbo.dp_college_location");
            DropTable("dbo.dp_college_department");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.dp_specialization");
            DropTable("dbo.dp_specialization_clinic");
            DropTable("dbo.dp_location");
            DropTable("dbo.dp_clinic_doctor");
            DropTable("dbo.dp_clinic_rating");
            DropTable("dbo.dp_state");
            DropTable("dbo.dp_city");
            DropTable("dbo.dp_bookmarks");
            DropTable("dbo.dp_appointment");
        }
    }
}
