﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface ISpecializationHospitalDao
    {
        IQueryable<SpecializationHospital> Get();
        Task<SpecializationHospital> GetAsync(int hospitalId, int specializationId);
        Task<List<SpecializationHospital>> GetAsync(int hospitalId);
        Task<List<SpecializationHospital>> GetAllAsync();
        Task<SpecializationHospital> GetByIdAsync(int id);
        Task<SpecializationHospital> AddAsync(SpecializationHospital model);
        Task<SpecializationHospital> UpdateAsync(SpecializationHospital model, params string[] args);
        Task DeleteAsync(SpecializationHospital model);
    }
}
