﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data.UnitOfWork
{
    public interface ISpecializationClinicDao
    {
        IQueryable<SpecializationClinic> Get();
        Task<SpecializationClinic> GetAsync(int clinicOrDoctorId, int specializationId);
        Task<List<SpecializationClinic>> GetAsync(int clinicOrDoctorId);
        Task<List<SpecializationClinic>> GetAllAsync();
        Task<SpecializationClinic> GetByIdAsync(int id);
        Task<SpecializationClinic> AddAsync(SpecializationClinic model);
        Task<SpecializationClinic> UpdateAsync(SpecializationClinic model, params string[] args);
        Task DeleteAsync(SpecializationClinic model);
    }
}
