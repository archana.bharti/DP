﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class SpecializationClinicDao : ISpecializationClinicDao, IDisposable
    {
        private DPContext context;
        public SpecializationClinicDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<SpecializationClinic> Get() 
        {
            return this.context.SpecializationClinics;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<SpecializationClinic>> GetAllAsync() 
        {
            return await this.context.SpecializationClinics.ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SpecializationClinic> GetByIdAsync(int id) 
        {
            return await this.context
                .SpecializationClinics
                .Include(x => x.Specialization)
                .Include(x => x.Clinic)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hospitalId"></param>
        /// <param name="specializationId"></param>
        /// <returns></returns>
        public async Task<SpecializationClinic> GetAsync(int clinicOrDoctorId, int specializationId) 
        {
            return await this.context
                .SpecializationClinics
                .Include(x => x.Specialization)
                .Include(x => x.Clinic)
                .Where(x => x.ClinicId == clinicOrDoctorId && x.SpecializationId == specializationId)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clinicOrDoctorId"></param>
        /// <returns></returns>
        public async Task<List<SpecializationClinic>> GetAsync(int clinicOrDoctorId)
        {
            return await this.context
                .SpecializationClinics
                .Include(x => x.Specialization)
                .Include(x => x.Clinic)
                .Where(x => x.ClinicId == clinicOrDoctorId)
                .ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<SpecializationClinic> AddAsync(SpecializationClinic model) 
        {
            this.context.SpecializationClinics.Add(model);
            await this.context.SaveChangesAsync();
            return model;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<SpecializationClinic> UpdateAsync(SpecializationClinic model, params string[] propertiesToUpdate) 
        {
            context.Update<SpecializationClinic>(model, propertiesToUpdate);
            await context.SaveChangesAsync();
            return await GetByIdAsync(model.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task DeleteAsync(SpecializationClinic model) 
        {
            SpecializationClinic m = await this.GetByIdAsync(model.Id);
            this.context.SpecializationClinics.Remove(m);
            await this.context.SaveChangesAsync();
        }
    }
}
