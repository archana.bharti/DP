﻿using DP.Services.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DP.Services.Data.UnitOfWork
{
    public class SpecializationHospitalDao : ISpecializationHospitalDao, IDisposable
    {
        private DPContext context;
        public SpecializationHospitalDao(DPContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<SpecializationHospital> Get()
        {
            return this.context.SpecializationHospitals;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hospitalId"></param>
        /// <param name="specializationId"></param>
        /// <returns></returns>
        public async Task<SpecializationHospital> GetAsync(int hospitalId, int specializationId)
        {
            return await this.context.SpecializationHospitals
                .Include(x => x.Hospital)
                .Include(x => x.Specialization)
                .Where(x => x.HospitalId == hospitalId && x.SpecializationId == specializationId)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hospitalId"></param>
        /// <returns></returns>
        public async Task<List<SpecializationHospital>> GetAsync(int hospitalId)
        {
            return await this.context.SpecializationHospitals
                .Include(x => x.Hospital)
                .Include(x => x.Specialization)
                .Where(x => x.HospitalId == hospitalId)
                .ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<SpecializationHospital>> GetAllAsync()
        {
            return await this.context.SpecializationHospitals.ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SpecializationHospital> GetByIdAsync(int id)
        {
            return await this.context.SpecializationHospitals
                .Include(x => x.Hospital)
                .Include(x => x.Specialization)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync();
        }

        public async Task<SpecializationHospital> AddAsync(SpecializationHospital model)
        {
            this.context.SpecializationHospitals.Add(model);
            await this.context.SaveChangesAsync();
            return await this.GetByIdAsync(model.Id);
        }

        public async Task<SpecializationHospital> UpdateAsync(SpecializationHospital model, params string[] propertiesToUpdate)
        {
            context.Update<SpecializationHospital>(model, propertiesToUpdate);
            await context.SaveChangesAsync();
            return await GetByIdAsync(model.Id);
        }

        public async Task DeleteAsync(SpecializationHospital model)
        {
            var m = await this.GetByIdAsync(model.Id);
            this.context.SpecializationHospitals.Remove(m);
            await this.context.SaveChangesAsync();
        }
    }
}
