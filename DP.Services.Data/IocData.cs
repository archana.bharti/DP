﻿using DP.Services.Data;
using DP.Services.Data.Models;
using DP.Services.Data.UnitOfWork;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Business
{
    public class IocData
    {
        public static void Register(IUnityContainer container)
        {
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IRoleStore<IdentityRole, string>, RoleStore<IdentityRole>>(new HierarchicalLifetimeManager());
            container.RegisterType<DbContext, DPContext>(); //new PerThreadLifetimeManager()

            //DA Layer
            container.RegisterType<IClinicAndDoctorDao, ClinicAndDoctorDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserDao, UserDao>(new HierarchicalLifetimeManager());
            container.RegisterType<ICommonDao, CommonDao>(new HierarchicalLifetimeManager());
            container.RegisterType<ILocationDao, LocationDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IFeatureDao, FeatureDao>(new HierarchicalLifetimeManager());
            container.RegisterType<ICollegeDao, CollegeDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IHospitalDao, HospitalDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IPdaDao, PdaDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IPdaSubscriptionDao, PdaSubscriptionDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IBookmarkDao, BookmarkDao>(new HierarchicalLifetimeManager());
            container.RegisterType<ISpecializationClinicDao, SpecializationClinicDao>(new HierarchicalLifetimeManager());
            container.RegisterType<ISpecializationHospitalDao, SpecializationHospitalDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IEndorsementDao, EndorsementDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IRecommendDao, RecommendDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IAppointmentDao, AppointmentDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IOfferDao, OfferDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IOptionDao, OptionDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IQualificationDao, QualificationDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IInsuranceDao, InsuranceDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IPollDao, PollDao>(new HierarchicalLifetimeManager());
            container.RegisterType<IContactUsDao, ContactUsDao>(new HierarchicalLifetimeManager());
        }
    }
}
