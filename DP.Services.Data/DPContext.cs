﻿using DP.Services.Data.Migrations;
using DP.Services.Data.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP.Services.Data
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class DPContext : IdentityDbContext<ApplicationUser>, IDisposable
    {
        public DPContext()
            : base("name=DPContext")
        {
            Database.SetInitializer<DPContext>(new CreateDatabaseIfNotExists<DPContext>());

            ///this code commented by me
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DPContext, Configuration>("DPContext"));

           // Database.SetInitializer(new MigrateDatabaseToLatestVersion<DPContext, Configuration>());
      
        }

        public static DPContext Create()
        {
            return new DPContext();
        }

        public DataTable GetDataTable(string sqlQuery)
        {
            var _context = DPContext.Create();
            try
            {
               
                DbProviderFactory factory = DbProviderFactories.GetFactory(_context.Database.Connection);

                using (var cmd = factory.CreateCommand())
                {
                    cmd.CommandText = sqlQuery;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = _context.Database.Connection;
                    using (var adapter = factory.CreateDataAdapter())
                    {
                        adapter.SelectCommand = cmd;

                        var tb = new DataTable();
                        adapter.Fill(tb);
                        return tb;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<ClinicAndDoctor> ClinicsAndDoctors { get; set; }
        public DbSet<College> Colleges { get; set; }
        public DbSet<CollegeDepartment> CollegeDepartments { get; set; }
        public DbSet<Endorsement> Endorsements { get; set; }
        public DbSet<EnumStatus> EnumStatuses { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<CollegeLocation> CollegeLocations { get; set; }
        public DbSet<CollegeRating> CollegeRatings { get; set; }
        public DbSet<PdaSubscription> PdaSubscriptions { get; set; }
        public DbSet<DoctorHospital> DoctorsHospitals { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<OfferMaster> OfferMasters { get; set; }
        public DbSet<OfferTransaction> OfferTransactions { get; set; }
        public DbSet<PersonalAdvice> PersonalAdvices { get; set; }
        public DbSet<QualificationShort> QualificationShorts { get; set; }
        public DbSet<QualificationLong> QualificationLongs { get; set; }
        public DbSet<QualificationDoctor> QualificationDoctors { get; set; }
        public DbSet<ClinicRating> ClinicRatings { get; set; }
        public DbSet<Recommend> Recommends { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<STDCode> STDCodes { get; set; }
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<HospitalCity> HospitalCities { get; set; }
        public DbSet<HospitalRating> HospitalRatings { get; set; }
        public DbSet<Specialization> Specializations { get; set; }
        public DbSet<SpecializationClinic> SpecializationClinics { get; set; }
        //public DbSet<SpecializationDoctor> SpecializationDoctors { get; set; }
        public DbSet<SpecializationHospital> SpecializationHospitals { get; set; }
        public DbSet<Bookmark> Bookmarks { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Insurance> Insurances { get; set; }
        public DbSet<InsuranceRating> InsurnaceRatings { get; set; }
        public DbSet<PollMaster> PollMasters { get; set; }
        public DbSet<Poll> Polls { get; set; }
        public DbSet<ContactUs> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<IdentityRole>()
                        .Property(c => c.Name)
                        .HasMaxLength(128)
                        .IsRequired();

            modelBuilder.Entity<ApplicationUser>()
                        .ToTable("AspNetUsers")
                        .Property(c => c.UserName)
                        .HasMaxLength(128)
                        .IsRequired();
        }
        
        public new void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
            base.Dispose();
        }
    }
}
